# How to contribute?

1. Read the README.md file first.
2. Sign your commits with PGP.
3. If you want to add content or change something make a fork and make a Merge Request.
