---
title:  "03. Internetanbindung"
date:   2019-02-17 06:05:00 +0100
---

## Knotenpunkte Unterseekabel

Quelle: [https://www.submarinecablemap.com/](https://www.submarinecablemap.com/)

![VNS - Untersee Internetkabel](/assets/img/VNS/03-untersee-internetkabel.png)

## ISP - Netzbetreiber

ISP = Internet Service Provider

![VNS - Tier Networks](/assets/img/VNS/03-Internet_Connectivity_Distribution_&_Core.png)

Quelle: <https://en.wikipedia.org/wiki/Tier_1_network#/media/File:Internet_Connectivity_Distribution_&_Core.svg>

- Tier Netzwerk ([Quelle][2])
    - Tier 1: Ein Netzwerk, welches keine (monetären) Kosten hat mit jedem anderen Netzwerk zu sprechen. Die Gesamtheit
              aller Tier 1 Netze umfassen das gesamte Internet. (Bsp.: Deutsche Telekom)
    - Tier 2: Kostenlose Kommunikation mit Tier 2 Providern und einkaufen bei Tier 1 Providern. Weiterverkauf an Tier 3
              Netze. (Bsp.: Vodafone)
    - Tier 3: Kein Weiterverkauf von Netzen, lediglich Peering untereinander. Vorwiegend Verkauf an Endanwender.
- AS (Autonomus System): Eine Sammlung von ein oder mehreren CIDR Präfixe,n welche von einer Autorität verwaltet werden.
                         Diese Präfixe werden dann routbar gemacht über ein AS. AS sind das Rückgrat des Exterior
                         Routings. ([Quelle][1])
## Letzte Meile

Digitale Vermittlungsstelle Ort (Telekombezeichnung) oder kurz DIVO

![VNS - Letzte Meile](/assets/img/VNS/03-Letzte%20Meile.png)

**DSLAM** (DSL Access Multiplexer): ist ein Gerät welches ermöglicht mehrere Kupfer Leitungen für eine bestimmte
Strecke über Glasfaser zu bündeln und anschließend diese wieder aufzutrennen.

![VNS - DSLAM](/assets/img/VNS/03-DSLAM.png)

[1]: https://tools.ietf.org/html/rfc1930
[2]: https://en.wikipedia.org/wiki/Tier_1_network
