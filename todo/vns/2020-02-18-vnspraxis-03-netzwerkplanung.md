---
title:  "03. Netzwerkplanung"
date:   2019-02-18 06:05:00 +0100
---

Dieser Eintrag ist nicht als Referenz für eine echte Netzwerkplanung zu betrachten, sondern darauf ausgelegt zu
erklären, was in der Berufsschule darunter in der 10. Klasse verstanden wird. Dies wird i.d.R. als Projektarbeit am Ende
des Schuljahres umgesetzt.

## Situation

Nachdem Sie sich zu den Begrifflichkeiten der Projektorganisation etwas Klarheit verschafft und den Projektablauf
strukturiert haben, sollen Sie nun Ihr Wissen auf das konkrete Projekt der LAN-Installation anwenden.
Ihr Vorgesetzter hat vom Auftraggeber Unterlagen erhalten, aus denen hervorgehen soll, wie das Netzwerk aufgebaut und
konfiguriert werden muss (dies ist ein Verkabelungsplan i.d.R.).
Er möchte nun, dass Sie diese Unterlagen durcharbeiten und daraus den Ist- und Sollzustand (Lastenheft) für das Projekt
definieren. In diesem Lastenheft muss festgelegt sein, WAS (welche Leistungen) Sie als Auftragnehmer zu erbringen haben,
z. B.:

- Aufbau und Konfiguration eines netzwerkfähigen Farb-Laserdruckers.
- Aufbau und Konfiguration eines Desktop-PCs für jeden Mitarbeiter mit aktueller Windows Version.
- usw.

Dieses Lastenheft soll im Anschluss als Grundlage für das zu erstellende Angebot dienen.

## Ist- und Sollzustand

1. Analysieren Sie in Ihrer Gruppe die Unterlagen im Anhang und machen Sie sich Notizen zu möglichen fehlenden
   Informationen.
2. Führen sie ein Kundengespräch, in dem möglichst alle fehlenden Informationen ermittelt und in Erfahrung gebracht
   werden. Erstellen Sie dazu auch ein Protokoll.
3. Erarbeiten Sie daraus den Ist-Zustand als Teil Ihrer Projektdokumentation.
4. Definieren Sie den Sollzustand im Rahmen Ihrer Projektdokumentation.

## Projektdokumentation und Angebotserstellung

1. Folgende Unterlagen sind für die weitere Planung und Umsetzung von Bedeutung und deshalb von Ihnen anzufertigen und
   in Ihrer Dokumentation zu ergänzen:
   - Eine vollständige Stückliste aller benötigten Geräte, Kabel, etc.
   - Ein logischer Netzwerkplan
   - Ein Verkabelungsplan
   - Eine kurze Begründung für die Auswahl der Komponenten und der Art der Realisierung, insbesondere wenn andere
     vergleichbare Alternativen eingesetzt werden könnten.
   - Konfigurationsübersicht für das Windows-Netzwerk, mit folgenden Informationen:
     - Rechnerbezogene Konfiguration (Namen, Adressen, freigaben, etc.)
     - Benutzerbezogene Konfiguration (Benutzerdaten, Rechte, etc.)
2. Neben der Fortschreibung der Dokumentation sollen Sie ein Angebot erstellen unter Angabe realistischer Preise für die
   Komponenten; eine Arbeitsstunde kann mit 100 € berechnet werden.

## Allgemeine Hinweise

- Es darf auf alle Hilfsmittel (Literatur etc.) zurückgegriffen werden.
- Größen, die nicht gegeben sind, können angenommen werden (darauf muss aber hingewiesen werden!)
- Teilen Sie die Arbeit innerhalb der Gruppe sinnvoll auf.
- Es darf noch nicht auf eine Client-Server-Struktur gesetzt werden.
