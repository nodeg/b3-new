---
title:  "09. Supernetting"
date:   2020-02-12 12:00:00 +0100
---

Router müssen mitunter tausende von Routingeinträgen verwalten. Für jedes IP-Paket wird der beste Weg zu einem Netz
definiert. Das erfordert Rechenleistung und kostet Bandbreite. Um die Anzahl der Einträge zu reduzieren, werden die
Routen beim sogenannten "Supernetting" zu Netzen zusammengefasst, bzw. gebündelt. Wie bei der Teilung von Netzen
(Subnetting) ist auch beim Supernetting die Subnetzmaske von besonderer Bedeutung. Diese wird in ihrer Länge variabel
eingesetzt (VLSM, Variable-Length Subnet Masking). Bei dieser Variante spricht man von Classless Inter-Domain Routing
(CIDR). Die Netze sind nun unabhängig von einer bestimmten Netzwerkklasse und werden in ihrer Größe nur über die
Subnetzmaske definiert.

## Wie berechnet man die Supernet-Maske?

1. List the networks in binary format.
2. Count the number of far left matching bits. This identifies the prefix length or subnet mask for the summarized
   route.
3. Copy the matching bits and then add zero bits to the rest of the address to determine the summarized network address.

The summarized network address and subnet mask can now be used as the summary route for this group of networks.

Summary routes can be configured by both static routes and classless routing protocols.
