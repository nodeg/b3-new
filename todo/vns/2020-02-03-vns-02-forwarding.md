---
title:  "02. Forwarding"
date:   2019-10-14 09:30:00 +0100
---

Ein großer Vorteil der Switching-Technologie ist die zielgerichtete Weiterleitung der Daten auf Basis der MAC-Adressen.
Im Regelbetrieb bekommt nur das System der eingetragenen Zieladresse die Daten zugeschickt.

Mit anderen Worten: Die Weiterleitung von Unicast-Frames, die an genau eine Station gehen - erfolgt größtenteils
zielgerichtet. Empfängt ein Switch ein Unicast-Frame, vergleicht er dessen Zieladresse mit den Einträgen in seiner
Forwarding-Tabelle. Dort sind alle bisher gelernten MAC-Adressen mit ihrem zugehörigen Ausgangsport gespeichert. Findet
der Switch die Zieladresse, kann er das Paket direkt am angegebenen Anschluss ausgeben. Andernfalls leitet er das Frame
an alle Ausgänge, ausgenommen den empfangenden Port, weiter. Dabei kommt momentan die gleiche Netzlast wie bei einem
Hub auf. Die Station, an die das Frame gerichtet war, schickt über kurz oder lang selbst Paket ab, wobei der Switch aus
der Quelladresse des Frames eine neue Zieladresse für die Forwarding-Tabelle lernt. Nach und nach kann der Switch immer
mehr Pakete zielgerichet weiterleiten und so das Netz entlasten. Das Ausgeben eines Frames mit bislang unbekannter
Zieladresse ist aber trotz gleicher Netzlast kein Broadcast: Letzterer hat ein anderes Adressformat, woran der Switch
erkennt, dass er Broadcasts generell an alle Ports weiterreichen muss. Der Switch und alle angeschlossenen Geräte bilden
eine gemeinsame Broadcast-Domäne.

Damit die Forwarding-Tabelle nicht überläuft, unterliegen ihre Daten einem Alterungsprozess. Kommt ein Eintrag eine
gewisse Zeit - typisch sind 300 Sekunden - nicht mehr zum Zug, fliegt er wieder raus. Das soll sicherstellen, dass
beispielsweise abgeschaltete LAN-Stationen automatisch aus der Forwarding Tabelle entfernt werden.

Wegen der internen Wegwahl leitet ein Switch die Daten grundsätzlich mit einer gewissen Verzögerung (Latenz, englisch
Latency) weiter. Drei Faktoren bestimmen primär den Gesamtdurchsatz: Zum einen die Switch-Architektur, zweitens seine
interne Bandbreite, die bestimmt, wie viele Verbindungen er parallel schalten kann und schließlich Pufferung von
Paketen, die nicht umgehend weitergeleitet werden können.

Unter Latenz versteht man die Zeit, die verstreicht, während der Switch ein Datenpaket verarbeitet. Wie groß die Latenz
ist, hängt nun vom verwendeten Switching-Verfahren ab. Dabei unterscheidet man verschiedene Verfahren, z.B.: Cut-Through
und Store-and-Forward.

Beim Cut-Through beginnt der Switch mit der Ausgabe des Datenstroms auf den Zielport, sobald er diesen über die
Zieladresse identifiziert hat. Also bereits, nachdem die ersten Bytes eines Pakets hereingekommen sind. Das mach
Cut-Through zum schnellsten Verfahren. Ein Nachteil liegt jedoch darin, dass fehlerhafte oder beschädigte Frames
ungehindert durchlaufen und auch auf der Ausgabeseite eine an sich unnötige Belastung des LAN darstellen.

Diese Belastung vermeidet Store-and-Forward: Hier liest der Switch erst das vollständige Frame ein, speichert es und
testet die Richtigkeit anhand der Prüfsumme (CRC). Ist das Frame fehlerfrei, gibt der Switch es auf den Zielport aus,
wenn nicht verwirft er es.

Wegen der Zwischenspeicherung ist Store-and-Forward grundsätzlich langsamer als Cut-Through, dafür minimiert es die
Netzbelastung mit fehlerhaften Paketen. Außerdem kann ein Store-and-Forward-Switch Frames zwischen unterschiedlich
schnellen Netzsegmenten - beispielsweise von 100 auf 1000 MBit/s vermitteln.

{{< mermaid class="text-center">}}
graph LR
    id1[Frame empfangen]
    id2{CRC nach IEEE 802.3 korrekt}
    id3{Zieladresse in Forwarding-Tabelle}
    id4[Frame verwerfen]
    id5[Frame an dedizierten Port weiterleiten]
    id6[Frame an alle Ports weiterleiten]

    id1 --> id2
    id2 -->|Ja|id3
    id2 -->|Nein|id4
    id3 -->|Ja|id5
    id3 -->|Nein|id6
{{< /mermaid >}}

Unicast: Adressierung einer Nachricht an einen einzigen Empfänger
