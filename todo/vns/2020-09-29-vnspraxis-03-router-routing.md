---
title:  "03. Router & Routing"
date:   2020-02-11 07:45:00 +0100
---

Bei diesem Thema hängt es extrem stark davon ab, welche Firma den Router gefertigt hat und was für erweiterte
Funktionalitäten der Router erfüllen soll. Grundsätzlich kann jeder Rechner mit mehr als einem Netzwerkport als Router
agieren. Ein Router ist per Definition nur dazu da um Pakete aus verschiedenen Netzen entgegenzunehmen und entsprechend
weiterzuleiten.

Alle Router bieten jedoch i. d. R. eine Funktionalität an, mit der man IP-Adressbereiche konfigurieren kann. Diese
Bereiche können dann zu bestimmten anderen Bereichen geroutet werden. Die Menüführung ist leider zu unterschiedlich als
das diese an dieser Stelle sinnvoll zu zeigen ist. Zu den theoretischen Grundlagen an dieser Stelle bitte einfach die
Handbücher der Hersteller zur Hand nehmen, sowie manpages auf Unix Betriebssystem und die Unterlagen aus dem
Theorienterricht.
