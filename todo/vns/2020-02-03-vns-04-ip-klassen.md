---
title:  "04. IP-Klassen"
date:   2019-10-21 09:30:00 +0100
---

Bei der Auswahl geeigneter IP-Adressen gilt es einige Eigenschaften und Bedingungen zu berücksichtigen. Dies gilt sowohl
für das interne Netzwerk des Autohauses, wie auch die Adressierung im Rechenzentrum oder das Routing im weltweiten
Internet.

Die IP-Adresse, wie sie bei TCP/IP derzeit verwendet wird, besteht bei IPv4 aus einer 32-Bit-Zahl. Meistens wird die
IP-Adresse als vier hintereinander geschriebene Dezimalzahlen dargestellt, die durch je einen Punkt getrennt werden. Jede
Dezimalzahl liegt im Bereich zwischen 0 und 255, also eine Zahl, die durch acht Bit, ein Byte, binär dargestellt werden
kann.

Eine IP-Adresse kann z.B. so aussehen: 192.168.1.94. Diese Schreibweise soll der besseren Lesbarkeit dienen, denn eine
32 Bit lange Binärzahl ist alles andere als einprägsam, die oben angegebene IP-Adresse sieht in binärer Schreib-weise so
aus: 11000000 10101000 00000001 01011110. Mithilfe dieser IP-Adressen verschickt das Internet-Protokoll die
Datenpakete an die richtigen Empfänger. Jeder Rechner im Internet und in den Intranets erhält solch eine IP-Adresse.

In dem weitläufigen Internet und auch in größeren Intranets befinden sich die Rechner nicht alle in einem
physikalischen Netzwerk. Deswegen unterteilt sich die IP-Adresse auch in zwei Informationseinheiten, die Netzadresse und
die Rechneradresse bzw. Host-Adresse. Die Anzahl an Bits innerhalb der IP-Adresse, die die Netz- und Rechneradresse
identifizieren, variieren mit der sogenannten Klasse, der die Adressen zugeordnet werden. Die Zuordnung in Netzklassen
(classful networks) ist inzwischen technisch überholt, hilft aber bei der ersten Einteilung und Bewertung von Adressen.
Beim „Classless Interdomain Routing, CIDR" wird auf diese Einteilung verzichtet.

Unter IP gibt es drei Hauptklassen mit den Bezeichnungen A-, B- und C-Netzwerke. Jede dieser drei Klassen wird an den
ersten drei Bits der IP-Adresse erkannt. Beginnt die IP-Adresse (als bits dargestellt) mit einer 0, so gehört die
Adresse der Klasse A an, Adressen mit 10 beginnend, gehören der Klasse B an, und eine IP-Adresse, die mit den Bits 110
anfangen, ist eine Adresse aus der C-Klasse. Der Vollständigkeit halber sei hier noch die inoffiziellen Klassen D und E
erwähnt, deren IP-Adressen mit 1110 (D) bzw. 1111 (E) anfangen und die für spezielle IP-Adressen reserviert sind.
Das Internet-Protokoll schaut sich zunächst die ersten Bits der IP-Adresse an und ordnet sie einer der drei Klassen zu.
Anhand der Klassenzuordnung weiß IP, wi eviel Bytes der IP-Adresse Netzadresse und wie viel Rechneradresse sind; welche
Bytes das sind, wird aus der Abbildung deutlich.

- IP-Adressen, deren erstes Byte kleiner als der Wert 12810 ist, gehören der Klasse A an und haben somit ein Byte für
  die Netzwerkadresse und drei Byte für die Rechneradresse. Es gibt also nur 127 Netze der Klasse A, aber jedes davon
  kann Millionen Rechner enthalten. Ein Beispiel: Die IP-Adresse 52.99.4.23 adressiert den Rechner 99.4.23 im Netz 52.
  Im Klasse A-Netz gibt es zwei reservierte Adressen, die 0 und die 127. Das Netzwerk 0 bezeichnet die Default-Route,
  also das standardmäßig eingestellte Gateway. Mit dem Netz 127 wird das Loopback-Device bzw. Localhost angegeben. Das
  Loopback-Device 127.0.0.1 adressiert zur Vereinfachung von Netzwerkanwendungen den lokalen Rechner genauso wie einen
  externen. Ein PING auf die IP-Adresse 127.0.0.1 wird auch bei abgezogener Netzverbindung beantwortet.
- IP-Adressen, deren erstes Byte zwischen 12810 und 19110 liegt, gehören der Klasse B an. Die Klasse 8-Netze haben zwei
  Bytes für die Netzadresse und zwei für die Rechneradresse, die Tausende von Netzen und Tausende von Rechnern
  adressieren können. Ein Beispiel: Die IP-Adresse 168.234.129.245 adressiert den Rechner 129.245 im Netz 168.234.
- IP-Adressen, deren erstes Byte zwischen 19210 und 22310 liegt, gehören der Klasse C an. Es gibt Millionen von Netzen
  in der Klasse C, aber jedes dieser Netze kann nur 254 Rechner enthalten.
  Ein Beispiel: Die IP-Adresse 192.168.1.2 adressiert den Rechner 2 Im Netz 192.168.1.

Ist Ihnen aufgefallen, dass in einem Klasse-C-Netz nur 254 Rechner adressiert werden können? Warum sind nicht 256
Rechner (Obis 255) möglich? Die Frage klärt sich dadurch, dass es innerhalb jedes Netzes zwei reservierte IP-Adressen
geben muss. Die eine IP-Adresse, in der die Bits der Rechneradresse alle auf O gesetzt sind, identifiziert das Netzwerk
selbst und heißt Netzwerkadresse. Die IP-Adresse 52.0.0.0 z. 8. bezeichnet das Netzwerk 52 aus der Klasse A, die
168.234.0.0 bezeichnet das Netzwerk 168.234 der Klasse B. In der zweiten IP-Adresse, der Broadcast-Adresse, sind alle
Bits der Rechneradresse auf 1 gesetzt. Die Broadcast-Adresse ist in etwa mit einer Rundfunkadresse vergleichbar. Wird
diese Adresse für die Versendung von Datenpaketen angegeben, so werden alle Rechner in dem Netz angesprochen.

Ein Beispiel: Ein Datagramm, das an die Broadcast-Adresse 52.255.255.255 gesendet wird, empfangen alle Rechner, die sich
im Netz 52 der Klasse A befinden.

IP-Adressen sind echte Mangelware, der auf 32 Bit beschränkte Adressen-Pool für öffentliche Adressen ist praktisch
erschöpft. Wollen Sie z. B. für Ihre 60 Rechner in Ihrer Firma ebenso viele IP-Adressen und aufgrund der nahenden
Expansion noch mehr IP-Adressen beantragen, so werden Sie mit großer Wahrscheinlichkeit Q je nach Provider jetzt schon
einige Schwierigkeiten bekommen. Außerdem müssen Sie einkalkulieren, dass jede einzelne IP-Adresse Geld kostet, und das
monatlich I Für Intranets sind daher die reservierten Adressbereiche nach RFC 15971 sehr interessant. Das
Nachfolgeprotokoll 1Pv6 (Internet Protocol Version 6) bietet hier Lösungen, benötigt aber Zeit für die Umstellung der
Geräte in der Praxis.

In der Pseudo-Norm RFC 1597 sind folgende IP-Adressen nach Klassen für den internen Gebrauch reserviert und somit auch
nicht im oder aus dem Internet adressierbar. Wir bezeichnen diese IP-Adressen im Folgen-den als private IP-Adressen.

Für die wenigsten Rechner besteht der Bedarf, dass sie aus dem Internet direkt ansprechbar sein müssen. Anders z. B. bei
Gateways/Routern, WWW-Server, Datenbank-Server, FTP-Server usw. oder Rechner, auf die explizit zugegriffen werden soll.
Alle diese Rechnersysteme benötigen offizielle, öffentliche IP-Adressen, da sie über das Internet erreichbar sein
müssen. Dieses sind aber in der Regel bei weitem nicht alle Rechner in einem Intranet, denn die meisten Systeme sollen
nur per WWW-Browser, Mail-Client oder FTP-Client in das Internet oder auch einfach nur firmenintern kommunizieren. Für
diese Rechner wären offizielle IP-Adressen Verschwendung, denn wie später noch gezeigt wird, können diese Rechner mit
privaten IP-Adressen durch eine Adressübersetzung (NAT) mit dem Internet kommunizieren.

Natürlich könnten Sie bei einem isolierten Intranet auch offizielle Adressen benutzen, laufen aber Gefahr, dass es bei
Anbindung an das Internet zu Adresskonflikten kommt, bzw. öffentliche Systeme nicht erreichbar sind. In der Praxis
sollten Sie also unbedingt darauf verzichten 1
