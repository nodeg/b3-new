---
title:  "99. Glossar"
date:   2019-02-17 18:00:00 +0100
---

# Glossar

## A

### analog

- stufenlos
- Bsp.: Kreisausschnitt mit $$90°$$

### asynchron

Das Gegenteil von synchron. Aktionen finden nicht gleichzeitig statt.

## B

### Basisband

- POTS: **P**lain **O**ld **T**elephone **S**ystem
- ein Kanal zu einer Zeit

### Breitband

- mehrere Kanäle zur gleichen Zeit $$\rightarrow$$ DSL
- Bsp.: Chor (mehrere Stimmen)

## C

## D

### dezentral

Der Begriff beschreibt mehrere (mindestens) Punkt zu Punkt Verbindungen zwischen `n` Teilnehmern. Wichtig ist hierbei,
dass die Gesamtstruktur der Verbindungen sich keinem bestimmen Schema zuordnen lässt. Die Idee ist, dass, bei Ausfall
einer Verbindung, die anderen Verbindungen es ermöglichen, den ausgefallenen Knoten zu ersetzen. Es muss somit lediglich
einen ununterbrochenen Weg zwischen dem Start- und Zielknoten bestehen.

### digital

Ein digitales Signal kennt keine runden Schritte (Gegensatz zum Analogsignal). Es kennt nur Schritte in Stufen bzw.
Treppen. Das Wort kommt vom lateinischen Wort `digitus`, was `Finger` bedeutet.

### Duplex

#### Halbduplex

Daten können abwechselnd in beide Richtungen übertragen werden.

#### Vollduplex

Daten können in beide Richtungen gleichzeitig übertragen werden.

## E

## F

### Frequenz

- Amplitude = Höhe des Ausschlages
- 1 Hertz $$\rightarrow$$ 1 Schwingung pro Sekunde. [Hz]
- Richtungsänderung (Start: Oben-Unten-Versatz)

## G

## H

## I

## J

## K

## L

## M

### Masche

- dezentrales Netz
- Bsp.: Peer to Peer

### Manchester-Code

- SFD = Start Frame Delimiter; Ende der Präambel
- Taktrate ist im Code durch Frequenzwechsel Integriert

## N

## O

## P

### parallel

Man unterscheidet zwischen Parallelität mit Synchronität und Asynchronität. Bei paralleler Synchronität sind (bspw.)
Nachrichten, welche über verschiedene Leitungen mit gleicher Länge gleichzeitig gesendet werden, zur selben Zeit am
Ziel. Bei asynchroner Synchronität kommen (bspw.) Nachrichten, welche über verschiedene Leitungen mit gleicher Länge zu
unterschiedlichen Zeiten gesendet werden, zu unterschiedlichen Zeit an.

### Punkt zu Punkt

Eine direkte Verbindung zweier Punkte. Diese können Computer, Telefone oder andere informationstechnische Geräte sein.

## Q

## R

## S

### seriell

Fachwort für nacheinander. In Serie.

#### mehrfach-seriell

mehrfach serielle Lanes

### Simplex

Daten können nur in eine Richtung übertragen werden. (Keine Antwortmöglichkeit!)

### Stern

- Punkt zu Mehrpunkt
- zentral organisiert
- Bsp.: Client-Server; Switch

## T

## U

## V

## W

## X

## Y

## Z
