---
title:  "01. Virtuelle Maschinen"
date:   2019-02-18 06:00:00 +0100
---

## Grundlegende Struktur

Untenstehend kann die Struktur einer virtuellen Maschine erkannt werden. Prinzipiell kann dies endlos verschachtelt
werden.

![VNS - Praxis - Schema virtuelle Maschinen](/assets/img/VNS-Praxis/01-virtuelle-maschinen.png)

Quelle: [https://www.ionos.de/digitalguide/fileadmin/DigitalGuide/Screenshots_2018/DE-virtuelle-maschine.png](https://www.ionos.de/digitalguide/fileadmin/DigitalGuide/Screenshots_2018/DE-virtuelle-maschine.png)

Es kann hier jedoch nicht nur dieselbe Hardwarearchitektur emuliert werden, sondern (je nach Lösung) auch andere
Hardware-Architekturen.

## Vor- und Nachteile von Virtualisierung

| Vorteile                                                  | Nachteile                                                      |
|-----------------------------------------------------------|----------------------------------------------------------------|
| höhere Systemauslastung                                   | single point of failure (durch Redundanz behebbar --> Vorteil) |
| ressourcensparend (Geld, Strom, Temperatur, Hardware)     | Softwarevirtualisierung: mittelbarer Zugriff auf Hardware      |
| Zeit $$\rightarrow$$ zentrale Administration              |                                                                |
| höhere Sicherheit für das Host-System                     |                                                                |
| flexible Anpassungsmöglichkeiten der virtuellen Maschinen |                                                                |
| geringer Wartungsaufwand                                  |                                                                |
| gekapselte Netzwerksimulation möglich                     |                                                                |

## Bsp.: Anlegen einer VM mit VirtualBox

1. virtuelle Maschine neu erstellen
![VNS - Praxis - Virtual Box Schritt 1](/assets/img/VNS-Praxis/01-virtualbox-01.png)

2. Namen der virtuellen Maschine anlegen
![VNS - Praxis - Virtual Box Schritt 2](/assets/img/VNS-Praxis/01-virtualbox-02.png)

3. virtuelle Festplatte erzeugen
![VNS - Praxis - Virtual Box Schritt 3](/assets/img/VNS-Praxis/01-virtualbox-03.png)

4. virtuelle Maschine bearbeiten
![VNS - Praxis - Virtual Box Schritt 4](/assets/img/VNS-Praxis/01-virtualbox-04.png)

5. Installationsmedium in das virtuelle Medienlaufwerk einlegen
![VNS - Praxis - Virtual Box Schritt 5](/assets/img/VNS-Praxis/01-virtualbox-05.png)

## Informationen außerhalb des Berufsschulstoffes

Wir behandeln in der Schule nur den Fall mit der Oracle VMWare Workstation. Zudem kratzen wir nur an dem Funktionsumfang
der uns zur Verfügung steht. Die Bilder oben sind aus der Oracle Virtual Box. Auch hier verwenden wir nur einen
Bruchteil der Funktionen die uns zur Verfügung zu stehen. Beide hier genannte Techniken sind Softwarevirtualisierungen,
dies heißt, dass auf dem normalen OS der Komplette Stack einer Virtuellen Maschine aufgesetzt wird. (Siehe erstes Bild
des Artikels).

Im Gegensatz zu dieser Art der Virtualisierung steht die Hardwarevirtualisierung, welche auf ein Hostsystem verzichtet.
Stattdessen wird nur eine minimale Verwaltungsschicht zur Aufteilung der Ressourcen implementiert, diese Schicht wird
Hypervisor genannt. (Untenstehend ein Schaubild.)

![VNS - Praxis - Hard- vs. Softwarevirtualisierung](/assets/img/VNS-Praxis/01-hard-vs-softwarevirtualiserung.png)

Quelle: [https://www.ibm.com/developerworks/aix/library/au-aixhpvirtualization/fig01.gif](https://www.ibm.com/developerworks/aix/library/au-aixhpvirtualization/fig01.gif)
