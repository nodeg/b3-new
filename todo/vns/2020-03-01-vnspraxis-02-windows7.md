---
title:  "02. Windows 7"
date:   2019-02-18 06:05:00 +0100
---

## Installation

### Primäre und erweiterte Partitionen

Eine Festplatte ist in sog. Partitionen aufgeteilt. Diese Partitionen werden im Betriebssystem Windows als Laufwerk mit
Laufwerksbuchstabe abgebildet. Anzahl und Art der Partitionen wird in der Partitionstabelle festgehalten. Diese Tabelle
befindet sich im Master-Boot-Record (MBR). Dies ist der erste Sektor auf der Festplatte mit der Startpartition.

In die Partitionstabelle können maximal vier Partitionen eingetragen werden. Das können entweder bis·zu vier primäre
oder bis zu drei primäre und eine erweiterte Partition sein. Eine erweiterte Partition dient als Rahmen für beliebig
viele logische Laufwerke.

### Dateisystem

Das Dateisystem ist die Ablageorganisation mit Dateinamen und Attributen auf einem Datenträger eines Computers. Im
Vergleich zu FAT bietet NTFS unter anderem einen gezielten Zugriffsschutz auf Datei- und Ordnerebene sowie größere
Datensicherheit durch Journaling. Allerdings ist eine Kompatibilität nicht so breit gegeben wie bei FAT. Ein weiterer
Vorteil ist, dass die Dateigröße nicht wie bei FAT auf 4 GB beschränkt ist, was beispielsweise für das Erstellen eines
DVD-Images notwendig ist.

### Partitionieren der Festplatte

Windows 7 wird in einer primären Partition (Basisdatenträger) installiert. Sollen zusätzlich zum Betriebssystem auch
Programme auf die Systempartition installiert werden, muss die Größe entsprechend erweitert werden (z.B. 10 - 20 GB).
Während der Installation genügt es, nur die Systempartition (NTFS) anzulegen. Nachträglich können weitere Festplatten
ins System eingebunden werden.

### BIOS-Einstellungen in einer virtuellen Maschine erzwingen

1. Lokale HD im Hostsystem (F:\)
2. \* .vmx Dateien anzeigen lassen
3. entsprechende Datei öffnen
4. bios.forceSetupOnce = " TRUE" ins Script einfügen und speichern
5. VM Neustart

### Systeminformationen

Start - Ausführen: msinfo32 (msinfo32.exe)
Kommandozeile: systeminfo (systeminfo .ex21

### Systemeigenschaften

Systemsteuerung: System
Tastenkombination: Windows+Pause

### Gerätemanager

Startmenü: Systemsteuerung - System - Gerätemanager
Kommandozeile: devmgmt.msc

### Info über Windows

Kommandozeile: winver (winver.exe)

### Ereignisanzeige

Startmenü: Systemsteuerung - Verwaltung - Ereignisanzeige
Kommandozeile: eventvwr.exe

## Dateisystem und Partitionen

## Der Bootvorgang

### Situationsbeschreibung

Um Probleme beim Start des Betriebssystems besser einschätzen und analysieren zu können, ist es wichtig, eine
grundsätzliche Vorstellung vom Bootvorgang zu erhalten. Deshalb sollen Sie sich den Ablauf des Systemstarts
vergegenwärtigen. Der folgende Text beschreibt den Ablauf beim Start von Windows 7. Erstellen Sie für den oben
beschriebenen Bootvorgang von Windows 7 ein Ablaufdiagramm unter folgenden Voraussetzungen:

- Im BIOS ist die einzige Festplatte als erstes Bootmedium eingetragen.
- Auf der Festplatte befindet sich nur Windows 7.

### Der Boot-Vorgang

Selbsttest
Wenn Sie einen Computer einschalten, führt er automatisch den POST (Power-on Self Test) durch. Dabei wird die Größe des
physikalischen Arbeitsspeichers ermittelt und die Verfügbarkeit bestimmter Hardware- Komponenten wie z. B. der Tastatur
überprüft.

Der Windows Boot-Manager
Anschließend versucht das BIOS, ein Betriebssystem zu starten. Dazu durchsucht es in der im BIOS-Setup definierten
Reihenfolge das Diskettenlaufwerk, das CD-ROM-Laufwerk bzw. die Master-Festplatte. Findet es im Diskettenschacht eine
nicht bootfähige Diskette, erscheint eine Fehlermeldung.
Auf der Masterdisk befindet sich im ersten Sektor der Masterboot-Sektor (MBR, Master Boot Record), aus dem das BIOS die
Partitionstabelle liest.
Daraufhin wird aus dem ersten Sektor der Systempartition, d. h. aus dem sogenannten Boot-Sektor, der Startcode für das
Betriebssystem in den Speicher geladen und gestartet. Nun wird das Dateisystem festgelegt und die Datei Bootmgr
gestartet. Dies ist der neue Windows 7 Boot-Manager, der in etwa die Arbeit übernimmt, die bei älteren NT-basierten
Betriebssystemen von der Datei NTDLR übernommen wurde.
Der Bootmgr wurde während des Setups von Windows 7 im Boot-Sektor hinterlegt. Er stellt das Ladeprogramm von Windows 7
dar und steuert nun den weiteren Bootvorgang.

BCD, Boot Configuration Data
Das Programm Bootmgr meldet sich als Boot-Lader von Windows 7. Bei Problemen können Sie an dieser Stelle durch Betätigen
der Taste F8 die erweiterten Windows-7-Startoptionen aufrufen.
Wenn Sie das System entsprechend eingerichtet haben und z. B. eine Mehrfachboot-Konfiguration verwenden, wird der
Boot-Vorgang jetzt angehalten. Sie können dann in einem Auswahlmenü mithilfe der Cursortasten aus den zur Verfügung
stehenden Betriebssystemen dasjenige auswählen, das gebootet werden soll.
Bei Computern mit BIOS befinden sich die Informationen über das Starten des Betriebssystems im
Boot-Configuration-Data-Bereich auf der Festplatte (Verzeichnis \boot\bcd ). Bei Computern mit EFI (Extensible Firmware
Interface) werden diese in der Firmware abgelegt. Die aus Vorgängerversionen von Windows 7 bekannte Konfigurationsdatei
boot.ini findet für den Windows-7-Start selbst keine Verwendung mehr.
Um Einträge in der BCD editieren zu können, wird von Microsoft zusammen mit Windows 7 das komplexe Kommandozeilentool
BCDEDIT.EXE ausgeliefert.

Windows 7 startet

WINLOAD
Wenn Sie Windows 7 als das zu startende Betriebssystem ausgewählt haben, lädt der Boot-Manager das Programm WINLOAD.
WINLOAD bestimmt die vorliegende Hardware und die installierten Hardware-Komponenten wie Prozessor, Bussystem,
Grafikkarte, Maus usw. Er lädt wichtige Konfigurations- und Treiberdateien und bereitet den Start des Kernels vor.

NTOSKRNL und HAL.DLL
WINLOAD lädt nun das Programm NTOSKRNL und die Programmbibliothek HAL.DLL (Hardware Abstraction Layer) in den Speicher.
Bei NTOSKRNL handelt es sich um den eigentlichen Betriebssystemkern von Windows 7. In der Datei HAL.DLL sind für
Windows 7 wichtige Informationen über den Umgang mit der Hardware des Rechners gespeichert.
Anschließend lädt WINLOAD einen Registrierungsschlüssel, in dem die Zugriffskonfiguration für die Initialisierung des
Rechners gemäß dem ausgewählten Hardware-Profil festgelegt ist.
Nun erscheinen der Windows-7-Bildschirm und eine Statusanzeige. Während sich diese ständig bewegt, beginnt WINLOAD mit
der Vorbereitung der Netzwerkeinstellungen.
Zur Sicherheit erzeugt WINLOAD eine Kopie aller, Informationen über die aktuelle Zugriffskonfiguration. Diese wird nach
der erfolgreichen Anmeldung beim Betriebssystem als zuletzt funktionierende Konfiguration (LastKnownGood) definiert.

Der Sitzungsmanager (SMSS)
Vom Kernel wird nun der Sitzungsmanager geladen, der u. a. Systemumgebungsvariablen definiert, das Windows-Subsystem
startet und vom textbasierten zum grafischen Anzeigemodus wechselt. Der SMSS startet anschließend den Anmeldemanager
WINLOGON.EXE, der die lokale Sicherheitsadministration aktiviert. Während das Anmeldefenster angezeigt wird und Sie sich
bereits anmelden können, ruft Windows 7 unter Umständen noch einige Netzwerkgerätetreiber auf.

## Backup & Recovery

Situationsbeschreibung: Sie erhalten einen Anruf von einem Mitarbeiter, dessen PC Sie betreuen. Er klagt, dass sein PC
nicht mehr richtig oder gar nicht mehr startet. Welche Möglichkeiten haben Sie nun also, um dieses
Windows 7-Rechnersystem zu retten?

1. Abgesicherter Modus
   Der abgesicherte Modus ist bei vielen PC-Problemen die erste Anlaufmöglichkeit. Um in ihn zu gelangen, drückt man
   während des Hochfahrens die Taste FS (erweiterte Startoptionen). Es erscheint ein Menü mit mehreren
   Auswahlmöglichkeiten, hier wählt man den abgesicherten Modus aus. In ihm werden nur die nötigsten Treiber und
   Programme geladen, damit keine Konflikte entstehen können. Im abgesicherten Modus kann man Programme deinstallieren,
   problematische Treiber entfernen oder Dienste deaktivieren.
2. Systemwiederherstellung
   Die Systemwiederherstellung merkt sich den aktuellen Zustand der zu überwachenden Festplatten und speichert ab dem
   Zeitpunkt des Systemwiederherstellungspunktes alle folgenden Veränderungen (gelöschte und überschriebene Dateien
   sowie veränderte Dateien). Die Systemwiederherstellung kann sowohl im abgesicherten als auch im normalen Modus
   benutzt werden. Im normalen Betriebsmodus findet man es unter "Start" $$\rightarrow$$ "Programme" $$\rightarrow$$
   "Zubehör" $$\rightarrow$$ "Systemprogramme" $$\rightarrow$$ "Systemwiederherstellung". Ein Wiederherstellungspunkt
   kann unter "Computer" $$\rightarrow$$ "Eigenschaften" $$\rightarrow$$ "Computerschutz" $$\rightarrow$$ "Erstellen"
   erzeugt werden.
3. Letzte als funktionierend bekannte Konfiguration
   Diese Option findet man ebenfalls in den erweiterten Startoptionen, die über F8 erreicht werden. Windows speichert
   bei jedem erfolgreichen Booten eine Liste aller installierten Treiber sowie eine Kopie der Registry. Wenn man z. B.
   einen neuen Treiber installiert hat und dieser Probleme bereitet, kann man mithilfe der letzten als funktionierend
   bekannten Konfiguration dieses Problem beheben und den PC normal starten.
4. Wiederherstellungskonsole
   Im Gegensatz zu Win XP wurde die Wiederherstellungskonsole in Windows 7 mit einer grafischen Oberfläche versehen. Sie
   bietet je nach Problemfall fünf unterschiedliche Lösungsmöglichkeiten (Wiederherstellungsoptionen) zur Diagnose und
   Reparatur Ihrer Windows 7-Installation. Die Wiederherstellungskonsole wird mithilfe der Installations-DVD gestartet
   oder auch nachinstalliert.
5. Computer reparieren
   Zu dieser Option gelangt man während des Hochfahrens über die Taste F8 (erweiterte Startoptionen). Ein zuvor
   angelegtes Systemabbild (unter "Systemsteuerung" $$\rightarrow$$ "Sicherung" eines Computers erstellen) kann hier
   wieder aufgespielt werden.
6. Wiederaufspielen eines Festplattenabbildes (image) mithilfe externer Software z.B. Acronis
   Ist ein Festplattenimage vorhanden und somit vorab erstellt worden, so ist es möglich mithilfe eines
   Notfallstartmediums dieses Abbild wieder aufzuspielen.
7. Sichern und Wiederherstellen
   Die Sicherung und Wiederherstellung von Benutzerdaten und auch Programm- oder Systemdateien kann über
   "Systemsteuerung" $$\rightarrow$$ "Sichern und Wiederherstellen" erfolgen. Das Sichern ist sowohl manuell als auch
   zeitgesteuert möglich.

## Dateisystemberechtigungen

Bei einer Kombination aus beiden Typen erhält die am meisten einschränkende Zugriffsberechtigung der beiden Typen den
Vorrang.

### Freigabeberechtigungen

- Mit FAT und NTFS möglich
- Nur für ganzen freigegebenen Ordner gültig (nicht für einzelne darin enthaltene Unterordner oder Dateien)
- Berechtigungen sind kumulativ (höchste Berechtigung aus individueller oder Gruppenberechtigung ist gültig)
- Nur drei Möglichkeiten
    - Lesen
    - Ändern
    - Vollzugriff
- Nicht wirksam bei lokaler

### NTFS-Berechtigungen

- Nur mit NTFS möglich
- Feinere Abstufung der Berechtigungen möglich
- Vererbung von Berechtigungen (auf Unterordner und Dateien)
- Auch kumulativ, jedoch hat eine Verweigerung Vorrang vor Ordnerberechtigungen
- Dateirechte haben Vorrang vor Ordnerrechten
- Wirksam bei lokaler Anmeldung

Berechtigungen auf Freigabeebene regeln nur den Netzzugriff auf gegebene Ressourcen. Sie sind bei lokalem Zugriff
unwirksam. NTFS-Berechtigungen bieten einige Vorteile gegenüber Freigabeberechtigungen:

| NTFS-Verzeichnisberechtigung | Zugriffsmöglichkeit                                                                                                                                        |
|------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Lesen                        | Unterverzeichnisse auflisten, Dateien lesen, Berechtigungen, Besitzrechte und Attribute einsehen                                                           |
| Schreiben                    | Unterverzeichnisse erstellen, Dateien erstellen, Berechtigungen und Besitzrechte einsehen und Attribute ändern                                              |
| Ordnerinhalt auflisten       | Unterverzeichnisse und Dateien auflisten und lesen                                                                                                         |
| Lesen, Ausführen             | Zusätzlich zu den Möglichkeiten SCHREIBEN und LESEN, AUSFÜHREN                                                                                             |
| Ändern                       | Zusätzlich zu den Möglichkeiten SCHREIBEN und LESEN, AUSFÜHREN u. Verzeichnisse löschen                                                                    |
| Vollzugriff                  | Zusätzlich zu den Möglichkeiten aus allen übrigen Berechtigungen: Unterverzeichnisse und Dateien löschen, den Besitz übernehmen und Berechtigungen ändern. |

Für Dateien können standardmäßig diese NTFS-Berechtigungen vergeben werden:

| NTFS-Verzeichnisberechtigung | Zugriffsmöglichkeit                                                                                                  |
|------------------------------|----------------------------------------------------------------------------------------------------------------------|
| Lesen                        | Dateien lesen, Berechtigungen, Besitzrechte und Attribute einsehen                                                   |
| Schreiben                    | Dateien überschreiben, Berechtigungen und Besitzrechte einsehen und Attribute ändern                                 |
| Lesen, Ausführen             | Zusätzlich zu den Möglichkeiten aus Lesen Anwendungen ausführen                                                      |
| Ändern                       | Zusätzlich zu den Möglichkeiten aus Schreiben und Lesen, Ausführen Datei ändern und löschen                          |
| Vollzugriff                  | Zusätzlich zu den Möglichkeiten aus den übrigen Berechtigungen: den Besitz übernehmen und Berechtigungen übernehmen. |

- feine Abstufung der Zugriffsberechtigungen
- Vererbung von Berechtigungen auf untergeordnete Verzeichnisse und Dateien
- Differenzierte Zugriffsbeschränkungen für Dateien und Verzeichnisse innerhalb einer Freigabe
- Wirksamkeit auch bei lokaler Anmeldung des zugreifenden Benutzers

NTFS-Berechtigungen für Verzeichnisse und Dateien unterscheiden sich voneinander! Verzeichnisse können standardmäßig mit
folgenden Berechtigungen ausgestattet werden:

Merkmale der NTFS-Berechtigungen

- Dateiberechtigungen haben Vorrang vor Verzeichnisberechtigungen!
- Das Verweigern setzt andere Berechtigungen außer Kraft!
- Berechtigungen werden vererbt; nachfolgende Ordner besitzen die gleichen Berechtigungen;
  Änderung: "Vererbbare übergeordnete Berechtigungen übernehmen" deaktivieren!
- Berechtigungen sind kumulativ; die Berechtigungen eines Benutzers und die Berechtigungen seiner Gruppe ergänzen sich.

## Benutzerverwaltung

Eine funktionierende Benutzerverwaltung mit Rechtevergabe ist nur auf den NT-basierten Systemen (NT, Win2000, XP,
Vista, Win7) durchführbar. Durch diese Technik können jedem PC-Benutzer definierte Rechte zugeordnet werden, auf
welche Ressourcen (zum Beispiel einzelne Ordner) und wie er auf diese zugreifen darf (Dateien löschen, ändern usw.).
Win7 kennt verschiedene Benutzergruppen: die Administratoren, die Hauptbenutzer, die Benutzer, die
Remotedesktopbenutzer, die Netzwerkkonfigurationsoperatoren, die Sicherungsoperatoren, die Replikationsoperatoren und
die Gäste.

Einige vordefinierte Gruppen:

Die Gruppe der Benutzer hat wenig Rechte. Benutzer können zum Beispiel keine Programme installieren oder löschen, die
Einträge in der Windows-Datenbank verändern oder wichtige Betriebssystemdateien verändern. Deshalb ist es weniger
gefährlich als "normaler Benutzer" im Internet zu surfen, da ein Angreifer von außen nur mit den Rechten des
angemeldeten Benutzers auf den Rechner zugreifen kann. Es ist ihm also nur schwer möglich einen Virus, oder Trojaner zu
installieren.
"Normale Benutzer" dürfen jedoch auf alle Laufwerke zugreifen und bereits installierte Programme ausführen.
Die Hauptbenutzer können etwas mehr als die Benutzer, sie dürfen z. B. Ordner und Drucker im Netzwerk freigeben und
einige Systemeinstellungen verändern, jedoch nicht auf die Benutzerverwaltung zugreifen, oder Treiber installieren.
Die Sicherungsoperatoren sind für die Datensicherung des Systems verantwortlich. Sie müssen alle Dateien und Ordner zum
Sichern lesen können und beim Wiederherstellen von der Sicherung auch überschreiben können.
Im Gegensatz zu den Benutzern und Hauptbenutzern sind die Mitglieder der Gruppe der Administratoren die Chefs des
Computers und dürfen den Computer uneingeschränkt verwalten.

Lokale Benutzer und Benutzerkonten:
Ein lokaler Benutzer kann auch Mitglied mehrerer Gruppen sein, er bekommt dann die Rechte der Gruppe mit den meisten
Rechten, die Rechte addieren sich also auf.
Man kann Rechte auch an einzelne Benutzer vergeben, die Strategie die Microsoft empfiehlt ist jedoch die Rechte an eine
Gruppe zu vergeben und dann Benutzer in diese Gruppe einzufügen. Alle Gruppenmitglieder erben jeweils die Rechte der
Gruppe. So hat der Benutzer Administrator nur deswegen volle Zugriffsrechte, weil er Mitglied der Gruppe der
Administratoren ist.
Win7 stellt für jeden Benutzer eine eigene Arbeitsumgebung (ein Benutzerprofil) zur Verfügung. So kann jeder Benutzer
seinen Desktop, seinen Bildschirmschoner, seine Ansichten usw. so einrichten, wie es ihm persönlich gefällt.
Nach der Installation von Win7 sind drei Benutzer angelegt, der Administrator (Mitglied der Gruppe der Administratoren),
der Gast (dieser Benutzer ist standardmäßig deaktiviert, er ist Mitglied der Gruppe der Gäste) und der "erste Benutzer".
Letzterer wird bei der Installation angelegt und ist automatisch Mitglied der Gruppe "Administratoren".

