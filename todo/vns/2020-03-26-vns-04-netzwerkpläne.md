---
title:  "03. Netzwerkpläne"
date:   2019-02-17 06:05:00 +0100
---

**Grundsätzliches zu Kabeln**: Bei einer modernen Verkabelung kommen LWL (=Lichtwellenleiter) und Twisted Pair Kabel
(kurz: TP-Kabel) vor. Erstere werden für die Verkabelung für die Vernetzung von Netzwerkschränken untereinander
eingesetzt, heißt auch für das Verlegen von Kabeln zwischen Gebäuden und Stockwerken. TP-Kabel werden vorallem
eingesetzt um Switches bzw. Patch-Dosen und PCs zu verbinden.

**Grundsätzliches zum Netzwerkschrank**: Der Netzwerkschrank dient für die Netzwerkbildung und zur Vernetzung zur
Vernetzung von mehreren Gebäuden bzw. Gebäudeteilen.

## Verkabelungsplan

Beim Verkablungsplan stehen die Kabelführung und die räumliche Anordnung der passiven Komponenten im Vordergrund.
Zusätzlich müssen die Stromversorgung, der Potentialausgleich und die Verbindung der Netzwerkschränke untereinander
ersichtlich sein.

![VNS - 04 - Verkabelungsplan Beispiel](/assets/img/VNS/04-verkabelungsplan.jpg)

Quelle: <https://www.netzwelt.de/netzwerk/verkabelung-planen-router-switch-patchpanel.html>

## Logischen Netzwerkplan

Der logische Netzwerkplan enthält alle aktiven Komponenten (und passive Komponenten wie bspw. Hubs). Ihm geht es im
Gegensatz zum Verkabelungsplan weniger um phyische Verbindungen, sondern um Informationen wie IP-Adressen, VLANs,
Subnetze und Angeschlossene Geräte. Die Einteilung der Komponenten kann (muss aber nicht) identisch sein.

![VNS - 04 - Logischer Netzwerkplan Beispiel](/assets/img/VNS/04-logischer-plan.png)

Quelle: <https://edu.juergarnold.ch/modul_145/article.html>
