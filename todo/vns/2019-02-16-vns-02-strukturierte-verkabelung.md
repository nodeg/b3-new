---
title:  "02. Strukturierte Verkabelung"
date:   2019-02-17 06:05:00 +0100
---

> nach DIN 11801 bzw. EN 50173

![2018-12-20 - Strukturierte Verkabelung](/assets/img/VNS/02 - Strukturierte Verkabelung.png)

- LWL: Lichtwellenleiter
- DIVO: Digitale Vermittlungsstelle Ost
- TAE: Teilnehmer Anschluss Einheit
- AE: Netzwerkdose ("Anschlusseinheit")
- SV: Standortverteiler
- GV: Gebäudeverteiler
- EV: Etagenverteiler

Die strukturierte Verkabelung bildet die Grundlage für eine zukunftsorientierte, anwendungsunabhängige
Netzwerkinfrastruktur. Dazu wurden mehrere Standards (Normen) von internationalen Gremien entwickelt. Bsp.:
EIA/TIA 568 (Pinbelegung), EN 50173 (anwendungsneutrale Verkabelungssysteme) und ISO/IEC 11801. Dort werden die
Topologien und die übertragungstechnischen Kenndaten für ein offenes, d.h. herstellerneutrales Verkabelungssystem
definiert.

In der EN 50173 wird eine Strukturierung in Hierarchieebenen festgelegt, die topologisch oder administrativ
zusammengehören.

## Primärer Bereich/Geländeverkabelung

Der Primärbereich sieht die Verkabelung von einzelnen Gebäuden untereinander vor. Dazu ist Glasfaserkabel
vorgeschrieben. Zur Anbindung an das Netzwerk werden in jedem Haus Gebäudeverteiler eingesetzt, die mit einem zentralen
Standortverteiler verbunden sind.

## Sekundärer Bereich/Gebäudeverkabelung

Der Sekundärbereich (stockwerksübergreifend incl. Steigleitungen) sieht die Verkabelung innerhalb des Gebäudes vor. Sie
wird auf den Etagen und Stockwerken des Hauses angewandt. Auf dieser Ebene sind Glasfaserkabel empfohlen, da Kupferkabel
lediglich bis zu einer Länge von 100m eingesetzt werden können. Das spezifische Gewicht von Kupfer wirkt sich zudem im
Steigleitungsbereich hoher Gebäude negativ aus. (Entdrillung durch Verformung/Streckung). Die einzelnen Etagen werden
über den Etagenverteiler mit dem Gebäudeverteiler verbunden.

## Tertiärer Bereich/Etagenbereich

Dieser Bereich beschreibt die Verbindung der einzelnen Arbeitsplatzrechner mit dem Etagenverteiler. Dazu wird
vorzugsweise ein Twisted-Pair-Kabel, aber auch Glasfaserkabel verwendet. Die Norm empfiehlt hierfür zwei Anschlüsse pro
Arbeitsplatz. Als Richtdaten wurde festgelegt, dass der Bereich für eine Etagengröße von 900 bis 1000m² gedacht ist.
Jedoch darf eine Kupferverkabelung eine Länge von 100m (z.B. 90m zum Arbeitsplatz plus 10m für
Geräteanschlusskabel/Patch-Kabel) nicht überschreiten.

## Weitere Darstellung zur strukturierten Verkabelung

## Collapsed Backbone

Bei einem Collapsed Backbone erfolgt die Bündelung aller Netzsegmente in einem einzigen Gerät. Alle Segmente werden in
diesem Gerät zusammengefasst, welches seinerseits damit selbst die Rolle des Backbone übernimmt. Daher nennt man den
Collapsed Backbone auch "Router Backbone" oder "Backbone-in-a-Box". Vom jeweiligen Zielhost wird ein direkt mehradriges
Kabel zu dem betreffenden Verteiler als reine Punkt zu Punkt Verbindung verlegt.
