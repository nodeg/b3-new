---
title:  "01. Server"
date:   2019-10-08 07:45:00 +0100
---

Ein Server ist ein Computer, welcher zentrale Aufgaben in einem Netzwerk erfüllt und bereitstellt.

## Client-Server-Modell

Quelle und gute Einführung zu dem Thema: <https://de.wikipedia.org/wiki/Client-Server-Modell>

Beim Client-Server-Modell gibt es zwei Komponenten. Den Server und den Client. Beide Teilnehmer stehen hier für logische
Rollen. Physikalisch sind beides Computer. Server haben im professionellen Bereich jedoch häufig spezielle Hardware.
Das Client-Server-Modell beschreibt im allgemeinen die Aufgabenverteilung in einem Netzwerk. Eine solche Aufgabe wird in
diesem Modell als Service bezeichnet.

Dieses Modell sieht vor, dass der Client einen Request (dt. Anforderung) stellt an den Server, welcher mit einer
Response (dt. Antwort) reagiert. Diese Antworten können den verschiedensten Zwecken dienen. Es kann lediglich die
Bestätigung des Erhalts des Requests für einen bestimmten Service sein, oder eine Weiterleitung zu einem anderen Server.
Die Art des Reqeuests und der Response sind vollständig abhängig vom Service.

Der Unterschied zu dem uns bekannten Peer-To-Peer ist, dass nun Server und Client nicht mehr gleichberechtigt sind und
nicht mehr beide Komponenten auf einem Computer zu finden sind i.d.R. Beide Modelle können (je nach Anwendung) dazu
benutzt werden als verteiltes System zu arbeiten. Bei Peer-To-Peer ist dies jedoch unüblich.

## DHCP

Das Dynamic Host Conflguratlon Protocol, das einem Endgerät nach einem DHCP-Request eine IP-Adresse zuweist. Die
Adresszuweisung kann dabei statisch oder dynamisch erfolgen.

- Statische Adresszuweisung: Der DHCP Server weist dem Host anhand der MAC-Adresse eine feste IP zu.
- Dynamische Adresszuweisung: Aus einem Adresspool(range) weist der DHCP-Server dem Client eine variable bzw. temporäre
  IP zu.

**ACHTUNG**: Aktivieren Sie niemals DHCP für ein System, das Ressourcen zur Verfügung stellt! Das schließt nicht nur
Domänen-Controller ein, sondern auch alle anderen Server (einschließlich Arbeitsstationen, die Verzeichnisse oder
Drucker freigeben), Anbieter von Netzwerkdiensten (DNS, WINS, usw.), Druck-Server und so weiter.

### Ablauf der Adresszuweisung unter DHCP

![VNS Praxis - 01 - DHCP Ablauf](/assets/img/VNS-Praxis/01-dhcpablauf.gif)

Quelle: <http://www.netzmafia.de/skripten/netze/netz9.html>

### Einsatz von DHCP unter Windows

Bevor DHCP eingesetzt werden kann, muss der Dienst Microsoft DHCP-Server zuerst auf dem Windows-Server installiert
werden. Zu den Konfigurationen unter DHCP zählen die Angabe des IP-Adressbereichs und die dazugehörige Subnetzmaske. Auf
diese Weise kann genau festgelegt werden, welche IP-Adressen verfügbar sind und vergeben werden dürfen. IP-Adressen
sollten dann ausgeschlossen werden, wenn sie statisch einem bestimmten Host zugeordnet sind oder wenn sie bereits zum
Bereich eines anderen DHCP-Servers gehören.

## DHCP auf einem Windows Server einrichten und verwalten

DHCP dient zur starken Vereinfachung der Administration von Clients In einem TCP/IP-basierten Netzwerk. Active
Directory (AD) setzt protokollseltig TCP/IP voraus, sodass sich eine Verwendung von DHCP geradezu anbietet.

### DHCP - Serverinstallation
Die DHCP-Serverfunktionen werden zunächst einmal unabhängig von Active Directory unterstützt. Der DHCP-Server kann also
sowohl auf einem alleinstehenden Windows-Server als auch auf einem Domänencontroller installiert und betrieben werden.
Allerdings wird im AD der Einsatz von DHCP-Servern hier durch die notwendige Autorisierung besser kontrollierbar.

Der DHCP-Server muss eine statische IP-Adresse besitzen. Für die Installation der DHCP-Serverfunktionalität können Sie
folgendermaßen vorgehen:

1. Öffnen Sie die `Start/Server-Manager`. Wählen Sie dann `Verwalten - Rollen und Features hinzufügen`. Nach der Auswahl
   des Installationstyps `Rollenbasiert` wählen Sie ihren Server aus und im Anschluss die Serverrolle `DHCP-Server`.
   Klicken Sie am Ende des Insallationsassistenten auf den Link `DHCP-Konfiguration abschließen`.
2. Im DHCP-Konfigurationsassistenten wird u.a. der Server im Active Directory autorisiert, um festzulegen, welcher
   Server zur Vergabe von IP-Adressen berechtigt ist. Für diese Autorisierung ist die Berechtigung eines
   Organisations-Admins erforderlich.

Danach steht der DHCP-Server im Netzwerk zur Verfügung und muss noch für den konkreten Anwendungsfall konfiguriert
werden.

### DHCP ohne AD

Verwenden Sie den Windows-Server in einem Netzwerkumfeld ohne Active Directory, steht der DHCP-Dienst sofort zur
Verfügung. Sie müssen dann nur noch den oder die relevanten IP-Bereiche definieren.

### Definieren von IP-Bereichen

Nach erfolgreicher Installation und Autorisierung des DHCP-Servers werden die weiteren Einstellungen über die
DHCP-Konsole durchgeführt (`Tools-DHCP`).
Über `Neuer Bereich` des Kontextmenüs für das 1Pv4-, oder IPv6-Serversymbol in der DHCP-Konsole können Sie einen neuen
IP-Bereich definieren. Es startet ein entsprechender Assistent und fragt nacheinander folgende Parameter ab:

- Bereichsname, -beschreibung
- IP-Adressbereich und Subnetzmaske
- Ausschlüsse und verzögerte Antwortzeit
- Lease-Dauer (bestimmt, wie lange ein Client die zugewiesene Adresse maximal behalten kann)
- Weitere DHCP-Optionen (Standardgateway, DNS-Suffix, DNS-Server, WINS-Server)
  Die Optionen können Sie auch später einstellen. Die Einstellungen durch den Assistenten betreffen nur diesen neuen
  Bereich. Die Serveroptionen (gültig für alle Bereiche, aber von niedrigerer Priorität) stellen Sie an anderer Stelle
  ein.

### DHCP-Client-Konfiguration

Jeder Client, der eine IP-Adresse per DHCP beziehen soll, muss entsprechen konfiguriert sein:
Über den Befehl `ipconfig /all` können Sie alle Einstellungen einsehen. Öffnen Sie dazu die Eingabeaufforderung.
