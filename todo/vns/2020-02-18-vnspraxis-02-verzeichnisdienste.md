---
title:  "02. Verzeichnisdienste"
date:   2019-10-15 09:30:00:00 +0100
---

## Was ist ein Verzeichnisdienst?

Ein Verzeichnisdienst ist ein Serverdienst der in einem Netzwerk für Clients eine zentrale Datensammlung bestimmter Art
zur Verfügung stellt. Verzeichnis bezieht sich hier auf das Telefonbuch und nicht auf einen Dateiordner. Zumeist werden
diese Art von Dienst zur Benutzerverwaltung eingesetzt in größeren Netzwerken.

Um mit einem solchen Service zu kommunizieren wird zumeist ein "Directory Access Protocol" (DAP) verwendet. Die wohl
bekannteste Implementierung dieses Protokolls ist das Protokoll LDAP. Die Datenbank intern basiert zumeist nach dem
X.500 Standard.

## Das Active Directory

Das Active Directory bündelt verschiedene Dienste und verwaltet diese in Abhängigkeit zentral miteinander. Die vier
Hauptkomponenten sind im Moment:

- Lightweight Directory Access Protocol (LDAP): Benutzerverwaltung und Verwaltung von anderen Objekten die vorher als
  Schema im LDAP hinterlegt worden sind (bspw. Drucker).
- Kerberos: Ein Authentifizierungsdienst mit dem man sich für die verschiedensten Dienste verifizieren lassen kann.
- Common Internet File System (CIFS): Dateiaustauschprotokoll für das Internet.
- Domain Name System (DNS): Die Vergabe von Rechnernamen ist für die vollständige Funktionstüchtigkeit erforderlich.

Mit Windows 2000 wurde ein neues Konzept des Verzeichnisdienstes eingeführt. Der Verzeichnisdienst ist im Prinzip die
Datenbank der vom Netzwerk benötigten Daten wie Benutzerkonten und Computerkonten. Aber auch Zusatzinformationen zu
Konten können im Active Directory (AD) abgelegt werden. Durch diese zentrale Verwaltung können z. B. Telefonnummern an
derselben Stelle verwaltet werden wie Benutzernamen, E-Mail-Adresse und Informationen zu Vorgesetzten oder
Abteilungszugehörigkeit. Auch lassen sich jederzeit neu Datensätze in das AD aufnehmen und domänenweit verfügbar machen.

### Logische Struktur

Eine Verzeichnisstruktur muss den Belangen einer Firma, den Verwaltern und den Benutzern gerecht werden. Vor der
Implementierung von Verzeichnisdiensten sind Firmenstruktur und Geschäftsbereiche im Hinblick auf logische zusammenhänge
zwischen Geschäftsprozessen, Abhängigkeiten und Hierarchien zu untersuchen.

Das Verzeichnis besitzt einen hierarchischen Aufbau. Innerhalb der Verzeichnishierarchie werden Ressourcen logisch
gruppiert. Durch die logische Anordnung von Ressourcen ist es möglich, Objekte ihres Namens zu lokalisieren und nicht
nach ihrer tatsächlichen Position.

Zur Beschreibung der logischen Struktur dienen folgende Komponenten:

- Objekt
- Organisationseinheit, OU
- Domäne
- Struktur
- Gesamtstruktur

### Objekt

TODO: Fix Absatz

Ein Objekt steht für eine Netzwerkressource. Es ist die kleinste Einheit, die verwaltet werden kann. Jeder Vorgang im
Netzwerk kann in letzter Konsequenz auf ein Objekt zurückgeführt werden. Ein Objekt ist beispielsweise ein Computer, ein
Faxgerät, ein Benutzer, eine Druckerwarteschlange oder eine Richtlinie. Ein Objekt besitzt einen Namen und einen Satz
von Attributen. Objekte können in Klassen (Objektklassen) zusammengefasst werden.

### Organisationseinheit

Das Containerobjekt OU (Organizational Unit) dient dazu, Objekte zu gruppieren. So kann eine Organisationseinheit z. B.
Benutzer, Benutzergruppen, Computer und andere OUs enthalten. Die Möglichkeit, eine OU in eine andere OU einzugliedern
begründet die hierarchische Struktur der Verzeichnisdienste.

Eine OU kann unterschiedlichen Zwecken dienen:

- Abbildung der Organisationsstruktur der Firma
- Abbildung des Verwaltungsschemas des Netzwerks
- Verwaltung von Objekten an andere Personen auf OU-Ebene delegieren

Durch die unbeschränkte Verschachtelungstiefe für Organisationseinheiten und das Fassungsvermögen von mehreren Millionen
Objekten, ist es möglich, ein Unternehmen in einer einzigen Domäne zu erfassen. Die Vorteile liegen in der einfachen
Verwaltung.

### Domäne

Die Domäne ist die eigentliche und wesentliche Einheit von Active Directory. Sie ist ein System, das sowohl nach innen
als auch nach außen hin abgeschlossen ist. So werden beispielsweise im Verzeichnis einer Domäne nur die Informationen zu
Objekten gespeichert, die in der Domäne enthalten-sind. Auf der anderen Seite ist der Zugriff auf Objekte der Domäne
von außen nur nach Anmeldung möglich.

Eine Domäne hat ein Verzeichnis und dieses ist vollständig. Das heißt, es gibt nichts, was zur Domäne gehört und nicht
im Verzeichnis vermerkt wäre.

Sicherheitsrichtlinien gelten innerhalb einer Domäne, und Vertrauensbeziehungen bestehen zwischen Domänen.

## Installation des Windows Domain Controllers

Damit das Netzwerk des Autohauses als C/S-Netzwerk betrieben werden kann, müssen auf dem Windows Server die Active
Directory Verzeichnisdienste installiert werden (s. Informationsblatt zum Acive Directory). Dadurch wird der Windows
Server zum Domain Controller hochgestuft.

Vor der Installation sollte der Server eine statische IP-Adresse erhalten!

Folgende Schritte sind bei der Installation auszuführen:

1. Im Servermanager kann über Verwalten $$\rightarrow$$ Rollen und Features hinzufügen der entsprechende Assistent
   gestartet werden.
2. Wählen Sie als Installationstyp „rollenbasiert" und anschließend den Server aus.
3. Aktivieren Sie die Server-Rolle „Active Directory-Domänendienste" und bestätigen Sie anschließen das Hinzufügen der
   dafür benötigten Features mit Features hinzufügen.
   Bestätigen Sie die folgenden Abfragen und wählen Sie Installieren.
4. Nach Bestätigen der erfolgreichen Installation muss im gleichen Dialogfenster noch der Server zu einem
   Domänencontroller hochgestuft werden (Anklicken des entsprechen den Links siehe unten).
5. Im nun startenden Konfigurationsassistenten für das Active Directory wählen Sie "Neue Gesamtstruktur" und legen den
   Domänennamen fest (z.B.: `nettmann.intern`)
   Lassen Sie im folgenden Dialog die Auswahl unverändert und legen Sie das geforderte Passwort fest.
   Die folgende DNS-Fehlermeldung können Sie ignorieren.
6. Bestätigen Sie im Folgenden den NetBIOS-Domänennamen mit WEITER, lassen Sie die Verzeichnisabfragen unverändert und
   bestätigen Sie im Anschluss die Zusammenfassung.

Der Server wird nach erfolgreicher Installation neu gestartet.
Nach dem Neustart kann im Servermanager, über "Tools", das Active Directory konfiguriert werden.

## Konfiguration der Windows Domäne

### Clients in die Domäne aufnehmen

Die Client-Rechner müssen nun Teil der Windows-Domäne werden. Neben der IP-Adresse mit Subnetzmaske ist nun auch die
Angabe des richtigen DNS-Servers von besonderer Bedeutung.

### Benutzer und Home-Laufwerk

## Alternativen

Das Active Directory ist sehr mächtig und in Firmen gerne genutzt. Es ist auch möglich, mit einem Mehraufwand, alle
Komponenten durch reine Open-Source Komponenten zu ersetzen und selbstständig ohne Windows Magie das System aufzubauen.
Hierfür wird ein openLDAP, Kerberos, DNS-Server der eigenen Wahl und evtl. ein DHCP/PXE Server benötigt.
