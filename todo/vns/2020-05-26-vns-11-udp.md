---
title:  "11. User Datagram Protocol"
date:   2020-03-18 10:30:00 +0100
---

UDP is a simple protocol that provides the basic transport layer functions. It has much lower overhead than TCP, because
it is not connection-oriented and does not offer the sophisticated retransmission, sequencing, and flow control
mechanisms that provide reliability.

This does not mean that applications that use UDP are always unreliable, nor does it mean that UDP is
an inferior protocol. It simply means that these functions are not provided by the transport layer pro-
tocol and must be implemented elsewhere, if required. Although the total amount of UDP traffic found
on a typical network is often relatively low, key application layer protocols that use UDP include:

- Domain Name System (DNS)
- Simple Network Management Protocol (SNMP)
- Dynamic Host Configuration Protocol (DHCP)
- Routing Information Protocol (RIP)
- Trivial File Transfer Protocol (TFTP)
- IP telephony or Voice over IP (VoIP)
- Online games

Some applications, such as online games or VoIP, can tolerate some data loss. If these applications used TCP, they could
experience large delays while TCP detects data loss and retransmits data. These delays would be more detrimental to the
performance of the application than small data losses. Some applications, such as DNS, simply retry the request if no
response is received; therefore, they do not need TCP to guarantee message delivery. The low overhead of UDP makes it
very desirable for such applications. Because UDP is connectionless, sessions are not established before communication
takes place as they are with TCP. UDP is said to be transaction-based; that is, when an application has data to send,
it simply sends the data.

Many applications that use UDP send small amounts of data that can fit in one segment. However, some applications send
larger amounts of data that must be split into multiple segments. The “UDP-Packet” is referred to as a datagram,
although the terms segment and datagram are sometimes used interchangeably to describe a transport layer PDU (Protocol
Data Unit). When multiple datagrams are sent to a destination, they may take different paths and arrive in the wrong
order. UDP does not track sequence numbers the way TCP does. UDP has no way to reorder the datagrams into their
transmission order. Therefore, UDP simply reassembles the data in the order that it was received and forwards it to the
application. If the data sequence is important to the application, the application must identify the proper sequence and
determine how the data should be processed.
