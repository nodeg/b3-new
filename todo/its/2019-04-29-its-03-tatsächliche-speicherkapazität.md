---
title:  "03. Tatsächliche Speicherkapazität"
date:   2019-02-13 06:10:00 +0100
---

Woher kommt die Differenz der Anzeige einer Festplatte beim Kauf (bspw. 1 TB) und der Anzeige in Microsoft Windows
(bspw. 931 GB)?

## 1. SI- und IEC Darstellung

Wie bei einer Festplatte entspricht die beworbene Kapazität (SI) nicht der eigentlichen Kapazität des Speichermediums
(IEC). Die Rohkapazität (Anzahl in Bytes) bleibt dabei jedoch unverändert.

## 2. Firmware und Metadaten

Firmware ist ein Programm, das in jedem Speichermedium integriert ist und grundlegende Funktionen für die Speicherung
bereitstellt (Speicherverhalten, Werkseinstellungen, Identität).

- Treiber sagt dem OS was es zu tun hat
- Firmware sagt Hardware wie sie sich zu verhalten hat.
- Metadaten sind Informationen über das Speichermedium selbst, wie z.B. Bezeichnung, Speicherkapazität, Hersteller,
  Seriennummer, etc.

## 3. Partitionierung

Datenträger können in verschiedene Partitionen unterteilt worden sein. Diese erlauben, mehrere Betriebssysteme auf einem
Datenträger zu installieren.

## 4. Formatting Overhead

Legt man auf einem Datenträger nur eine einzige, komplett neu formatierte Partition an, zeigen deren Eigenschaften, dass
selbst dort bereits Speicherplatz belegt wird, obwohl noch keine Userdaten vorhanden sind:

Bei der Installation des OS, wird eine leere Partition so vorbereitet/formatiert, dass gewisse Bereiche zur Organisation
des Dateisystems verwendet werden.

bei NTFS $$\rightarrow$$ MFT (Master File Table)

- Metadaten über die Partition (Bootsektor, File System Version, Berechtigungen, ...)
- Metadaten über die Daten (Name, Ort, Speicherdatum, Rechte, ...)

$$\rightarrow 12,5\%$$ der Daten

## 5. Over Provisioning

Die Installation einer Solid State Drive (SSD) stellt eine einfache Verbesserung der Leistungsfähigkeit von PC-Systemen
dar. Beim Kauf kann es mitunter jedoch zu Unstimmigkeiten kommen: Viele SSDs werden oft mit anderen Speicherkapazitäten
angeboten als traditionelle Festplatten.

Solid State Laufwerke unterscheiden Form und Schnittstellen kaum von Festplatten (HDDs). Beim Innenleben und der
Funktionsweise sieht es ganz anders aus.

Bei der Fertigung einer SSD, wird ein Teil der nutzbaren Kapazität dem SSD-Speichercontroller (via Firmware) zugewiesen.
Dieser Speicherplatz ist für den Benutzer nicht mehr sichtbar und wird hauptsächlich für Vorgänge verwendet, die die
Lebensdauer der gesamten SSD verbessern.

- Over Provisioning: Überbereitstellung von Speicherkapazität
- OP wird vom Hersteller festgelegt und die Höhe nicht beworben (Minimum: $$7,37\%$$)
