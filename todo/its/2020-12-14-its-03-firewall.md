---
title: "03. Firewall"
date: 2020-11-20 08:30:00 +0200
---

## Firewall - Konzepte

Der Einsatzzweck definiert die geeignete Technik

Bei den Firewalls unterscheidet man vom Konzept her drei Arten des Zugangs:

- die Paket-Filterung,
- Stateful-(Packet)-Inspection und das
- Application (Level) Gateway.

Jeder Proxy auf dem Application Gateway kann speziell für den Dienst, für den er zuständig ist, weitere
Sicherheitsdienste anbieten. Bedingt durch den jeweiligen speziellen Proxy und das Wissen um den Kontext eines
speziellen Dienstes ergeben sich im Application Gateway umfangreiche Sicherungs- und Protokollierungsmöglichkeiten.
Entsprechend der Vielzahl der angebotenen Dienste gibt es anwendungsspezifische Gateways beispielsweise für Telnet,
E-Mail, HTTP u.a..

Darüber hinaus gibt es High-Level-Security-Systeme, die sich aus den verschiedenen Konzepten zusammensetzten. Die
Funktion "Firewall" kann auf einem Router integriert sein oder als eigenes Gerät, als sogenannte "Appliance", realisiert
sein.

![ITS - Firewall](/assets/img/ITS/03-firewall-einsatz.png)

Die Datenpaket-Filterung ist die einfachste Firewall-Konfiguration, bei der die ein- und ausgehenden Datenpakete auf dem
OSI-Data-Link-Layer, dem Network-Layer und dem Transport-Layer anhand einer vorhandenen Tabelle analysiert und
kontrolliert werden können. Dieser Packet-Filter interpretiert die Header der Datenpakete und verifiziert, ob die Daten
den definierten Regeln entsprechen. Die Packet-Filter sind transparent in die Leitung eingefügt.

Als Stateful-Inspection-Firewall* bezeichnet man Systeme, die neben den Adressinformationen in den Headern der Schicht
drei und vier zusätzlich den Zustand der Verbindung (State) prüfen. Sie prüft dabei auch die Informationen im Flag-Feld
des TCP-Segmentes.

Die sicherste aber auch aufwendigste Alternative einer Firewall stellt das Application Gateway dar, welches hinreichende
Sicherheitsmechanismen über mehrere Schichten realisiert. Es kann die Netze logisch und physikalisch entkoppeln und
erwartet von jedem Benutzer eine vorherige Identifikation und Authentifikation.

Das Application Gateway empfängt die Datenpakete an den entsprechenden Ports (Layer 4). Soll nur ein Dienst über den
Port möglich sein, wird auf dem Application Server eine Software aktiv, die das Paket von einer Netzwerkseite auf die
andere Netzwerkseite überträgt, ein sogenannter Proxy. Aus Sicht des zugreifenden Benutzers sieht es so aus, als würde
er mit dem eigentlichen Serverprozess des Dienstes auf einem Zielrechner kommunizieren. Tatsächlich kommuniziert der
Benutzer aber mit dem Proxy, dem Stellvertreter, der nach beiden Seiten als Vermittler auftritt, so dass niemals eine
direkte Verbindung zwischen Zielrechner und Besucher zustande kommt.

Ein High-Level-Firewall-System fasst mehrere Firewall-Konzepte intelligent zusammen und garantiert so ein Höchstmaß an
Sicherheit.

Weitere Informationen:

- <https://lernplattform.mebis.bayern.de/pluginfile.php/31896537/mod_page/content/24/Herdt_NW_Sicherheit_Firewalls.pdf>

## Firewall mit DMZ

Varianten

Der Zugriff auf die internen Server aus dem Internet kann auf unterschiedliche Weise realisiert werden.

Weitere Infos:

- <https://lernplattform.mebis.bayern.de/pluginfile.php/31896547/mod_label/intro/Herdt_NW_Sicherheit_Firewalls.pdf>

### Portforwarding auf Server im LAN

In einfachen Umgebungen mit geringen Sicherheitsanforderungen wird ein Zugriff auf die externe öffentliche IP-Adresse
des Zugangsrouters auf die private IP-Adresse des Servers im LAN weitergeleitet. Dazu muss die NAT/PAT-Konfiguration des
Routers angepasst werden. Der Zugriff erfolgt ohne Authentifizierung.

![ITS - DMZ - Portforwarding](/assets/img/ITS/its-03-Router_DMZ.png)

### DMZ mit einstufiger Firewall

An einer separaten Schnittstelle des Firewallrouters wird das DMZ-Netz konfiguriert. So lassen drei Netzbereiche
definieren. Die Firewall auf dem Router ist so einzurichten, dass ein Zugriff aus dem WAN nur über definierte IP- und
Portadressen in die DMZ möglich ist. Wird auf der WAN-Schnittstelle NAT aktiviert, wird auch ein Portforwarding
notwendig.

![ITS - DMZ - Einstufige Firewall](/assets/img/ITS/its-03-Firewall-DMZ-einstufig_1.png)

### DMZ mit zweistufiger Firewall

In einer Umgebung mit hohen Sicherheitsanforderungen ist der Zugriff aus dem Internet nur auf einen dafür bestimmten
Netzbereich möglich. Dieser Bereich ist damit weder im internen LAN (sicher, privat) noch im WAN/Internet (unsicher,
öffentlich), es ist ein Zwischennetz (Perimeternetz) bzw. in der "DMZ: Demilitarisierten Zone"[^1].

Zwei Firewallrouter trennen die Netzbereiche LAN-DMZ-WAN.

![ITS - DMZ - Zweistufige Firewall](/assets/img/ITS/its-03-Firewall-DMZ-zweistufig.png)

## Fußnoten

[^1]: Eine DMZ ist eine "Demilitarisierte Zone / Demilitarized Zone" beschreibt ein Gebiet zwischen zwei Staaten,
      welches weder zum einen noch zum anderen Staat gehört und nicht vom Militär eines der beiden Staaten besetzt
      werden darf. Ein Beispiel dafür ist das Grenzgebiet zwischen
      [Nord- und Süd-Korea](https://upload.wikimedia.org/wikipedia/commons/e/ed/Panmunjeom_DMZ.png).
