---
title:  "02. SI und IEC Präfixe"
date:   2019-02-13 06:05:00 +0100
---

# SI und IEC Präfixe

Die Informatik verwendet als kleinste Einheit das Bit. 1 Bit enthält einen Zustand. Er kann 1 oder 0 sein. Aufgrund,
dass Computer aufgrund technischer Gegebenheiten nicht im Dezimalsystem, sondern Dualsystem arbeiten und gebaut werden,
ist die nächstgrößere Einheit das Byte.

Ein Byte enthält 8 Bit. Dies entspricht $$2^3$$. Mit voranschreitender Zeit werden die Kapazitäten der Datenträger und
Bandbreiten der Computer und anderer elektronischer Geräte immer größer. Um diese riesigen Zahlen ausdrücken zu können
werden die bekannten SI-Präfixe verwendet (siehe unten).

Auf Betriebssystemen wie bspw. Microsoft Windows werden (zur Anwenderfreundlichkeit) SI-Einheiten angezeigt, obwohl
IEC-Einheiten verwendet werden. Dies sollte bei der Arbeit mit Windows beachtet werden, da sonst es zu
Missverständnissen oder Problemen bei der Speicherkapazität von Festplatten kommen kann!

- $$1kB = 1000 Byte \rightarrow$$ SI-Einheit mit Basis 10
- $$1kiB = 1024 Byte \rightarrow$$ IEC-Einheit mit Basis 2

Der Standard der IEC Einheiten existiert seit November 2000.

## Formeln

Umrechnung von SI in IEC:

$$
a=\text{Umzurechnende Zahl; } b=\text{Exponentialdarstellung des IEC-Präfixes} \\
a*2^{b}
$$

Umrechnung von IEC in SI:

$$
a=\text{Umzurechnende Zahl; } b=\text{Exponentialdarstellung des IEC-Präfixes; }c=\text{Exponentialdarstellung des SI-Präfixes; } \\
a*\frac{10^{b}}{2^{c}}
$$

## Tabelle: SI- in IEC-Einheiten

| SI-Einheit | Umrechnung                                                   | in IEC-Einheit | prozentualer Anteil |
| ---------- | ------------------------------------------------------------ | -------------- | ------------------- |
| 1 TB       | $$\frac{10^{12} }{2^{40}} \frac{\text{Terra}}{\text{Tebi}}$$ | 0,9095 TiB     | $$\approx. 91\%$$   |
| 1 GB       | $$\frac{10^{9}}{2^{30}} \frac{\text{Giga}}{\text{Gibi}}$$    | 0,9313 GiB     | $$\approx. 93\%$$   |
| 1 MB       | $$\frac{10^{6}}{2^{20}} \frac{\text{Mega}}{\text{Mibi}}$$    | 0,9537 MiB     | $$\approx. 95\%$$   |
| 1 kB       | $$\frac{10^{3}}{2^{10}} \frac{\text{Kilo}}{\text{Kibi}}$$    | 0,9766 kiB     | $$\approx. 98\%$$   |

## Tabelle: IEC- in SI-Einheiten/Zahlen

| IEC-Einheit | Präfix | Exponent |      als Zahl       |
| ----------- | :----: | :------: | :-----------------: |
| 1 Tebi      |   Ti   | $$2^{40}$$ | $$1.099.511.627.776$$ |
| 1 Gibi      |   Gi   | $$2^{30}$$ |   $$1.076.741.824$$   |
| 1 Mebi      |   mi   | $$2^{20}$$ |     $$1.048.576$$     |
| 1 Kibi      |   ki   | $$2^{10}$$ |       $$1.024$$       |
| 1           |        | $$2^{0}$$  |         $$1$$         |

## Tabelle: SI-Präfixe

| Präfix  | Abkürzung |  Maßstab  |   ausgeschrieben    |
| ------- | :-------: | :-------: | :-----------------: |
| 1 Terra |     T     | $$10^{12}$$ | $$1.000.000.000.000$$ |
| 1 Giga  |     G     | $$10^{9}$$  |   $$1.000.000.000$$   |
| 1 Mega  |     M     | $$10^{6}$$  |     $$1.000.000$$     |
| 1 Kilo  |     k     | $$10^{3}$$  |       $$1.000$$       |
| 1       |           | $$10^{0}$$  |         $$1$$         |
| 1 Milli |     m     | $$10^{-3}$$ |       $$0,001$$       |
