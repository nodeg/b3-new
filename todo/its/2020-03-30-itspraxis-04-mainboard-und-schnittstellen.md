---
title:  "04. Mainboard und Schnittstellen"
date:   2019-02-13 06:10:00 +0100
---

## Die Hauptplantine

Die Hautptplantine wird auch Mainboard genannt. Sie übernimmt folgende Aufgaben grob:

- Bietet mechanische Schnittstellen für andere PC-Komponenten
- Verbindung der PC-Komponenten über Leiterbahnen (Kommunikation, Datenübertragung)
- Stromversorgung

## Aufbau und Bestandteile

Die Trägerplatte enthält in mehreren Schichten Leiterbahnen, über die die entsprechenden Bauteile miteinander verbunden
sind und über die Daten in Form von elektrischen Signalen übertragen werden.

![ITS Praxis - 04 - Mainboard](/assets/img/ITS-Praxis/04-mainboard.jpg)

Quelle: <https://www.heise.de/select/ct/2016/3/1454312896436123>

## Format eines Motherboards

Formfaktor (Standard oder Spezifikation)

- Angabe über Beschaffenheit einer PC-Komponente
- Beschreibt Leiterplatten, Laufwerke, Netzteile, Mainboards
- Bspw. für Mainboard: Abmessungen, Bestückungshöhen der elektronischen Bauelemente, Anzahl, Ort und Größe der
  Befestigungslöcher, Schnittstellenpositionen

Dominierendes Format: ATX

- Advanced Technology Extended
- Seit 1996 führendes Format

Trend: Ein-Platinen-Rechner $$\rightarrow$$ ATX nicht mehr konkurrenzlos

| Formfaktor | Abmessungen Mainboard |
|------------|-----------------------|
| AT         | 305mm x 330mm         |
| ATX        | 305mm x 244mm         |
| Micro-ATX  | 229mm x 191mm         |
| Flex-ATX   | 244mm x 244mm         |
| Mini-ITX   | 170mm x 170mm         |

## BIOS-Chip, CMOS-RAM und System Clock Battery

Der BIOS-Chip beinhaltet:

- Basic
- Input
- Output
- System

$\rightarrow$ UEFI

Der CMOS-RAM speichert folgende System-Informationen

- Bootreihenfolge
- Uhrzeit
- Energieeinstellung
- CPU-Taktraten
- Passwort
- ...

Die System Clock Battery sorgt dafür das der CMOS-Speicher eine ständige Stromversorgung besitzt. Diese wird benötigt um
Informationen zu speichern, sonst gehen diese verloren.

$\rightarrow$ CR2032 Lithium-Knopfzelle != Lithium-Ionen-Akku

## Hardware Schnittstellen

- Anfangszeit: Jede Anwendung hat eine eigene Schnittstelle
- Heute: Universal-Schnittstellen (USB, PCIe); Meta-Schnittstellen, die mehrere Schnittstellen in einer vereinen
  (Thunderbolt vereint DP und PCIe oder M.2)

Die Spezifikation einer Schnittstelle enthält Informationen über:

- Übertragungsgeschwindigkeit
- Den Stecker bzw. der Buchse
- Übertragungsverfahren
- Position (intern, extern)
- Pin-Belegung
- Schnittstellenleitungen

1. Analog vs. Digital
    - Analog: Allmählicher Übergang von einem Zustand zum Anderen
    - Digital: Sprunghafter Änderung eines Zustands
2. Serielle vs. Parallele Datenübertragung
    - Serielle Datenübertragung: Bits eines Datensatzes werden zeitlich hintereinander über eine Datenleitung gesendet.
    - Parallele Datenübertragung: Zeitgleiche Übertragung jedes Bit eines Datensatzes über jeweils eine eigene Leitung.

## Interne und externe Schnittstellen

Siehe [Schnittstellenübersicht]({% post_url 2019-06-16-itspraxis-99-anschluesse %})

## Prozessorsockel und Kühler

- Aktiver Kühler (Lüfter)
- Passiver Kühler (Kühlrippen)
- Heatpipes
- Pad (Wärmeleitmedium)
- Wärmeleitpaste um Hitze zu leiten und mechanischer Dämpfer
