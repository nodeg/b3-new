---
title:  "05. Chipsatz"
date:   2019-03-27 06:00:00 +0100
---

## Allgemeines

Der Chipsatz verbindet die einzelnen Elemente eines Computers auf logischer und physikalischer Ebene miteinander. Dabei
werden unterschiedliche Spannungspegel, Taktfrequenzen und Protokolle Ineinander umgewandelt. Der Chipsatz legt fest,
welche Komponenten in einem Rechner verwendet werden können und hat großen Einfluss auf die Gesamtsystemleistung.

Bei frühen Heimcomputern bestand das System neben dem Mikroprozessor typischerweise aus einer Reihe von eigenständigen
Schnittstellen-Chips, die alle direkt vom Prozessor angesprochen wurden.

Mit fortschreitender Integrationstiefe wurden immer mehr dieser verteilten Funktionen in größeren Chips
zusammengefasst - dem sogenannten Chipsatz. So bildeten sich gewisse "Standards" heraus.

Die klassische Chipsatz(Brücken)-Architektur besteht grob gesehen aus zwei Bausteinen:

- Der eine kümmert sich um die Kommunikation mit zwischen Hauptprozessor (CPU), Arbeitsspeicher (RAM) und
  Grafikkarte (GPU): Northbridge: Verwaltung von Schnittstellen größerer Bandbreite $$\rightarrow$$ Leistungsstarker
  Chip
- Ein zweiter Baustein integriert alle anderen Komponenten über die peripheren Schnittstellen und verbindet diese mit
  dem restlichen System.<br>
  Southbridge: Verwaltung der langsameren Peripherieschnittstellen $$\rightarrow$$ geringer Bandbreite $$\rightarrow$$
  leistungsschwächerer Chip<br>
  Architektur von 1990 bis 2010 circa.

## Entwicklung der System-Architektur und Auswlrkun1 auf den Chipsatz

![ITS-Praxis - 05 - Intel Hub Architecture](/assets/img/ITS-Praxis/05-AMD_single-socket_system_2003.gif)

Intel Hub Architecture (IHA) ab 1999
Memory Control Hub (übernimmt Aufgaben der Northbridge)
Direct Media Interface (anstatt langsamer PCI-Bus)
I/O Controller Hub (übernimmt Aufgaben der Southbridge)

![ITS-Praxis - 05 - Intel Hub Architecture](/assets/img/ITS-Praxis/05-pre-Nehalem_Intel_system_ICH.gif)

AMD Hypertransport Northbridge ab 2003<br>
Die Anbindung von Arbeitsspeicher (RAM) an Prozessor war Flaschenhals $$\rightarrow$$ nun schnellerer RAM Zugriff
Hyper Transport (anstatt langsamen PCI-Bus)

![ITS-Praxis - 05 - Intel Hub Architecture](/assets/img/ITS-Praxis/05-p55-system_PCH_2008.gif)

Platform Controller Hub (PCH) ab 2009<br>
Zunehmende Bedeutung der Grafikleistung $$\rightarrow$$ nun Grafikkarte direkt an den Prozessor angebunden<br>
Nur noch einfache Schnittstellen werden über einen IO-Chip bereitgestellt
$$\rightarrow$$ Alle Leistungsbeanspruchenden Funktionen werden nun von CPU übernommen
