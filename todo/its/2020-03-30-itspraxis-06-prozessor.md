---
title:  "06. Prozessor"
date:   2019-04-03 06:00:00 +0100
---

## Allgemeines & Allgemeiner Aufbau

- Mikrochip, der Berechnungen durchführt und andere Prozesse steuert
- \>95% aller Prozessoren weltweit in Embedded Systems verbaut
- In PC Systemen ist der Prozessor die CPU - Central Processing Unit

![ITS-Praxis - 06 - Prozessor Aufbau](/assets/img/ITS-Praxis/06-Prozessor-Aufbau.png)

| Funktionsblock                    | Funktion                                                      |
|-----------------------------------|---------------------------------------------------------------|
| Control Unit                      | Steuerwerk: steuert Prozesse                                  |
| Processing Unit (Rechenwerk/kern) | Arithmetic Logical Unit: math. (+ - * /) und log (Und / Oder) |
|                                   | Floating Point Unit: Gleitkommazahlberechnungen               |
| Register                          | Hilfsspeicher zur Befehlsabarbeitung                          |
| Speichercontroller                | Speicherverwaltung von Cache und RAM                          |
| Bus-Interface                     | Verbindung zwischen internen Prozessorbus und Systembus       |
| Cache                             | Schneller Datei- und Befehlsspeicher                          |

## Kenngrößen eines Prozessors

### Herstellungstechnologie und Strukturgröße

![ITs-Praxis - 06 - Morre's Law](/assets/img/ITS-Praxis/06-morres-law.png)

Quelle: <https://medium.com/predict/moores-law-is-alive-and-well-eaa49a450188>

Bauteile werden immer kleiner!

Prognose 1965:

- Anzahl der verbauten Transistoren verdoppelt sich jährlich
- Integrationsdichte (Bauteilanzahl pro Fläche) steigt

Problem:

- Tunneleffekt (physikalische Grenzen)
- Signallaufzeiten zu lang und Zeit zwischen den Takten immer kürzer
- Hitzeentwicklung

Prognose seit 2015:

- Alle 2,5 Jahre
- Vermutliches Ende: 2025

### Vergleich zweier CPU Generationen

- Beschreibt die Größe der Transistoren und deren Verbindungen
- Je kleiner die Strukturen, desto mehr Transitoren passen auf gleichen Raum (Integrationsdichte steigt)
- Die Verbindungen sind kürzer $$\rightarrow$$ Signallaufzeiten werden geringer

### Anzahl der Kerne

#### Single-Core Prozessor (ohne SMT)

Der Prozessor besitzt:

- 1 physikalisches Rechenwerk mit
- 1 Register und
- 1 Leitung

#### Multi-Core Prozessor

Der Prozessor besitzt:

- 2 physikalische Rechenwerke mit
- 2 separaten Registern und
- 2 Leitungen

#### Single-Core Prozessor (mit SMT)

Der Prozessor besitzt:

- 1 physikalisches Rechenwerk mit
- 2 Hilfsregistern und nur
- 1 Leitung

### Taktrate und Rechenleistung

Der interne Takt (Prozessortakt) ist die Geschwindigkeit, mit der der Prozessor intern selbst arbeitet.<br>
$$\rightarrow$$ Größe: Frequenz, Einheit: Hz (entspricht Arbeitsschritte pro Sekunde)

1 Hz = 1 * 1/s = 1 s^-1

### OPS

Der Takt ist entscheidend für die Operationen pro Sekunde

### IPS

"Instructions per Second" gibt die Anzahl Anweisungen an, welche ein Prozessor durchschnittlich innerhalb einer Sekunde
verarbeitend kann.

### FLOPS

Die durchschnittliche Rechenleistung wird über Floating Point Operations per Second (FLOPS) angegeben.

## Weitere Eigenschaften eines Prozessors

Die Verbindungsart des Prozessors mit

- Chipsatz
- RAM
- Grafikkarte

Beeinflusst die Schnelligkeit der Datenübertragung (externer Takt).

Wenn Übertragungszeit niedrig, dann Verarbeitungsgeschwindigkeit hoch.

In der Regel hat eine CPU folgende wichtige Verbindungen:

- Memory Bus zum RAM
- PCIe-Bus
- Direct Media Interface (DMI) zum Chipsatz/Peripherie

DMI Takt: 8.0 GT/s $$\rightarrow$$ Giga Transfers per Second

### Prozessor-Cache

- Level 1 - Trennung Daten und Befehle
- Level 2 - Gemeinsamer Speicher zweier Rechenwerke
- Level 3 - Gemeinsamer Speicher aller Rechenwerke
- Level 4 - Gemeinsamer Speicher von CPU und Grafikkarte $$\rightarrow$$ Eigener Chip

Zwischenspeicher, der Daten und Befehle zur Verarbeitung lädt, noch bevor sie gebraucht werden (Cache prefetching).<br>
$$\rightarrow$$ Kein Leerlauf der CPU

### Prozessorsockel

Mainboardschnittstelle, die einige Eigenschaften der CPU festlegt:

- Anzahl, Belegung, Anordnung Kontakte (Pins)
- Höchstmögliche externe Taktfrequenzen
- Verwendete Betriebsspannung

$$\rightarrow$$ Elektrische und mechanische Standards
