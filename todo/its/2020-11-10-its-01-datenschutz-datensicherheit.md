---
title: "01. Datenschutz & Datensicherheit"
date: 2020-11-10 08:30:00 +0200
---

## Allgemeines

Datenschutz heißt, dass man Selbstbestimmt entscheiden kann

- wem man,
- wann und
- zu welchem Zweck seine Daten zur Verfügung stellt.

Datensicherheit bezeichnet den Schutz der Daten mithilfe von

- Vertraulichkeit,
- Integrität und
- Verfügbarkeit.

Sind Datenschutz & -sicherheit gewährleistet, so sind die deutschen rechtlichen Grundlagen auf Datenschutz
gewährleistet.

## Risiken und Schutzmaßnahmen von und für Daten

| RISIKO / GEFAHR                 					| SCHUTZ DURCH                                                  |
| ------------------------------------------------- | ------------------------------------------------------------- |
| Physikalisch (Einbruch)         					| Kensington™©®-Schlösser, Videoüberwachung                     |
| Softwarelücken            					    | Betriebsystemupdates/Upgrades/Trennen vom Netzwerk            |
| Hackangriffe                    					| VPN, Firewall, IDS/IPS                                        |
| Phishing                        					| Schulen der Mitarbeiter + Content Filtering + Proxy           |
| Hardwaredefekte               			        | Backup auf ein NAS/Cloud/SAS/..., Reservebausteine bereithalten, RAID-Systeme (RAID 1, 5, 10) |
| Serverausfall                                     | Redundante Infrastruktur (HA, Cluster, ...)                   |
| Diebstahl                                         | Festplattenverschlüsselung / Datenverschlüsselung             |
| "Fremde Mächte" fragen Daten an                   | Datensparsamkeit + Verschlüsselung                            |
| Hackangriffe                    					| Systeme von allen Netzwerken trennen (airgap)                 |
| Inkompatibilitäten mit neuer Software             | Daten zeitnah migrieren                                       |
| Unautorisierte Zugänge innerhalb des Betriebes    | Mitarbeiter mit entprechenden Rechten versehen                |
| Viren-/Schadsoftware                              | Update der Betriebssysteme / aktuelle Antivirensoftware       |
| Bruteforce-Angriffe                               | Cloudflare™©®/Fail2Ban                                        |
| Festplattenausfälle/-beschädigungen               | Regelmäßige Backups der Nutzerrechner/Server                  |
| Windoof® screensaver zeigt Kundenbilder           | Secreensaver deaktivieren                                     |
| ISP-downtimes                                     | hybride, (Kabel + DSL + LTE/5G) redundante Uplinks            |
| Außendienst hat auswärts Probleme                 | Remote Wartungs Software $$\rightarrow$$ TeamViewer/VNC/...   |
| Fragwürdige Downloadangebote                      | Adblocker                                                     |
| Nutzer installieren fake Browser addons           | Rechteverwaltung                                              |
| Benutzer löschen wichtige Daten                   | Rechteverwaltung, Backups, VCS                                |
| Hackerangriffe v2                                 | Cyberwehr + Hackback + Revengehacker                          |
| Admins wollen ihre Systeme nicht sichern          | dunno, siehe mebis™                                           |
| Admin gefeuert $$\rightarrow$$ Festplatten wipe   | neue Firma starten                                            |

## Mögliche einkaufbare Dienstleistungen

Möchte man aktuell einen Dienst einkaufen (egal ob Firmenintern oder -extern) gibt es aktuell drei Möglichkeiten wie
dies möglich ist:

- IaaS: steht für "Infrastructure as a Service". Hierbei wird dem Kunden die Infrastruktur so bereitgestellt, dass er
  sich nicht um sie kümmern muss. Infrastruktur bezieht sich meist auf Komponenten wie Server, Netzwerke, Virtuelle
  Maschinen oder ähnliche Dinge. Die Zielaudienz sind IT-Systemadministratoren.
- PaaS: steht für "Platform as a Service". Hierbei wird dem Kunden die Plattform so bereitgestellt, dass er sich nicht
  um sie kümmern muss. Plattform bezeichnet Dinge wie bspw. Datenbanken, Webserver, Speicherplatz oder komplexere Dinge
  wie auch Container Laufzeitumgebungen. Die Zielaudienz sind Entwickler.
- SaaS: steht für "Software as a Service". Hierbei wird dem Kunden die Software selbst so bereit gestellt, dass er sich
  nicht um sie kümmern muss. Dies sind bspw. Dinge wie Microsoft Office 365, Jira & Confluence Cloud, Webshops und
  andere Dinge. Die Zielaudienz sind Endanwender.

Alle drei Möglichkeiten haben individuelle Möglichkeiten und direkte Implikationen bzgl. Datenschutz und Sicherheit.
Ziel von allen drei Methoden ist die Zuständigkeit an Spezialisten abzugeben die mehr Erfahrung mit der Administration
des Aspektes haben.

Für alle drei genannten Möglichkeiten wird häufig auch der Begriff Cloud verwendet, da keine physikalische Infrastruktur
mehr zu verwalten ist. Stellenweise findet sich auch der Begriff Serverless Computing. Vorteile der Cloud sind hohe
Flexibilität und geringeres Ausfallrisiko, sowie ein fehlendes Risko für den Kunden bei der Neuinvestition in
Infrastruktur.
