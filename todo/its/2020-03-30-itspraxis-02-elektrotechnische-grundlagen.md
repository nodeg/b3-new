---
title:  "02. Elektrotechnische Grundlagen"
date:   2019-02-13 06:05:00 +0100
---

## ATX-Stromversorgung

Aufbau eines PCs und Anbindung des ATX-Netztells Anhand des PC Building Simulators haben Sie bereits digital ein
PC-System zusammengestellt. Nun sollen Sie die Möglichkeit haben, an einem realen Rechner zu arbeiten. Ziel dieser
Station Ist es dass Sie sich mit der Stromversorgung Ihres PC-Systems auseinandersetzen. Dazu legen Sie hier den Fokus
Ihrer Arbeit auf das Netzteil bzw. PSU (Power Suppy Unit).

Beschreiben Sie kurz In eigenen Worten die Funktion des Netzteils:

- Stromversorgung der Komponenten
- Umwandlung von Wechselspannung zu Gleichspannung

Die Anschlüsse des Netzteils werden auch PSU-Connectors genannt und besitzen unterschiedliche Spannungsschienen
(Leitungen mit verschiedenen Spannungen). Die Anschlüsse und deren Spannungen unterscheiden sich je nach
Einsatzfunktion.

| Nr. | PSU-Connector                | Spannungsschienen    | Funktion                      |
|-----|------------------------------|----------------------|-------------------------------|
| A   | Floppy Drive Power           | 12V/5V               | Diskette/alte Zusatzkarten    |
| B   | 4 Pin (MOLEX 8981)           | 12V/5V               | Laufwerke/Lüfter/Zusatzkarten |
| C   | SATA Power                   | 3,3V/12V/5V          | Festplatten/Laufwerke         |
| D   | 6+2 Pin (PCIe Power)         | 12V                  | Grafikkarten/CPU Power        |
| E   | 4+4 Pin (Entry Power Supply) | 12V                  | CPU Zusatzpower               |
| F   | 24 PIN (ATX Main Power)      | 12V/5V/3,3V/-5V/-12V | Hauptplatinen-Anschluss       |

| A                                                                                  | B                                                                               | C                                                                        | D                                                                                 | E                                                                                      | F                                                                                     |
|------------------------------------------------------------------------------------|---------------------------------------------------------------------------------|--------------------------------------------------------------------------|-----------------------------------------------------------------------------------|----------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------|
| ![ITS Praxis - 02 - Floppy Drive Power](/assets/img/ITS-Praxis/02-floppyindex.jpg) | ![ITS Praxis - 02 - 4 Pin Molex](/assets/img/ITS-Praxis/02-peripheralindex.jpg) | ![ITS Praxis - 02 - SATA Power](/assets/img/ITS-Praxis/02-sataindex.jpg) | ![ITS Praxis - 02 - 6+2 PCI Power](/assets/img/ITS-Praxis/02-pcie6plus2index.jpg) | ![ITS Praxis - 02 - 4+4 Pin Entry Power](/assets/img/ITS-Praxis/02-eps4plus4index.jpg) | ![ITS Praxis - 02 - 24 Pin ATX Main Power](/assets/img/ITS-Praxis/02-main24index.jpg) |

Source: <http://www.playtool.com/pages/psuconnectors/connectors.html>

![ITS Praxis - 02 - ATX 2.0 Power Connector schema](/assets/img/ITS-Praxis/02-atx20-power-connector-schema.png)

Source: <https://superuser.com/questions/905705/atx-dell-psu-to-offical-atx-2-0-spec-pinout>

## Stromwirkung auf Menschen

Immer wieder kommt es in Haushalten, Unternehmen oder auf Baustellen zu Spannungsunfällen, die auch tödlich enden
können. Um dio Gefahren im Umgang mit elektrischen Geräten besser einschätzen zu können, sollen Sie sich an dieser
Station näher mit den Wirkung des elektrischen Stroms auf den Menschen beschäftigen.

Diese Grundlagen erarbeiten Sie sich selbstständig durch die Ihnen zur Verfügung stehenden Mittel (bspw. Lehrbücher,
Internet, ... ).

Welche Spannungsart und in wieviel dieser Spannung liegt an einer Haushaltssteckdose in Deutschland an?
$$\rightarrow$$ AC 230V/50 Hz $$\rightarrow$$ bis zu 16A

### Gründe für Elektrounfälle: Kurzschluss, Verpolung, Überlast, Funktensprung, Unwissenheit, schlechte Qualität

1. Erklären Sie warum der elektrische Strom aus verschiedenen Gründen für den Menschen und Tier gefährlich ist. Zum
   besseren Verständnis sollten Sie damit anfangen, wo elektrische Impulse im menschlichen Körper auch ohne äußere
   Einwirkungen vorkommen.
   Nervensystem/Statische Elektrizität
    - Nervenstörung/Lähmung
    - Konversion von Energie $$\rightarrow$$ Thermische Energie durch Körperwiederstand
2. Die folgende Grafik zeigt die Wirkungsbereiche der Stromstärke (Wechselstrom von 5OHz) auf erwachsene Personen.
   ![ITS Praxis - 02 - Wirkungsbereich Strom](/assets/img/ITS-Praxis/02-wirkungsbereich_strom.gif)
   1. Bereich: Keine Auswirkungen
   2. Bereich: Spürbar/Muskelkontraktion
   3. Bereich: Gewebeschäden
   4. Bereich: Schwere Schäden oder Tod

Quelle: <http://www.brieselang.net/stromwirkung-auf-menschen.php>

### Die fünf Sicherheitsregeln

> Wegen Unfallgefahr ist das Arbeiten an Teilen, die unter Spannung stehen, verboten!

| Schritt                                                       | Icon                                                                                    |
|---------------------------------------------------------------|-----------------------------------------------------------------------------------------|
| Freischalten - Stecker/Schalter aus                           | ![ITS Praxis - 02 - Freischalten](/assets/img/ITS-Praxis/02-freischalten.gif)           |
| Gegen Wiedereinschalten sichern / Nicht Schalten              | ![ITS Praxis - 02 - Wiedereinschalten](/assets/img/ITS-Praxis/02-wiedereinschalten.png) |
| Spannungsfreiheit feststellen                                 | ![ITS Praxis - 02 - Spannungsfreiheit](/assets/img/ITS-Praxis/02-spannungsfreiheit.png) |
| Erden und Kurzschließen                                       | ![ITS Praxis - 02 - Erdung](/assets/img/ITS-Praxis/02-erdung.jpg)                       |
| Isolation von der Erde/Benachbarte Teile abdecken/abschranken | ![ITS Praxis - 02 - Isolieren](/assets/img/ITS-Praxis/02-isolieren.png)                 |

- Quelle Freischalten: <https://www.seton.de/ProduktImages/400px/14/_m/DMNE_Gebotschild_2037_M014_M.gif>
- Quelle Wiedereinschalten: <https://image.jimcdn.com/app/cms/image/transf/none/path/s644f0c6e7dc14f24/image/i7fa0a16a0208c316/version/1562660680/image.png>
- Quelle Spannungsfreiheit: <https://www.flaticon.com/free-icon/multimeter_649776>
- Quelle Erdung: <https://www.lampe.de/magazin/wp-content/uploads/2016/06/Zeichen_Schutzklasse_I.jpg>
- Quelle Isolieren: <https://img.icons8.com/cotton/2x/sleeping-mat--v1.png>

### Auffinden einer Person

> Im Notfall muss Erste Hilfe geleistet werden. Alles Ist besser, als gar nichts zu tun.

Klären Sie im Team wie Sie beim Auffinden einer Person schrittweise vorzugehen haben. Sie können Stichpunkte oder auch
Diagramme, wie beispielsweise einen Programmablaufplan, zur Darstellung nutzen.

- Stromquelle abschalten
- Notarzt rufen
- Verbrennung kühlen

### Notruf absetzen

Das Absetzen des Notrufes gehört mit zu den wichtigsten Aufgaben des Ersthelfers. Wenn ein Notruf nicht rechtzeitig
getätigt wird, dann verzögert sich automatisch das Eintreffen von professioneller Hilfe und dies kann unter Umständen
für den Verletzten tödlich enden. Nach welchem Frage-Schema ist dabei vorzugehen?

W-Fragen + Rückfragen (erwähnen das es ein Unfall mit Strom ist)

## Verbrauch eines Computers

Green-IT bezeichnet in der Informatik den Einsatz von Technologien, der unter Berücksichtigtmg des gesamten
Produktlebenszyklus im Vergleich zu bisherigen Lösungen zu einer deutlichen Entlastung der Umwelt führt.

Green-IT trägt also beispielsweise dazu bei, Energie effizienter zu nutzen, Ressourcen zu schonen und das Klima zu
schützen. So gelingt es auch, dass in anderen Branchen und Lebensbereichen Einsparungen erreicht werden.

Neben der Herstellung und der Entsorgung benötigen Green-IT-Produkte im Betrieb weniger Energie und Ressourcen als
herkömmliche Produkte der Informations- und Kommunikationstechnologie.

### Informieren: Messgerät und Energieverwaltung

1. Welche Aufgaben erfüllt ein Energiemonitor bzw. Energiemessgerät? Welche Funktionen stehen Ihnen durch das gegebene
   Messgerät zur Verfügung? Halten Sie Ihre Erkenntnisse fest.
   $$\rightarrow$$ Messen von Spannung/Stromstärke und Leistung, sowie der zeitlichen Berechnug des Stromverbrauches.
2. ACPI Ist ein offener Industriestandard und hauptsächlich für die Energieverwaltung bekannt. Was bedeutet die
   Abkürzung ACPI? $$\rightarrow$$ ACPI = Advanced Configuration and Power Interface
3. ACPI unterscheidet unter anderem zwischen den Betriebszuständen des gesamten Systems (S-States) anhand einzelner
   Bezeichnungen von ACPI SO bis SS.

   | ACPI Betriebszustand | Bedeutung             |
   |----------------------|-----------------------|
   | ACPI S0              | Volle funktionsstärke |
   | ACPI S3              | Standby               |
   | ACPI S4              | Ruhezustand           |
   | ACPI S5              | Aus (Soft-Off-Modus)  |

4. Eine Grundaussage über die ACPI-Zustände kann wie folgt formuliert werden: Je größer die Zahl des ACPI-Modus, desto
   mehr aus.
5. Bringen Sie die einzelnen Zustände mit der Energieverwaltung von Windows in Verbindung.

   | ACPI Betriebszustand | Bedeutung                                                                                                                                                |
   |----------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------|
   | ACPI S0              | Windows ist hochgefahren und voll einsatzfähig                                                                                                           |
   | ACPI S1/S2           | S1 und S0 unterscheiden sich kaum; S2 ist ungenau definiert und kommmt deshalb nicht vor                                                                 |
   | ACPI S3              | Standby/"Energie sparen": Suspend to RAM, System-abbild wird im RAM gespeichert und per Tastendruck wieder geladen                                       |
   | ACPI S4              | Ruhezustand/"Herunterfahren mit Schnellstart" (seit Win 8); Suspend to Disc, RAM-Inhalt wird auf Festplatte gespeichert (Bootvorgang wird beschleunigt)  |
   | ACPI S5              | "komplett" Ausgeschalteter Zustand, PC lässt sich per Power-Taster starten                                                                               |

### Informieren: Benchmarktools und Systemüberwachung

Computer-Benchmarks dienen dem Vergleich der Rechenleistung von PC-Systemen. Meist wird dabei Software verwendet, um
die Leistung verschiedener Hardware-Komponenten zu messen. Dazu können die einzelnen PC-Bestandteile an ihre Grenzen
gebracht werden.

So kann eine Vollauslastung eines Systems simuliert und überwacht werden. Im folgenden ist der Ablauf zu erkennen:

1. Stecken Sie eine Verteilerdose in eine nahe, freie Steckdose im Labor.
2. Stecken Sie in die Verteilerdose den Enrgiemonitor.
3. Nehmen Sie ein Kaltgerätekabel (Anschluss PC-Netzteil zu Steckdose) und schließen Sie den Testrechner ("Glasrechner")
   den Energiemonitor an.
4. Nutzen Sie Maus, Tatstatur und die HDMI-Bildschrim-Verbindung eines Laborrechners und verbinden Sie diese Komponenten
   mit dem Testrechner.
5. Schließen Sie den Testrechner NICHT ans Netzwerk (Ethernet) an, sondern nutzen Sie zur Recherche und Auswertung einen
   Labor-PC. Starten Sie nun den Testrechner und machen Sie sich mit den bereits installierten Benchmark-Tools (welche
   keine Admin-Rechte benötigen) vertraut. Klären Sie mit welchem Programm sich welche Systemkomponente wie ansprechen
   und testen lässt.

## Energiespeicher

### Energiespeicher bei IT-Geräten, Ladung, Kapazität, Energie, Leistung, Zeit

Das Mainboard und seine Komponenten werden über den ATX-Netzteilstecker mit elektrischem Strom versorgt. Bel Notebooks
sowie Smartphones werden Akkumulatoren (Akkus) eingesetzt.

1. Klären Sie den Unterschied zwischen Batterie und Akkumulator.
   $$\rightarrow$$ Akku ist wiederaufladbar $$\rightarrow$$ chemischer Prozess der Entladung ist umkehrbar
2. Was versteht man unter dem Begriff Kapazität einer Batterie (Energiespeicher).
   $$\rightarrow$$ Höchstmöglicher Ladungszustand
   Wenn über eine gewisse Zeit elektrischer Strom in einen Energiespeicher geflossen ist, besitzt er eine bestimmte
   Ladung:

   $$Q (\text{Ladung}) = I (\text{Strom}) * t (\text{Zeit der Aufladung/Entladung})$$

   Je nachdem, wie hoch die Spannung ist, wird dabei mehr oder weniger Energie aufgewandt:

   $$
   \begin{aligned}
    Q * U (\text{Spannung}) & = I * t * U (\text{Spannung}) \\
    W (\text{Energie})      & = I * t * U
   \end{aligned}
   $$

   Möchte man nur die momentane Leistungsaufnahme/abgabe berücksichtigen, die beim Laden bzw. Entladen vorherrscht,
   betrachtet man lediglich die Stromstärke und die Spannung ohne die Zeit:

   $$w / t$$
   $$P (\text{Leistung}) = I * U = I * U$$

3. Wenn Sie mit dem Notebook arbeiten, zeigt Windows an, wie viel Akkulaufzeit noch bleibt Die Berechnung dahinter lässt
   sich mit Hilfe der technischen Daten des verbauten Notebook-Akkus und der genannten Formeln nachstellen.
   a. Wie viele Stunden kann der Akku mit 440 mA belastet werden. bis er leer ist?
   b. Wie viel Rest-Kapazität ($$Q$$) besitzt der Akku. wenn er noch zu 35% geladen ist.
   c. Wie lang muss er von 35" auf 1"°" ~n ~,den. wenn das Netzteil Ihn m,t 2 A l.ldMtN>m (II vtno,st7
4. Sie nutzen eine sehr rechenintensive App auf ihrem Smartlphone. Nach einer Stunde ist der anfangs volle Akku nur noch
   bei 48% seiner Ladung. Auf dem Akku steht: 2800mAh/4,3V
   Wie viel Strom benötigt die App während sie ausgeführt wird? Wie lang können Sie das Smartphone im Stromsparbetrieb
   (120 mA) noch betreiben, bevor es leer ist? $$\rightarrow$$ 1456mAh - 11 Std. (siehe PDF)
5. Bei Smartphones findet man für die Akkukapazität meist eine Angabe In mAh und bei Tablets sowie Laptops eher eine in
   Wh. Ihr Smartphone-Akku besitzt folgenden Aufdruck: 4,3 V / 3000 mAh 11,8 V/ 57Wh Auf Ihrem Laptop-Akku sehen Sie die
   Werte: Vergleichen Sie beide Angaben, indem Sie berechnen, welcher Akku mehr Ladung bzw. mehr Energie speichern kann.
6. Wie groß ist die Kapazität eines 12V-Akkus eines Laptops (in Wh) der von 25% auf 85% innerhalb einer Stunde mit einem
   Ladestrom von 2 A aufgeladen wird?
7. Die maximale Leistungsaufnahme eines Prozessors in einem Notebook beträgt 35 W. Während der Arbeit mit Word messen
   Sie mit einem Energiemonitor eine durchschnittliche Leistungsaufnahme von 25 W. Bei voll geladenem Akku, kann das
   Notebook ohne Netzteil eine Stunde lang mit 57 W belastet werden. (Energie W = 57 Wh) Wie lange können Sie mit dem
   Notebook an einem Word-Dokument schreiben, wenn der Akku vollgeladen ist und das Notebook nicht mehr am Stromnetz
   angeschlossen ist?
8. Welche Zeit kann ein Notebook mit einer Leistungsaufnahme von 30 W mit einer Knopfzelle, wie die eines Mainboards
   (CR 2032 Lithium), betrieben werden? Technische Daten der Knopfzelle: CR2032 20,0 mm/ 3,2 mm/ 3,0 V/ 230 mAh

## Der elektrische Stromkreis

Um in der IT komplexe Software verständlich zu machen, muss nicht jeder Anwender jedes Detail kennen. Jedoch muss die
Funktionalität des Programms nachvollziehbar und kein geheimnisvolles Gebilde sein - in der Elektrotechnik gilt dieses
Prinzip genauso. Wo also Struktogramme zur Darstellung einer Software dienen, nutzen Schaltpläne der Darstellung einer
elektrischen Verschaltung.

Das Lesen und Zeichnen von Schaltungen bzw. Schaltplänen, also das Erkennen von Bauteilen und deren Eigenschaften, ist
für einen Elektronik-Einsteiger nicht einfach. Dieses notwendige Verständnis bekommt man nicht nur daurch das Lesen von
Büchern, sondern vielmehr durch selbstständiges Experimentieren an echten Schaltungen.

Dafür ist es jedoch notwendig, dass man vor der praktischen Umsetzung gewisse Grundlagen beherrscht. Diese Grundlagen
erarbeiten Sie sich selbstständig durch die Ihnen zur Verfügung stehenden Mittel.

### Verwendung eines Multimeters

### Schaltung planen

1. Informieren Sie sich über das korrekte Anschließen aHer Kompon enten bei gleichzeitiger Strom- und Spannungsmessung
   und zeichnen -Sie damit ein korrektes Schaltbild des Aufbaus. Arbeiten Sie mft der korrekten Farbgebung der
   Anschlüsse bzw. Verbindungen.
2. Sie sehen den entsprechenden Versuchsaufbau mit allen dazugehörigen Messgeräten, der Stromversorgung und den
   Verbrauchern - jedoch ohne die Steckverbindungen. Benennen Sie alle Bauteile korrekt und zeichnen Sie die Leitungen
   ein (in Farbe). Achten Sie auf die Schalterstellung der Multimeter. Welche Farbgebung entspricht dem positiven und
   welche dem negativen Anschluss?

> Die Messgeräte werden bei falscher Benutzung beschädigt!

Neben der Einstellung des korrekten Messbereichs der Multimeter müssen Sie unbedingt folgende Regeln beachten, damit die
Messgeräte nicht beschädigt werden:

> Ein Strommessgerät wird immer in Reihe angeschlossen!

> Ein Spannungsmessgerät wird immer parallel angeschlossen!

## Logarithmische Darstellungsweise

In einem Koordinatensystem sind Sie bisher meist mit einer Darstellungsform von Informationen
konfrontiert: der linearen Darstellung.

### Lineare Darstellung

Eine lineare Skala besitzt eine Achseneinteilung, bei der in gleichgroßen Abständen die Zahlenwerte einer
darzustellenden Größe mit einem konstanten Wert auseinander liegen .

Beispiel: Von einer Hauptmarkierung zur nächsten liegt immer ein Wertunterschied von 10.

Wenn Sie nun den markierten Wert ablesen wollen, ergibt er sich bekanntermaßen wie folgt:

1. Der Zahlenwert von 0 bis 20 ist bekannt: 20
   Der Zahlenwert von 20 bis zur Markierung ist unbekannt: ?
2. Der mit Lineal gemessene Abstand a (von Zahlenwert 20 bis 30) beträgt beispielsweise 1,5 cm.
   Der ebenso gemessene Abstand b (von Zahlenwert 20 bis zur Markierung) beträgt 1,2 cm .
3. Nun berechnet man das Verhältnis c von Abstand a und b:
4. Der Zahlenwert, der zwischen Zahlenwert 20 und 30 liegt ist 10. Das errechnete Verältnis c beträgt 0,8. Multipliziert
   man beides, bekommt man den Zahlenwert von 20 bis zur Markierung:
   = 10 * Verhältnis c = 10 * 0,8 = 8
5. Jetzt muss nur noch der Zahlenwert 20 mit dem errechneten Zahlenwert 8 addiert werden: Der abgelesene Zahlenwert
   beträgt an der markierten Position 28.

### Logarithmische Darstellung

Die logarithmische Darstellung verwendet eine Achsenbeschriftung, bei der der Logarithmus eines Zahlenwerts aufgetragen
wird. Das heißt, dass der Wertebereich der dargestellten Daten mehrere Größenordnungen umfasst. Durch die
logarithmische Darstellung können Zusammenhänge im Bereich von kleinen und gleichzeitig auch von großen Werten
überschaubar dargestellt werden.

Grundsätzlich gilt, dass auf einer logarithmischen Achse gleiche Abstände einen gleichen Faktor zwischen den
Zahlenwerten wiedergeben . Steht also ein Abstand für den Faktor 10, dann entspricht der doppelte Abstand im Diagramm
dem Faktor 100 ($$= 10^2$$)

Beispiel: Von einer Hauptmarkierung zur nächsten liegt immer ein Faktor von 10.

Erklärung: Welche Zahl $$y$$ ergibt bei $$10^y$$ den Zahlenwert $$x$$?
           Schreibweise 1 : $$log_10 (x) = y  // log_10 (10) = 1$$
           Schreibweise 2 : $$10^Y = X        // 10^1 = 10$$

Dieser Schritt wird wichtig sein, um die spätere Berechnung nachvollziehen zu können.

Wenn Sie nun den markierten Wert ablesen wollen, müssen Sie wie folgt vorgehen:

1. Der Zahlenwert von 10 bis zur Markierung ist unbekannt: ?
2. Der mit Lineal gemessene Abstand a (von Zahlenwert 10 bis 100) beträgt beispielsweise 1,5 cm.
   Der ebenso gemessene Abstand $$b$$ (von Zahlenwert 10 bis zur Markierung) beträgt 1,2cm.
3. Nun berechnet man das Verhältnis $$c$$ von Abstand $$a$$ und $$b$$;
4. Da sich der Wert im Zahlenbereich zwischen 10 und 100 befindet, wird das oben errechnete Verhältnis $$c$$ mit 10
   multipliziert: $$? = 10 * \text{Verhältnis } c = 10 * 6,3096 = 63,1$$

Der abgelesene Zahlenwert beträgt an der markierten Position 63,1.
