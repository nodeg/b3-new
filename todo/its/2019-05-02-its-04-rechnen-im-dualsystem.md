---
title:  "04. Rechnen im Dualsystem"
date:   2019-02-13 06:10:00 +0100
---

## Addition

| Rechnung | Ergebnis | Übertrag |
| :------: | :------: | :------: |
|  $$0+0$$   |   $$0$$    |   $$0$$    |
|  $$0+1$$   |   $$1$$    |   $$0$$    |
|  $$1+0$$   |   $$1$$    |   $$0$$    |
|  $$1+1$$   |   $$0$$    |   $$1$$    |
| $$1+1+1$$  |   $$1$$    |   $$1$$    |

Problem: Addiert man zwei Binärzahlen miteinander und entsteht ein Übertrag, dann wird das sogenannte Carry-Bit gesetzt.

$$\rightarrow$$ Ist das Carry-Bit $$0_2$$, dann ist das Ergebnis korrekt, ist es $$1_2$$ gibt es einen
Overflow/Überlauf.<br>
$$\rightarrow$$ Das Ergebnis sollte geprüft werden, falls das Carry-Bit gesetzt ist.

Mögliche Lösungsansätze für das Behandeln eines Überlaufes:

- Programm wird unterbrochen mit einer Fehlermeldung
- Datentyp zur Konstruktionszeit ändern
- Ignorieren und verwerfen (schlecht!)

## Subtraktion

| Rechnung | Ergebnis | Übertrag (Entleihung) |
| :------: | :------: | :-------------------: |
|  $$0-0$$   |   $$0$$    |          $$0$$          |
|  $$0-1$$   |   $$1$$    |          $$1$$          |
|  $$1-0$$   |   $$1$$    |          $$0$$          |
|  $$1-1$$   |   $$0$$    |          $$0$$          |
| $$0-1-1$$  |   $$0$$    |          $$1$$          |
| $$1-1-1$$  |   $$1$$    |          $$1$$          |

## Negative Zahlen im Dualsystem

In bisherigen Betrachtungen wurden immer nur positive Ganzzahlen betrachtet. Somit ist der Wertebereich einer
$$n$$-stelligen Dualzahl $$0$$ bis $$2^{n}-1$$. Möchte man negative Zahlen darstellen, muss man somit die negativen
Zahlen mit in einer $$n$$-stelligen Dualzahl darstellen verschiebt sich der Wertebereich. Je nach Darstellungsform
wechselt die Anzahl der darstellbaren Zahlen im positiven und negativen Bereich. Untenstehend ist ein Zahlenkreis mit
einem Beispiel einer möglichen Darstellungsform zu sehen.

## Einerkomplement

Das Einerkomplement benutzt das Most-Significant-Bit (MSB) als Indikator dafür, ob eine Zahl positiv oder negativ ist.
Um eine negative Zahl zu errechnen, muss die positive Zahl im Dualsystem invertiert werden. Aufgrund dieser
Darstellungsform gibt es zwei Repräsentationsformen, weshalb es ungeeignet ist für die Benutzung im Computer und für
mathematisch korrekte Berechnungen.

| Dualzahl mit MSB+ | Dezimalwert | Dualzahl mit MSB- | Dezimalwert |
| ----------------- | ----------- | ----------------- | ----------- |
| $$0000$           | $$+0$$      | $$1111$$          | $$-0$$      |
| $$0001$           | $$+1$$      | $$1110$$          | $$-1$$      |
| $$0010$           | $$+2$$      | $$1101$$          | $$-2$$      |
| $$0011$           | $$+3$$      | $$1100$$          | $$-3$$      |
| $$0100$           | $$+4$$      | $$1011$$          | $$-4$$      |
| $$0101$           | $$+5$$      | $$1010$$          | $$-5$$      |
| $$0110$           | $$+6$$      | $$1001$$          | $$-6$$      |
| $$0111$           | $$+7$$      | $$1000$$          | $$-7$$      |

## Zweierkomplement

Das Zweierkomplement entsteht aus dem Einerkomplement und benutzt somit ebenfalls das Einerkomplement. Um zum
Zweierkomplement zu gelangen muss jedoch die Zahl $$1$$ zur negativen Zahl hinzuaddiert werden. Hiermit wird das Problem
der doppelten Null gelöst und es ist im negativen Wertebereich eine Zahl mehr möglich zu speichern.

| Dualzahl mit MSB+ | Dezimalwert | Rechnung   | Dualzahl mit MSB- | Dezimalwert |
| ----------------- | ----------- | ---------- | ----------------- | ----------- |
| $$0000$$          | $$+0$$      | $$1111+1$$ | $$0000$$          | $$+0$$      |
| $$0001$$          | $$+1$$      | $$1110+1$$ | $$1111$$          | $$-1$$      |
| $$0010$$          | $$+2$$      | $$1101+1$$ | $$1110$$          | $$-2$$      |
| $$0011$$          | $$+3$$      | $$1100+1$$ | $$1101$$          | $$-3$$      |
| $$0100$$          | $$+4$$      | $$1011+1$$ | $$1100$$          | $$-4$$      |
| $$0101$$          | $$+5$$      | $$1010+1$$ | $$1011$$          | $$-5$$      |
| $$0110$$          | $$+6$$      | $$1001+1$$ | $$1010$$          | $$-6$$      |
| $$0111$$          | $$+7$$      | $$1000+1$$ | $$1001$$          | $$-7$$      |
|                   |             | $$1111+1$$ | $$1000$$          | $$-8$$      |

## Subtraktion mittels Komplementbildung

## Vorteile der Komplementbildung

$$
15_{10}+1_{10}=16_{10} \\
1111_{2}+0001_{2}=10000_{2} \rightarrow \text{Falsch!}
-1_{10}+1_{10}=0{10}
$$
