---
title:  "99. Anschlüsse"
date:   2019-04-14 06:00:00 +0100
---

Alle Informationen hier basieren auf den zur Recherche Zeit verfügbaren Artikel in Wikipedia.

## USB

Abkürzung für Universal Serial Bus. Physikalisch besteht eine Punkt-zu-Punkt-Verbindung, welche logisch von einem
Host-Controller (bzw. Master) kontrolliert wird und bis zu 127 Peripherie-Geräte (Slave-Clients) verwalten kann. Soll
mehr als ein Gerät an einen Master angeschlossen werden wird ein Hub benötigt, hierdurch entstehen Baumstrukturen mit
dem Master als Wurzel. Alle USB Standards sind Abwärtskompatibel zu ihren Vorgängern. Der USB Standard ermöglicht eine
Spannungsversorgung der Slave-Geräte, je nach Standard gibt es geringe Abweichungen. Möchte man Geräte mit einem höheren
Bedarf an Spannung versorgen, so muss man ein USB-PD fähiges Gerät und Kabel benutzen.

### USB 1.0

Von einem Konsortium aus den Unternehmen Compaq, DEC, Intel, IBM, Microsoft, NEC und Nortel entwickelt und 1996
eingeführt. Damalige Transferrate von 12 Mbit/s und eine großflächige Unterstützung von Geräten (Maus, Tastatur,
Drucker, etc.). Durch die mangelnde Verbreitung auf Mainboards und fehlende Unterstützung von Betriebssystemen fand die
Schnittstelle wenig Verbreitung anfangs.

### USB 1.1

1998 behob diese Schnittstelle zahlreiche Fehler des Vorgängers und fügte geringfügige Verbesserungen hinzu.

### USB 2.0

Im Jahr 2000 wurde diese Schnittstellenversion veröffentlicht. Mit diesem Standard wurde auch die Maximalgeschwindigkeit
auf bis zu 480 Mbit/s, hiermit wurde die Benutzung von Festplatten und Videogeräten praktikabel. Ab 2002 fand die
erhöhte Geschwindigkeit auch praktische Anwendung.

### USB 3.0

Seit 2013 wird USB 3.0 auch USB 3.1 Gen 1 genannt, da sich USB 3.1 Gen 1 und Gen 2 nur durch den verwendeten
Leitungscodec unterscheiden. Für USB 3.0 bzw. USB 3.1 Gen 1 ist der Leitungscodec `8b10b`. Die maximale Brutto-Datenrate
beträgt 4 Gbit/s (entspricht 0,5 GB/s).

### USB 3.1

Identisch zu USB 3.0, jedoch ist der Leitungscodec `128b132b` hierdurch werden bis zu 10 Gbit/s bzw. 1,2 GB/s
ermöglicht.

### USB 3.2

Mit USB 3.2 wurden die alten Namen von USB 3.0 und 3.1 wieder erneuert. USB 3.0 wird zu USB 3.2 Gen bzw. SuperSpeed USB
und stellt die Geschwindigkeitsklasse 5 Gbit/s dar. USB 3.1 Gen 2 wird zu USB 3.2 Gen 2 bzw. Super Speed USB 10 Gbps was
die Geschwindigkeitsklasse von 10 Gbit/s darstellt. USB 3.2 Gen 2x2 bzw. SuperSpeed USB 20 Gbps ist die Neuerung und
ermöglicht bei Benutzung von Typ-C Steckern an beiden Enden und einem Typ-C Kabel die höhere Übertragungsrate von
20 Gbit/s. Sonst unterscheiden sich die USB 3.x Standards technisch nicht voneinander.

## Thunderbolt

Thunderbolt entstand aus einer Kooperation von Intel und Apple. Als Übertragungsmedium wird im Moment Kupfer verwendet,
es ist jedoch aufgrund der Struktur auch möglich optische Kabel zu verwendet. Intel beabsichtigt mit Thunderbolt eine
universelle Schnittstelle anzubieten, welche sich großflächig durchsetzt. In den Steckern der Kabel sind aktive
Komponenten verbaut, welche die Erhitzung und das klobige Design erklärt.

### Thunderbolt 1

Wurde intern auf der IDF 2009 und im Februar 2011 offiziell vorgestellt. Physikalisch und elektrisch ist der Standard
kompatibel mit Displayport. Die Übertragungsgeschwindigkeit beträgt 10 Gbit/s.

### Thunderbolt 2

Juni 2013 wurde die zweite Version der Schnittstelle vorgestellt, hiermit wurde eine Übertragungsrate von 20 Gbit/s
ermöglicht. Dies wurde ermöglicht durch die Zusammenlegung der bis dato getrennten Display und Datenkanäle. Hierdurch
ist die Datenrate flexibler einsetzbar. Physikalisch und elektrisch ist der Standard kompatibel mit Displayport.

### Thunderbolt 3

Mitte 2015 stellte das Konsortium mit dem Typ-C Stecker die dritte Version vor. Die Übertragungsgeschwindigkeit wurde auf
40 Gbit/s erhöht.

## Lightning

## FireWire

### FireWire 400

### FireWire 800

## SATA

### SATA 3G

### SATA 6G

### eSATA 6G

## M.2

## PCIe 64-bit Slot

## SAS 3

## DisplayPort 1.4

## HDMI 2.1

## Tabellarische Zusammenfassung

| Name             | Datenübertragungsart | max. Bandbreite | max. Buslänge | max. Anzahl Geräte | Energieversorgung | Markteinführung |
| ---------------- | -------------------- | --------------- | ------------- | ------------------ | ----------------- | --------------- |
| USB 1.x          | seriell              | 12 MBit/s       | 5m            | 127                | 0,1A 5W           | 1996            |
| USB 2.0          | seriell              | 480 MBit/s      | 5m            | 127                | 500mA 5V          | 2000            |
| USB 3.0          | seriell              | 5 GBit/s        | 5m            | 127                | 900mA             | 2014            |
| USB 3.1          | seriell              | 10 GBit/s       | 5m            | 127                | 900mA             | 2014            |
| USB 3.2          | seriell              | 20 GBit/s       | 1m            | 127                | 5-2V 5A 100W      | 2017            |
| Thunderbolt 1    | seriell              | 10 GBit/s       | 30m           | 6                  | 550mA 10W         | 2009/10         |
| Thunderbolt 2    | seriell              | 2x 10GBit/s     | 190m          | 6                  | 550mA 10W         | 2013            |
| Thunderbolt 3    | seriell              | 40 GBit/s       | 2m            | 6                  | 100W              | 2015            |
| Lightning        |                      | 5 GBit/s        | 2m            | 1                  |                   | 2012            |
| FireWire 400     |                      | 400 MBit/s      | 4,5m          | 63                 | 1,5A 8-35W        | 1995/2001       |
| FireWire 800     |                      | 800 MBit/s      | 4,5m          | 1                  | 7W                | 2007            |
| SATA 3G          |                      | 3 GBit/s        | 1m            | 16                 |                   | 2005            |
| SATA 6G          |                      | 6 GBit/s        | 1m            | 16                 |                   | 2007            |
| eSATA 6G         |                      | 6 GBit/s        | 2m            | 1                  |                   | 2008            |
| M.2              |                      | 15,7 GByte/s    |               | 1                  |                   | 2012            |
| PCIe 64-Bit Slot |                      | 53,015 GByte/s  |               | 1                  | 75W               | 2019            |
| SAS 3            |                      | 12 GBit/s       | 6m            | 16284/128          |                   | 2013            |
| DisplayPort 1.4  |                      | 62 GBit/s       | 2m            | 1                  |                   | 2016            |
| HDMI 2.1         |                      | 48 GB/s         | 10m           | 1                  |                   | 2017            |

