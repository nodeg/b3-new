---
title: "02. Speichersysteme"
date: 2020-11-15 08:30:00 +0200
---

## DAS / NAS / SAN

Computersysteme wie zum Beispiel PCs benötigen Speicherplatz, um Daten ablegen zu können. Neben der eigenen Festplatte
bieten spezielle Speichersysteme zusätzlichen Speicher. Kapazität, Zugriffsgeschwindigkeit, Kompatibilität
(Schnittstellen, Anbindung) und Verfügbarkeit sind bei der Auswahl besonders zu beachten. Drei grundsätzliche Konzepte
können unterschieden werden: DAS/NAS/SAN. Jedes Konzept bietet spezifische Eigenschaften und einen eigenen Einsatzbereich.

A NAS (Network-attached storage) unit is a computer connected to a network that provides only file-based data storage
services to other devices on the network. Although it may technically be possible to run other software on a NAS unit,
it is not designed to be a general purpose server. For example, NAS units usually do not have a keyboard or display, and
are controlled and configured over the network, often using a browser.

A full-featured operating system is not needed on a NAS device, so often a stripped-down operating system is used. For
example, FreeNAS, an open source NAS solution designed for commodity PC hardware, is implemented as a stripped-down
version of FreeBSD.

NAS systems contain one or more hard disks, often arranged into logical, redundant storage containers or RAID arrays.
NAS uses file-based protocols such as NFS (popular on UNIX systems), SMB/CIFS (Server Message Block/Common Internet File
System) (used with MS Windows systems), or AFP (used with Apple Macintosh computers). NAS units rarely limit clients to
a single protocol.

The key difference between direct-attached storage (DAS) and NAS is that DAS is simply an extension to an existing
server and is not necessarily networked. Direct-attached storage (DAS) refers to a digital storage system directly
attached to a server or workstation, without a storage network in between. It is a retronym, mainly used to
differentiate non-networked storage from SAN and NAS. A DAS can, like SAN or NAS, enable storage capacity extension,
while keeping high data bandwidth and access rate.

NAS is designed as an easy and self-contained solution for sharing files over the network.

Both DAS and NAS can potentially increase availability of data by using RAID or clustering.

When both are served over the network, NAS could have better performance than DAS, because the NAS device can be tuned
precisely for file serving which is less likely to happen on a server responsible for other processing. Both NAS and DAS
can have various amount of cache memory, which greatly affects performance. When comparing use of NAS with use of local
(non-networked) DAS, the performance of NAS depends mainly on the speed of and congestion on the network.

NAS provides both storage and a file system. This is often contrasted with SAN (Storage Area Network), which provides
only block-based storage and leaves file system concerns on the "client" side. SAN protocols are SCSI, Fibre Channel,
iSCSI, ATA over Ethernet (AoE), or HyperSCSI.

One way to loosely conceptualize the difference between a NAS and a SAN is that NAS appears to the client OS (operating
system) as a file server (the client can map network drives to shares on that server) whereas a disk available through a
SAN still appears to the client OS as a disk, visible in disk and volume management utilities (along with client's local
disks), and available to be formatted with a file system and mounted.

A storage area network (SAN) is a dedicated network that provides access to consolidated, block level data storage. SANs
are primarily used to make storage devices, such as disk arrays, tape libraries, and optical jukeboxes, accessible to
servers so that the devices appear like locally attached devices to the operating system. A SAN typically has its own
network of storage devices that are generally not accessible through the local area network by other devices. The cost
and complexity of SANs dropped in the early 2000s to levels allowing wider adoption across both enterprise and small to
medium sized business environments.

Quelle: www.wikipedia.org (aus div. Artikeln)

Weitere Informationen:

- <https://lernplattform.mebis.bayern.de/pluginfile.php/31896522/mod_page/content/13/ebook_Storage_Mittelstand_TKrenn.pdf>

## Überblick RAID-Level

RAID steht für Redundant Array of Independent Disks, zu Deutsch redundante Anordnung unabhängiger Festplatten. Bei einem
RAID-System handelt es sich um einen Verbund mehrerer Festplatten, in dem Daten so gespeichert werden (außer RAID 0),
dass sie vor Verlust geschützt sind.

Vor- und Nachteile der wichtigsten RAID-Systeme, damit Sie das Passende wählen können:

- JBOD (ab zwei Festplatten): In diesem Verbund werden mehrere Festplatten zu einem großen Laufwerk kombiniert. Geht
  eine kaputt, sind alle Disks betroffen. Aus diesem Grund ist JBOD nicht zu empfehlen (keine Fehlertoleranz).
- RAID 0 (ab zwei Festplatten): In diesem RAID-Modus werden ebenfalls mehrere Harddisks verbunden, wobei allerdings die
  Geschwindigkeit im Vordergrund steht. Speichern Sie eine Datei, wird diese auf die verschiedenen Festplatten verteilt.
  Dadurch können die Harddisks parallel arbeiten und schneller auf die Daten zugreifen. Während ein RAID-0-System im
  Computer sinnvoll sein kann, nutzt es in einem NAS nur wenig: Das Netzwerk bremst den Tempogewinn. Auch bei einem
  RAID-0-System sind alle Daten verloren, wenn eine Festplatte des Verbunds ausfällt (keine Fehlertoleranz).
- RAID 1 (ab zwei Festplatten): Bei RAID 1 steht die Datensicherheit im Vordergrund. Alle Daten werden doppelt
  gespeichert, sodass auf zwei Festplatten jeweils dieselben Daten liegen. Dadurch steht Ihnen zwar nur die Hälfte des
  tatsächlichen Speicherplatzes zur Verfügung, dafür sind Ihre Daten geschützt. Fällt eine der Festplatten aus, liest
  das NAS automatisch von der anderen Festplatte. In der Zwischenzeit kann die defekte Festplatte ausgetauscht werden.
  Trotz dieser Sicherheit schützt ein RAID Ihre Daten nur beschränkt. Lesen Sie dazu unbedingt den Tipp "RAID ersetzt
  kein Backup".
- RAID 5 (ab drei Festplatten): In einem RAID-5-Verbund sind die Daten ebenfalls vor dem Ausfall einer Festplatte
  geschützt – allerdings verlieren Sie weniger Speicherplatz, dafür etwas mehr Geschwindigkeit als bei RAID 1. Während
  bei Ersterem nur die Hälfte des Speicherplatzes genutzt wird, ist es bei RAID 5 die Anzahl der Festplatten minus
  einer. Nutzen Sie zum Beispiel vier 500-GB-Harddisks, können Sie 1500 GB als Speicher brauchen. Der Rest ist für die
  Datensicherung reserviert. Welche Festplatte ausfällt, ist dabei egal. Geben jedoch zwei Festplatten gleichzeitig den
  Geist auf, sind die Daten verloren. Defekte Harddisks sollten also sofort ersetzt werden. Dies ist sowieso notwendig,
  da der Verbund bei einem Ausfall zwar noch zuverlässig, aber langsam arbeitet. Für ein NAS mit drei Festplatten oder
  mehr ist RAID 5 oft die beste Lösung.
- RAID 6 (ab vier Festplatten): Bei RAID 6 werden zwei Festplatten zur Sicherung verwendet, Sie verlieren also gleich
  viel Platz wie bei RAID 5+Spare. RAID 6 ist durch die höhere Komplexität etwas langsamer, dafür wird auch der
  gleichzeitige Ausfall von zwei Harddisks verkraftet.
- RAID 10 (ab vier Festplatten): Ein RAID-10-Verbund ist ein RAID 0 über mehrere RAID 1. Es werden dabei die
  Eigenschaften der beiden RAIDs kombiniert: Sicherheit und gesteigerte Schreib-/Lesegeschwindigkeit.

Die Kombinationen setzten sich aus einzelnen RAID-Leveln zusammen. Somit können die Vorteile einzelner RAIDs verbunden
werden. RAID-10 setzt sich aus Level 1 und 0 zusammen und spiegelt zuerst die Daten, bevor sie dann auf einen weiteren
Plattenverbund verteilt werden.

RAID-51 beschreibt somit eine blockweise Verteilung aller Daten auf den Platten und die zusätzliche Spiegelung auf einem
weiteren Laufwerksverbund.

> Hinweis: RAID ersetzt kein Backup!

## RAID - Speicherberechnung

Die Fehlertoleranz der RAID-Systeme wird durch zusätzlich verwendeten Speicher erkauft - Daten werden redundant
gespeichert. Diese Redundanz ergibt durch das Kopieren von Daten (RAID 1) oder das Bilden und Speichern sogenannter
Paritätsinformationen (RAID 5 u.a.). Aus diesen Verfahren ergibt sich die geringere nutzbare Speichergröße in einem
vorhandenen Festplattenverbund.

In einem Verbund ist es stets die kleinste Festplatte, die den verfügbaren Gesamtspeicherplatz limitiert.

Beispiel: RAID 5 mit 4 Festplatten: 2x3TiB, 2x2TiB $$\rightarrow$$ In diesem System sind nur 6 TiB von insgesamt 10 TiB
nutzbar. 2 TiB werden als Paritätsinformation benötigt, 2x1 TiB sind durch die Mischbestückung nicht verwendbar.

Der RAID-Rechner von Synology bietet eine anschauliche Kalkulationsmöglichkeit (siehe unten).

> Praxistipp: Verwenden Sie bei der Einrichtung eines RAID möglichst gleiche Festplatten (Größe, Geschwindigkeit, Alter)!

## Fehlerkorrektur mit Parity-Information

Zur Bildung von fehlertoleranter Systeme werden bei einigen RAID-Leveln Paritätsinformationen gespeichert. Mit Hilfe von
Parity-Informationen können bei Ausfall eines Datenträgers verlorengegangene Daten wiederhergestellt werden.

Funktion:

Betrachtet werden die Daten eines Stripesets über das Festplatten-Array. Bspw. aus den Bits der Festplatten #1, #2 und
\#3 wird das Paritätsbit für die Festplatte #4 berechnet. Für diese Berechnung wird eine XOR-Verknüpfung genutzt.

Während beim RAID-Level 3 diese Paritätsinformationen immer auf die gleich Festplatte (z.B. #4) geschrieben werden,
wechselt beim RAID-Level 5 der Speicherort dieses Paritätsbits von Platte zu Platte.

Weitere Informationen:

- <https://www.thomas-krenn.com/de/wiki/RAID>
- <https://www.tecchannel.de/a/raid-im-ueberblick-grundlagen-raid-0-bis-7,401665>
- <https://www.synology.com/de-de/support/RAID_calculator>
