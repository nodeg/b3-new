---
title:  "06. Halbleiterspeicher"
date:   2019-04-05 07:45:00 +0100
---

## Allgemeines

Halbleiterspeicher sind elektronische Schaltungen, die auf einem wenige Millimeter großen Plättchen aus
Halbleiter-Material aufgebracht werden. Die Kombination aus beidem wird Chip genannt.
Ein solcher Chip (engl. "Die") ist zum Schutz und zur einfacheren Kontaktierung oft in einem vielfach größeren
Kunststoffgehäuse eingekapselt.

Diese Speicherbausteine sind ein wesentlicher Bestandteil eines Rechnersystems. Dabei unterscheidet man zwischen zwei
Arten von Halbleiterspeichern. Es wird unterschieden in Festwert- und Schreib-Lese-Speicher.

## Festwertspeicher

Festwertspeicher finden zum Beispiel im BIOS- oder UEFI-Chips Einsatz. Dabei sind zwei zentrale Eigenschaften von
Bedeutung.

- nicht flüchtiger (non volatile bzw. NIV) Programmspeicher: Der Programmspeicher des BIOS muss ohne Stromzufuhr auch
  über einen längeren Zeitraum sicherstellen, dass alle BIOS Informationen konsistent erhalten bleiben.
- Read-Only Eigenschaften: Das BIOS/UEFI darf unter keinen Umständen verändert werden, da sonst die einwandfreie
  Funktion des Systems nicht sichergestellt werden kann. Deswegen kann das Programm nur gelesen und nicht ohne weiteres
  beschrieben werden.

## Schreib-Lese-Speicher

![ITS - Halbleiterspeicher - 1](/assets/img/ITS/06-Halbleiterspeicher-1.jpg)

Die elektronischen Schaltungen eines Halbleiterspeichers (Speicherbaustein), bestehen aus einer Vielzahl von
Speicherzellen. Um die bei Computern und Microcontrollern erforderliche Speichergröße zu erreichen, werden mehrere DRAM
zu einem Speichermodul zusammengefügt.

### Aufbau

![ITS - Halbleiterspeicher - 2](/assets/img/ITS/06-Halbleiterspeicher-2.jpg)

### Bauform

![ITS - Halbleiterspeicher - 3](/assets/img/ITS/06-Halbleiterspeicher-3.jpg)

### Datenübertragungsrate

Die Datenübertragungsrate wird auch als Übertragungsgeschwindigkeit, Datenrate und Datentransferrate, Bandbreite
bezeichnet. Dabei entspricht sie der Anzahl der Informationseinheiten, die in einer bestimmten Zeit aus dem Speicher
gelesen werden können.

Angabe in:

- Byte pro Sekunden (B/s)
- Kilobyte pro Sekunde (kB/s)
- Megabyte pro Sekunde (MB/s)
- Gigabyte pro Sekunde (GB/s)

Die Datenrate hängt vom verwendeten Halbleiterspeicher und dem Speicherbussystem ab.

Diese Datentransferrate von Speicherbausteinen ergibt sich durch die Multiplikation aus deer Taktfreqenz des Speicherbus
und der übertragenden Wortlänge. Demnach hat ein Speicherchip mit einer externen Taktrate von 133 MHz und einer
Wortlänge von 8 Byte eine Bandbreite von 1,05 GB/s.

![ITS - Halbleiterspeicher - 4](/assets/img/ITS/06-Halbleiterspeicher-4.jpg)

### Speicherkapazität

$$A$$ = Anzahl der Adressleitungen, die Prozessor und RAM verbinden.
$$n$$ = Anzahl der Adressen, die die Adressleitungen kodieren können.
$$d$$ = Länge des Datenworts (feste Anzahl Bits), dass sich hinter jeder Adresse verbirgt.

Speicherkapazität = $$d*n = d*2^A$$

Beispiel: Speicherkapazität = $$32bit * 2^{32} = 16 GB$$

### Zugriffszeit

Je niedriger die Timings, umso besser die Leistungsfähigkeit des RAMs.

Latenz (CL): CL16
RAS to CAS Delay (tRCD): 18
Ras Precharge Time (tRP): 18
Row Active Time (tRAS): 36

Aber: Die Taktfrequenz ist genauso wichtig

Zeitunterschied: RAM mit höheren Timings schneller mit Bearbeitung fertig

![ITS - Halbleiterspeicher - 5](/assets/img/ITS/06-Halbleiterspeicher-5.jpg)
