---
title: "06. Backups"
date: 2021-01-25 07:45:00 +0200
---

## Backupverfahren

Hinter auf den ersten Blick verwirrenden Bezeichnungen wie inkrementelle Sicherung, differentielles Backup,
Rotationsschema oder Großvater-Vater-Sohn-Prinzip verbergen sich bei genauerem Hinsehen recht einfach nachzuvollziehende
Überlegungen.

Alle Backup-Strategien bauen auf drei grundlegenden Verfahren zur Datensicherung auf. Dabei handelt es sich um die

- Vollsicherung sowie das
- differentielle Backup und das
- inkrementelle Backup.

Die beiden letzteren Methoden nutzen zur Reduzierung der zu sichernden Datenmenge – und damit zur Beschleunigung des
Backups – die Tatsache aus, dass das Betriebssystem neu erstellte oder veränderte Dateien mit einem Archivbit
kennzeichnet.<br>
Das vollständige Backup überträgt unabhängig vom Zustand des Archivbits sämtliche Daten von der Festplatte des zu
sichernden Systems auf Band. Dabei setzt die Backupsoftware das Archivbit zurück und markiert die Dateien auf diese
Weise als gesichert. Das differentielle Backup sichert alle Files, die seit dem letzten Vollbackup erstellt oder
modifiziert wurden. Dazu sucht es alle Dateien mit gesetztem Archivbit und schreibt sie auf Band. Ältere Varianten des
Files werden dabei von der neuen Version überschrieben. Das Archivbit bleibt gesetzt, so dass die Dateien beim nächsten
differentiellen Backup erneut mitgesichert werden. Dies erzeugt zwar eine gewisse Datenredundanz, hat jedoch den
Vorteil, dass die Wiederherstellung verlorener Dateien lediglich die Tapes mit der letzten Vollsicherung sowie dem
letzten differentiellen Backup erfordert.<br>
Das inkrementelle Backup (Zuwachssicherung) speichert alle Dateien, die seit dem letzten Backuplauf, gleich ob
vollständig oder nicht, erstellt oder verändert wurden. Dazu schreibt es wie das differentielle Backup alle Files mit
gesetztem Archivbit auf Band, setzt anschließend aber das Archivbit zurück. Zwar bleiben alle Dateivarianten erhalten,
und die zu sichernde Datenmenge reduziert sich gegenüber dem differentiellen Backup nochmals. Jedoch erfordert die
Wiederherstellung der Daten das Einspielen des letzten Vollbackups plus aller seitdem erfolgten Zuwachssicherungen. Das
bedeutet gegenüber dem Restore per differentiellen Backup meist einen deutlich gesteigerten Arbeits- und Zeitaufwand.

### Rotation

Durch die Kombination von Vollsicherungen mit dazwischenliegenden differentiellen oder inkrementellen Sicherungen lässt
sich schon mit wenigen Bändern der Datenzustand eines beträchtlichen Zeitraums abdecken. Dazu erstellen Sie einen Satz
von Bändern, die Sie im Wechsel nach einem vorgeplanten Rotationsverfahren zur Sicherung benutzen.<br>
Das verbreitetste Rotationsschema ist die dreistufige Großvater-Vater-Sohn-Methode.

![Rotationssicherung im 5-Tages-Rhythmus](/assets/img/ITS/03-06-backups-novastor_backup_todisk_totape.png)

Grafik: Beispiel - Rotationssicherung im 5-Tages-Rhythmus; Q: Backup & Restore Strategien für Unternehmen; novastor

Eine weitere Sicherungstechnologie ist die der Snapshots. Sollen beispielsweise virtuelle Maschinen gesichert werden,
kann eine geeignete Snapshotlösungen eine VM zu einem bestimmten Zeitpunkt sichern und bei Bedarf wieder herstellen.

## Backupstrategien und Medien

Ein weiterer Teil einer Backup-Strategie ist die Planung der Kopien und die Prozesse der Wiederherstellung.
Hier gilt es, folgende Fragen zu klären:

- Welche Daten sollen gesichert werden?
- Wie groß ist die zu sichernde Datenmenge?
- Schnell und einfach - was steht im Vordergrund: Sicherung oder Wiederherstellung?
- Wo werden welche Sicherungsmedien aufbewahrt?
- Welche Backup-Medien sollen eingesetzt werden?
- Skalierbarkeit: Kann das System erweitert werden?

Ein wichtiger Aspekt der Backupstrategie ist das Definieren der zu sichernden Daten. Diese müssen systematisch erfasst
werden. Handelt es sich um Systemdateien, Nutz-/Anwendungsdaten, Metadaten etc.

Es gilt zu bestimmen, welche Daten sich in welchen Zeitabschnitten ändern und wie häufig die aktuelle Version gesichert
werden muss. Sind diese Daten möglicherweise geöffnet oder befinden sich im Zugriff?<br>
Wichtig ist außerdem die Klärung, welche Daten in welchen Zeiträumen wiederherstellbar sein müssen. Hier unterscheiden
sich die Backupverfahren als auch die Medien. Für einen sehr schnellen Zugriff auf Sicherungen ist die Festplatte die
erste Wahl, deutlich langsamer sind Bandsysteme. Die Datenwiederherstellungszeit aus der Cloud ist in erster Linie von
der Internet-Anschlussbandbreite abhängig. Sie ist in den meisten Fällen daher langsamer als lokale Lösungen.

Das klassische Speichermedium ist das Magnetband. Die Magnetbandtechnologie hat sich in den vergangenen Jahrzehnten
weiterententwickelt. Ein LTO-8-Band bietet eine Speicherkapazität von bis zu 12 TB (bis 30 TB komprimiert) bei einer
Datentransferrate von über 300 MB/s. Magnetbänder sind im Vergleich zu Festplatten relativ unempfindlich und gut
transportierbar.

Für schnelle Backups werden häufig Festplattenspeicher verwendet. Hier gilt es Maßnahmen gegen den Datenverlust durch
Festplattenausfall zu berücksichtigen.

Speichersysteme können auch unterschiedliche Medien in den Sicherungsprozess einbinden. Hybride Backupstrategien
integrieren verschiedene Medien und Prozesse. Die Fa. IBM bietet zum Beispiel mit FLAPE einen Kombinationsspeicher aus
Flash - und Magnetbandtechnik. Der Sicherungsprozess könnte so in Phasen aufgeteilt werden:

Optische Speicher wie CD und DVD oder USB-Speicher spielen evtl. im Privatbereich noch eine Rolle. Im Geschäftsumfeld
erfüllen diese Systeme nicht mehr die Anforderungen.

### 3 - 2 - 1: Auf Nummer sicher!

Eine klassische Maxime für Backups ist die sogenannte 3-2-1-Regel: Bewahren Sie

- drei Kopien Ihrer Daten auf
- zwei verschiedenen Medientypen auf und
- eine Kopie sollte zudem an einem separaten (externen) Standort gespeichert werden.

Typische Medientypen waren früher insbesondere Bandgeräte (bei Unternehmen), optische Datenträger (bei Privatanwendern)
und natürlich Festplatten. In Unternehmen werden Bandgeräte zunehmend durch die Cloud ersetzt oder ergänzt.
Cloud-Speicher werden auch deswegen immer relevanter, weil sie sowohl die Forderung nach einem anderen Medientypen wie
auch nach einem separaten Speicherort erfüllen. Die Kosten für die Cloud sind bei kleinen Datenmengen insbesondere dann
niedrig, wenn Sie sich durch die Speicherung in der Cloud auch die Kosten für eine zusätzliche lokale Kopie sparen. Bei
kleinen Datenmengen verlangen Cloud-Backups keine großen Investitionen.<br>
Übergibt man die Daten einem Cloud-Dienstleister, sind Fragen des Datenschutzes zu beachten! Der Auftraggeber ist
verantwortlich für die Einhaltung des Datenschutzes und muss einen geeigneten Auftragnehmer für die Speicherung und
Verarbeitung der Daten auswählen. Ergänzende Hinweise finden Sie hier.

![Backup&Restore Strategien für Unternehmen](/assets/img/ITS/03-06-backups-datenaufbewahrung.png)

(Q: nach Backup Für Dummies, Acronis Sonderedition; Gafik: novastor: Backup&Restore Strategien für Unternehmen)

## Archivierung


## Weiterfürhende Links

- <https://www.cio.de/a/aktuelle-datensicherungsmethoden-im-ueberblick,3564528>
- <https://www.rowa.ch/vor-und-nachteile-der-datensicherung-auf-ein-nas/>
- <https://ebblogs.de/how-to/dauerhafte-datensicherung-geeignete-speicher/>
