---
title: "05. Kryptografie"
date: 2020-12-15 14:45:00 +0200
---

## Public-Key-Infrastrukturen

Wahrung von Authentizität und Integrität bei der Datenübertragung

Zur Gewährleistung der sicheren Übertragung von Daten in offenen Netzen muss sich ein Empfänger davon überzeugen können,
dass der Absender derjenige ist, für den er sich ausgibt (Authentizität). Weiterhin muss er feststellen können, ob die
empfangene Datei auf dem Weg zu ihm verändert wurde (Integrität). Schließlich möchte sich der Empfänger gegebenenfalls
darauf verlassen können, dass der Inhalt der Datei rechtlich verbindlich ist (Rechtsverbindlichkeit). Typischerweise
findet in diesem Zusammenhang die sog. digitale Signatur Verwendung.

Für die Authentisierung des Absenders werden heute elektronische Verschlüsselungsverfahren verwendet. Der Quasi-Standard
für die Authentisierung sind die als Public-Key-Verfahren bezeichneten Methoden, die auf der Idee beruhen, dass ein
Absender einem Empfänger eine elektronische Nachricht übermittelt, ohne dass vorher ein geheimer Schlüssel vereinbart
wurde. Dazu besitzt jeder Nutzer ein sich ergänzendes elektronisches Schlüsselpaar, das aus einem geheimen privaten und
einem öffentlich zugänglichen Schlüssel besteht. Letzterer wird in einer Datenbank für jedermann zugänglich
bereitgehalten. Der private Schlüssel ist ausschließlich beim Absender sicher verwahrt, beispielsweise auf einer
Chipkarte, sog. Smart Cards.

Der Absender einer Nachricht erzeugt nun mit seinem geheimen Schlüssel eine elektronische Signatur als individuelle
digitale Unterschrift. Hierzu bildet er unter Verwendung eines mathematischen Algorithmus eine komprimierte Form der zu
verschickenden Datei, den sogenannten Hashwert. Der Hashwert der Datei wird nun mit dem privaten Schlüssel chiffriert.
Dadurch wird die Datei - nun komprimiert - zur Vermeidung späterer Änderungen versiegelt. Die Datei und die
korrespondierende Signatur mit dem Hashwert der Datei werden anschließend dem Empfänger übersandt.

Der Empfänger wiederum erzeugt mittels des gleichen Algorithmus ebenfalls den Hashwert der ihm übermittelten Datei und
vergleicht das von ihm erzeugte Ergebnis mit dem vom Absender signierten Wert. Hierzu entschlüsselt er den ihm
übermittelten Hashwert mit dem öffentlichen Schlüssel des Absenders. Eine auch nur geringfügige Veränderung der Datei
auf dem Weg zum Empfänger hätte eine Änderung des Hashwertes bewirkt. Stimmen jedoch beide Varianten der Hashwerte - der
übermittelte und der selbst erzeugte - überein, so steht fest, dass die verschickte Datei nicht verändert wurde.

Zum anderen ist die Urheberschaft des Absenders gewährleistet. Nur der authentische Absender mit seinem geheimen
Schlüssel als Gegenstück zum öffentlichen Schlüssel konnte die digitale Signatur ursprünglich erzeugt haben.
Anderenfalls wäre eine Überprüfung mit seinem öffentlichen Schlüssel nicht möglich gewesen.

In der PKI sind die Zuordnung der Schlüssel zu einer Person mit Hilfe von Zertifikaten, die Verwaltung der Zertifikate,
die technischen Voraussetzungen zur Erzeugung, Überprüfung und Speicherung der Schlüssel und weitere Verfahrensabläufe
zu organisieren. Ein wesentlicher Bestandteil einer modernen PKI ist hierbei das Trustcenter als vertrauenswürdiger
Dritter.

Das beschriebene Verfahren zur Feststellung der Daten-Integrität kann leicht korrumpiert werden. Die übertragene
Nachricht kann von einem Angreifer verändert und eine neue Prüfsumme berechnet werden. Der Empfänger kann diese
Manipulation nicht feststellen. Aus diesem Grunde wird das Verfahren zur Bestimmung der Datenintegrität leicht
verändert. Um eine Manipulation der Prüfsumme zu verhindern, wird die Prüfsumme verschlüsselt übertragen. Hierzu wird
ein asymmetrisches Verfahren benutzt. Die Prüfsumme wird mit dem privaten Schlüssel des Absenders verschlüsselt. Der
Empfänger kann mit dem öffentlichen Schlüssel des Absenders die Prüfsumme entschlüsseln und mit der selbst berechneten
Prüfsumme vergleichen.


## Online-Kommunikation über SSL/TLS

Für eine sicher Datenübertragung von Webdiensten wird das Protokoll HTTP/S verwendet. "S" steht hier für "secure" und
bedeutet, dass HTTP in Kombination mit SSL, Secure Socket Layer (früher) oder TLS, Transport Layer Security (aktuelle
Variante) verwendet wird.

Für eine gesicherte Webkommunikation müssen Webserver und Browser des Clients das Protokoll unterstützen.

Das folgende Video erklärt ein paar grundlegende Funktionsprinzipien:

Mit dem Aufbau der Verbindung legen die Kommunikationspartner die Verwendung von Verfahren zur Verschlüsselung,
Authentifizierung und Sicherstellung der Datenintegrität fest. Welche Verfahren das sind, kann über das Schlosssymbol in
der Browserleiste eingesehen werden.
