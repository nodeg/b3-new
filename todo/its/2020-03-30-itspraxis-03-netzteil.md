---
title:  "03. Das Netzteil"
date:   2019-02-13 06:10:00 +0100
---

## Elektrische Leistung

Formel: $$P = U * I$$

Einheit: Watt \[W\]

## Combined Power

Mehrere Spannungsschienen teilen sich eine gemeinsame Ausgangslast. Der erste Wert eines Koordinatenpunktes bezeichnet
die y-Achse und der zweite Wert die x-Achse.

![ITS-Praxis - Netzeil - Combined Power](/assets/img/ITS-Praxis/03-Netzteil-Combined-Power.jpg)

Quelle: <https://b2c-images.idg.zone/2013332_950x475.jpg>

## Elektrische Arbeit

Formel: $$W_el = P_U * t$$

Einheit: $$[W*s]$$

Umrechnung: $$1W * k * h = 1W * 1000 * 3600s = 3600000Ws$$

## Wirkungsgrad

Formel: $$= n = W_ab / W_zu < 1 = P_ab / P_zu < 1$$

Einheit: Einheitslos; häufig in %

Merksatz: Der Wirkungsgrad beschreibt die Effizienz des Netzteils.
