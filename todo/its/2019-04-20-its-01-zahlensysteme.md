---
title:  "01. Zahlensysteme"
date:   2019-02-13 06:00:00 +0100
---

## Einführung

Was ist ein Zahlensystem? - Ein System für die Zuordnung von Symbolen zu Werten.

Wozu ein Zahlensystem? - Einheitliche Vorschriften bei der Verarbeitung von Ziffern, dies ermöglicht eine identische
Interpretation der Symbole von jedem.

Zahlensysteme werden benötigt, um Missverständnisse während des Umgangs mit ihnen zu vermeiden. Genauso wie eine Sprache
Grammatik hat, so benötigen Zahlen ein formales System, welches ihnen Sinn verleiht. Denn wer hat sonst festgelegt, das
100g das zehnfache von 10g sind. Der geschmackliche Unterschied in einem Gericht ist nämlich definitiv zu merken, wenn
eine Zutat in der zehnfachen Menge vorhanden ist.<br>
Ein anderes Beispiel ist die Zeit. Warum hat eine Minute nicht bspw. 100 Sekunden statt 60? Es wurde sich irgendwann auf
eine Interpretation geeinigt und nun verwendet die Mehrheit diese Repräsentationsform der Zeit.

Was für ein Zahlensystem verwenden wir im Alltag? - Wir verwenden das Dezimalsystem. Das Dezimalsystem soll später in
einem eigenen Abschnitt erklärt werden.

Was für Zahlensysteme gibt es noch? - Als Beispiele wären hier das Oktal-, Sedezimal-/Hexadezimal- und Binärsystem zu
nennen. Grundsätzlich ist ein Zahlensystem aber nur eine Vorschrift wie bestimmte Symbole zu interpretieren sind. Es
gibt somit unendlich viele Systeme! Strichlisten, römisches System, ...

## Dezimalsystem

Das Dezimalsystem besteht aus 10 Zeichen. Jedes der Zeichen wird Ziffer genannt. Wir ordnen es den Stellenwertsystemen
zu.

Zustände: 0, 1, 2, 3, 4, 5, 6, 7, 8, 9

Zustandswerte: 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 $$\rightarrow$$ Basis 10

Stellen mit entsprechenden Stellenwerten

| Stelle     | n.         | ...  | 4.       | 3.       | 2.       | 1.       |
| ---------- | ---------- | ---- | -------- | -------- | -------- | -------- |
| Potenz     | $$10^{n-1}$$ | ...  | $$10^{3}$$ | $$10^{2}$$ | $$10^{1}$$ | $$10^{0}$$ |
| Zahlenwert |            | ...  | 1000     | 100      | 10       | 0        |

Darstellung eines Dezimalsystems im Stellenwertsystem:

````
    9        5         8        2
    4.       3.        2.       1.
9 * 1000 + 5 * 100 + 2 * 10 + 2 * 1 = 9582
````

Informationsgehalt einer vierstelligen Dezimalzahl:

$$
10^4 \widehat{=} 10000 \text{ Möglichkeiten }
\begin{cases}
   0000 \\
   9999
\end{cases}
$$

## Dualsystem

Das Dezimalsystem (lateinisch dualis = zwei enthaltend) besteht aus 2 Zeichen. Jedes Zeichen ist ein Zustand. Auch das
Dualsystem ist ein Stellenwertsystem.

Zustände: 0, 1

Zustandswerte: 1., 2. $$\rightarrow$$ Basis: 2

| Stelle     | n.         | ...  | 4.       | 3.       | 2.       | 1.       |
| ---------- | ---------- | ---- | -------- | -------- | -------- | -------- |
| Potenz     | $$2^{n-1}$$  | ...  | $$2^{3}$$  | $$2^{2}$$  | $$2^{1}$$  | $$2^{0}$$  |
| Zahlenwert |            | ...  | 8        | 4        | 2        | 1        |


$$
\begin{matrix}
1     &   & 0     &   & 1     &   & 1     & \\
4.    &   & 3.    &   & 2.    &   & 1.    & \\
1*2^3 & + & 0*2^2 & + & 1*2^1 & + & 1*2^0 & =11_{(10)}
\end{matrix}
$$

$$
2^4 \widehat{=} 16 \text{ Möglichkeiten }
\begin{cases}
   0000 \\
   1111
\end{cases}
$$

## Umwandlung einer Dezimalzahl in eine Dualzahl

Es gibt mehrere Varianten eine Zahl aus dem Dezimalsystem in eine Dualzahl umzuwandeln. In der Berufsschule verwenden
wir das Rest-Verfahren. Hierbei wird die Zahl immer durch zwei geteilt und der Rest wird aufgeschrieben. Dies geschieht
rekursiv, solange bis die Zahl 0 sich ergeben hat. Anschließend werden von unten nach oben die Reste abgelesen. Im
Folgenden ein Beispiel:

$$
\begin{matrix}
109 & / & 2 & = & 54 & R1 \\
54  & / & 2 & = & 27 & R0 \\
27  & / & 2 & = & 13 & R1 \\
13  & / & 2 & = & 6  & R1 \\
6   & / & 2 & = & 3  & R0 \\
3   & / & 2 & = & 1  & R1 \\
1   & / & 2 & = & 0  & R1
\end{matrix}\uparrow 1101101_{2} = 109_{10}
$$

## Betrachtung einer IP-Konfiguration

IPv4-Adressen sind 32-Bit Dualwerte, die aus Gründen der Übersichtlichkeit im Dezimalsystem angegeben werden. Hierbei
wird jedes Oktett durch einen Punkt getrennt, gleiches gilt für Subnetzmasken.

IPv4-Adresse:

$$
\begin{matrix}
11000000 & . & 10101000 & . & 10110010 & . & 00010100 & _{2} \\
192      & . & 168      & . & 178     & . & 20      & _{10} \\
\end{matrix}
$$

IPv4-Subnetzmaske:

$$
\begin{matrix}
11111111 & . & 11111111 & . & 1111111 & . & 0000000 & _{2} \\
255      & . & 255      & . & 255     & . & 0       & _{10} \\
\end{matrix}
$$

> Nachtrag 2021-04-17: Ist eine IPv4 Adresse in einer IPv6 Adresse enthalten muss der Dualwert (IPv4) in Hexadezimal
> (IPv6) umgerechnet werden.

## Hexadezimalsystem

Das Hexadezimal- (altgriechisch hex = 6, lateinisch deka = 10) oder auch Sedezimalsystem (lateinisch sedecim = 16)
genannte System ist ein Stellenwertsystem, welches auf der Basis 16 arbeitet. Auch hier ist jedes Zeichen ein Zustand.

Zustände: 0, 1, 2, 4, 5, 6, 7, 8, 9, A, B, C, D, E, F

Zustandswerte: 16 $$\rightarrow$$ Basis: 16

| Stelle     | n.         | ...  | 4.       | 3.       | 2.       | 1.       |
| ---------- | ---------- | ---- | -------- | -------- | -------- | -------- |
| Potenz     | $$16^{n-1}$$ | ...  | $$16^{3}$$ | $$16^{2}$$ | $$16^{1}$$ | $$16^{0}$$ |
| Zahlenwert |            | ...  | 4096     | 256      | 16       | 1        |


$$
\begin{matrix}
1      &   & 2      &   & 3      &   & A       & \\
4.     &   & 3.     &   & 2.     &   & 1.      & \\
1*16^3 & + & 2*16^2 & + & 3*16^1 & + & 10*16^0 & =4666_{(10)} \\
4096   & + & 512    & + & 48     & + & 10      & =4666_{(10)} \\
\end{matrix}
$$

$$
16^4 \widehat{=} 35536 \text{ Möglichkeiten }
\begin{cases}
   0000 \\
   FFFF
\end{cases}
$$

## Umwandlung einer Dezimalzahl in eine Hexadezimalzahl

Identisches Vorgehen wie beim Umwandeln einer Dezimalzahl in eine Hexadezimalzahl!

$$
\begin{matrix}
109 & / & 16 & = & 6 & R13 & = D\\
6   & / & 16 & = & 0 & R6  & = 6\\
\end{matrix}\uparrow 6D_{16} = 109_{10}
$$
