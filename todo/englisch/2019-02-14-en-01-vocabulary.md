---
title:  "01. Vocabulary"
date:   2019-02-14 10:30:00 +0100
---

# A

| English   | Synonym | German             |
| --------- | ------- | ------------------ |
| as        | because | weil               |
| to advise |         | raten, vorschlagen |

# B

| English | Synonym | German |
| ------- | ------- | ------ |
|         |         |        |

# C

| English   | Synonym | German     |
| --------- | ------- | ---------- |
| customer  |         | Kunde      |
| complaint |         | Beschwerde |

# D

| English       | Synonym | German      |
| ------------- | ------- | ----------- |
| delivery date |         | Lieferdatum |
| discount      |         | Rabatt      |
| delivery      |         | Lieferung   |

# E

| English   | Synonym      | German     |
| --------- | ------------ | ---------- |
| to effect | to carry out | ausführen  |
| enquiry   |              | Anfrage    |
| execution |              | Ausführung |

# F

| English | Synonym | German |
| ------- | ------- | ------ |
|         |         |        |

# G

| English             | Synonym  | German                  |
| ------------------- | -------- | ----------------------- |
| to grant a discount | to allow | einen Nachlass gewähren |

# H

| English      | Synonym | German      |
| ------------ | ------- | ----------- |
| to hesitate  |         | zögern      |
| to hand over |         | aushändigen |

# I

| English     | Synonym | German   |
| ----------- | ------- | -------- |
| invoice     |         | Rechnung |
| immediately | at once | sofort   |

# J

| English | Synonym | German |
| ------- | ------- | ------ |
|         |         |        |

# K

| English | Synonym | German |
| ------- | ------- | ------ |
|         |         |        |

# L

| English | Synonym | German |
| ------- | ------- | ------ |
|         |         |        |

# M

| English | Synonym | German |
| ------- | ------- | ------ |
|         |         |        |

# N

| English | Synonym | German |
| ------- | ------- | ------ |
|         |         |        |

# O

| English | Synonym | German     |
| ------- | ------- | ---------- |
| only    | merely  | nur        |
| offer   |         | Angebot    |
| order   |         | Bestellung |

# P

| English              | Synonym | German                   |
| -------------------- | ------- | ------------------------ |
| please find enclosed |         | in der Anlage finden Sie |

# Q

| English  | Synonym | German |
| -------- | ------- | ------ |
| quantity |         | Menge  |

# R

| English                 | Synonym | German                 |
| ----------------------- | ------- | ---------------------- |
| request for payment     |         | Zahlungsaufforderung   |
| request for information |         | Bitte um Informationen |
| request for quotation   |         | Bitte um Angebot       |

# S

| English            | Synonym    | German              |
| ------------------ | ---------- | ------------------- |
| supplier           |            | Lieferer            |
| to submit an offer |            | ein Angebot abgeben |
| from stock         | from store | vom Lager           |

# T

| English           | Synonym | German              |
| ----------------- | ------- | ------------------- |
| terms of delivery |         | Lieferbedingungen   |
| terms of payment  |         | Zahlungsbedingungen |

# U

| English | Synonym | German         |
| ------- | ------- | -------------- |
| unit    | item    | Stück, Einheit |

# V

| English | Synonym | German |
| ------- | ------- | ------ |
|         |         |        |

# W

| English    | Synonym | German  |
| ---------- | ------- | ------- |
| well known |         | bekannt |

# X

| English | Synonym | German |
| ------- | ------- | ------ |
|         |         |        |

# Y

| English | Synonym | German |
| ------- | ------- | ------ |
|         |         |        |

# Z

| English | Synonym | German |
| ------- | ------- | ------ |
|         |         |        |
