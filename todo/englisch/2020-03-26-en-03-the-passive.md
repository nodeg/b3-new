---
title:  "03. The Passive Voice"
date:   2019-12-16 10:00:00 +0100
---

## General

- In a sentence in the active voice, the subject is taking action. The sentence is expressing something about the
  subject of the sentence.
- In a sentence in the passive voice, with the subject of the sentence is something done. The sentence is still
  expressing something about the subject of the sentence. The acting part can be added with the so called "by-clause".

## Rules

> to be + past participle

1. The object of the sentence in active voice will be the subject of the sentence in the passive voice.
2. The subject of the sentence in active voice will be the object of the sentence in the passive voice. (by-agent -
   very often missing)

We are only able to transform a sentence in active voice into passive voice if the active voice sentence has an object.

## Examples

| Voice   | Sentence                                              | Tense                  |
|---------|-------------------------------------------------------|------------------------|
| Active  | I write a program.                                    | Simple Present         |
| Passive | A program is written (by me).                         | Simple Present         |
| Active  | I am writing a program.                               | Present Continous      |
| Passive | A program is being written now.                       | Present Continous      |
| Active  | I wrote a program                                     | Simple Past            |
| Passive | A program was written.                                | Simple Past            |
| Active  | I was writing a program when suddenly the phone rang. | Past Continous         |
| Passive | A program was being written.                          | Past Continous         |
| Active  | I've just made coffee. You want some?                 | Present Perfect Simple |
| Passive | Coffee has just been made by me. You want some?       | Present Perfect Simple |
| Active  | I had written a program.                              | Past Perfect           |
| Passive | A program had been written by me.                     | Past Perfect           |
| Active  | I will write program.                                 | Will Future            |
| Passive | A program will be written by me.                      | Will Future            |
| Active  | I may write a program.                                | Modals                 |
| Passive | A program may be written by me.                       | Modals                 |
