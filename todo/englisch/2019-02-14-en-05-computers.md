---
title:  "05. Computers"
date:   2019-02-14 11:15:00 +0100
---

## Types of Computers

### Desktop

The desktop is a personal computer (PC) in a form intended for regular use at a single location. They come in a variety
of types ranging from large vertical tower cases to small form factor models that can be tucked behind an LCD monitor.
Most of these computers have separate screens and keyboards.

### Mainframe

Mainframes are powerful computers used mainly by large organizations for critical applications, typically bulk data
processing such as census, industry and consumer statistics, enterprise resource planning and financial transaction
processing.

### Laptop

Laptops are personal computers designed for mobile use that is small and light enough for one person to rest on their
lap. A laptop integrates most of the typical components of a desktop computer, including a display a keyboard, a
pointing device (a touchpad) and speakers into a single unit. A laptop is powered by electricity via an AC adapter, and
can be used on the go using a rechargeable battery.

### Tablet

Tablets are laptop PCs equipped with a stylus (a small pen-shaped instrument that is used to input commands to a
computer screen, mobile device or graphics tablet) or a touchscreen. This form factor is intended to offer a more
mobile PC; this PCs may be used where notebooks are impractical or unwieldy, or do not provide the needed functionality.

### Netbook

Netbooks (sometimes also called mini notebooks or ultraportables) are a branch of subnotebooks, a rapidly evolving
category of small, lightweight and inexpensive laptop computers suited for general computing and accessing Web-based
applications; they are often marketed as "companion devices", i.e. to augment a user's other computer access.

### Handheld-PC

Handheld-PC is a term for a computer built around a form factor which is smaller than any standard laptop computer. It
is sometimes referred to as a Palmtop.

## Hierarchy of computer parts

- Software: the programs and data of a computer
- Hardware: mechanical and electronic equipment which makes up a computer
- CPU: the brain of the computer
- Peripherals: physical units attached to the computer

{{< mermaid class="text-center">}}
graph TD
    id1[Computer System]
    id2[Software]
    id3[Hardware]
    id4[CPU]
    id5[main memory]
    id6[RAM]
    id7[ROM]
    id8[Peripherals]
    id9[input/output devices]
    id10[Keyboard]
    id11[Mouse]
    id12[Monitor]
    id13[Printer]
    id14[storage devices]
    id15[Optical devices]
    id16[Floppy]
    id17[Hard drives]
    id18[CD]
    id19[DVD]
    id1 --> id2
    id1 --> id3
    id3 --> id8
    id8 --> id9
    id9 --> id10
    id9 --> id11
    id9 --> id12
    id9 --> id13
    id8 --> id14
    id14 --> id15
    id15 --> id18
    id15 --> id19
    id14 --> id16
    id14 --> id17
    id5 --> id6
    id5 --> id7
    id3 --> id5
    id3 --> id4
{{< /mermaid >}}
