---
title:  "04. Project Management & Scrum"
date:   2020-02-14 10:00:00 +0100
---

## Project management

Project management is the practice of initiating, planning, executing, controlling, and closing the work of a team to
achieve specific goals and meet specific success criteria at the specified time. The primary challenge of project
management is to achieve all of the project goals within the given constraints. This information is usually described in
project documentation, created at the beginning of the development process. The primary constraints are scope, time,
quality and budget. The secondary—and more ambitious—challenge is to optimize the allocation of necessary inputs and
apply them to meet pre-defined objectives.

Source: <https://en.wikipedia.org/wiki/Project_management>

## Scrum

### At a glance

![EN - 04 - Scrum at a glance](/assets/img/EN/04-scrum-framework-at-a-glance.jpg)

Source: <https://projectresources.cdt.ca.gov/agile/agile-tools-and-techniques/>

### Product Owner
### Scrum Master
### Development Team
### Requirements & User Stories
### Product Backlog
### Sprint Planning
### Estimation & Capacity-Velocity
### Sprint Goal and Sprint Backlog
### Definition of Done, Increment
### Daily Scrum
### Visualisation
### Sprint Review & Sprint Retrospective
