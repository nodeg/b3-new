---
title:  "04. General Tips"
date:   2019-02-14 11:15:00 +0100
---

# Letters

- "Ms", wenn genauer Status unbekannt!
- Anreden ohne Punkt, also: Ms, Mr, Mrs!
- Kein Komma nach der Anrede!
- Groß anfangen nach einer Anrede!
- Immer im Plural reden, da man für die Firma spricht!