---
title:  "02. Programming"
date:   2019-12-13 11:00:00 +0100
---

## What is programming?

Programming is the (idea/process) of (designing/producing) a (set/lis t) of (protocols/instructions) written in (code/
symbols) to make a (computer/machine) (perform/ent ertain) a (specified/difficult) (problem/task).

## Programming Languages

Unfortunately, computers cannot understand ordinary spoken English or any other natural language. The only language they
can understand directly is called machine code. This consists of the 1 s and Os (binary code) that are processed by the
central processing unit (CPU).

However, machine code as a means of communication is very difficult to write. For this reason, we use symbolic languages
that are easier to understand. Then, by using a special program, these languages can be translated into machine code.
For example, the so-called assembly languages use abbreviations such as ADD, SUB, MPY to represent instructions.

Basic languages, where the program is similar to the machine code version, are  known as low-level languages. In these
languages, each instruction is equivalent to a single machine code instruction, and the program is converted into
machine code by a special program called an assembler. These languages are still quite complex and restricted to
particular machines.

To make the programs easier to write and to overcome the problem of intercommunication between different types of
machines, higher-level languages were designed such as BASIC, COBOL , FORTRAN or PASCAL, C and C++. These are all
problem-oriented rather than machine-oriented. Programs written in one of these languages (known as source programs) are
converted into a lower-level language by means of a compiler (generating the object program). On compilation, each
statement in a high-level language is generally translated into many machine code instructions.

People communicate instructions to the computer in symbolic languages and the easier the communication can be made, the
wider the application of computers will be.

Instructions are written in a programming language, e.g. C++. This process is known as programming.

Compilers translate the original code into an assembly language or machine code so that the CPU can understand it.

Instructions are compiled and  packaged into a program. The software is ready to be run on a computer.

## Program logic

- Sequence: is a series of commands whereby B automatically and necessarily follows A.
- Iteration: repeats a given sequence of commands a s long as given conditions exist or until a given condition is
  fulfilled.
- Selection: executes one of two or more possible instructions in a program depending on whether a given condition has
  been fulfilled or not.

## Error alert!

Run-time errors are errors, which occur while the program is running. An example would be "Segmentation Fault" (Windows
shows blue screen).

They can sometimes cause a program to crash. One way to avoid run-time errors is to provide code to check for the
availability of resources. lf a run-time error occurs, you can use a debugger to help you locate the error.

Syntax errors are mistakes due to faulty grammar, misspellings and false punctuation. An example would be to forget to
set a closed bracket at the end of data you wanted to set in brackets. Translator programs reject lines with these kinds
of mistakes which gives you a chance to repair the damage. Of course, it's better to avoid problems right from the start
by being careful in points of grammar, dotting and punctuation when writing code. Once a mistake has occurred read the
error message carefully to rectify the matter.

Logic errors are mistakes caused by using illogical instructions or a false sequence of instructions. They are
especially tricky because the program can still run but it won't be doing its job properly. An example would be to write
instructions that depend for their execution on conditions that, based on logic, simply cannot be fulfilled. To avoid
such problems one should plan a program carefully in advance with a good structogram, for example. Once an error occurs,
you can use a debugger to help trace the source of the trouble.
