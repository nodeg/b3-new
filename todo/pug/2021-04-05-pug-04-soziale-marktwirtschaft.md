---
title:  "04. Soziale Marktwirtschaft"
date:   2020-11-25 06:00:00 +0100
---

## Soziale Marktwirtschaft – freiheitlich und sozial ausgerichtet

### Soziale Marktwirtschaft — ein steter Entwicklungsprozess

Aufgrund der Erfahrungen aus der Weltwirtschaftskrise (1924—1932) und dem Nationalsozialismus (1933—1945) gab es unter
den Siegermächten UdSSR, USA, Frankreich und Großbritannien starke Strömungen, die für eine Sozialisierung von Banken
und Großindustrie eintraten. Am Ende setzten sich die Befürworter einer marktwirtschaftlichen Ordnung mit sozialen
Elementen durch.

Als geistige Begründer unserer modernen sozialen Marktwirtschaft gelten Ludwig Erhard und Alfred Müller-Armack, der
1948 wesentliche Eckpunkte formulierte, z. B.:

- Mitgestaltungsrecht des Arbeitnehmers ohne Einengung der Unternehmerverantwortung
- Erwerbsstreben der Einzelnen wird im Interesse des Gesamtwohls gelenkt
- Bekämpfung des Marktmissbrauchs von Monopolen
- konjunkturpolitische Beschäftigungs- und Tarifpolitik

Diese Eckpunkte waren für viele Menschen nur "graue Theorie", insbesondere mit Blick auf die Kriegsspuren, Teilung
Deutschlands, 10 Millionen Vertriebene, Nazi-Prozesse in Nürnberg usw. Was soziale Marktwirtschaft tatsächlich bedeutet,
wurde den Menschen erst mit den spürbaren Verbesserungen ihrer alltäglichen Lebenssituation bewusst. Der Anstieg des
privaten Konsums führte zu einer hohen Nachfrage, zu einem vielfältigen Güterangebot und zu einem hohen
Beschäftigungsstand. Eine Mehrung des allgemeinen Wohlstands in den Nachkriegsjahren folgte rasch, wodurch das Vertrauen
in die soziale Marktwirtschaft und in die deutsche Politik wuchs.

### Fundamente der sozialen Marktwirtschaft

Gesellschaft, Staat, Rechts- und Wirtschaftsordnung stehen in einer Wechselbeziehung. Das Grundgesetz legt sich nicht
auf eine bestimmte Wirtschaftsordnung fest, sondern gibt lediglich die Rahmenbedingungen vor, unter denen freiheitliche
und soziale Prinzipien unserer Gesellschaft zu gestalten sind. Die politische Bedeutung der beiden Prinzipien wird am
Beispiel "Sozialpflichtigkeit des Eigentums" (Art. 14 Abs. 2 GG) deutlich:

- Fall "Wohnungseigentum": Die Handlungsfreiheit eines Vermieters hat dort ihre Grenzen, wo der Mieter willkürlichen
  Forderungen (z. B. plötzliche Kündigung, überzogene Mieterhöhung) ausgesetzt wäre.
- Fall "Grundstückseigentum": Kann durch den Bau einer Umgehungsstraße eine Vielzahl von Bewohnern vor Schadstoffen und
  Lärm geschützt werden, so kann der Grundbesitzer zum Verkauf gegen eine entsprechende Entschädigung verpflichtet
  werden. Weigert sich der Eigentümer, so ist ein Enteignungsverfahren möglich.

| Merkmale der sozialen Marktwirtschaft | |
| --- | --- |
| Freiheitliche Prinzipien, z.B. | Soziale Prinzipien, z.B. |
| Art. 2 GG; „Freie Entfaltung der Persönlichkeit“, z.B.: Konsum-, Gewerbe- , Handlungs-, Vertragsfreiheit beim Abschluss von Kaufverträgen. | Schutzvorschriften schützen den Verbraucher, z. B. bei Fernabsatz-, Haustürgeschäften oder bei Abschluss von Kreditverträgen. |
| Art. 9 GG; „Vereinigungsfreiheit", z. B. Interessenvertretung durch Arbeitgeberverbände und Gewerkschaften. | Tariflicher Schutz, z. B. Lohn- und Manteltarifverträge sowie deren Allgemeinverbindlichkeit, Tarifautonomie der Sozialpartner. |
| Art. 12 GG; „Freie Berufswahl und Berufsausübung", d.h. Auswahlrecht bei der Besetzung eines Arbeitsplatzes haben Arbeitnehmer und Arbeitgeber. | Technischer und sozialer Arbeitsschutz schaffen Rechtssicherheit, z. B. :Arbeitsstättenverordnung, Jugendarbeitsschutzgesetz, Kündigungsschutzgesetz. |
| Art. 14 GG; „Sozialpflichtigkeit des Eigentums", d.h.: Wer über Eigentum verfügt, kann dadurch auch Verpflichtungen gegenüber der Allgemeinheit haben. | Enteignung des Eigentums ist möglich, wenn das Interesse der Allgemeinheit höher zu bewerten ist als das Einzelinteresse. Der Eigentümer ist in diesem Fall zu entschädigen. |


### Soziale Marktwirtschaft auf dem Prüfstand

In den Fünfziger- und Sechzigerjahren verfielen Politiker und Bürger dem Glauben, die Dynamik des "deutschen
Wirtschaftswunders" werde sich langfristig fortsetzen. Die sozialen Leistungen wurden stetig bis zur Jahrtausendwende
ausgebaut. Soziale Leistungen haben jedoch ihren Preis. Die öffentlichen Haushalte bei Staat und Kommunen mussten sich
von Jahr zu Jahr mehr verschulden.

Ein Wirtschaftswachstum auf niedrigem Niveau zwingt Gesetzgeber, Unternehmer, Gewerkschaften und jeden einzelnen Bürger
zur Überprüfung der sozialen Errungenschaften. Die Sozialreformen in den letzten Jahren sind nur ein Anfang, um die
Ausgaben im sozialen Bereich und die Lohnnebenkosten der Unternehmen zu senken. Beispielsweise wird der Beitragssatz zur
gesetzlichen Krankenversicherung nicht mehr zu gleichen Teilen durch Arbeitnehmer und Arbeitgeber finanziert;
Arbeitnehmer zahlen 0,9 Prozentpunkte mehr als Arbeitgeber. Die wahren sozialen Notwendigkeiten müssen weiterhin
finanzierbar bleiben, z. B.:

- Kostenfreiheit des Schulwegs
- soziale Absicherung im Alter
- Nachqualifizierung von jugendlichen Arbeitslosen
- soziale Errungenschaften in der Arbeitswelt
- Mindestlöhne in möglichst vielen Branchen

Inwieweit die Arbeitgeberseite dabei in die Pflicht genommen werden kann, beispielsweise den Erhalt von Arbeitsplätzen
und sozialen Standards wie Kündigungsschutz oder Lohnfortzahlung im Krankheitsfall zu garantieren, bleibt abzuwarten.

## Freie Marktwirtschaft vs Zentralverwaltungswirtschaft

|                                      | Freie Marktwirtschaft | Zentralverwaltungswirtschaft   |
| ------------------------------------ | --------------------- | ------------------------------ |
| Wer steuert und pant die Wirtschaft? | Jeder Einzelne entscheidet aufgrund von Angebot und Nachfrage. | Staatliche Planbehörde; Sie legt z.B. zentral fest, was und wie viel produziert wird. |
| Wem gehören die Produktionsmittel?   | Produktionsmittel sind Privateigentum. | Produktionsmittel sind Staatseigentum. |
| Wie erfolgt die Preisbildung?        | Preise werden durch Angebot und Nachfrage gebildet. | Preise werden staatlich festgelegt. |
| Welche Zielsetzung verfolgt die jeweilige Wirtschaftsform? | Gewinnerzielung | Planerfüllung |

Nachteile der Freien Marktwirtschaft:

- Monopole und Kartelle können die Preise ansteigen lassen.
- Keine soziale Absicherung der Arbeitnehmer
- Konjunkturschwankungen
- Arbeitslosigkeit
- Ausbeutung wirtschaftlich Schwächerer
- Unternehmen können den Wettbewerb durch Absprachen einschränken.
- Erfindungen und wirtschaftliche Höchstleistungen durch Wettbewerb

Nachteile der Zentralverwaltungswirtschaft:

- Arbeitsplatz und Beruf können nicht frei gewählt werden.
- aufwändiger Verwaltungsapparat
- mangelnder Leistungsanreiz
- Versorgungslücken
- Phantasielose Güterproduktion nach Plan
