---
title: "02. 5 Wahlrechtsgrundsätze"
date: 2019-10-17 10:40:00 +0200
---

# 5 Wahlrechtsgrundsätze

* allgemein: deutsche Staatsbürger ab 18 Jahren
* gleich: Jede Stimme zählt gleich!
* frei: keine Beeinflussung, kein Zwang
* unmittelbar: ohne Zwischeninstanzen (z.B. Wahlmänner)
* geheim: keine Kenntnis über das Wahlverhalten anderer
