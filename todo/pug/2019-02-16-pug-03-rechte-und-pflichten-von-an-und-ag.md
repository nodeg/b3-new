---
title:  "03. Rechte und Pflichten von AN und AG"
date:   2019-02-11 06:10:00 +0100
---

| Pflichten der Arbeitnehmer                                   | Pflichten der Arbeitgeber                                    |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| Arbeitspflicht: Die im Arbeitsvertrag vereinbarte Leistung persönlich erbringen. | Entgeltpflicht: AG muss vereinbarte Vergütung und Nebenleistungen pünktlich ausbezahlen. |
| Weisungsgebundenheit: AN muss Anweisungen des Vorgesetzten folgen | Fürsorgepflicht: AG muss sozialen Arbeitsschutz (Arbeitszeit- und Urlaubsregelung, Unfallverhütungsvorschriften) einhalten. |
| Treuepflicht: AN darf Betriebsgeheimnisse und Kundendaten nicht an Dritte weitergeben $$\rightarrow$$ Ausnahmen: Ärzte, Priester | Beschäftigungspflicht: AG muss AN entsprechend dem Arbeitsvertrag beschäftigen. |
| Sorgfaltspflicht: AN muss Tätigkeiten nach bestem Wissen ausführen und Betriebsmittel pfleglich behandeln | Zeugnispflicht: AG muss bei Beendigung des Arbeitsverhältnisses dem AN ein Arbeitszeugnis ausstellen.|
| Erholungspflicht: Andere Tätigkeiten gegen Entgelt an Wochenenden oder im Urlaub sind nur mit Zustimmung des AG zulässig | Bei Kündigung müssen Lohnsteuerkarte und -bescheinigung ausgehändigt werden. |

Wege ein Arbeitsverhältnis zu beenden:

1. Aufhebungsvertrag<br>
   = Beide Seiten beenden das Arbeitsverhältnis einvernehmlich unter bestimmten Konditionen
2. Ordentliche Kündigung
3. Außerordentliche (fristlose) Kündigung
4. Ablauf eines befristeten Arbeitsverhältnisses

## Wettbewerbsverbot des ANs

Ein AG kann dem AN verbieten während des Beschäftigungsverhältnisses, ohne ein explizites Einverständnis, einer
Tätigkeit nachzugehen, die in Konkurrenz zur Tätigkeit des AGs steht. Nach Beendigung eines Arbeitsverhältnisses kann
dies vertraglich maximal auf bis zu zwei Jahre ausgeweitet werden (unter Voraussetzungen).

Quelle: <https://de.wikipedia.org/wiki/Wettbewerbsverbot>

## Die Personalakte

Die Personalakte enthält sämtliche relevanten Dokumente und Urkunden die für das Arbeitsverhältnis von Relevanz sind.
Im Folgenden einige Beispiele für mögliche Dokumente:

- Personalbezogene Unterlagen und Vertragsunterlagen
  - Bewerbungsschreiben des Mitarbeiters (Angebot)
  - Arbeitszeugniskopien des Arbeitnehmers (bisherige)
  - Schulabschlusszeugnis
  - Berufsabschluss
  - Lebenslauf und Passbild
  - ...
- Sozialversicherungs- und Steuerunterlagen
  - Anmeldung zur Krankenkasse
  - Nachweis der monatlichen Krankenkassenbeiträge
  - Sozialversicherungsausweis/Ausweis zur Versicherungsnummer
  - Nachweis zur Anlage vermögenswirksamen Leistungen
  - ...
- Kopien amtlicher Urkunden
  - Kopie der Fahrerlaubnis (Führerschein)
  - Schwerbehindertenausweis
  - ...
- Nachstehende Angaben, soweit für die Gehaltshöhe oder sonstige arbeitsvertragliche Regelungen von Bedeutung
  - Antrag auf Orts-, Sozial- oder Familienzuschlag
  - Sterbeurkunde des Ehegatten
  - Heiratsurkunde,
  - ...
- Sonstige Unterlagen
  - Personalbogen (Deckblatt)
  - Urlaubsliste und Fehlzeitenübersicht
  - Beurteilungen und Bewertungen
  - Ermahnungen (so genannte Missbilligungen) und Abmahnungen
  - ...

Ein AN hat jederzeit das Recht die Personalakte einzusehen. Auch in Begleitung einer betriebsfremden Person.

Quelle: <https://de.wikipedia.org/wiki/Personalakte>
