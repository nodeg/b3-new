---
title:  "06. Kommunalwahlrecht"
date:   2019-12-18 11:00:00 +0100
---

(Ober-)Bürgermeister =Exekutive

- Kandidaten verschiedener Parteien (oder Parteilos)
- 1 Stimme für den Wähler
- gewählt ist, wer die absolute Mehrheit (>50%) erreicht

$$\rightarrow$$ Stichwahl der zwei Bestplatzierten

Stadtrat (Gemeinderat) = Exekutive

- Anzahl der Stimmen entspricht = Anzahl der Stadträte
- Kumulieren (Häufeln): bis zu 3 Stimmen
- Panaschieren: Kandidaten verschiedener Listen unterstützen

Ein Link zur Erklärung der Kommunalwahl ist [hier](https://www.br.de/wahl/kommunalwahl-bayern-wahlrecht102.html).
