---
title:  "03. Wettbewerb und Werbung"
date:   2020-11-20 06:00:00 +0100
---

## Warum muss ein Wettbewerb sichergestellt sein?

### Info-Text

Der Wettbewerb ist unverzichtbarer Bestandteil einer marktwirtschaftlichen Ordnung. Im Idealfall geben die Verbraucher
den Anbietern am Markt durch ihren preis- und qualitätsbewussten Einkauf eine Rückmeldung über ihre Wünsche. Ein
Anbieter, dessen vorrangiges Ziel der Gewinn sein muss, wird versuchen, den Wünschen der Verbraucher zu entsprechen, und
im Vergleich zu anderen Anbietern am Markt eine möglichst hohe Produktqualität zu niedrigen Kosten anzustreben. Dieser
Leistungswettbewerb unter den Anbietern sichert also dem kritischen Verbraucher eine hohe Produktqualität zum
angemessenen Preis.

Problematisch wird die Situation am Markt, wenn es um Produkte geht, die nur noch von einem Anbieter angeboten werden.
Bei sogenannten Monopolen bilden sich die Preise nicht mehr am Markt über den Wettbewerb, sondern ausschließlich über
die Höhe der Nachfrage. Monopole können damit überhöhte Gewinne bei gleichbleibender Produktqualität erreichen, was
nicht im Sinne des Wettbewerbs sein kann.

Eine weitere, den Wettbewerb verzerrende Maßnahme der Anbieter ist es, sich dem Wettbewerbsdruck zu entziehen, indem man
Marktteile und Preise mit den Mitbewerbern abspricht. Diese sogenannten Kartelle sind in der Bundesrepublik seit 1957
verboten.

Auch der Zusammenschluss von Unternehmen, Fusion genannt, kann zu unerwünschten marktbeherrschenden Stellungen führen,
weil sich bei einer Fusion immer die Anbieterzahl am Markt verringert.

Der Staat versucht daher den Wettbewerb durch folgende Maßnahmen zu sichern:

- Das Kartellgesetz von 1957 verbietet grundsätzlich Kartelle, außer zur Absprache von Normen. Die Aufsicht hierüber
  führt das Bundeskartellamt in Berlin.
- Das Bundeskartellamt prüft außerdem die Fusion von Unternehmen, wenn der Marktanteil über 20% beträgt oder die
  Unternehmen mehr als zehntausend Beschäftigte haben oder wenn eines der beiden Unternehmen einen hohen Jahresumsatz
  aufweist.

Allerdings wurde in der Vergangenheit den meisten Fusionen zugestimmt, da die internationale Wettbewerbsfähigkeit der
Unternehmen nicht gefährdet werden sollte. Gerade um die Jahrtausendwende kam es zu vielen Fusionen von Großkonzernen,
vor allem auf dem Markt der Informations- und Telekommunikationsbranche.

Daraus ergeben sich folgende Dinge:

1. **Welche Vorteile bietet der freie Wettbewerb?** - Anbieter, welche die Wünsche der Konsumenten erfüllen, können hohe
   Gewinne erwirtschaften. Konkurrenten, die mindere Qualität liefern oder aber zu hohe Preise verlangen werden durch
   die "Macht" der Nachfrage des Konsumenten vom Markt verdrängt.
2. **Warum wird die Qualität durch Wettbewerb oftmals gesteigert?** - Freier Wettbewerb führt zu möglichst hoher
   Produktqualität zum niedrigsten Preis, da die Verbraucher durch ihre Nachfrage direkt den Markt lenken. Anbieter, die
   die Vorstellungen der Konsumenten nicht erfüllen können, werden vom Markt verdrängt.
3. **Welchen Nachteil haben Monopole und Kartelle? Wie greift der Staat ein?**
   - Im Fall von Monopolen und Kartellen können Unternehmen die Preise und Qualität ihrer Produkte frei gestalten, da
     Konsumenten keine Alternative haben/es keine Konkurrenz gibt. Sie verlangen hohe Preise für gleichbleibende (oder
     sogar mindere) Produktqualität, welche die Verbraucher wohl oder übel zahlen müssen.
   - Der Staat greift ein, in dem er Kartelle grundsätzlich verbietet. Das Bundeskartellamt setzt dieses Verbot durch.
4. **Welchen Vorteil versprechen sich Unternehmen von Fusionen?** - Fusionen garantieren/fördern die internationale
   Wettbewerbsfähigkeit von Unternehmen. Aus diesem Grund wurde in den vergangenen Jahren den meisten Fusionen
   zugestimmt.

### Weitere wichtige Begrifflichkeiten

- Monopol: Der Markt wird von einer Partei kontrolliert.
- Oligopol: Der Markt wird von einer kleinen Gruppe an Teilnehmern kontrolliert (meist Großunternehmer).
- Polypol: Der Markt ist durch Konkurrenz auf Seiten des Angebots und der Nachfrageseite kontrolliert.

Alle drei Begriffe können sich sowohl auf Angebote, als auch auf die Nachfrage beziehen.

Unternehmenszusammenschlüsse können drei Formen annehmen:

- Anorganischer Zusammenschluss: wird auch Diversifikation genannt. Dient zur Erweiterung des eigenen Produktportfolios
  und zum Einstieg in neue Märkte.
- Vertikaler Zusammenschluss: bezeichnet den Zusammenschluss von Unternehmen in einer Wertschöpfungskette. Ein
  Beispiel wäre der Kauf einer Druckerei von einem Zeitungsunternehmen und anschließendem Vertrieb in eigenen Kiosken.
- Horizontaler Zusammenschluss: bezeichnet den Zusammenschluss von Unternehmen mit ähnlichen Produkten oder
  Dienstleistungen.

## Markt und Absatz

### Der Kreis schließt sich

Alle Firmen gebe sich bei der Entwicklung ihrer Produktidee und bei der Material- und Geldbeschaffung viel Mühe.
Nun wollen die Firmen ihre Produkte auf dem Markt verkaufen. Sie wissen, dass die Konkurrenz groß ist, denn viele
Anbieter mit den unterschiedlichsten Produkten werben dort um die Gunst der Käufer. Auf jedem Markt gibt es also
Verkäufer (Anbieter) und Käufer (Nachfrager).

Auf dem Markt treffen Angebot und Nachfrage zusammen.

So funktioniert auch der Wirtschaftskreislauf:

TODO: Lücke ausfüllen

Im einfachen Wirtschaftskreislauf fließt zwischen den privaten Haushalten und den Betrieben ein ständiger Geld-
und Güterstrom.

Um erfolgreich zu verkaufen muss man bereits bei der Beschaffung und der Produktion den Absatz mit
einberechnen. Günstiger Materialeinkauf ist genauso wichtig wie sorgfältige Produktion. Nun steht der Absatz an:
- Wie lassen sich unsere Produkte und Dienstleistungen möglichst gut verkaufen?
- Welche Preise verlangen wir?
- Wie gestalten wir die Werbung?
- Wie präsentieren wir unser Angebot?
- Wie organisieren wir den Verkauf?

Der Absatz ist die dritte Stufe der betrieblichen Grundfunktion. Er umfasst alle Tätigkeiten, die mit dem Verkauf
von Produkten oder Dienstleistungen zusammenhängen.

Der Absatz muss sorgfältig geplant werden. Dazu entwickeln Firmen verkaufsfördernde Strategien (Marketing).

### Vom Verkäufer zum Käufer

| Der Verkäufer muss:          | Der Käufer soll:             |
| ---------------------------- | ---------------------------- |
| Nachfrage wecken             | Am Angebot interessiert sein |
| Die Konkurrenz beobachten    | Zugreifen und kaufen         |
| Die Produktqualität beachten | Mit der Ware zufrieden sein  |
| Werbung treiben              | Wieder kaufen                |
| Die Preise festlegen         | Stammkunde werden            |
| Den Verkauf organisieren     |                              |

Marketing bedeutet das Denken zum Markt hin, also vom Unternehmen zum möglichen Käufer.

Auch die Produktgestaltung ist sehr wichtig. Produkteigenschaften wie Qualität, gutes Design sprechen
zahlungskräftige Kunden an, die dann beim nächsten Einkauf wieder auf diese Produktmarke zurückgreifen. Auch die
Nachkaufgarantie ist ein gutes Produktmerkmal. Kann ich das Produkt immer erweitern, wird es weitergegeben und
natürlich von dieser Marke gekauft.

### Aufmerksamkeit wecken

Über die Verpackung und die Gestaltung des Verkaufsraumes wird in Firmen viel nachgedacht. Bei der Verpackung muss man
einiges beachten:

- Umweltfreundlichkeit: recycelbar oder als Pfand
- Schutz vor Bruch, Verderb, Diebstahl
- Gebrauchsfunktion: Standsicherheit, Verschließbarkeit
- Lagerfunktion: Transport, Stapelfähigkeit
- Gute Packungsgröße, tragfähig
- Information über Inhalte und Verwendbarkeit
- Verkaufsförderung durch Form

### Werbung

Alle Unternehmen überprüfen die Werbewirkung ihrer Maßnahmen, da Werbung jede Menge Geld kostet. Die Wirtschaftswerbung
orientiert sich an den vier Stufen der AIDA-Formel:

A= Attention: Die Aufmerksamkeit der Zielpersonen auf das Produkt lenken, das sich von anderen abheben muss
I= Interest: Interesse der umworbenen Person wecken. Sie soll sich mit dem Produkt weiterbeschäftigen.
D= Desire: den Wunsch wecken das Produkt auszuprobieren und zu besitzen.
A= Action: der Verbraucher soll das Produkt kaufen.

Früher sollte Werbung nur informieren. Heute werden auch verstärkt Gefühle angesprochen, die neue Bedürfnisse wecken und
zum Kauf anregen.

Dafür nutzen die Firmen verschiedene Werbemittel:

- Plakate, Prospekte, Faltblätter, Beilagen
- Rundfunkwerbung, Fernsehwerbung
- Schaufenstergestaltung
- Busse/Straßenbahnen, T-Shirts

Auch die Werbezeit spielt eine große Rolle, z.B.:

- Schreibwaren $$\rightarrow$$ ___________________
- Spielwaren $$\rightarrow$$ ____________________________
- Gartenartikel $$\rightarrow$$ __________________________
- Freizeitartikel $$\rightarrow$$ _____________________________

Doch die größte Rolle bei einer gezielten Produktwerbung spielt die Zielgruppe der Käufer. Die Werbung für Kinder ist
anders gestaltet als die für Erwachsene. Auch der Zeitpunkt der Werbung ist unterschiedlich. Viele Untersuchungen
belegen, dass Produkte die von Jugendlichen gekauft wurden auch noch später bevorzugt werden.

Daneben gibt es aber auch noch andere verkaufsfördernde Maßnahmen:

- Aktionswochen, Sonderpreise, Verkaufswettbewerbe
- Kundendienst, Info-Hotline, Schulungen
- etc.

## Werbung und Verkaufsstrategien

### Bedürfnisse und Wünsche steuern unsere Kaufentscheidung

Wir kaufen, um unsere Bedürfnisse zu befriedigen. Das klingt logisch, oder?

Welche grundlegenden Bedürfnisse es gibt, hat Abraham Maslow, Gründervater der Humanistischen Psychologie, in dem Model
der "Bedürfnispyramide" beschrieben.

Die untersten Stufen sind die wichtigsten, denn sie dienen ganz konkret dem Überleben.

Die höherstehenden Bedürfnisse spielen laut Maslow erst eine Rolle, wenn jeweils die darunter liegenden Stufen
befriedigt sind.

## Arten von Werbung

Werbung hat das Ziel, das Kaufverhalten von Verbrauchen so zu stuern, dass bestimmte Produkte/Marken bevorzugt werden.

Man unterscheidet zwischen informierender und manipulierender Werbung.

- informierende Werbung: Weißt auf bestimmte Produkte und produktspezifikationen hin; Angaben über Preis, Menge, Qualtität
- maninpulierende Werbung: Zielt auf die Gefühle des Verbrauchers ab; Spricht Gefühle an, macht keine direkten Aussagen über dsa Produkt.

### Das AIDA Werbemodel

Wirksame Werbung:

- muss zunächst Aufmerksamkeit wecken, um überhaupt wahrgenommen und nicht einfach übersehen zu werden. Dies kann je
  nach Medium z.B. über den Slogan oder über Bilder, Musik, Personen etc. erfolgen.
- muss die geweckte Aufmerksamkeit auf das Produkt übertragen und Interesse für das Produkt wecken.
- muss in ihrem Gesamterscheinen (zielgruppengenaue Ansprache der Bedürfnisse) einen Kaufwunsch auslösen.
- muss schließlich die Kaufhandlung bewirken.

AIDA ist ein Werbemodel, das bereits 1898 entwickelt wurde. Es wurde für Verkaufsgespräche entwickelt, dient jedoch auch
als Leitfaden, wie Anzeigen, Werbeclips etc. aufgebaut sein müssen, um zum Erfolg zu führen.

Der Name AIDA setzt sich aus den Anfangsbuchstaben der einzelnen Phasen zusammen:

TODO: AIDA erklären

| Buchstabe | Bedeutung | Beschreibung                        |
| --------- | --------- | ----------------------------------- |
| A         | Attention | Aufmerksamkeit muss geweckt werden. |
| I         | Interest  | Interesse                           |
| D         | Desire    | Wunsch auf Besitz von Produkt       |
| A         | Action    | Kaufhandlung                        |
