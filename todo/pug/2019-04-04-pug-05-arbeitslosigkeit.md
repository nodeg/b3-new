---
title:  "05. Arbeitslosigkeit"
date:   2019-04-01 10:00:00 +0100
---

## Was ist Arbeitslosigkeit?

Nach § 16 i.V.m. §138 SGB III eine Person, die:
- ohne Arbeitsverhältnis ist (Beschäftigungslosigkeit)
- als arbeitslos gemeldet ist (Meldung)
- theoretisch Arbeiten kann (Verfügbarkeit)
- aktiv versucht, Arbeit zu finden (Eigenbemühung)

## Arten der Arbeitslosigkeit

### Konjunkturelle Arbeitslosigkeit

- Erklärung:<br>
  Es wird weniger angefragt, daher muss weniger produziert werden. Weniger Arbeitnehmer*innen sind folglich notwendig,
  um die Nachfrage zu decken. Arbeitslosigkeit entsteht durch sinkende Nachfrage, an die sich das Angebot anpasst.
- Beispiel:
  Person während der Weltwirtschaftskrise 2008 verliert ihren Job durch Einbruch der Wirtschaft.

### Saisonale Arbeitslosigkeit

- Erklärung:
  Beschäftigungsschwankung innerhalb des Jahres.
  Wegen starker saisonaler Bindung des Arbeitsbereiches, gibt es nicht ganzjährig eine gleichbleibende Nachfrage, an die
  sich das Angebot anpasst.
- Beispiel:
  Eine in einer Skianlage arbeitende Person, welche im Sommer wegen Schneemangel geschlossen ist.

### Friktionelle Arbeitslosigkeit

- Erklärung:
  Kurzarbeitslosigkeit.
  Arbeitslosigkeit, die durch nicht fließenden Übergang zwischen zwei Beschäftigungsverhältnissen entsteht.
- Beispiel:
  Eine Person ist mit ihrer Ausbildung fertig und bewirbt sich für den kommenden Monat auf eine Stelle im Betrieb.

### Strukturelle Arbeitslosigkeit

- Erklärung:
  Strukturelle Veränderung der Wirtschaft.
  Wegen technischen Fortschritts, neuen Regelungen oder ortsbedingten Faktoren, wird die Anzahl an benötigten
  Arbeitsstellen reduziert.
- Beispiel:
  Personen, die im Tagebau tätig sind, welche bis 2038 wegen des Kohleausstiegs arbeitslos werden.

### Mismatch Arbeitslosigkeit

- Erklärung:<br>
  Benötigte Kenntnisse und Wissen der Arbeitnehmer stimmen nicht überein. Durch Wandel der für den Arbeitsbereich
  notwendigen Kenntnisse bei gleichbleibendem Wissensstand der Arbeitnehmer, decken sich die Voraussetzungen für den
  Arbeitsplatz nicht mehr mit den Kenntnissen der Mitarbeiter.
- Beispiel:<br>
  In einer IT-Firma wird auf ein anderes Produkt umgestellt, weswegen Expert*innen im Bereich des alten Produkts nicht
  mehr ihre Kenntnisse anwenden können.

## Arbeitslosenstatistik

- offizielle Arbeitslosigkeit im Januar 2019: 2.405.586 (5,3%)
- Personengruppen im Bereich Unterbeschäftigung, die nicht oben enthalten sind:

| Gruppe                                                       | Anzahl |
| ------------------------------------------------------------ | ------ |
| älter als 58, beziehen Arbeitslosengeld II                   | 170021 |
| Ein-Euro-Jobs (Arbeitsgelegenheiten)                         | 65828  |
| Förderung von Arbeitsverhältnissen                           | 7202   |
| Fremdförderung                                               | 207182 |
| Bundesprogramm "Soziale Teilhabe am Arbeitsmarkt"            | 28     |
| berufliche Weiterbildung                                     | 165979 |
| Aktivierung und berufliche Eingliederung (z.B. Vermittlung durch dritte) | 184610 |
| Beschäftigungszuschuss (für schwer vermittelbare Arbeitslose) | 1941   |
| kranke Arbeitslose (§146 SGB III)                            | 61279  |

$$\rightarrow$$ nicht gezählte Arbeitslose gesamt: 864.070<br>
$$\rightarrow$$ tatsächliche Arbeitslosigkeit im Januar 2019: 3.270.055 (7,2%)

Kritik: Oft sind politische oder gesetzliche Änderungen für ein Absinken der AL-Zahlen verantwortlich.

## Arbeitslosigkeit als Stressfaktor
- Druck vom Arbeitsamt:
    - Drohende Leistungskürzungen
    - Termindruck
    - Mann muss jede Stelle annehmen
    - Meldung von Ortsabwesenheit (max. 21 Tage/Jahr)
- Vorurteile ggü. Arbeitslosen: faul, bemühen sich nicht, sind sich für best. Arbeiten "zu gut"
- Teufelskreis: Arbeitslosigkeit $$\rightarrow$$ Verlust der Wohnung $$\rightarrow$$ niedrigerer Lebensstandard
  $$\rightarrow$$ eingeschränkte Mobilität $$\rightarrow$$ psychische Probleme $$\rightarrow$$ keine oder wenige
  gesellschaftliche Teilhabe
- Leistungsgesellschaft: Der Wer eines Menschen bemisst sich oft am Erfolg bzw. an der Arbeitswilligkeit

## Quellen
- Manuel Plate, 2008 - 2011 Arbeitsvermittler bei Agentur für Arbeit
- Wolfgang Röhrig, Bündnis Grundeinkommen
- Ziyad Lunat, Confederal Group of the European United Left/Nordic Green Left
- Kanke et al., Zur Sache: Sozialkunde - Politik und Sozialkunde für berufliche Schulen, S. 46 - 49
- <https://dejure.org/gesetze/SGB_III/138.html> (07.02.2019)
- <https://www.die-linke.de/themen/arbeit/tatsaechliche-arbeitslosigkeit/2019/> (07.02.2019)
- <https://statistik.arbeitsagentur.de/Statischer-Content/Grundlagen/Methodenberichte/Uebergreifend/Generische-Publikationen/Methodenbericht-Arbeitslosigkeit-Unterbeschaeftigung.pdf> (07.02.2019)
- <https://statistik.arbeitsagentur.de/Navigation/Statistik/Statistik-nach-Themen/Arbeitslose-und-gemeldetes-Stellenangebot/Arbeislose-und-gemeldetes-Stellenangebot-Nav.html> (07.02.2019)
