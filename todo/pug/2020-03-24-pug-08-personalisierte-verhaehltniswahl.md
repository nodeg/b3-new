---
title:  "08. Personaliserte Verhältniswahl"
date:   2020-03-24 10:00:00 +0100
---

## Die Wahl des Deutschen Bundestages

Die Wahlen zum Deutschen Bundestag finden in einer personalisierten Verhältniswahl statt. Das ist ein Wahlsystem, das
Elemente der Verhältnis- und der Mehrheitswahl vereinigt. Die Vor- und Nachteile der beiden Systeme sollen dadurch
ausgenutzt bzw. abgeschwächt werden. Das ist der Grund, warum der Wähler bei der Bundestagswahl zwei Stimmen abgibt.

Von den vorgesehenen 598 Abgeordneten des Bundestages wird die Hälfte über die Erststimme direkt gewählt. Von jedem der
299 Wahlkreise zieht der so genannte Direktkandidat einer Partei, der die meisten Stimmen erhalten hat, in den deutschen
Bundestag ein; hier handelt es sich um eine Mehrheitswahl. Von diesem Politiker wird erwartet, dass er im Bundestag die
Interessen seines Wahlkreises vertritt. Über die Zweitstimme werden weitere 299 Sitze über Landeslisten der Parteien
vergeben; hier gilt das Prinzip der Verhältniswahl. Die Zweitstimme ist die wichtigere der beiden Stimmen, da über sie
die Sitzverteilung im Parlament ermittelt wird. Erhält zum Beispiel eine Partei 30 Prozent der Zweitstimmen, erhält sie
auch 30 Prozent der Sitze im Parlament. Die Zahl der gewählten Direktkandidaten wird von dieser Summe abgezogen. Die
verbleibenden Mandate sind die Listenmandate der Partei. Sie werden in der Reihenfolge auf die Kandidaten verteilt, in
der diese auf der vor der Wahl aufgestellten Liste platziert sind.

Gewinnt eine Partei mehr Direktmandate als ihr nach den Zweitstimmen zustehen, darf sie diese Mandate behalten. Über die
Landeslisten werden dann keine Mandate mehr vergeben. Die Zahl der Abgeordneten des Bundestages erhöht sich
entsprechend. Es entstehen dann sogenannte Überhangmandate. Bei den Bundestagswahlen 2017 entstanden insgesamt 49
Überhangmandate. Dazu kamen noch 62 Ausgleichsmandate, so dass sich die Anzahl der Abgeordneten auf 709 erhöhte. Da das
Bundesverfassungsgericht Überhangsmandate für unrechtmäßig erachtet hat, weil diese dem Grundsatz der gleichen Wahl
widersprechen, wurden 2013 die sogenannten Ausgleichsmandate eingeführt. Hierbei werden den Parteien je nach
Zweitstimmenanteil zusätzliche Sitze zugesprochen, so dass Verzerrungen aufgrund der Überhangsmandate vermieden werden
können.

Das Bundeswahlgesetz bestimmt außerdem, dass bei der Verrechnung nur Parteien berücksichtigt werden, die mindestens 5
Prozent der im Wahlgebiet abgegebenen gültigen Zweitstimmen erreichen. Im Bundestag erhalten also nur die Parteien über
die Zweitstimmen Sitze, die eine bestimmte Sperrklausel, die sogenannte 5%-Hürde, überwinden. Erringt eine Partei
weniger als 5 Prozent der Zweitstimmen, kann sie nur in den Bundestag einziehen, falls sie drei Direktmandate erhalten
hat. Ist dies nicht der Fall, bleibt die Partei außen vor. Errungene Direktmandate verbleiben aber bei den Kandidaten.
Bei der Bundestagswahl 2002 erhielt zum Beispiel die PDS nur zwei Direktmandate und erreichte weniger als 5% der
Zweitstimmen. Die Partei war somit nicht mehr im Parlament vertreten. Die zwei Direktkandidatinnen wurden aber
selbstverständlich Abgeordnete. Durch diese Fünfprozentklausel soll bei der Verhältniswahl verhindert werden, dass ins
Parlament zu viele kleine Parteien einziehen. Der Sinn dieser Regelung ist, eine Zersplitterung der Sitzverteilung zu
verhindern und damit eine stabile Mehrheit zu fördern. Ohne eine solche Sperrklausel finden sich häufig viele kleine
Parteien im Parlament, teils auch Splittergruppen oder extreme Parteien. Dies erschwert die Regierungsbildung.

## Wahlsysteme im Überblick

### Mehrheitswahlsystem (Persönlichkeitswahl)

- Land wird in Wahlkreise/Stimmkreise eingeteilt (Anzahl der Wahlkreise abhängig von der Anzahl der Sitze im Parlament
  z. B. 500 Sitze = 500 Wahlkreise).
- Gewählt ist, wer in einem Wahlkreis die Mehrheit der Stimmen erringt.
- Die für die anderen Kandidaten abgegebenen Stimmen gehen verloren.
- Relatives Mehrheitswahlsystem: Kandidat erhält den Sitz, der die meisten Stimmen hat (es genügt die einfache/relative
  Mehrheit).
- Absolutes Mehrheitswahlsystem: Kandidat erhält den Sitz, der die absolute Mehrheit hat (also mehr als 50 Prozent der
  abgegebenen Stimmen). Bei keiner absoluten Mehrheit, gibt es eine Stichwahl zwischen den zwei Kandidaten mit den
  meisten Stimmen.

| Vorteile                                                  | Nachteile                                                                                                                                       |
|-----------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------|
| Begünstigung von Zwei-/Dreiparteien-parlamenten.          | Viele Wählerstimmen gehen verloren.                                                                                                             |
| Führt zu eindeutigen Mehrheitsverhältnissen im Parlament. | Zusammensetzung des Parlaments drückt den Wählerwillen nur unzureichend aus (keine Vielfalt – nur die Siegerparteien ziehen ins Parlament ein). |
| Vorhersehbare Regierungsbildung.                          | „Papierkorbstimmen“: Stimmen für Kandidaten kleinerer Parteien haben keine Konsequenz für die Zusammensetzung des Parlaments.                   |
| Regierungsbildung wird erleichtert.                       | Gefahr für Minderheiten, dass sie nicht berücksichtigt werden.                                                                                  |
| Stabile/Starke Regierung.                                 | Scheidet ein Abgeordneter aus, muss dessen Wahlkreis neu gewählt werden.                                                                        |
| Keine Parteienvielfalt.                                   |                                                                                                                                                 |
| Geringe Chance für extreme Parteien.                      |                                                                                                                                                 |
| Wähler kennen den Kandidaten ihres Wahlkreises.           |                                                                                                                                                 |
| Abgeordnete weniger von ihrer Partei abhängig.            |                                                                                                                                                 |

### Verhältniswahlsystem (Listenwahl)

- Parteien legen Listen mit ihren Kandidaten fest.
- Abgegebene Stimmen werden in allen Wahlkreisen zusammengezählt.
- Anzahl der Sitze einer Partei im Parlament richtet sich nach dem prozentualen Anteil an den Wählerstimmen (z. B. 20 % Wählerstimmen à 20 % der Parlamentssitze).
- Sitze im Parlament werden an die Kandidaten in der Reihenfolge verteilt, in der sei auf der Liste stehen.
- Bei diesem Wahlsystem sind auch kleine Parteien/Minderheiten im Parlament vertreten.

| Vorteile                                                             | Nachteile                                                                                      |
|----------------------------------------------------------------------|------------------------------------------------------------------------------------------------|
| Keine Wählerstimme geht verloren.                                    | Listen werden von der Partei aufgestellt à Wähler hat keinen direkten Einfluss auf Kandidaten. |
| Jede einzelne Stimme beeinflusst die Zusammensetzung des Parlaments. | Wahlkandidaten sind den Wählern meist unbekannt.                                               |
| Wählerwille wird gut zum Ausdruck gebracht.                          | Abgeordnete fühlen sich eher der Partei verantwortlich.                                        |
| Zusammensetzung des Parlaments entspricht exakt dem Wählerwillen.    | Viele Parteien im Parlament.                                                                   |
| Begünstigung Kleiner/Mittlerer Parteien.                             | Regierungsbildung schwierig à keine eindeutige Mehrheit à Koalitionsbildung notwendig.         |
| Scheidet ein Abgeordneter aus, rückt ein Listenkandidat nach.        | Wähler kann nicht mitentscheiden, wer Koalitionspartner wird.                                  |
|                                                                      | Zugang von radikalen Splitterparteien.                                                         |
