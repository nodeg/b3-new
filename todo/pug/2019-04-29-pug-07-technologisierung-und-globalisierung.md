---
title:  "07. Technologisierung und Globalisierung bestimmen unsere Arbeitswelt"
date:   2019-04-29 10:30:00 +0100
---

## Wirtschaftsstruktur im Wandel
- Veränderung durch die Industrialisierung: Technologisierung und Massenproduktion $$\rightarrow$$ mehr Beschäftigte in
  der Produktion; Rückgang in der Landwirtschaft
- 2\. Weltkrieg: Aufstieg der Produktion (Waffen und danach Wiederaufbau)
- Anstieg im tertiären Sektor seit 1960ern $$\rightarrow$$ Emanzipation: Viele Frauen in Dienstleistungsberufen
- "quartärer Sektor": Hoher Anteil der IT- und Telekommunikationsbranche

## Technologiefortschritt - Unternehmen profitieren davon

- Veränderung des produzierenden Gewerbes hin zum Dienstleistungsgewerbe und Handel
- Rationalisierung von menschlichen Arbeitskräften durch Roboter
    - Investitionskosten werden durch geringere Personalkosten ersetzt
    - Keine Krankheitstage der Roboter
    - Keine Gewerkschaft oder AN-Vertretung
    - Keine Urlaubsansprüche
    - Möglichkeit der 24/7 Produktion
- Unternehmen nutzen Outsourcing, um sich auf ihre Kernaufgaben zu konzentrieren
- Neue Techniken schaffen neue hochqualifizierte Arbeitsplätze

## Globalisierung - wirtschaftlich fallen nahezu alle Grenzen

- Deutschland ist eine Exportwirtschaft
- Wichtigster Punkt: Konkurrenzfähigkeit bzgl. Qualität, Service, Preis oder Lieferzeit nötig
- Entscheidender Faktor für eine Exportwirtschaft: Die richtige Standortwahl!

## Warum es viele Unternehmen zurück nach Deutschland zieht

- 2004-06: 16% der Unternehmen verlagern die Produktion ins Ausland
- 2007-09: 9% der Unternehmen verlagern die Produktion ins Ausland

Gründe für den Rückgang der Auslagerung der Produktionsstätten und den Rückzug nach Deutschland:

- Zulieferprobleme
- Hohe Transportkosten
- Schlechte Qualifikation bei hohen Personalkosten
- Schlechtere Produktqualität

## Glossar

- Technologisierung: stete Weiterentwicklung der Technik und deren Ausbreitung in alle Lebensbereiche
- Globalisierung: weltweites Netz von Wirtschaftsbeziehungen
- Rationalisierung: Erhöhung der Produktivität eines Betriebs
- Outsourcing: spezialisierte Dienstleitungsunternehmen übernehmen standardisierte und wiederkehrende Aufgaben,
  z.B. Werksschutz, Kantinenbewirtschaftung, Gebäudereinigung, Lohnbuchhaltung und Ingenieursdienstleistungen

## Off-Topic: Standortfaktoren

- Harte Standortfaktoren
    - Kosten (z.B. Lohnkosten, Mieten, Grundstücks und Baupreise, Steuern und Gebühren)
    - Infrastruktur (z.B. Verkehrsanbindung, Gewerbeflächen, Telekommunikationsnetze)
    - Agglomerationsfaktoren (z.B. Nähe zu Beschaffungsmärkten/Lieferanten, Nähe zu Absatzmärkten/Kunden, Nähe zu
      Dienstleistern für Outsourcing)
- Weiche Standortfaktoren
    - Infrastruktur (für Arbeitnehmer: Wohnraumangebot, kulturelles Angebot, Freizeit-, Bildungs- und
      Einkaufsmöglichkeiten)
    - Naherholungswert (landschaftliche Attraktivität, Belastung durch Verkehr und Umweltverschmutzung)
    - Verlässlichkeit des Rechtssystems, Kriminalitäts- bzw. Korruptionsrate
    - kulturelle Offenheit und Toleranz
    - Qualifikation und Motivation der Mitarbeiter
