---
title:  "01. Der Deutsche Staat"
date:   2019-10-10 11:00:00 +0100
---

## Grundlegendes

{{< mermaid class="text-center">}}
graph TD
    A[Demokratie]
    B[Sozialstaat]
    C[Rechtsstaat]
    D[Bundesstaat]
    E[Föderalismus]
    F[Art. 20 GG]
    G[Staatsstruktur-Prinzipien]

    A-->F
    B-->F
    C-->F
    D-->F
    E-->F
    F-->G
{{< /mermaid >}}

$$\rightarrow$$ GG seit 1949

## Deutsch-Deutsche Wegmarken

- 1949: Gründung Bundesrepublik und DDR
- 1953: Volksaufstand in der DDR, Fluchtbewegung
- 1955: Bundesrepublik in die NATO, DDR in den Warschauer Pakt
- 1958: Chruschtschows Berlin-Ultimatum
- 1961: Mauerbau, Grenzbefestigungen, Schießbefehl
- 1964: Zwangsumtausch, Besuchserlaubnis für DDR-Rentner
- 1968: Visumzwang, Erhöhung Zwangsumtausch
- 1970: Brandt in Erfurt, Stoph in Kassel, Ostverträge
- 1971: Viermächteabkommen Berlin, Transitabkommen
- 1972: Verkehrsvertrag, Grundlagenvertrag
- 1973: Beginn grenznaher Verkehr, BRD und DDR in der UNO
- 1974: Ständige Vertretungen
- 1981: Schmidt besucht DDR
- 1984: Kredite für die DDR, Abbau der Selbstschussanlagen
- 1987: Honecker besucht DDR
- 1989: Fluchtwelle, Öffnung der Mauer, Zusammenbruch des SED-Regimes
- 1990: Freie Wahlen in der DDR, Wirtschafts-, Währungs- und Sozialunion
- 3\. Oktober 1990: Vereinigung

## Sieben Regierungsbezirke (Bayerns):

|    |      Name     | Regierungssitz |
|:--:|:-------------:|:--------------:|
| 1. | Oberbayern    | München        |
| 2. | Niederbayern  | Landshut       |
| 3. | Oberpfalz     | Regensburg     |
| 4. | Oberfranken   | Bayreuth       |
| 5. | Mittelfranken | Ansbach        |
| 6. | Unterfranken  | Würzburg       |
| 7. | Schwaben      | Augsburg       |

## Bundesländer mit Landeshauptstädten:

|     |      Name               | Landeshauptstadt |
|:---:|:-----------------------:|:----------------:|
| 1.  | Bayern				    | München          |
| 2.  | Baden-Württemberg	    | Stuttgart        |
| 3.  | Berlin				    | Berlin           |
| 4.  | Brandenburg			    | Potsdam          |
| 5.  | Bremen				    | Bremen           |
| 6.  | Hamburg			        | Hamburg          |
| 7.  | Hessen				    | Wiesbaden        |
| 8.  | Mecklenburg-Vorpommern  | Schwerin         |
| 9.  | Niedersachsen			| Hannover         |
| 10. | Nordrhein-Westfalen		| Düsseldorf       |
| 11. | Rheinland-Pfalz			| Mainz            |
| 12. | Saarland 			    | Saarbrücken      |
| 13. | Sachsen			        | Dresden          |
| 14. | Sachsen-Anhalt			| Magdeburg        |
| 15. | Schleswig-Holstein		| Kiel             |
| 16. | Thüringen			    | Erfurt           |

## Staatsprinzipien Deutschlands

- **Meinungs- und Diskussionsfreiheit**
- **Unabhängigkeit der Justiz**
- **Gesetze**
- **Regelmäßige Wahlen** mit Mehrparteiensystem und politischer Opposition
- **Opposition**: Gegenentwürfe zur aktuellen Regierung, welche aktiv unterstützt wird
- **Föderalismus**: Verteilung der Staatsentscheidungen auf mehrere Ebenen
- **Subsidiarität**

## Staatsaufgaben

| Kernaufgaben      |                                                                                                 |                                                  | weitere Aufgaben                                                                                   |                                                                      |                                                                         |
|-------------------|-------------------------------------------------------------------------------------------------|--------------------------------------------------|----------------------------------------------------------------------------------------------------|----------------------------------------------------------------------|-------------------------------------------------------------------------|
|                   | innere Sicherheit                                                                               | äußere Sicherheit                                | wirtschaftliche und soziale Mindeststandards                                                       | Schutz der natürlichen Lebensgrundlagen                              | effiziente Verwaltung                                                   |
| Was ist geregelt? | 1. Maßnahmen gegen Gewalt, Kriminalität und Angriffen auf das Zusammenleben in der Gesellschaft | Angriffe und Bedrohungen von außen               | Sicherung eines Existenzminimums für die Bürger in allen Lebenslagen $\rightarrow$ "soziales Netz" | Umweltschutz (vgl. Art 20a GG)                                       | Verwaltung soll politische Entscheidungen zügig und effizient umsetzten |
|                   | 2. Schutz des Staates vor Gegnern im Inneren                                                    |                                                  |                                                                                                    |                                                                      |                                                                         |
| Beispiel:         | Zu 1.: Diebstahl eines Handys                                                                   | Diplomatie                                       | Sicherung der Grundbedürfnisse, z.B. durch Arbeitslosengeld                                       | Förderung von alternativen Energiequellen wie Wind- und Solarenergie | Einführung von Bürgerämtern                                             |
|                   | Zu 2.: Radikalenerlass                                                                          | Armee                                            |                                                                                                    |                                                                      |                                                                         |
|                   |                                                                                                 | Mitgliedschaft in internationalen Organisationen |                                                                                                    |                                                                      |                                                                         |
