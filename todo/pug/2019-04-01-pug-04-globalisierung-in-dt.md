---
title:  "04. Globalisierung in Deutschland"
date:   2019-04-01 09:00:00 +0100
---

## Ist Globalisierung gut für Deutschland?

| Ja                                                           | Nein                                                         |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| Durch den Export werden viele Arbeitsplätze in Deutschland geschaffen | Durch Outsourcing ins Ausland gehen viele Arbeitsplätze in Deutschland verloren oder verlieren an Sicherheit |
| Umweltschädliche oder gefährliche Arbeiten können ins Ausland ausgelagert werden. | In anderen Ländern gelten nicht so strenge Umweltauflagen. Umweltverschmutzung und ihre Folgen betreffen letztlich auch Deutschland. |
| Durch Verträge und Abkommen können internationale Standards zum Umweltschutz durchgebracht werden. | Das Ökosystem in Deutschland wird durch neue Arten, z.B. graue Eichhörnchen, bedroht. |
| Deutschland gilt als Motor für Innovation, da es viele hochqualifizierte und gut ausgebildete Fachkräfte gibt. Andere Länder sind also von Deutschland abhängig. | Deutschland wird zunehmend abhängig von aufstrebenden Industrienationen, z.B. technischer Innovationen aus China |
| Der Kontakt zu Menschen aus verschiedenen Ländern hilft, Vorurteile abzubauen. Dadurch könnte es weniger Rassismus geben. | Durch die Globalisierung immigrieren viele Menschen nach Deutschland und die Kulturen vermischen sich. Dadurch könnte Rassismus geschürt werden. |
| Die Währungsunion stärkt den Kurs des Euros.                 | Durch die Währungsunion leidet auch Deutschland bei einer Finanzkrise in einem EU-Land |
| Aus den Beziehungen zu anderen Ländern ergibt sich ein diplomatischer Vorteil: Die Richtlinien werden vereinheitlicht und Verstöße können mit Sanktionen geahndet werden. Z.B. Verstöße gegen die Menschenrechte | Deutschland verliert an Souveränität, da das EU-Parlament viele politische Entscheidungen trifft. Z.B. Produktionsstandards in der Landwirtschaft |
| Gegenseitige Abhängigkeit durch Handelsabkommen sichert den Frieden | Sanktionen der EU bleiben wirkungslos.                       |
| Europaweit gilt die Menschenrechtskonvention. Z.B. gegen Sklaverei und für gerechten Lohn für geleistete Arbeit | In Billiglohnländern werden die Arbeitskräfte ausgebeutet, z.B. Textilarbeiter in Indien. |
| Man kann innerhalb der EU seinen Wohn- und Arbeitssitz frei wählen. | Menschen migrieren innerhalb von Europa aus bestimmten Ländern in bestimmten Ländern z.B. um zu studieren oder arbeiten |
| Der Handel wird durch offene Grenzen begünstigt.             | Preise werden durch Importe von Billigwaren gedrückt, der Wettbewerb wird verschärft. |


