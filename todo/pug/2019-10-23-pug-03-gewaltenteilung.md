---
title:  "03. Gewaltenteilung"
date:   2019-10-17 11:00:00 +0100
---

| Vertikale\Horizontale Gewaltenteilung |       Exekutive      |     Legislative     |                    Judikative                  |
|---------------------------------------|:--------------------:|:-------------------:|:----------------------------------------------:|
|                  Bund                 | Bundesregierung      | Bundestag/Bundesrat | Bundes-Verfassungsgericht/Oberster Gerichtshof |
|                  Land                 | Landesregierung      | Landtag/Senat       | Landgerichte                                   |
|                Kommune                | (Ober-)Bürgermeister | Gemeinde/Stadtrat   | Amtsgerichte                                   |

Anmerkungen:

- Die Bundesregierung hat als unterstützendes Mitglied das Bundeskabinett (Bundeskanzler und Minister)
- Die Landesregierung hat als unterstützendes Mitglied den Ministerpräsidenten mit den Landesministern
- Das Volk wählt den Gemeinde- bzw. Stadtrat und den (Ober-)Bürgermeister
- Der Landtag wählt den Ministerpräsidenten
- Die Landesregierungen entsenden Vertreter in den Bundesrat
- Der Bundestag wählt den Bundeskanzler
- Bundestag und Bundesrat wählen jeweils zur Hälfte die Richter des Bundes-Verfassungsgerichtes
- Die Bundesregierung hat das Initiativrecht für Gesetzgebungen

> Gewaltenverschränkung von Legislative und Exekutive, sowie Einflussnahme durch Wahl der Richter auf die Exekutive
