---
title:  "07. Staatsorgane"
date:   2020-02-06 11:00:00 +0100
---

## Der Bundespräsident

Die Aufgaben

- Nach seiner Stellung
    - Völkerrechtliche Vertretung des Bundes
    - Repräsentation nach innen und außen - Ehrenhoheit
    - Prüfung, Unterzeichnung und Verkündung der Bundesgesetze
    -  Erklärung des Gesetzgebungsnotstands
- Nach dem Grundgesetz
    - Vorschlag, Ernennung und Entlassung des Bundeskanzlers
    - Ernennung und Entlassung der Bundesminister
    - Ernennung und Entlassung der Bundesrichter, Bundesbeamten und Offiziere
    - Begnadigungsrecht
    - Nach dreimalig gescheiterter Kanzlerwahl oder nach einer gescheiterten Vertrauensfrage hat er die Entscheidung zur
      Auflösung des Deutschen Bundestages

Die Wahl

- Voraussetzungen
    - mind. 40 Jahre alt
    - darf keinen anderen Beruf ausführen
    - muss nicht in einer Partei sein, wenn ja muss man das Amt ruhen lassen
    - Deutscher Staatsbürger
- Ablauf/Wähler
    - Wahl auf 5 Jahre
    - direkte Wiederwahl nur einmal möglich
    - im 1. + 2. Wahlgang absolute Mehrheit nötig, beim 3. Wahlgang reicht die einfache Mehrheit
    - gewählt wird von den Bundestagsabgeordneten + gleiche Anzahl an Vertretern der Bundesländer

Der Ehrensold

- jährliches Amtsgehalt: 236000€,m auch nach der Amtszeit
- Aufwandsgeld in Höhe von 78.000€ jährlich, aber nur während der Amtszeit
- jährliches Amtsgehalt nennt sich nach der Amtszeit Ruhebezüge

## Bundesverfassungsgericht

- liegt in Karlsruhe; Gründung: 28.09.1951
- besteht aus zwei Senaten
    - Mit 8 Richtern besetzt
    - Beschlussfähig wenn 6 Richter anwesend sind
- 16 Richter (mind. 40 Jahre + Befähigung zum Richter)<br>
  $$\rightarrow$$ jeder Richter wird von vier wissenschaftlichen Mitarbeitern unterstützt<br>
  $$\rightarrow$$ die Richter werden jeweils zur Hälfte vom Bundestag und Bundesrat gewählt<br>
  $$\rightarrow$$ 2/3 Mehrheit notwendig
- Präsident: Prof. Dr. Dres. h.C. Andreas Voßkuhle

Aufgabe

- Durchsetzung der Grundrechte
- Kann Gesetze als verfassungswidrig erklären
- Einschreiten bei Streit zwischen staatlichen Organen
- Entscheidungen können nicht angefochten werden
- unpolitisch
- wird nur auf Antrag tätig $$\rightarrow$$ Bürger können, sollten sie sich durch die deutsche öffentliche Gewalt in den
  Grundrechten verletzt fühlen, eine Verfassungsbeschwerde einreichen
- BReg, LReg oder 1/4 der Mitglieder des Bundestages können Verfassungsmäßigkeit von einer Rechtsnorm überprüfen lassen
- Parteien verbieten auf Antrag des Bundestages

## Bundesregierung

Bildung der Bundesregierung

- Besteht aus Bundeskanzler und Bundesministern $$\rightarrow$$ Kabinett
- Bundeskanzler wird von Bundespräsident vorgeschlagen
- Bundesminister werden auf Vorschlaf des Bundeskanzlers vom Bundespräsidenten ernannt.

Aktuelle Bundesminister

- Quelle: <https://www.bundesregierung.de/breg-de/bundesregierung/bundeskabinett>
- Horst Seehofer - CSU - Bundesminister des Innern, für Bau und Heimat
- Olaf Scholz - SPD - Bundesminister der Finanzen
- Peter Altmaier - CDU - Bundesminister für Wirtschaft und Energie
- Heiko Maas - SPD - Bundesminister des Auswärtigen
- Annegret Kramp-Karrenbauer - CDU - Bundesministerin der Verteidigung

Aufgaben der Bundesregierung

- Quelle: <http://www.jura-schemata.de/art.-65.htm>
- Art 65 GG
    - Kanzlerprinzip: verleiht dem Bundeskanzler die Kompetenz, die Richtlinien der Politik zu bestimmen.
    - Ressortprinzip: begründet die selbstständige und höchstmögliche Eigenverantwortung der Minister im Rahmen ihrer
      Zuständigkeit. Die Minister haben die Organisationsgewalt für ihre Ressorts.
    - Kollegialprinzip: bestimmt, dass alle wichtigen Fragen im gesamten Kabinett entschieden werden.
- Beispiel mit Veranschaulichung
    - Kanzlerprinzip: Flüchtlingspolitik von Angela Merkel
    - Ressortprinzip: PKW-Maut Vertragsunterzeichnung von Andreas Scheuer (<https://www.welt.de/politik/deutschland/article204497160/Pkw-Maut-Das-eigene-Ministerium-warnte-Andreas-Scheuer.html>)
    - Kollegialprinzip: Grundrente (<https://www.sueddeutsche.de/politik/bundesregierung-neuer-streit-um-die-grundrente-1.4784902>)

## Bundestag

- Sitz in Berlin
- 709 Mandate (gesetzlich 598)
- aufgeteilt in Fraktionen

Funktionen:

- Wahlfunktion
- Gesetzgebung
- Kontrollfunktion
- Budgetfunktion
- Kommunikationsfunktion

Wahlfunktion

- Bundeskanzler
- Bundestagspräsident & Präsidium
- 16 Richter des Bundesverfassungsgerichts
- Bundespräsidenten (durch Bundesversammlung)

Gesetzgebung (Legislative)

- beschließt Gesetze (unter Beteiligung des Bundesrats)

Kontrollfunktion

- Recht die Bundesregierung kontrollieren
- Kontrolle der Nachrichtendienste
- Recht, Regierungsmitglieder zu Beratungen zu zitieren
- Untersuchungsausschüsse
- Regierung per Misstrauensvotum abzurufen

Budgetfunktion

- erstellt Bundeshaushaltsplan
- legt Diäten fest:
- - Orientierung an Gehalt der obersten Bundesrichter
- - etwa 10 000€ Monatlich

Kommunikationsfunktion

- Diskussion politischer Themen
- Lösungsfindung
- Erklärung der Beschlüsse gegenüber der Öffentlichkeit

## Bundesrat

Zusammensetzung

- Setzt sich aus 69 Mitgliedern zusammen.
- Die Vertreter der Bundesländer hängen von dessen Einwohnerzahl ab (Mindestens 3, höchstens 6)
- Keine Wahl durchs Volk, sondern über die Landesregierung

Aufgaben

- Aktive Mitbestimmung Gesetzgebung
- Anliegen der Länder vertreten, aber auch
- Bedürfnisse des Bundes wahren

Gesetzgebung

- Mitwirkung bei jedem Gesetz des Bundestags
- Einspruchs- & Zustimmungsgesetze
- Einspruchsgesetz:
    - Einfluss geringer als bei Zustimmungsgesetz $$\rightarrow$$ Kann durch Bundestag überstimmt werden
   	- Bundesrat mit absoluter Mehrheit kann nur überstimmt werden, wenn Bundestag auch die absolute Mehrheit erlangt
- Zustimmungsgesetze:
    - Gesetze, die die Verfassung verändern
   	- Gesetze, die in bestimmter Weise Auswirkungen auf die Finanzen der Länder haben
   	- Gesetze, für deren Umsetzung in die Organisations- & Verwaltungshoheit der Länder eingegriffen wird


