---
title:  "09. Soziales Netz"
date:   2019-06-24 11:00:00 +0100
---

## Was heißt eigentlich sozial?

Das Zusammenleben der Menschen in der Gesellschaft betreffend; Mitglieder einer Gesellschaft sind wie in einem Vertrag
miteinander verbunden.

## Die vier Säulen der Sozialpolitik

| Generationenvertrag                             | Äquivalenzprinzip                                            | Solidarität                                                  | Subsidiarität                                                |
| ----------------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Junge arbeitende Leute zahlen Rente der Älteren | Je Länger man arbeitet und je mehr man verdient, desto höher ist die Rente | Mit den Beiträgen aller Versicherten werden die notwendigen Leistungen jedes Einzelnen finanziert | Die nächsthöhere Instanz wird erst dann aktiv, wenn die untere Ebene mit der Aufgabenerfüllung   überfordert ist. |

## Prinzipien der sozialen Sicherung

| Leistungen nach dem ...  | Versicherungsprinzip                                         | Versorgungsprinzip                                           | Fürsorgeprinzip                         |
| ------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | --------------------------------------- |
|                          | Einer für alle, alle für einen                               | Alle belohnen eine für besondere Leistungen                  | Alle helfen dem Einzelnen in Not        |
| durch die ...            | Sozialversicherung                                           | öffentliche Versorgung                                       |                                         |
| erhalten ...             | Mitglieder der Sozialversicherung, wenn sie Beiträge gezahlt haben | bestimmte Bevölkerungsgruppen<br>wenn sie besondere Opfer oder Leistungen für die Gemeinschaft erbracht haben | Alle Bürger<br>wenn sie bedürftig sind. |
| finanziert durch die ... | Versicherungsbeträge u. staatliche Zuschüsse                 | Steuermittel<br>Bsp. Invalidenrente, Verdienstorden          | Steuermittel<br>Bsp.: AG II (Hartz IV)  |

Ziel: Soziale Gerechtigkeit (Gleiche Chancen für alle, gesellschaftlich aufzusteigen)
