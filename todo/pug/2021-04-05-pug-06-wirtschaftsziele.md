---
title:  "06. Wirtschaftsziele"
date:   2020-12-12 06:00:00 +0100
---

## Ziele staatlicher Wirtschaftspolitik

Unter dem Eindruck der Rezession von 1966/67 entstand das Stabilitäts- und Wachstumsgesetz.
Vier Wirtschaftsziele sind so zu steuern, dass sich eine stetige Mehrung des materiellen Wohlstands
einstellt. Später kamen die Ziele 5 und 6 hinzu.

> § 1 Gesetz zur Förderung der Stabilität und des Wachstums der Wirtschaft (1967) Bund und Länder haben bei ihren
> wirtschafts- und finanzpolitischen Maßnahmen die Erfordernisse des  gesamtwirtschaftlichen Gleichgewichts zu beachten.
> Die Maßnahmen sind so zu treffen, dass sie im Rahmen der marktwirtschaftlichen Ordnung gleichzeitig zur Stabilität des
> Preisniveaus, zu einem hohen Beschäftigungsstand und außenwirtschaftlichem Gleichgewicht bei stetigem und angemessenem
> Wirtschaftswachstum beitragen.

Das oben abgedruckte Gesetz hat nun folgende Ziele:

1. Stabilität des Preisniveaus
2. hoher Beschäftigungsstand
3. außenwirtschaftliches Gleichgewicht
4. angemessenes Wirtschaftswachstum

Erweiterungen der Ziele sind die folgenden beiden:

5. Umweltschutz
6. gerechte Einkommens- und Vermögenverteilung

Wie werden oben angeführte Ziele nun gemessen:

| Ziel | Maßgrößen / Maßnahmen |
| --- | --- |
| Hoher Beschäftigungsstand | --> Jeder der arbeiten kann, soll eine Arbeit haben |
| | Berechnung anhand der Arbeitslosenquote. (Ziel: Arbeitslosenquote unter 3 Prozent) |
| | $Arbeitslosenquote = (Zahl der Arbeitlosen / Zahl der Erwerbstätigen-Arbeitslosen) * 100$ |
| angemessenes Wirtschaftswachstum | --> Zunahme des Angebotes von Gütern und Dienstleistungen |
| | Beurteilung anhand des BIP (Bruttoinlandsprodukt) |
| | Angestrebt wird ein jährliches Wachstum des BIP von 3-4% |
| Stabilität des Preisniveaus | -->  Preise sollen bleiben im Durchschnitt und verändert bleiben (Inflationsrate unter 2%) |
| | Beurteilung der Entwicklung der Preise anhand eines Warenkorbes |
| außenwirtschaftliches Gleichgewicht | --> Ausgeglichenes Verhältnis zwischen Import und Export (Einfuhren und Ausfuhren) |
| | Gemessen wird hier die Differenz zwischen Exporteinnahmen und Importausgaben. |
| | Ziel: 1-2% mehr Export als Import (positiver Außenbeitrag) |
| Umweltschutz | Ziel: Erhaltung einer lebenswerten Umwelt / Energiewandel |
| | Maßnahmen: Umstieg auf erneuerbare Energien, Plastikverbot etc. |
| | Messgröße??? |
| gerechte Einkommens- und Vermögenverteilung | Ziel: Verkleinerung der Schere zwischen Arm und Reich |
| | Maßnahmen: Mindestlohn, Sozialsystem (finanziert durch Steuern) |
| | Messgröße??? |

Die folgenden Ziele werden als magisch angesehen, weil zwischen allen Zielen komplexe Zielbeziehungen bestehen. Alle
Ziele gleichzeitig zu erreichen ist nahezu unmöglich und grenzt an "Magie".

![PuG - Magisches Sechseck](/assets/img/pug/06-wirtschaftsziele-1.png)

## Preisniveaustabilität

Der Warenkorb als Messgröße zur Ermittlung des Preisniveaus

Preisniveaustabilität herrscht, wenn der Durchschnitt der Preise konstant bleibt. Einzelne Preise können dabei durchaus
schwanken. In entwickelten Volkswirtschaften gibt es mehr als 10 Millionen Güter, die von verschiedenen Unternehmen und
in verschiedenen Regionen zu verschiedenen Preisen angeboten werden. Für die Ermittlung des Preisniveaus muss, um dieser
Datenfülle Herr zu werden, eine statistische Zahl ermittelt werden, die noch aussagekräftig ist.

Das Preisniveau wird mithilfe des Preisindex für Lebenshaltung gemessen. Dazu wird vom Statistischen Bundesamt aus der
Fülle der Waren und Dienstleistungen eine repräsentative Anzahl von ca. 700 Güterarten getroffen, die den sogenannten
"Warenkorb" darstellen. Die ausgewählten Waren bilden das statistisch durchschnittliche Konsumverhalten aller Haushalte
in der Bundesrepublik Deutschland ab. Um dies festzulegen, wird in repräsentativ ausgewählten Haushalten das
Konsumverhalten untersucht und statistisch erfasst. Dazu führen diese Haushalte Haushaltsbücher. Um Erfassungsfehler zu
minimieren, werden die erfassten Daten mit verschiedenen Statistiken, z.B. Verbraucherstatistiken, verglichen und
entsprechend korrigiert.

Für die Berechnung des monatlichen Verbraucherindexes werden in 188 Gemeinden, die über das gesamte Bundesgebiet
verteilt sind, Preisermittlungen durchgeführt. Monatlich werden in verschiedenen Unternehmen, die nach Geschäftstyp
unterteilt sind, etwa 350.000 Einzelpreise erhoben und dann mit ihrer Gewichtung im Warenkorb entsprechend bewertet. Ein
beobachtetes Produkt ist dann z.B. eine bestimmte Sorte Milch, ungeachtet dessen, dass es viele verschiedene Sorten und
Hersteller dieses Produktes gibt. Wenn ein Produkt, dass einmal für die Preisbeobachtung ausgewählt wurde, nicht mehr
ausreichend oft nachgefragt wird, wird es gegen ein anderes der gleichen Gütergruppe ausgetauscht. Anhand der erhobenen
Preise wird die Preisveränderung - die Inflationsrate - gemessen. Dabei gilt das Ziel der Preisniveaustabilität als
erreicht, wenn die Preissteigerungsrate gegenüber dem Vorjahr unter 2% liegt.

![PuG - Warenkorb](/assets/img/pug/06-wirtschaftsziele-2.png)
