---
title:  "04. Staatsstrukturprinzipien"
date:   2019-10-17 12:00:00 +0100
---

## Demokratie

bedeutet wörtlich übersetzt "Sache der Allgemeinheit" und meint eine Staatsform, in der ein
Staatsoberhaupt gewählt wird - im Gegensatz zu einer Monarchie, in der das Amt des Staatsoberhaupts in der Regel von
einem König auf die Erben übergeht, also vererbt wird.

## Republik

heißt, dass die Staatsgewalt vom Volke ausgehen soll. Durch Wahlen und Abstimmungen wird diese Staatsgewalt ausgeübt,
bzw. Repräsentanten, auf Zeit übertragen, die im Auftrag des Volkes politische Entscheidungen treffen sollen.

## Sozialstaat

bedeutet, dass die Gesetzgebung (Parlamente), die Ausführung der Gesetze (Regierung und Verwaltung) und Rechtssprechung
(Gerichte) von verschiedenen, voneinander unabhängigen Personen und Personengruppen durchgeführt werden soll.

## Bundesstaat/Föderalismus

bezeichnet allgemein die Vereinigung souveräner (selbstständiger) Staaten zu einem Bund, auf den bestimmte Rechte und
Aufgaben übertragen werden. In der Bundesrepublik Deutschland haben deshalb die Bundesländer einerseits eigene
Länderparlamente, -regierungen und -gerichte, andererseits wirken sie über den Bundesrat an der Bundespolitik mit. Man
nennt dieses Organisationsprinzip auch Föderalismus.

## Rechtsstaat

verpflichtet den Staat, die sozialen (gesellschaftlichen) Verhältnisse zu gestalten. Dazu gehören wirtschaftspolitische
Aktivitäten, um z.B. die Entwicklung in der Wirtschaft zu ermöglichen oder Arbeitslosigkeit abzubauen. Ebenso müssen
soziale Maßnahmen ergriffen werden, um in Not geratenen Bürgern das Existenzminimum zu sichern. Mit
Vorsorgeeinrichtungen sollen die Menschen im Alter, bei Krankheit oder Unfällen geschützt werden
(Sozialversicherung, Renten).

## Gewaltenteilung

besagt, dass die staatliche Gewalt an die Verfassung und Rechtssprechung gebunden ist. Alle Maßnahmen der Staatsorgane
können von unabhängigen Richtern überprüft werden. Voraussetzungen für dieses Prinzip sind die in der Verfassung
zugesicherten Grundrechte (Freiheitsrechte), die Gewaltenteilung und die Gesetzmäßigkeit der Verwaltung.

## Sonstiges

### Art. 1 GG - die Menschenwürde

### Art. 79 (3) GG - Die Ewigkeitsklausel

### Subsidiaritätsprinzip

Aufgaben werden auf der kleinstmöglichen Ebene versucht zu lösen und erst dann eskaliert auf die nächsthöhere Ebene.

Bsp.: Gerichte: Amtsgericht $$\rightarrow$$ Landesgericht $$\rightarrow$$ Bundesverfassungsgericht(BVerfG)
$$\rightarrow$$ EU-Gerichtshof (EUGH)
