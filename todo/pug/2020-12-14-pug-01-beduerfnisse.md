---
title:  "01. Bedürfnisse"
date:   2020-11-10 06:00:00 +0100
---

Definition: Unter Bedürfnissen versteht man das Gefühl des Mangels, verbunden mit dem Wunsch oder der Notwendigkeit,
diesen  Mangel zu beseitigen.

| Existenzbedürfnisse                        | Kulturbedürfnisse                                                         | Luxusbedürfnisse                                                        |
|--------------------------------------------|---------------------------------------------------------------------------|-------------------------------------------------------------------------|
| Befriedigung lebensnotwendig               | Beinflusst von unserer kulturellen Umgebung                               | Nicht von allen Menschen als Mangel wahrgenommen                        |
|                                            | Entsteht erst sobald Existenzbedürfnisse gedeckt sind                     |                                                                         |
| Bsp.: Trinken, Essen, Schlaf, Wärme, Beruf | Bsp.: Netflix, Informationen/Nachrichten, Schokolade, Auto, Fahrrad, ÖPNV | Bsp.: Schnelles Internet, Schmuck, Markenklamotten, Videospiele, Urlaub |

Weitere Unterscheidung: Individualbedürfnisse und Kollektivbedürfnisse

![PuG - Maslowsche Bedürfnispyramide](/assets/img/pug/01-beduerfnispyramide-maslow.jpg)

Quelle der Grafik: <https://karrierebibel.de/beduerfnispyramide-maslow/>
