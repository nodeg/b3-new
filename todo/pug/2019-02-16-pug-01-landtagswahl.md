---
title:  "01. Landtagswahlen"
date:   2019-02-11 06:00:00 +0100
---

## Landtagswahl

Jeder hat bei einer Landtagswahl 2 Stimmen. Eine Erst- und eine Zweitstimme.

Die Erststimme ist für den Direktkandidaten im Stimmkreis, um zu gewinnen, reicht eine relative Mehrheit. Die
Zweitstimme wird für eine Partei abgegeben bzw. für einen Listenkandidaten.

Aktuell sind im Landtag in Bayern 91 der Sitze für Direktkandidaten vergeben und 89 Sitze für Listenkandidaten. Damit
eine Partei im Landtag einzieht benötigt sie 5 % der Stimmen, diese können sich aus Erst- und Zweitstimmen beliebig
zusammen setzen.

## Landtagswahl 2018 Bayern

### Wählerbewegungen

1. AfD = Alternative für Deutschland
   - Erfolg wegen zentralen Themen Zuwanderung, Angst vor Islamisierung, kulturelle Identität
   - Vor allem von CSU- und Nichtwählern gewählt, aber auch von der SPD
2. CSU = Christsoziale Union
   - -0,5 Mio. Stimmen: Etwa an die 50 % an die Freien Wähler (FW) und 50 % an die Grünen verloren
   - Strategisches Dilemma: 50 % der Wähler haben Angst vor Islamisierung und Verlust der kulturellen Identität, 50 %
     nicht.
3. Bündnis 90/Die Grünen
   - Zentrale Themen: Umwelt und liberale Zuwanderungsgesetze
4. SPD = Sozialdemokratische Partei Deutschlands
   - Identität? Thema?
   - Thema "soziale Gerechtigkeit" verwaschen
   - Starke Forderung von SPD-Wähler, dass sich die Partei in der Opposition erneuern soll.

### Wahlanalyse

1. Nach Bildungsabschluss<br>
   Je höher der Bildungsabschluss, desto weniger Stimmen für CSU (Zu wenig Veränderung bzw. Kurswechsel) und AfD (Je
   höher die Bildung, desto weniger anfällig für populistische Parteien) und desto mehr Stimmen für Grüne (Fokus auf
   Umweltschutz $$\rightarrow$$ eher eine Sorge von wirtschaftlich besser Situierten; Zuwanderungspolitik schreckt
   Menschen mit Abstiegsängsten ab) und FDP ("Unternehmerpartei" mit Besserverdienern als Zielgruppe)
2. Nach Geschlecht<br>
   AfD doppelt so viele Stimmen von Männern als von Frauen, weil AfD traditionelle Geschlechterrollen vertritt
   ($$\leftrightarrow$$ Emanzipation, neue Konzepte von Familie und Partnerschaft)
3. Nach Alter
   - 60+: am wenigsten AfD (kein Vertrauen in eine neue Partei) und am meisten CSU (Stammwähler $$\rightarrow$$
     Abwanderung für SPD, regionales Engagement und Verwurzelung) $\rightarrow$ historisches Bewusstsein
   - 18-29: die meisten Menschen wählen CSU, die wenigsten SPD (kein Vertrauen in die "Volkspartei"), AfD drittstärkste
     Partei (Wunsch nach Veränderung)
