---
title:  "02. Die Ausbildung"
date:   2019-02-11 06:05:00 +0100
---

## duale Ausbildung

| duale Ausbildung/betriebliche Ausbildung                     | schulische Ausbildung                                        | kooperative Ausbildung                                       |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Die Ausbildung findet an zwei Ausbildungsorten statt, im Betrieb und in der Berufsschule | Die Ausbildung findet nur in der Schule statt                | Die Ausbildung ist aufgeteilt zwischen Berufsschule und einem anderen pädagogischen Institut |
| Fachinformatiker, KFZ-Mechatroniker                          | Krankenpfleger, Physiotherapeut, medizinisch-technischer Assistent | Statt mit einem Betrieb wird der Ausbildungsvertrag mit einem Bildungsträger (z.B. regionales Ausbildungszentrum oder eingetragener Verein) abgeschlossen. |

- Vorteile der dualen Ausbildung:
  - Berufsschule: Theoretisches Fachwissen (im Fachunterricht) und Allgemeinwissen (z.B. in Ethik, Englisch, Deutsch)
  - Betrieb: Fachpraxis<br>
  $$\rightarrow$$ Ziel: Erwerb von Schlüsselqualifikationen: z.B. Sozialkompetenz, Kritikfähigkeit,
  Verantwortungsbewusstsein, Kreativität
- Ausbildungsordnungen legen die Ausbildungsinhalte fest und gelten deutschlandweit
- Mit erfolgreichem Abschluss der 12. bzw. 13. Klasse kann man die mittlere Reife erwerben (Berufsschule) benötigt wird
  ein Notendurchschnitt im Abschlusszeugnis von 3,0 oder besser und in Englisch die Note 4 oder besser

## Schulpflicht in Deutschland

- 9 Jahren Vollzeitschulpflicht
  - Mittelschule, Mittelschuldabschluss oder Quali
  - Mit mittlerer Reife sind Leute berufsschulpflichtig, wenn sie eine Ausbildung anfangen.
  - Mit Fachabitur ist man nicht berufsschulpflichtig, sondern berufsschulberechtigt: Man kann in die BS gehen, muss
    aber nicht (außer sind im Ausbildungsvertrag festgelegt).
- 3 Jahre Berufsschule/Teilzeitschule
  - bzw. kürzer bei schulischer Ausbildung oder
  - BVJ/BIJ (= Berufsvorbereitungs-/-integrationsjahr) oder
  - JOA ( = Jugendliche ohne Ausbildungsplatz)<br>
    $$\rightarrow$$ mit abgeschlossener Berufsausbildung oder erfolgreichem Besuch von BIG/BVJ oder JOA gilt die
    BS-Pflicht als erfüllt.
- Wer ist nicht berufsschulpflichtig?
  - (Fach-)Abitur
  - 21\. Lebensjahr vollendet (BS-Pflicht endet zu Beginn des nächsten Schuljahres)
  - Freiwilliges soziales oder ökologisches Jahr
  - Abgeschlossene Ausbildung oder BVJ/BIJ oder JOA
  - Vorbereitungsdienst, z.B. Polizei, Bundeswehr
- Organisation des BS-Unterrichts:
  - Blockunterricht: Wochenweise abwechselnd im Betrieb und in der BS z.B. 2 Wochen BS, 6 Wochen Betrieb
  - Einzelunterricht: Berufsschulunterricht findet an ein oder zwei Tagen in der Woche statt

## der Ausbildungsvertrag

- Gehalt (abh. Von Ausbildungsjahr)
- Adressen von Betrieb und Auszubildendem/r
- Urlaubstage
- Ausbildungsberuf (+Fachrichtung) + Dauer
- Arbeitsstunden pro Tag und Woche
- Unterschrift
- Probezeitdauer
- Name des Ausbilders
- Kündigungsbestimmungen
- Informationen zur sachlichen und zeitlichen Gliederung der Ausbildung
- Ergänzende Ausbildungsmaßnahmen, z.B. Erwerb eines Zertifikates, Weiterbildung

## Pflichten während der Ausbildung - das BBiG

### offizielle Paragrafen

#### Berufsbildungsgesetz (BBiG) § 13 Verhalten während der Berufsausbildung

Auszubildende haben sich zu bemühen, die berufliche Handlungsfähigkeit zu erwerben, die zum Erreichen des
Ausbildungsziels erforderlich ist. Sie sind insbesondere verpflichtet,

- 1\. die ihnen im Rahmen ihrer Berufsausbildung aufgetragenen Aufgaben sorgfältig auszuführen,
- 2\. an Ausbildungsmaßnahmen teilzunehmen, für die sie nach § 15 freigestellt werden,
- 3\. den Weisungen zu folgen, die ihnen im Rahmen der Berufsausbildung von Ausbildenden, von Ausbildern oder
  Ausbilderinnen oder von anderen  weisungsberechtigten Personen erteilt werden,
- 4\. die für die Ausbildungsstätte geltende Ordnung zu beachten,
- 5\. Werkzeug, Maschinen und sonstige Einrichtungen pfleglich zu behandeln,
- 6\. über Betriebs- und Geschäftsgeheimnisse Stillschweigen zu wahren,
- 7\. einen schriftlichen oder elektronischen Ausbildungsnachweis zu führen.

#### Berufsbildungsgesetz (BBiG) § 14 Berufsausbildung

(1) Ausbildende haben

- 1\. dafür zu sorgen, dass den Auszubildenden die berufliche Handlungsfähigkeit vermittelt wird, die zum Erreichen des
  Ausbildungsziels erforderlich ist, und die Berufsausbildung in einer durch ihren Zweck gebotenen Form planmäßig,
  zeitlich und sachlich gegliedert so durchzuführen, dass das Ausbildungsziel in der vorgesehenen Ausbildungszeit
  erreicht werden kann,
- 2\. selbst auszubilden oder einen Ausbilder oder eine Ausbilderin ausdrücklich damit zu beauftragen,
- 3\. Auszubildenden kostenlos die Ausbildungsmittel, insbesondere Werkzeuge und Werkstoffe zur Verfügung zu stellen,
  die zur Berufsausbildung und zum Ablegen von  Zwischen- und Abschlussprüfungen, auch soweit solche nach Beendigung
  des Berufsausbildungsverhältnisses stattfinden, erforderlich sind,
- 4\. Auszubildende zum Besuch der Berufsschule anzuhalten,
- 5\. dafür zu sorgen, dass Auszubildende charakterlich gefördert sowie sittlich und körperlich nicht gefährdet werden.

(2) Ausbildende haben Auszubildende zum Führen der Ausbildungsnachweise nach § 13 Satz 2 Nummer 7 anzuhalten und diese
regelmäßig durchzusehen. Den Auszubildenden ist Gelegenheit zu geben, den Ausbildungsnachweis am Arbeitsplatz zu
führen.

(3) Auszubildenden dürfen nur Aufgaben übertragen werden, die dem Ausbildungszweck dienen und ihren körperlichen
Kräften angemessen sind.

### Pflichten eines Auszubildenden:

1. Sorgfältig arbeiten
2. Berufsschule besuchen, lernen und Prüfungen ablegen
3. Auf Ausbildende hören
4. Regeln/Hausordnung der BS/des Betriebs beachten
5. Keinen Schaden anrichten
6. Verschwiegenheit
7. Berichtsheft führen

### Pflichten des Ausbilders:

1. Erwerb notwendiger Kenntnisse und Fähigkeiten sicherstellen
2. Eine Verantwortliche für die Ausbildung festlegen
3. Materialien kostenlos für Verfügung stellen
4. Azubi für BS freistellen
5. Azubi charakterlich und körperlich schützen
