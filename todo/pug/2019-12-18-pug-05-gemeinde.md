---
title:  "05. Die Gemeinde"
date:   2019-12-18 10:00:00 +0100
---

Die Gemeinde ist die kleinste politische Einheit!

Man unterscheidet bei den Aufgaben von Gemeinden freiwillige Aufgaben, Pflichtaufgaben und übertragene Aufgaben.

- Freiwillige Aufgaben: Kultur-, Sport- und Freizeiteinrichtungen, Suchtberatungsstellen
- Pflichtaufgaben: Wasser-, Strom-, Gasversorgung, Bestellung von Grund- und Hauptschulen
- Übertragene Pflichtaufgaben: Ausstellung von Personalausweisen, Lohnsteuerkarten, Durchführung von Wahlen

Kommunen genießen ein Selbstverwaltungsrecht:

- Personalhoheit: das Recht, Personal einzustellen und zu entlassen
- Planungshoheit: das Recht, z.B. Flächenentzug und Bebauungspläne auszustellen
- Finanz- und Steuerhoheit: das Recht, einen Haushaltsplan auszustellen, sowie Steuern und Abgaben zu erheben

Die Gemeindeaufgaben werden finanziert durch:

- Steuern (z.B.: Anteile an Lohn-)
- Gebühren (z.B.: Müllentsorgung, Parkhausnutzung, Eintrittsgebühr)
- Kostenbeiträge (z.B.: Straßen, Kanal-, TODO: weitere Beispiele)

Die Landesregierungen bieten den Gemeinden Hilfe über das Konnexitätsprinzip an. Was bedeutet dies?

Stärkung Mitspracherecht bei Gesetzentwürfen, von denen Gemeinde unmittelbar betroffen sind. IN Einzelfällen dürfen
höhere Schulden machen.

Die Aufgaben im Folgenden sind Beispielhaft aufgeführt:

Bund:

- Auslandsbeziehungen, Verteidigung, Abwehr des internationalen Terrorismus
- Währungs-, Geld- und Münzwesen
- Straßenverkehrsordnung
- Bau von Autobahnen

Land

- Schulgesetze
- Ladenschluss- und Gaststättenrecht
- Polizeirecht

Kommunen

- Müllabfuhr, Versorgung mit Strom, Gas und Wasser
- Bau von Schulgebäuden
- Feuerwehr, Rettung
- Betrieb von Schwimmbädern, Sportanlagen, Bibliotheken
