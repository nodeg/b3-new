---
title:  "02. Verträge und Verbraucherschutz"
date:   2020-11-15 06:00:00 +0100
---

## Gründe für den Verbraucherschutz

![PuG - Verbraucherschutz 01](/assets/img/pug/02-vertraege-verbraucherschutz-1.png)

![PuG - Verbraucherschutz 02](/assets/img/pug/02-vertraege-verbraucherschutz-2.png)

Vielen Verbrauchern mangelt es an der Fachkompetenz, um Qualität und Standards ähnlicher Produkte objektiv zu
beurteilen, Gerätebeschreibungen und Vertragsbedingungen zu verstehen oder auch ihre Rechte durchzusetzen. Deswegen
schützt der Gesetzgeber die Konsumenten durch eine Vielzahl von gesetzlichen Regelungen, die im bürgerlichen Gesetzbuch
(BGB) zusammengefasst sind. Darüber hinaus können sich Konsumenten mit ihren Fragen an die Fachleute der
Verbraucherzentralen wenden.

## Haustürgeschäfte

Um den Verbraucher vor übereilt geschlossenen Verträgen auf öffentlichen Plätzen, bei "Kaffeefahrten" oder am
Arbeitsplatz im Nachhinein noch zu schützen, besteht ein Recht auf Widerruf oder auf Rückgabe der Ware.

- Das Widerrufsrecht (§355 BGB) wird vom Kunden durch dessen zweite Unterschrift im Vertrag bewusst zur Kenntnis
  genommen. Dem Unternehmen kann der Widerruf unbegründet innerhalb von zwei Wochen in Textform oder durch Rücksendung
  der Ware erklärt werden. Die Widerspruchsfrist beginnt mit dem Zeitpunkt der "gesondert erfolgten Unterschrift".
- Das Rückgaberecht (§356 BGB) kann anstelle des Widerrufsrechts eintreten, wenn vertraglich ausdrücklich auf dieses
  Recht verwiesen wird. Die Rückgabefrist beträgt ebenso zwei Wochen und beginnt zu dem Zeitpunkt, zu dem die Ware beim
  Kunden angeliefert wurde. Eine Begründung für die Rückgabe ist nicht erforderlich.

## Fernabsatzgeschäfte

Ein Fernabsatzgeschäft liegt vor, wenn die Anbahnung un der Abschluss eines Vertrags über zu erbringende
Dienstleistungen oder zu liefernde Waren über ein Fernkommunikationsmittel - z.B. E-Mail, Internet, SMS, Telefax,
Telefon, Rundfunk, Telemedien, Brief, Katalog - erfolgen. Der Kunde muss vor Vertragsabschluss vom Vertragspartner über
die wesentlichen Vertragsinhalte (s. Zahlenbild) informiert werden. Auch hier gilt das Widerrufs- und Rückgaberecht.

## Online-Shoppen ohne Risiko

Wer bei Unternehmen im Internet Waren bestellt, kann diese innerhalb von zwei Wochen nach Erhalt der Ware ohne Angabe
von Gründen zurückschicken (Widerrufsrecht).

Weißt das Unternehmen nicht ausreichend und eindeutig auf diese Rechte hin, verlängert sich die Rückgabefrist.

Ausgenommen sind:

- verderbliche Waren
- individuell nach Wunsch gefertigte Artikel
- Verträge über Pauschalreisen
- CDs, DVDs, Videos und Software deren Siegel geöffnet wurde

Ab einem Bestellwert von 40 Euro geht der Rücktransport auf Kosten und Risiko des Händlers. Der Kunde ist allerdings
verpflichtet, die Ware richtig einzupacken.

Achtung:

- Vor der Rücksendung darf die Ware nur getestet, nicht genutzt werden; sonst kann der Lieferant Wertersatz verlagen!
- Kein Rückgaberecht gibt es bei Käufen von Privatanbietern.

Treten Schäden innerhalb des ersten halben Jahres auf, kann die Ware in der Regel auch zu einem anderen Zeitpunkt
zurückgegeben werden (Reklamation). Transportschäden müssen sofort geltend gemacht werden.

Bezahlung am besten per Rechnung nach Erhalt der Ware oder mittels Einzugsermächtigung (kann rückgängig gemacht werden).

## Allgemeine Geschäftsbedingungen

Das "Kleingedruckte" auf der Rückseite von Vertragsformularen regelt die allgemein gültigen Bedingungen, zu denen der
Vertrag abgeschlossen wird, z.B. Liefer- und Zahlungsbedingungen, Eigentumsvorbehalt, Erfüllungsort, Gerichtsstand,
Garantievereinbarungen. Es dürfen nur solche Bedingungen gesetzt werden, die den Kunden nicht unangemessen
benachteiligen. Nicht zulässig sind bspw. eine spätere Preiserhöhung oder der Ausschluss von Gewährleistungen. Der
Verkäufer muss den Kunden ausdrücklich auf die AGB hinweisen.

## Produkthaftungsgesetz

Nach dem Produkthaftungsgesetz haftet der Hersteller oder auch Lieferant für Folgeschäden, die durch den Ge- oder
Verbrauch eines fehlerhaften Produktes entstehen. Der Hersteller ist verpflichtet, nur solche Produkte in den Verkehr zu
bringen, die dem aktuellen Stand der Technik entsprechen. Dabei ist nicht das Herstelldatum, sondern der
Auslieferungszeitpunkt maßgebend. Die Haftungspflicht besteht zehn Jahre ab der Auslieferung. Ausgenommen sind
Arzneimittel und landwirtschaftliche Produkte.

## Handlungsfähigkeit für Rechtsgeschäfte

> WE = Willenserklärung

- Geschäftsunfähigkeit
  - §104 Ziff. 1,2
  - Geburt bis 7 Jahre
  - Die Folge ist:
      - Abgabe einer WE: immer unwirksam (§105 Abs. 1)
      - Zugang einer WE: immer unwirksam (§131 Abs. 1)
- Beschränkte Geschäftsfähigkeit
  - §§106, 2
  - 7-18 Jahre
  - Die Folge ist:
    - Abgabe einer WE: teils, teils (§§107, 108, 110, 111, 112, 113)
    - Zugang einer WE: teils, teils (§131 Abs. 2 mit 1)
- Geschäftsfähigkeit
  - §104 Ziff. 1,2
    - av 18 bis Tod
    - Die Folge ist:
      - Abgabe einer WE: immer wirksam
      - Zugang einer WE: immer wirksam

"Taschengeldparagraf"

Werden Rechtsgeschäfte von Minderjährigen ohne vorherige Zustimmung der Eltern  abgeschlossen, dann ist der Vertrag
schwebend unwirksam, d. h., er ist entweder  unwirksam bei Ablehnung durch die Eltern oder er wird wirksam bei
nachträglicher Genehmigung. Minderjährige dürfen Geschenke auch ohne Zustimmung der Eltern annehmen (Schenkung ist ein
Vertrag) sowie Kaufverträge mit dem ihnen zur  Verfügung gestellten Geld abschließen, wobei Barzahlung Bedingung ist
(§ 110 BGB = Taschengeldparagraf).
