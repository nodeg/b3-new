---
title:  "99. Methoden"
date:   2019-04-01 12:00:00 +0100
---

## Karikatur richtig interpretieren

1. Schritt: Beschreibung
	- So beschreiben, dass jemand, der die Karikatur nicht sieht, sie sich vorstellen kann
    - Bildunterschrift nicht vergessen!
2. Schritt: Interpretation
    - Was bedeutet das, was Sie sehen?
    - Was möchte der Karikaturist kritisieren?
3. Schritt: Beurteilung
	- Stimmen Sie der Aussage der Karikatur zu? Warum (nicht)?
	- (Welche weiteren Problemfelder eröffnet das Thema der Karikatur?)

