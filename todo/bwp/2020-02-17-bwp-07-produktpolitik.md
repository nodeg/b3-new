---
title: "08. Produktpolitik"
date: 2020-02-17 15:15:00 +0200
---

## Produktplanungsprozess

Die Entwicklung eines neuen oder modifizierten Produktes vollzieht sich in mehreren Phasen. Man spricht in diesem
Zusammenhang auch vom Produktplanungsprozess.

Der erste Schritt ist die Suche nach neuen Produktideen. Neue Produktideen können durch Ideensammlung und/oder
Ideenproduktion entstehen. Unter Ideensammlung ist die systematische Erfassung von Produktideen gemeint. Hierbei kann
es sich um Ideen der Wettbewerber oder aber um Anregungen der eigenen Mitarbeiter handeln, z.B. aus den Abteilungen:
Konstruktion, Marketing, Verkauf, Reklamation, Kundenservice oder dem betrieblichen Vorschlagswesen. Auch die
Ergebnisse der in Großunternehmen vorhandenen Forschungs- und Entwicklungsabteilungen spielen eine große Rolle. Die
Ideenproduktion meint die gezielte "Produktionu neuer Ideen durch spezielle Kreativitätstechniken, wie z.B. dem
Brainstorming, Brainwriting, der Methode 6-3-5 und der Morphologie (siehe auch Kreativitätstechniken im ABC des
Marketings).

Im Anschluss daran, sind diejenigen Produktideen auszuwählen, die weiterverfolgt werden sollen (Screening-Phase). Diese
Phase erfolgt in zwei Schritten. Im Rahmen der Grobauswahl werden zunächst diejenigen Vorschläge ausgesondert, die nicht
mit den Unternehmenszielen oder dem Unternehmensimage vereinbar sind. Zudem müssen die Marktchancen, das benötigte
Know-how, der Investitionsbedarf und fertigungstechnische Gesichtspunkte berücksichtigt werden. Die verbleibenden
Produktvorschläge werden nun einer genaueren Prüfung (Feinauswahl) unterzogen. So wird im Rahmen einer
Wirtschaftlichkeitsanalyse, z.B. einer Break-even-Analyse, die Absatzmenge ermittelt, die erforderlich ist. Um die mit
Entwicklung, Produktion und Absatz des Produktes verbundenen Kosten zu decken. Grundlage hierfür sind Prognosen über
Absatzchancen, am Markt realisierbare Verkaufspreise und Kosten, die durch das neue Produkt entstehen.

Hat man sich dazu entschieden, einen oder mehrere Produktvorschläge zu realisieren, erhalten die Abteilungen
Konstruktion und Marketing in Phase 3 gemeinsam die Aufgabe, ein Produktkonzept zu erstellen. Aus Sicht des Käufers
werden zunächst die Eigenschaften festgelegt, die das Produkt aufweisen sollte, um auf dem Markt erfolgreich zu sein.
Anschließend wird über den Namen, die Form, die Farbe, den Funktionsumfang, die Verarbeitungsqualität, das Image und die
Verpackung des Produktes entschieden. Neben dem eigentlichen Produkt trägt insbesondere die Verpackung wesentlich zur
Kaufentscheidung des Kunden bei. Die Verpackung erfüllt unterschiedliche Aufgaben: Neben der Schutzfunktion liegt die
Funktion der Verpackung für viele Händler und Hersteller, insbesondere im Bereich der absatzfördernden Wirkung. Um dem
ständigen Anstieg der Verpackungsmengen entgegenzuwirken, wurde 1991 die Verpackungsverordnung erlassen. Diese schreibt
den Herstellern und Händlern vor, Verpackungen zurückzunehmen und fachgerecht zu verwerten oder aber über ein
flächendeckendes duales System entsorgen zu lassen. Aus diesem Grund spielen bei der Verpackungsgestaltung Umweltaspekte
ebenfalls eine große Rolle. Für die Kunden hingegen steht meist die Informations- bzw. Servicefunktion im Vordergrund.
Bei Verpackungen ist grundsätzlich zwischen Verkaufsverpackungen, Umverpackungen und Transportverpackungen zu
unterscheiden. Verkaufsverpackungen umgeben das Produkt unmittelbar, wie z.B. der Joghurtbecher. Umverpackungen hingegen
sind zusätzliche Umhüllungen der Verkaufsverpackung, die die Selbstbedienung erleichtern sollen oder vor Diebstahl
schützen sollen sowie nur der absatzfördernden Wirkung wegen verwendet werden.

Aufgrund der Verpackungsverordnung ist der Anteil der Umverpackungen am gesamten Verpackungsaufkommen rückläufig.
Transportverpackungen sind Verpackungen, die überwiegend für den Transport vom Hersteller zum Händler eingesetzt werden,
wie z.B. Joghurtpaletten.

In der nächsten Phase muss die Konstruktionsabteilung anhand des Produktkonzepts mithilfe leistungsfähiger CAD-Systeme
ein fertigungs- und funktionsgerechtes Produkt gestalten, d.h. Baugruppe und Einzelteile müssen ausgearbeitet, Maße
festgelegt, die zu verwendenden Werkstoffe ausgewählt sowie Fertigungsunterlagen (Konstruktionszeichnungen, Stücklisten
etc.) erstellt werden. Hierbei ist den Bestimmungen des Kreislaufwirtschafts- und Abfallgesetzes Folge zu leisten.

Danach wird meist ein Prototyp gefertigt, der unternehmensintern einer intensiven Qualitätskontrolle unterzogen wird.

Ist das Ergebnis zufriedenstellend, wird das Produkt in der Realität erprobt, d.h., das Produkt wird einem Markttest
unterzogen, um zu überprüfen, wie es bei den Kunden ankommt. Eine Möglichkeit besteht darin, dass Produkt probeweise auf
einem Testmarkt anzubieten. Um die Vergleichbarkeit des Testmarktes mit dem Gesamtmarkt zu gewährleisten, sollte der
Testmarkt räumlich begrenzt sein und in seiner Zusammensetzung der Grundgesamtheit entsprechen. Eine weitere Möglichkeit
ist der sogenannte Store-Test. Hier wird das Produkt probeweise in ausgewählten Einzelhandelsgeschäften angeboten. Neben
den Markttests besteht darüber hinaus die Möglichkeit, die Akzeptanz des Produktes im Rahmen von Labortests zu
überprüfen. Mithilfe von Testpersonen kann die Qualität der Produkte und deren Verpackung bewertet werden. Auch
Möglichkeiten der Preisgestaltung können hier ausgelotet werden. Hat das Produkt die Produkttests erfolgreich
durchlaufen, kann das Produkt als marktreif angesehen werden. Das Produkt kann produziert und auf dem Markt eingeführt
werden.

## Produkt und Produktprogrammpolitik

Im Rahmen der Produktprogrammplanung wird festgelegt, welche Produkte hergestellt werden sollen. Man spricht in diesem
Zusammenhang auch vom Produktmix. Die Produktprogrammplanung unterscheidet zwischen folgenden produktpolitischen
Maßnahmen:

- Produktinnovation
- Produktmodifikation
- Produktelimination

Mit Produktinnovation ist die Entwicklung und Einführung neuer Produkte gemeint, die entweder noch gar nicht am Markt
angeboten wurden (Marktneuheit) oder aber für das betreffende Unternehmen eine Neuheit darstellen
(Unternehmensneuheiten). Im Rahmen der Produktinnovation ist zwischen der Produktdifferenzierung und der
Produktdiversifikation zu unterscheiden.

Bei der Produktdifferenzierung wird ein am Markt eingeführtes Produkt hinsichtlich einzelner Produktmerkmale (Form,
Farbe, Material, Ausstattung etc.) verändert. Neben dem ursprünglichen Produkt werden nun auch die Varianten des
Produktes angeboten.

Bei der Produktdiversifikation werden neue Produkte in das Produktprogramm aufgenommen. Hier ist zwischen horizontaler,
lateraler und vertikaler Produktdiversifikation zu unterscheiden. Handelt es sich um artverwandte Produkte der gleichen
Fertigungsstufe, spricht man von horizontaler Produktdiversifikation. Beispiel: Ein TV-Gerätehersteller produziert nun
auch DVD-Recorder. Stehen die Produkte in überhaupt keinem Zusammenhang mit dem bestehenden Produktprogramm, ist von
lateraler Produktdiversifikation die Rede. Beispiel: Ein Lebensmittelhersteller produziert neuerdings auch
Computerzubehör. Mit vertikaler Produktdiversifikation ist gemeint, dass Produkte der vor- bzw. nachgelagerten
Wirtschaftsstufe in das Produktprogramm aufgenommen werden. Beispiel: Ein Automobilhersteller produziert die Autositze
künftig selbst.

Bei der Produktmodifizierung (auch Produktvariation genannt) wird ein am Markt eingeführtes Produkt hinsichtlich
einzelner Produktmerkmale (Form, Farbe, Material, Qualität, Haltbarkeit, Ausstattung etc.) verändert (modifiziert) bzw.
verbessert. Der Unterschied zur Produktdifferenzierung ist u.a. darin zu sehen, dass das ursprüngliche Produkt vom Markt
genommen wird. Wird ein Produkt in mehrfacher Hinsicht verändert bzw. stark verbessert ist die Grenze zwischen
Produktmodifikation und Produktinnovation fließend.
