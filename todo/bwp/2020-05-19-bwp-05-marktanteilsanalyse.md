---
title: "05. Marktanteilsanalyse"
date: 2019-12-20 11:00:00 +0200
---

Nachdem ich einige Zeit gegoogelt habe und nichts fand, fragte ich unseren Lehrer. Diesen möchte ich an dieser Stelle
zitieren:

> Bei der Marktanteilsanalyse fällt es schwer theoretischen Hintergrund zu liefern, da es sich hier nicht so wirklich um
> ein Instrument des Marketings handelt (im Gegensatz zur SWOT-Analyse oder dem Produktlebenszyklus). Eher ist es so,
> dass man sich den Markt einfach mal ansieht und dort Anteile (in %) und Wachstum (absolut und in %) usw. analysiert.
> Es ist also ein Sammelsurium an Einzelrechnungen und -aufgaben, die nicht festgelegt sind.
> Deshalb findet man auch nicht unbedingt etwas zum Suchbegriff "Marktanteilsanalyse".
>
> Im Grunde kann man jede Aufgabenstellung hierzu lösen, wenn man die Prozentrechnung beherrscht und mit Begriffen wie
> z. B. Markt, Marktwachstum, absolut, relativ, Marktvolumen halbwegs umgehen kann.