---
title: "30. Ergänzungen"
date: 2021-04-17 12:00:00 +0200
---

Dieser Eintrag soll ein Rundumschlag mit Ergänzungen sein, die in der IHK Prüfung verlangt werden könnten, jedoch nicht
in der Berufsschule aufgrund von Zeitmangel oder Wahrscheinlichkeit nicht beigebracht werden.

## Berufsgenossenschaft

- Aufgabe zur Prävention: Verhütung von Arbeitsunfällen, Berufskrankheiten und arbeitsbedingten Gesundheitsgefahren.
- Aufgabe zur Nachsorge: Im Fall eines Unfalls oder einer Krankheit gibt die Genossenschaft finanzielle und medizinische
  Hilfe für den Betroffenen.
- Rechtsform: Körperschaft des öffentlichen rechts mit Selbstverwaltung.
- Finanzierung: Beträge der Mitglieder (Arbeitnehmer) durch Pflichtmitgliedschaft in einer Genossenschaft.
- Stand April 2021: jeweils neun gewerbliche und landwirtschaftliche Berufsgenossenschaften existieren.<br>
  Die landwirtschaftlichen Berufsgenossenschaften sind am 1. Januar 2013 zusammengefasst worden.
- Gliederung nach Wirtschaftszweigen

Quelle: <https://de.wikipedia.org/wiki/Berufsgenossenschaft>

## Arbeitsunfälle

> Offizielle Synonyme: Betriebsunfall, Berufsunfall

ein Arbeitsunfall beschreibt einen Unfall eines Arbeitnehmers der während der Arbeitszeit oder auf dem Arbeitsweg
geschieht. Wird ein Unfall als Arbeitsunfall klassifiziert, so ist dieser unverzüglich der Berufsgenossenschaft zu
melden, die gesetzliche Krankenversicherung ist hierbei von ihrer Leistungspflicht befreit.

Quelle: <https://de.wikipedia.org/wiki/Arbeitsunfall>

## Betriebsrat

Der Betriebsrat ist eine Arbeitnehmervertretung in Betrieben, Unternehmen oder Konzernen. Er ist ein Mitbestimmungsorgan
für Entscheidungen nach dem Betriebsverfassungsgesetz. Er ist nur für private Unternehmungen zuständig, im öffentlichen
Dienst übernimmt die Aufgabe der Personalrat.

Quelle: <https://de.wikipedia.org/wiki/Betriebsrat>

## Betriebsvereinbarung

Eine Vereinbarung zwischen Arbeitgeber und Betriebsrat, welcher verbindliche Normen für alle Arbeitnehmer eines
Betriebes formuliert. Für Arbeitnehmer gilt bei Arbeitsverträgen, Arbeitsgesetzen, Betriebsvereinbarungen etc. das
[Günstigkeitsprinzip](https://de.wikipedia.org/wiki/G%C3%BCnstigkeitsprinzip). Es besagt, dass bei überlappenden
Regelungen die für den Arbeitnehmer günstigste Regelung greift.

In der Betriebsvereinbarung können nur Dinge bestandteil haben, bei denen der Betriebsrat ein gesetzliches
Mitbestimmungsrecht hat. Es gibt erzwingbare Betriebsvereinbarungen und freiwillige Betriebsvereinbarungen.

Quelle: <https://de.wikipedia.org/wiki/Betriebsvereinbarung>

## Gewerkschaft

Eine Gewerkschaft ist eine Interessenvertretung der Arbeitnehmer für ihre wirtschaftlichen, sozialen und kulturellen
Interessen. Ein Mitglied einer Gewerkschaft wird als Gewerkschafter bezeichnet.

Eine Gewerkschaft hat die Möglichkeit Betriebsratswahlen in einem Betrieb zu initiieren und die tarifvertraglichen
Zielsetzungen mithilfe von Arbeitskampfmaßnahmen umzusetzen. Eine Gewerkschaft besitzt eine Satzung.

Das Pendant zu Gewerkschaft (Interessenvertretung von AN) ist meist der Arbeitgeberverband (Interessenvertretung der
AG).

Quelle: <https://de.wikipedia.org/wiki/Gewerkschaft> & <https://de.wikipedia.org/wiki/Gewerkschaften_in_Deutschland>

## Tarifvertrag

Ein Tarifvertrag ist ein Vertrag zwischen zwei Tarifvertragspartnern. Er unterliegt der Tarifautonomie, welche im
Grundgesetz festgehalten ist. Tarifvertragspartner sind Arbeitgeber oder Arbeitgeberverbände und Gewerkschaften.

Der Inhalt eines Tarifvertrages sind beispielsweise:

- Arbeitsentgelt
- Arbeitszeiten
- Urlaubsanspruch
- Arbeitsbedingungen
- Abschluss und Kündigung von Arbeitsverhältnissen
- Laufzeit des Tarifvertrages

Quelle: <https://de.wikipedia.org/wiki/Tarifvertrag>

## Wirtschaftlichkeitsanalyse

Eine Wirtschaftlichkeitsanalyse wird allgemein immer durch die Gegenüberstellung von Einsatz und Gewinn durchgeführt. Im
Detail ist jedoch der Anwendungsfall der Analyse zu betrachten.

## Fragen im Bewerbungsgespräch

Grundsätzlich unzulässig sind folgende Themen:

- Schwangerschaft
- Familienstand
- Glauben & politische Überzeugung
- Behinderung - Ausnahme: Zweifel an der Eignung für den Job
- Krankheit - Ausnahme: Offenbarungspflicht, falls andernfalls dadurch Mitarbeiter gefährdet werden.
- Vermögensverhältnisse - Ausnahme: Stelle für Führungskräfte
- Lohnpfändungen - Ausnahme: Bei größeren Pfändungen
- Wettbewerbsverbote - sind verpflichtend von AN anzugeben
- Vorstrafen - Ausnahme: Nur bei Relevanz für den Job. Meist verlangt daher der AG rechtmäßig, bei den entsprechenden
  Berufsfeldern (sozialer Bereich, kritische Infrastruktur, ...), vom AG das (erweiterte) Führungszeugnis.
- Drohende Haftstrafe - sind verpflichtend von AN anzugeben
- Alter
- Herkunft - ABER: Fragen nach Muttersprache(n) bzw. Sprachkenntnissen ist erlaubt.

Quelle: <https://www.kanzlei-hasselbach.de/2017/unzulaessige-fragen-vorstellungsgespraech/02/>

## Jugend Auszubildenden Vertretung (JAV)

Die JAV ist zuständig für Jugendliche unter 18 Jahren und zur Berufsausbildung beschäftigten unter 25 Jahren. Eine JAV
kann nur gegründet werden, wenn im Unternehmen ein Betriebsrat existent ist (im öffentlichen Dienst ein Personalrat).
Da die JAV Teil des Betriebsrates ist, können dessen Mitglieder nicht im Betriebsrat ein weiteres Amt belegen oder
vice-versa.

Aufgaben:

- Wahrnehmung der Belange der Auszubildenden
- Beantragung von Maßnahmen beim Betriebsrat oder der Personalvertretung (speziell zu Ausbildung, Übernahme,
  Gleichstellung von Männern und Frauen)
- Überwachung von Gesetzen, Vorschriften, Tarifverträgen usw.
- Anregungen der Auszubildenden an den Betriebs-/Personalrat
- Integration von Auszubildenden

Rechte:

- Teilnahme an Betriebsrats- bzw. Personalratssitzungen
- Teilnahme an Ausschusssitzungen des Betriebsrates, so weit Ausschüssen des Betriebsrats Angelegenheiten zur
  selbständigen Erledigung übertragen wurden oder Angelegenheiten behandelt werden, die die jugendlichen Arbeitnehmer
  oder Auszubildenden betreffen
- Teilnahme an Besprechungen zwischen Betriebs-/Personalrat und Arbeitgeber, so weit Angelegenheiten behandelt werden,
  die die jugendlichen Arbeitnehmer oder Auszubildenden betreffen
- Durchführung von Jugend- und Auszubildendenversammlungen
- Durchführung von Jugend- und Auszubildendenvertretungssitzungen
- Abhalten von Sprechstunden
- Teilnahme an erforderlichen Schulungen auf Kosten des Arbeitgebers
- Freistellungen von der arbeitsvertraglichen Pflicht zur Erledigung der Aufgaben als JAV-Mitglied ohne Minderung des
  Arbeitsentgeltes oder einem Verlust der eingebrachten Arbeitszeit
- Die JAV hat Anspruch auf die Nutzung eines eigenen Büroraums.

Quelle: <https://de.wikipedia.org/wiki/Jugend-_und_Auszubildendenvertretung>

## Projektstrukturplan

TODO - siehe <https://de.wikipedia.org/wiki/Projektstrukturplan>

## V-Modell

TODO - siehe <https://de.wikipedia.org/wiki/V-Modell>

## Projektmanagement - magisches Dreieck

TODO - siehe <https://blog.hubspot.de/marketing/magisches-dreieck>
