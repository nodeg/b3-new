---
title: "03. SWOT-Analyse"
date: 2019-12-14 10:05:00 +0200
---

Die SWOT-Analyse ist ein Instrument zur Gegenüberstellung innerbetrieblicher Stärken ("Strengths") und Schwächen
("Weaknesses") einerseits, sowie externer Chancen ("Opportunities") und Risiken ("Threats") andererseits. Die
innerbetrieblichen Stärken und Schwächen sind durch das Management beeinflussbar, die externen Chancen und Risiken
hingegen nicht. Ziel der SWOT-Analyse ist es, begründet strategische Empfehlungen herzuleiten. Die SWOT-Analyse
durchläuft dazu drei Schritte:

## Schritt 1 - interne Analyse: Leitungsfähigkeit des Unternehmens

Erfassen und Gewichten eigener Stärken und Schwächen wie zum Beispiel:

Personal

- Führungsstil
- Motivation
- Qualifikation
- Personalressourcen

Produktion

- Qualität der Produktionsanlagen
- Produktionskapazität
- Produktionskosten
- Flexibilität

Marketing/Vertrieb

## Schritt 2 - externe Analyse: Entwicklung der Umwelt

Erfassen und Gewichten externer Chancen und Risiken wie zum Beispiel:

Wirtschaft:

- Kaufkraftentwicklung der potenziellen Kunden
- Internationaler Wettbewerb (Globalisierung)
- Staatliche Investitionsanreize für Unternehmen

Demografische und sozial-psychologische Entwicklungstendenzen:

- Demografische Entwicklung
- Trend zur natürlichen und kritischen Lebensweise
- Multikulturelle Entwicklung in Europa

Technologie:

- Kürzere Produktentwicklungszeiten
- Trend zur natürlichen und kritischen Lebensweise
- Multikulturelle Entwicklung in Europa

Ökologie:

- Verschärfte Umweltschutzbedingungen
- Entsorgung/Recycling
- Trend zur Abfallvermeidung

## Schritt 3: Ableiten von Strategien

Werden die Stärken und Schwächen mit den Chancen und Risiken kombiniert, lassen sich theoretisch vier Arten von
Strategien für das Unternehmen ableiten:
