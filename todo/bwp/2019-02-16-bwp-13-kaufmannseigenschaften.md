---
title:  "13. Kaufmannseigenschaften"
date:   2019-02-15 06:45:00 +0100
---

Im Wirtschaftsleben ist die Frage, ob ein Geschäftspartner Kaufmann ist oder nicht, von erheblicher rechtlicher
Bedeutung. Durch die am 1.7.1998 in Kraft getretene Reform des Handelsrechts wurde der **Begriff des Kaufmanns** neu
definiert. Kaufmann ist nach § 1 Handelsgesetzbuch grundsätzlich jeder Gewerbetreibende. Davon ausgenommen sind
lediglich die sogenannten Kleingewerbetreibenden, deren Unternehmen „nach Art und Umfang einen in kaufmännischer Weise
eingerichteten Geschäftsbetrieb nicht erfordert“. Für die Abgrenzung zwischen Kaufleuten und Kleingewerbetreibenden gibt
es keinen festen Maßstab; im Zweifelsfall entscheiden die Gerichte unter Berücksichtigung
der Art des Unternehmens, der Umsatz- und Beschäftigtenzahlen, des Betriebsvermögens usw. Wer Kaufmann ist, muss sich
beim zuständigen Registergericht (Amtsgericht) ins **Handelsregister** eintragen lassen. Kleingewerbetreibende haben die
Wahl: Lassen sie sich eintragen, erwerben sie dadurch die Kaufmannseigenschaft und haben dann die gleichen Rechte und
Pflichten wie alle anderen Kaufleute.

Zu den **Pflichten des Kaufmanns** gehört eine ordnungsgemäße Buchführung, aus der ein Überblick über die geschäftlichen
Vorgänge und die Lage des Unternehmens gewonnen werden kann.
Zum Ende jedes Geschäftsjahrs muss eine Inventur und eine Bilanz erstellt werden. Bücher, Inventuren und Bilanzen sind
10 Jahre, Geschäftsbriefe 6 Jahre aufzubewahren. Auf allen Geschäftsbriefen an einen bestimmten Empfänger müssen die
wesentlichen Rechtsverhältnisse des Unternehmens angegeben sein (u. a. die vollständige Firmenbezeichnung, Rechtsform
und Sitz des Unternehmens, das Registergericht und die Handelsregisternummer). Kaufleute genießen auch
**Vorteile und Rechte**, die sich aus der Eintragung ins Handelsregister ergeben: Sie führen ihr Unternehmen unter der
eingetragenen Firma, die dadurch gegenüber anderen, gleich oder ähnlich lautenden Firmennamen geschützt ist. Sie allein
dürfen Zweigniederlassungen gründen und nur sie sind berechtigt, Prokura (Details zum Thema Prokura siehe:
[Vollmacht und Prokura]({% post_url 2019-03-26-bwp-18-vollmacht-und-prokura %})) zu erteilen.

Für **Handelsgeschäfte**, wie sie von Kaufleuten betrieben werden, gelten eine Reihe von Sondervorschriften (§§ 343 ff.
HGB), die von den weniger strengen Bestimmungen des Bürgerlichen Gesetzbuchs abweichen. So müssen Kaufleute die ihnen
gelieferten Waren unverzüglich untersuchen und Mängel oder Fehllieferungen dem Verkäufer gegenüber sofort rügen. Im
gegenseitigen Geschäftsverkehr können sie für ihre Forderungen vom Tag der Fälligkeit an Zinsen verlangen.
Bestätigungsschreiben nach mündlicher Verhandlung sind gültig, wenn ihnen nicht ausdrücklich widersprochen wird usw.
