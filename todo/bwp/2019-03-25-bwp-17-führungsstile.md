---
title:  "17. Führungsstile"
date:   2019-03-25 18:00:00 +0100
---

|             | Autoritär                                  | Kooperativ                                    | Laissez faire                     |
| ----------- | ------------------------------------------ | --------------------------------------------- | --------------------------------- |
| Kennzeichen | Der Vorgesetzte:                           | Der Vorgesetzte:                              | Der Vorgesetzte:                  |
|             | Entscheidet alleine                        | Entscheidet mit dem MA                        | Kontrolliert MA nicht             |
|             | Erteilt Befehle an MA                      | Gewährt Freiräume                             | Zieht keine Konsequenzen          |
|             | Kontrolliert MA                            | Greift ein, wenn nötig                        | Achtet nicht auf Effizienz        |
| Vorteile    | Klare Regelungen und Aufgabenverteilung    | MA sind am Entscheidungsprozess beteiligt     | Ma werden nicht kontrolliert      |
|             | Schnelle Entscheidung                      | MA bringen Kompetenzen stärker ein            | Freie Entfaltung                  |
|             | MA ohne Verantwortung (kann auch Nachteil sein) |                                          |                                   |
| Nachteile   | Mitarbeiter fühlen sich unwohl/demotiviert | Kompetenzüberschneidungen bzw. Streitigkeiten | Planlos/unkontrolliertes Arbeiten |
|             | Kompetenzen werden nicht optimal genutzt   | Qualifiziertes Personal $\rightarrow$ Kosten  | Gleichgültigkeit                  |
