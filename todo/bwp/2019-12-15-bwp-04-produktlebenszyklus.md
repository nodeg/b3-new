---
title: "04. Produktlebenszyklus"
date: 2019-12-14 10:10:00 +0200
---

Das Konzept des Produktlebenszyklus geht davon aus, dass jedes Produkt - ebenso wie jedes Lebewesen - einen Lebenszyklus
durchläuft, der abhängig vom Produkt unterschiedlich lange dauert. Dieser sogenannte "Produktlebenszyklus", der sich
auf auf Produktgruppen, Geschäftsfelder und sogar ganze Märkte beziehen kann, wird als Umsatzfunktion in Abhängigkeit
von der Zeit dargestellt und in fünf Phasen (alternativ 4 Phasenschema - bei dem die Reife und Sättigungsphase
zusammengefasst werden) eingeteilt.

## Einführungsphase

Die Produktentwicklung ist abgeschlossen und das Produkt wird am Markt neu eingeführt. Das Unternehmen versucht,
Meinungsführer und Trendsetter zum Kauf des Produktes anzuregen, damit sich das Produkt erfolgreich ausbreitet. Wird das
Produkt vom Markt nicht angenommen, spricht man von einem "Flop" (engl. Misserfolg). Hat es jedoch Erfolg, nimmt der
Umsatz - zunächst noch auf sehr geringem Niveau - stetig zu. Gleichzeitig sind die Kosten in dieser Phase sehr hoch. Das
liegt einerseits an den erheblichen Werbungs- und Vertriebskosten und zum anderen an den i.d.R. noch hohen Stückkosten,
die mit geringen Stückzahlen verbunden sind. Mit Erreichen der Gewinnschwelle wird die Wachstumsphase erreicht.

## Wachstumsphase

Das Produkt verbreitet sich am Markt und er Umsatz steigt mit zunehmenden Wachstumsraten. Immer mehr Käuferschichten
werden auf das Produkt aufmerksam, u. a. durch sogenannte "Mundpropaganda", Testberichte, Fachzeitschriften.
Gleichzeitig drängen die ersten Wettbewerber mit Nachahmungsprodukten auf den Markt. Die Vermarktungskosten sinken, der
Umsatz nimmt rasch zu, wodurch sich die Ertragssituation deutlich verbessert.

## Reifephase

Der Umsatz des Produktes steigt immer noch, allerdings nur noch mit abnehmenden Wachstumsraten, das bedeutet, dass das
Wachstum sich verlangsamt. Die Grenze zur Sättigungsphase verläuft dabei fließend. Immer mehr Wettbewerber drängen auf
den Markt und der Kampf um Marktanteile nimmt zu, denn viele Anbieter können nur noch wachsen, wenn sie anderen
Anbietern, Marktanteile abnehmen. Noch kann das Unternehmen i.d.R. gute Gewinne abschöpfen.

## Sättigungsphase

Der Markt ist gesättigt, der Produktumsatz steigt kaum noch, erreicht sein Maximum und stagniert oder beginnt bereits
langsam zu sinken. Die Grenze zur Degenerationsphase verläuft dabei fließend. Die Sättigungsphase ist durch starken
Kostenwettbewerb gekennzeichnet. I.d.R. werden noch Gewinne erzielt, jedoch in deutlich abnehmendem Maße.

## Degenerationsphase

Der Produktumsatz nimmt deutlich und zunehmend ab: Das Produkt "stirbt". Dieser Umsatzrückgang kann auf
unterschiedliche Ursachen zurückzuführen sein, beispielsweise verdrängen technische Innovationen das bisherige Produkt
(DVD ersetzt CD), oder der Geschmack bzw. Zeitgeist der Kunden ändert sich (Parfüm). Das Marketing nimmmt das Produkt
bei einsetzenden Verlusten vom Markt ("Produkteliminierung").

## Norm Empfehlung für das Marketing

Um zweckmäßige Entscheidungen treffen zu können, muss die Marketingabteilung wissen, wo sich die Produkte bzw.
Produktgruppen des Unternehmens im Produktlebenszyklus befinden, denn daraus lassen sich bestimmte Norm-Empfehlungen
ableiten. Dabei bedeutet "Norm", dass die abzuleitenden Empfehlungen sehr allgemeiner Natur sind und nicht in jedem
Einzelfalle gelten müssen. Man unterscheidet dabei strategische und operative Norm-Empfehlungen.

## Strategische Norm-Empfehlungen

Da die strategische Marketingplanung für die langfristige Planung des gesamten Produktionsporgramms bzw. Sortiments
des Unternehmens zuständig ist, werden hier häufig die Lebenszyklen ganzer Produktgruppen oder Märkte in die
Sättigungs- oder Degenerationsphase, so muss die strategische Marketingplanung frühzeitig die Entwicklungen neuer
Produkte ("Innovationen") oder die Aufnahme neuer Produktgruppen oder Geschäftsfelder in die Wege leiten.
