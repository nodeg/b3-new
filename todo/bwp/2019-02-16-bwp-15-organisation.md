---
title:  "15. Organisation"
date:   2019-02-15 06:55:00 +0100
---

> Eine betriebliche Organisation schafft ein System von Regelungen zur Verwirklichung der betrieblichen Ziele.

## Die Aufbauorganisation

- gliedert in der Aufgabenanalyse die Gesamtaufgabe eines Unternehmens in Aufgabenbereiche
- grenzt die Verantwortungsbereiche ab und bestimmt in der Aufgabensynthese die Stellen und Abteilungen, die diese
  bearbeiten.

Insbesondere innovative und schnell wachsende IT-Unternehmen schaffen ständig neue Stellen. Der Bildung einer Stelle
sollte eine genaue Aufgabenanalyse vorausgehen. Dabei wird die Gesamtaufgabe des Unternehmens in Teilaufgaben zerlegt.
Die Gliederung kann nach verschiedenen Merkmalen oder Prinzipen erfolgen:

| Gliederungsmerkmal | Beispiel                         |
| ------------------ | -------------------------------- |
| Objekt             | Produkt A, Produkt B             |
| Verrichtung        | Einkaufen, Produzieren, Vertrieb |
| Phase              | Test, Zusammenbau, Verstellen    |
| Rangstufe          | Abteilung, Gruppe, Leitung       |

Aus dem Ergebnis der Aufgabenanalyse entsteht ein Aufgabengliederungsplan oder Aufgabenbaum.

![BWP - Organisation - 01](/assets/img/BWP/01-15-Organisation-01.png)

## Die Ablauforganisation

gliedert ein Unternehmen in Prozesse, die dann nach Aufgabenbereichen unterteilt werden.

![BWP - Organisation - 02](/assets/img/BWP/01-15-Organisation-02.png)

Weiterführende Links hierzu sind:

- <https://de.wikipedia.org/wiki/Ablauforganisation>
- <https://www.microtech.de/erp-wiki/ablauforganisation/>
