---
title:  "24. Optimale Bestellmenge"
date:   2019-05-02 11:00:00 +0100
---

Viele Gegenstände (Papier, Schrauben oder bspw. Holz) sind nicht sofort in der Firma vorhanden und müssen erst bestellt
werden. Um möglichst effizient vorzugehen ist es wichtig die richtige Anzahl zu bestimmen, die bestellt wird. Dies ist
von viele Faktoren abhängig, welche sich gegenseitig bedingen.

| Bestellkosten   | Lagerkosten |
| --------------- | ----------- |
| Mengenrabatte   | Miete       |
| Transportkosten | Strom       |
| Personal        | Heizung     |
| ...             | Klimaanlage |
|                 | Personal    |
|                 | ...         |


