---
title:  "04. Ökonomisches Prinzip"
date:   2019-02-15 06:15:00 +0100
---

- **Minimalprinzip**: Anton möchte **1 kg Tomaten** *(bestimmter Zweck)* und sucht sich das **günstigste Angebot**
  *(Möglichst geringer Einsatz)*.
- **Maximalprinzip**: Anton hat **3 €** *(bestimmter Einsatz)* und möchte dafür **so viele** Tomaten **wie möglich**
  *(Maximaler Erfolg)*.
