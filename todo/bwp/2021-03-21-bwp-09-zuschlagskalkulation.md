---
title: "09. Zuschlagskalkulation"
date: 2021-01-15 09:50:17 +0200
---

## Allgemeines

Zur Erinnerung: Schema zur Gesamtkalkulation (BAB 2) mit der kleinen Erweiterung

- Sondereinzelkosten der Fertigung (SEKF), die nicht bei jedem Auftrag anfallen, z. B. spezielle Konstruktionszeichnung
  $$\rightarrow$$ zu den Fertigungskosten hinzuzurechnen
- Sondereinzelkosten der Vertriebs, die nicht generell anfallen z. B. Spezialverpackung 🡪 zu den Selbstkosten
  hinzuzurechnen

|     | Kostenart                                         | Abkürzung | Hinweis |
| --- | ------------------------------------------------- | --------- | ------- |
|     | Materialeinzelkosten: Fertigungsmaterial          | FM        | gegeben, bzw. aus Ergebnistabelle unter „Aufwendungen für Rohstoffe“ abzulesen |
| +   | Materialgemeinkosten                              | MGK       | Summe aus BAB I --> damit zu berechnen MGK in % gemessen an der Zuschlagsgrundlage des Fertigungsmaterials |
| =   | Materialkosten                                    | MK        | Summe FM + MGK |
|     | Fertigungseinzelkosten: Fertigungslöhne           | FL        | gegeben, bzw. aus Ergebnistabelle unter „Löhne“ abzulesen |
| +   | Fertigungsgemeinkosten                            | FGK       | Summe aus BAB I --> damit zu berechnen FGK in % gemessen an der Zuschlagsgrundlage der Fertigungslöhne |
| +   | Sondereinzelkosten der Fertigung                  | SEKF      |   |
| =   | Fertigungskosten                                  | FK        | Summe FL + FGK + SEKF |
| =   | Herstellkosten der Erzeugung                      | HKdE      | Summe MK + FK |
| -   | Bestandsmehrung fertige / unfertige Erzeugnisse   | Bmeh      | Wenn AB < SB |
| +   | Bestandsminderung fertige / unfertige Erzeugnisse | Bmin      | Wenn AB > SB |
| =   | Herstellkosten des Umsatzes                       | HKdU      ||
| +   | Verwaltungsgemeinkosten                           | VwGK      | Summe aus BAB I --> damit zu berechnen VwGK in % gemessen an der Zuschlagsgrundlage der HKdU |
| +   | Vertriebsgemeinkosten                             | VtGK      | Summe aus BAB I --> damit zu berechnen VtGK in % gemessen an der Zuschlagsgrundlage der HKdU |
| +   | Sondereinzelkosten des Vertriebs                  | SEKVt     |  |
| =   | Selbstkosten                                      |           |  |

Neue Information: Schema zur **Zuschlagskalkulation**, hier für die **Stückkalkulation** eines einzigen Produktes: hier
sind die Bestandsveränderungen nicht mit einzubeziehen, da es sich nur um 1 Stück handelt! Daher wird nicht mehr
zwischen HKdE und HKdU unterschieden $$\rightarrow$$ diese werden sofort als Herstellkosten (HK) bezeichnet.

|     | Kostenart                                | Abkürzung | Hinweis |
| --- | ---------------------------------------- | --------- | ------- |
|     | Materialeinzelkosten: Fertigungsmaterial | FM        |         |
| +   | Materialgemeinkosten                     | MGK       |         |
| =   | Materialkosten                           | MK        |         |
|     | Fertigungseinzelkosten: Fertigungslöhne  | FL        |         |
| +   | Fertigungsgemeinkosten                   | FGK       |         |
| +   | Sondereinzelkosten der Fertigung         | SEKF      |         |
| =   | Fertigungskosten                         | FK        |         |
| =   | Herstellkosten                           | HK        |         |
| +   | Verwaltungsgemeinkosten                  | VwGK      |         |
| +   | Vertriebsgemeinkosten                    | VtGK      |         |
| +   | Sondereinzelkosten des Vertriebs         | SEKVt     |         |
| =   | Selbstkosten                             | SK        |         |
| +   | Gewinn                                   |           | Gewinn in % der Selbstkosten |
| =   | Barverkaufspreis                         | BVP       |         |
| +   | Kundenskonto                             |           | in % des Zielverkaufspreises!!! (Zielverkaufspreis mit 100 % ansetzen, daher z. B. bei 2 % Skonto und keiner Vertreterprovision der BVP nur 98 %!!!) |
| +   | Vertreterprovision                       |           | in % des Zielverkaufspreises!!! (siehe Beschreibung Skonto) |
| =   | Zielverkaufspreis                        | ZVP       |         |

## Beispiel Zuschlagskalkulation

Die Baumüller GmbH kalkuliert den Listenpreis für eine ihrer Fräsmaschinen mit folgenden Werten:

- Gewinnspanne: 15 %
- Kundenskonto: 2 %
- Vertreterprovision: 5 %
- Kundenrabatt: 10 %

Einzelkosten:

- Fertigungsmaterial: 5.000,00 €
- Fertigungslöhne: 2.180,00 €
- Sondereinzelkosten der Fertigung: 98,00 € (TÜV Überprüfung)
- Sondereinzelkosten des Vertriebs: 43,00 € (Frachtkosten)

Gemeinkosten lt. BAB:

- Materialgemeinkostenzuschlagssatz: 9 %
- Fertigungsgemeinkostenzuschlagssatz: 78 %
- Verwaltungsgemeinkostenzuschlagssatz: 12 %
- Vertriebsgemeinkostenzuschlagssatz: 8 %

Der Listenpreis errechnet sich nun folgendermaßen:

|     | Kalkulation                               | €         | %    | %    |
| --- | ----------------------------------------- | --------: | ---: | ---: |
|     | Fertigungsmaterial (FM)                   | 5.000,00  | 100  |      |
| +   | Materialgemeinkosten (MGK)                | 450,00    | 9    |      |
| =   | Materialkosten (MK)                       | 5.450,00  |      |      |
|     | Fertigungslöhne (FL)                      | 2.180,00  | 100  |      |
| +   | Fertigungsgemeinkosten (FGK)              | 1.700,40  | 78   |      |
| +   | Sondereinzelkosten der Fertigung (SEKF)   | 98,00     |      |      |
| =   | Fertigungskosten (FK)                     | 3.978,40  |      |      |
| =   | Herstellkosten (HK)                       | 9.428,40  | 100  |      |
| +   | Verwaltungsgemeinkosten (VwGK)            | 1.131,41  | 12   |      |
| +   | Vertriebsgemeinkosten (VtGK)              | 754,27    | 8    |      |
| +   | Sondereinzelkosten des Vertriebes (SEKVt) | 43,00     |      |      |
| =   | Selbstkosten (SK)                         | 11.357,08 | 100  |      |
| +   | Gewinn                                    | 1.703,56  | 15   |      |
| =   | Barverkaufspreis (BVP)                    | 13.060,64 |      | 93   |
| +   | Kundenskonto                              | 280,87    |      | 2    |
| +   | Vertreter-Provision                       | 702,18    |      | 5    |
| =   | Zielverkaufspreis (ZVP)                   | 14.043,69 | 90   | 100  |
| +   | Kundenrabatt                              | 1.560,41  | 10   |      |
| =   | Listenverkaufspreis (LVP)                 | 15.604,10 | 100  |      |
