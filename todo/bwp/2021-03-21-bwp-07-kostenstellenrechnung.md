---
title: "07. Kostenstellenrechnung"
date: 2021-01-10 09:50:17 +0200
---

## BAB I

Die Gemeinkosten sollen verursachungsgerecht auf die einzelnen Kostenstellen verteilt werden. Hierfür gibt es ein
Hilfsmittel, den **Betriebsabrechnungsbogen (BAB)**.

Bei den einzelnen Spalten finden Sie Abkürzungen:
- MGK = Materialgemeinkosten (also die Summe der Gemeinkosten, die Sie der Materialkostenstelle zugeordnet haben).
- FGK = Fertigungsgemeinkosten
- VwGK = Verwaltungsgemeinkosten
- VtGK = Vertriebsgemeinkosten

| Gemeinkosten      | Summe        | Verteilungsgrundlage                  | Material  | Fertigung  | Verwaltung | Vertrieb  |
| ----------------- | -----------: | ------------------------------------- | --------: | ---------: | ---------: | --------: |
| Hilfsstoffe       | 20.000,00    | direkt                                | 0,00      | 18.000,00  | 500,00     | 1.500,00  |
| Strom             | 3.354,00     | nach kWh (900 : 17.350 : 6.730 : 820) | 117,00    | 2.255,50   | 874,90     | 106,60    |
| Steuern           | 4.400,00     | 0 : 3 : 1 : 0                         | 0,00      | 3.300,00   | 1.100,00   | 0,00      |
| Heizkosten        | 63.000,00    | nach qm (200 : 900 : 400 : 300)       | 7.000,00  | 31.500,00  | 14.000,00  | 10.500,00 |
| Sozialabgaben     | 312.840,00   | 65 : 415 : 230 : 80                   | 25.740,00 | 164.340,00 | 91.080,00  | 31.680,00 |
| Telefon           | 483,00       | nach Geräten (2 : 4 : 30 : 6)         | 23,00     | 46,00      | 345,00     | 69,00     |
| Feuerversicherung | 2.500,00     | 2 : 10 : 7 : 1                        | 250,00    | 1.250,00   | 875,00     | 125,00    |
| Kalkul. ABSA      | 10.476,00    | 98 : 315 : 275 : 185                  | 1.176,00  | 3.780,00   | 3.300,00   | 2.220,00  |
|                   | Gemeinkosten |                                       | **MGK**   | **FGK**    | **VwGK**   | **VtGK**  |
|                   |              |                                       | 34.306,00 | 224.471,50 | 112.074,90 | 46.200,60 |

Der BAB zeigt eine tabellarisch durchgeführte Kostenstellenrechnung. Er ist waagrecht in Kostenstellen und senkrecht in
Kostenarten gegliedert. Er dient zur verursachungsgerechten Verteilung der Gemeinkosten auf die Kostenstellen.

## BAB II

| Gemeinkosten      | Summe        | Verteilungsgrundlage                  | Material  | Fertigung  | Verwaltung | Vertrieb  |
| ----------------- | -----------: | ------------------------------------- | --------: | ---------: | ---------: | --------: |
| Hilfsstoffe       | 20.000,00    | direkt                                | 0,00      | 18.000,00  | 500,00     | 1.500,00  |
| Strom             | 3.354,00     | nach kWh (900 : 17.350 : 6.730 : 820) | 117,00    | 2.255,50   | 874,90     | 106,60    |
| Steuern           | 4.400,00     | 0 : 3 : 1 : 0                         | 0,00      | 3.300,00   | 1.100,00   | 0,00      |
| Heizkosten        | 63.000,00    | nach qm (200 : 900 : 400 : 300)       | 7.000,00  | 31.500,00  | 14.000,00  | 10.500,00 |
| Sozialabgaben     | 312.840,00   | 65 : 415 : 230 : 80                   | 25.740,00 | 164.340,00 | 91.080,00  | 31.680,00 |
| Telefon           | 483,00       | nach Geräten (2 : 4 : 30 : 6)         | 23,00     | 46,00      | 345,00     | 69,00     |
| Feuerversicherung | 2.500,00     | 2 : 10 : 7 : 1                        | 250,00    | 1.250,00   | 875,00     | 125,00    |
| Kalkul. ABSA      | 10.476,00    | 98 : 315 : 275 : 185                  | 1.176,00  | 3.780,00   | 3.300,00   | 2.220,00  |
|                   | Gemeinkosten |                                       | **MGK**   | **FGK**    | **VwGK**   | **VtGK**  |
|                   |              |                                       | 34.306,00 | 224.471,50 | 112.074,90 | 46.200,60 |
|                   |              | Zuschlagsgrundlage                    |           |            |            |           |
|                   |              |                                       |           |            |            |           |
|                   |              | GK (%)                                |           |            |            |           |

Angabe:

- FM: 410.850,00
- FL: 129.006,00
- FE:
    - AB: 14.300,00
    - SV: 25.680,00
- UFE:
    - AB: 26.700,00
    - SB: 21.400,00

FM = Fertigungsmaterial (Rohstoffe) $$\rightarrow$$ Einzelkosten<br>
FL = Fertigungslöhne $$\rightarrow$$ Einzelkosten

|     | Gesamtkalkulation                   | €          | €          | GK (Gemeinkosten)-Zuschlagsätze |
| --- | ----------------------------------- | ---------: | ---------: | ------------------------------: |
|     | Fertigungsmaterial (FM)             | 410.850,00 |            | 100 %                           |
| +   | Materialgemeinkosten (MGK)          | 34.306,00  |            | 8,35 %                          |
| =   | Materialkosten (MK)                 | 445.156,00 |            |                                 |
|     | Fertigungslöhne (FL)                | 129.006,00 |            | 100 %                           |
| +   | Fertigungsgemeinkosten (FGK)        | 224.471,50 |            | 174 %                           |
| =   | Fertigungskosten (FK)               | 353.477,50 |            |                                 |
| =   | Herstellkosten der Erzeugung (HKdE) | (MK+FK)    | 798.633,50 |                                 |
| -   | Bestandsveränderung FE (Mehrung)    |            | 11.380,00  |                                 |
| +   | Bestandsveränderung UFE (Minderung) |            | 5.300,00   |                                 |
| =   | Herstellkosten des Umsatzes (HKdU)  |            | 792.553,50 | 100 %                           |
| +   | Verwaltungsgemeinkosten (VwGK)      |            | 112.074,90 | 14,14 %                         |
| +   | Vertriebsgemeinkosten (VtGK)        |            | 46.200,60  | 5,83 %                          |
| =   | Selbstkosten (SK)                   |            | 950.829,00 |                                 |

"Nebenrechnung" Lagerbestandsveränderungen:

- FE = Fertigerzeugnisse
    - AB (= Anfangsbestand): 14.300,00
    - SB (= Schlussbestand): 25.680,00
    - Bestandsmehrung (Mehrbestand): 11.380,00
- UFE = Unfertige Erzeugnisse
    - AB: 26.700,00
    - SB: 21.400,00
    - Bestandsminderung (Minderbestand): 5.300,00
