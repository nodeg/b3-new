---
title: "01. Marktforschung"
date: 2019-10-14 10:05:00 +0200
---

Fundierte Entscheidungen können nur getroffen werden, wenn die Situation auf den Märkten, auf denen sich ein
Unternehmen bewegt, bekannt ist. Die systematische Sammlung und Auswertung von diesbezüglichen Informationen wird
Marktforschung genannt.

Zunächst sollte die unternehmensspezifische Ausgangslage geklärt werden. In diesem Zusammenhang ist das Rechnungswesen
oder das Controlling gehalten, Statistiken über die Umsatz-, Absatz-, Kosten- und Gewinnentwicklung sowie die Zahl der
Reklamationen bereitzustellen. Auch Berichte von Außendienstmitarbeiten stellen eine wichtige interne Informationsquelle
dar. Da die oben aufgeführten Informationen schon für andere Zwecke, wie z.B. den Jahresabschluss, bereitgestellt
wurden, spricht man in diesem Zusammenhang von Sekundärforschung oder auch Desk Research.

Im nächsten Schritt ist die Wettbewerbssituation, in der sich das Unternehmen befindet, zu analysieren. Hierbei sind
zunächst das Marktvolumen, das Marktwachstum, und die Marktanteile der Wettbewerber zu ermitteln. Darüber hinaus muss
das eigene Unternehmen mit den Wettbewerbern hinsichtlich Absatzprogramm, Produktqualität, Preispolitik, Service,
Lieferzeit, Werbeaufwand und Vertriebswege miteinander verglichen werden. Die benötigten Informationen können auf
externem Wege, z.B. aus Verbandsstatistiken, Fachzeitschriften, Veröffentlichungen der IHK oder diverser
wirtschaftswissenschaftlicher Institute herausgearbeitet werden. Auch Internetauftritte der Wettbewerber sowie deren
Kataloge und Preislisten sind geeignete Informationsquellen. Da auch hier auf bereits vorhandene Informationen
zurückgegriffen wird, die ursprünglich für andere Zwecke bereitgestellt wurden, spricht man wie im obigen Fall von der
Sekundärforschung.

Ein weiterer wichtiger Baustein der Marktforschung ist in der Analyse der Zielgruppe zu sehen. Kundenwünsche, Kaufmotive,
Preisvorstellungen sowie die Zusammensetzungen der Kunden nach Alter, Geschlecht, Ausbildung und Einkommen lassen sich
meist nicht aus schon vorhandenen Informationen herausarbeiten. Aus diesem Grund sind die Informationen i.d.R. durch
eigens durchgeführte Untersuchungen zu beschaffen. Man spricht in diesem Zusammenhang von Primärforschung oder auch
Field Research. Die Primärforschung ist sehr kostenintensiv und wird häufig an hierauf spezialisierte
Marktforschungsinstitute vergeben.

Primärforschung erfolgt in Form von Befragungen, Beobachtungen und Experimenten. Die Befragungen können schriftlich,
mündlich, telefonisch oder computergestützt durchgeführt werden. Hier kann z.B. mit offenen oder geschlossenen (hier
sind die Antworten vorgegeben) Fragen gearbeitet werden.

Während bei den Befragungen die Meinungen und Ansichten der Zielgruppe im Vordergrund stehen, geht es bei den
Beobachtungen um das Kaufverhalten oder bspw. die Mimik beim Betrachten der Produkte.

Darüber hinaus besteht die Möglichkeit, Produkte, Verpackungen, Werbemittel oder Preise im Rahmen von Experimenten zu
testen. Hierbei ist zwischen Labor und Feldexperimenten zu unterscheiden. So kann man im Rahmen von Laborexperimenten
Testpersonen unterschiedliche Produkt- oder Verpackungsentwürfe vorstellen, die anschließend bewertet werden müssen.
Feldexperimente können in Markt- oder Storetests unterschieden werden. Unter Markttest ist ein probeweiser
kontrollierter Verkauf in einem regional begrenzten Testmarkt zu verstehen. Beim Storetest werden die Produkte in 20
bis 30 ausgewählten Testgeschäften verkauft.

Im Rahmen von Befragungen ist zu klären, ob alle potenziellen Kunden (Grundgesamtheit) befragt werden soll
(Vollerhebung) oder ob eine stichprobenartige Befragung (Teilerhebung) ausreicht.

Im letzteren Fall ist darauf zu achten, dass die Stichprobe repräsentativ ist, d.h. die Grundgesamtheit widerspiegelt.
Soll eine stichprobenartige Befragung durchgeführt werden, ist zunächst zu klären, wie groß die Stichprobe sein soll.
Je größer der Umfang der Stichprobe, desto zuverlässiger das Ergebnis, aber desto höher sind auch die Kosten. Ist dies
geschehen, muss entschieden werden, ob die zu befragenden Personen zufällig oder bewusst ausgewählt werden sollen.

Zufallsverfahren werden auch Randomverfahren genannt. Bei einfachen Randomverfahren werden die zu befragenden Personen
beliebig aus einer Datei/Kartei ausgewählt. So könnte z.B. jeder zehnte Kunde befragt werden. Zufallsverfahren bieten
sich vor allem an, wenn es sich bei der Grundgesamtheit um einen homogenen Personenkreis handelt.

Ist die Grundgesamtheit eher heterogen, bieten sich bewusst Auswahlverfahren an. Das hier am häufigsten angewandte
Verfahren ist das Quotenauswahlverfahren. Hier erhalten die mit der Befragung beauftragten Personen Vorgaben, nach denen
sie sich bei der Auswahl der zu befragenden Personen richten müssen. Dies können z.B. Alter, Geschlecht,
Einkommensverhältnisse oder Bildungsstand sein. Die konkrete Auswahl bleibt dann den mit der Befragung beauftragten
Personen überlassen.
