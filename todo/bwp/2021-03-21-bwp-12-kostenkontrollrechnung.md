---
title: "12. Kostenkontrollrechnung"
date: 2021-01-25 09:50:17 +0200
---

- Der Vergleich von Vor- und Nachkalkulation heißt: **Kostenkontrollrechnung**.
- Die Erfahrungs- und Durchschnittswerte aus vorangegangenen Abrechnungsperioden nennt man **Normalkosten**!
- Als **Istkosten** bezeichnet man die in der betrachteten Abrechnungsperiode *tatsächlich* angefallenen Werte.
- In der Kostenkontrollrechnung werden die Ist-Kosten den Normalkosten gegenübergestellt.
- Sind die Normalkosten größer als die Ist-Kosten (+-Zeichen), so spricht man von einer **Überdeckung**.
- Sind die Normalkosten kleiner als die Ist-Kosten (- -Zeichen), so spricht man von einer **Unterdeckung**.

Beispiel:

Die IT-Solutions GmbH bietet auch Tastaturen an. Dabei wurde im Dezember 2016 mit folgenden Werten pro Tastatur
kalkuliert:

- Fertigungsmaterial 2,50 €
- Fertigungslöhne 1,80 €
- Normal-Gemeinkostenzuschlagssätze:
  - Materialgemeinkosten 8,35 %
  - Fertigungsgemeinkosten 174 %
  - Verwaltungsgemeinkosten 14,14 %
  - Vertriebsgemeinkosten 5,83 %
- und außerdem mit einem Gewinn von 5 %.
- Skonto und Rabatt wird dem Kunden nicht gewährt $$\rightarrow$$ BVP = ZVP = LVP

- V: Vorkalkulation / Normalkosten
- D: Über (+) oder Unterdeckung (-)
- N: Nachkalkulation / Ist-Kosten

|                            | € (V) | % (V)  | % (V) | € (D) | € (N) | % (N)  |
| -------------------------- | ----: | -----: | ----: | ----: | ----: | -----: |
| Fertigungsmaterial         | 2,50  | 100,00 |       | -0,20 | 2,70  |        |
| + Materialgemeinkosten     | 0,21  | 8,35   |       | -0,05 | 0,26  | 9,50   |
| = Materialkosten           | 2,71  | 108,35 |       |       | 2,96  |        |
| Fertigungslöhne            | 1,80  |        | 100   | +0,10 | 1,70  |        |
| + Fertigungsgemeinkosten   | 3,13  |        | 174   | +0,26 | 2,87  | 169,00 |
| = Fertiungskosten          | 4,93  |        | 274   |       | 4,57  |        |
| = Herstellkosten (MK + FK) | 7,64  | 100,00 |       |       | 7,53  |        |
| + Verwaltungsgemeinkosten  | 1,08  | 14,14  |       | +0,07 | 1,01  | 13,40  |
| + Vertriebsgemeinkosten    | 0,45  | 5,83   |       | -0,02 | 0,47  | 6,22   |
| = Selbstkosten             | 9,17  | 119,97 | 100   | +0,16 | 9,01  |        |
| + Gewinn                   | 0,46  |        | 5     |       | 0,62  | 6,88   |
| = Verkaufspreis            | 9,63  |        | 105   |       | 9,63  |        |
