---
title:  "01. Bedürfnisse"
date:   2019-02-15 06:05:00 +0100
---

# Bedürfnisse

**WICHTIG**: Ein Bedürfnis kann mehreren Kategorien zugeordnet werden.

{{< mermaid class="text-center">}}
graph TD
    A[Bedürfnisse]
    B[Individualbedürfnisse]
    C[Kollektivbedürfnisse]
    E[Existenzbedürfnisse]
    F[Kulturbedürfnisse]
    G[Luxusbedürfnisse]

    A --> B
    A --> C
    B --> E
    B --> F
    B --> G
{{< /mermaid >}}

- **Individualbedürfnisse**: Bedürfnisse, des Einzelnen, die jeder für sich allein befriedigt.
- **Kollektivbedürfnisse**: Bedürfnisse der Gesellschaft, welche gemeinsam befriedigt werden.
- **Existenzbedürfnisse**: unbedingt, notwendig $$\rightarrow$$ zur Lebenserhaltung
- **Kulturbedürfnisse**: weitgehend, selbstverständlich $$\rightarrow$$ zur Anerkennung in der Gesellschaft mit einer
  bestimmten Kultur und zur Anhebung der Lebensart
- **Luxusbedürfnisse**: relativ, entbehrlich $$\rightarrow$$ zur Erhöhung von Lebensstandard und Prestige, meistens mit
  großem Aufwand verbunden

## Bedürfniseinteilung

- **Dringlichkeit**:
  - Granulare Unterteilung: Existenz-, Kultur- und Luxusbedürfnisse
  - Unterschiedliche Einordnung für identische Bedürfnisse. (Abhängig von Umwelt u. ä. $$\rightarrow$$ Industrieland
    $$\leftrightarrow$$ Entwicklungsland)
- **Art der Befriedigung**:
  - Granulare Unterteilung: Materielle und Immaterielle Bedürfnisse
- **Demjenigen, der die Bedürfnisse erfüllt**:
  - Granularere Unterteilung: Individual- und Kollektivbedürfnisse

## Bedürfnismatrix

| Einteilung nach...                   | Arten            | Beschreibung                                 | Beispiel                                     |
| ------------------------------------ | ---------------- | -------------------------------------------- | -------------------------------------------- |
| der Dringlichkeit                    | Existenz         | zur Erhaltung des Lebens                     | Schlaf, Essen, Trinken                       |
|                                      | Kultur           | Anpassung an den jeweiligen Kulturkreis      | Information                                  |
|                                      | Luxus            | Positive Abhebung vom Kulturkreis            | Urlaub                                       |
| dem Träger der Bedürfnisbefriedigung | individuell      | befriedigt den Einzelnen selbst              | Schlafen, Essen, Urlaub                      |
|                                      | kollektiv        | befriedigt die Gemeinschaft (z. B. Staat)    | Infrastruktur                                |
| dem Bedürfnisgegenstand              | materiell        | auf Sachen bezogen                           | Kleidung, Nahrungsmittel                     |
|                                      | immateriell      | auf Dienstleistungen, Rechte, Geist bezogen  | Anerkennung, soziale Kontakte                |
| dem Bewusstseinsgrad                 | offen/bewusst    | sind aktuell bewusst vorhanden               | Entspannung                                  |
|                                      | versteckt/latent | sind noch nicht oder unbewusst vorhanden     | Urlaub – erst durch Werbung bewusst gemacht. |

