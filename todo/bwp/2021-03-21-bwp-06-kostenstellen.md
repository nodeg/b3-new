---
title: "06. Kostenstellen"
date: 2020-12-20 09:50:17 +0200
---

Während es bei der Kostenartenrechnung um das Problem ging, welche Kosten angefallen sind, geht es jetzt um die Frage,
wo die Kosten angefallen sind. Es muss ein Weg gefunden werden, wie die Gemeinkosten möglichst verursachungsgerecht auf
die Kostenträger umgelegt werden können. Jede Entstehung von Kosten lässt sich einem bestimmten räumlichen Bereich
zuordnen, den Kostenstellen.

In der Reihe der betrieblichen Grundfunktionen fallen bei nahezu allen Unternehmen folgende vier Hauptkostenstellen an:

- Kostenstelle I: Material
- Kostenstelle II: Fertigung
- Kostenstelle III: Verwaltung
- Kostenstelle IV: Vertrieb
