---
title:  "16. Leitungssysteme"
date:   2019-02-15 07:00:00 +0100
---

## Einliniensysteme

Jede untergeordnete Stelle kann nur von einer direkt übergeordneten Instanz Weisungen erhalten und berichtet ihrerseits
auch nur an diese.

| Vorteile                             | Nachteile              |
| ------------------------------------ | ---------------------- |
| Abgegrenzte Zuständigkeiten          | Abteilungen "isoliert" |
| Klarer Aufbau                        |                        |
| schnelles Treffen von Entscheidungen |                        |

## Mehrliniensysteme

Jede untergeordnete Stelle kann von mehreren direkt übergeordneten Instanzen Weisungen entgegennehmen und an diese
berichten.

| Vorteile                     | Nachteile                                    |
| ---------------------------- | -------------------------------------------- |
| demokratische Entscheidungen | Kompetenzüberschneidungen und Streitigkeiten |
| Entlastung der Führungsebene |                                              |
| Verkürzte Instanzwege        |                                              |

## Stab-Linien-System

Den Instanzen sind Stäbe ohne Weisungsbefugnis zur Unterstützung (Beratung, Information, Entscheidungsvorbereitung)
zugeordnet.

| Vorteile                                      | Nachteile                              |
| --------------------------------------------- | -------------------------------------- |
| Entlastung Führungskräfte                     | höhere Kosten durch teure Spezialisten |
| Verbesserte Entscheidungen durch Spezialisten | fachliche Abhängigkeit der Instanzen   |

## Spartenorganisation (Divisionale o. Profit-Center)

Alle Verrichtungen, die sich auf gleichartige Objekte beziehen, werden zusammengefasst. (Unternehmen im Unternehmen)

| Vorteile                             | Nachteile             |
| ------------------------------------ | --------------------- |
| Mitarbeiter denken produktorientiert | Spartenegoismus       |
| Rivalität                            | überzogene Konkurrenz |

## Matrix-Organisation

Überlagerung von zwei Weisungssystemen, wobei das eine verrichtungsorganisiert, das andere produkt- oder
projektorganisiert ist.

| Vorteile                                          | Nachteile                                         |
| ------------------------------------------------- | ------------------------------------------------- |
| Problemlösung durch verschiedene Fachspezialisten | Kompetenzprobleme                                 |
| Geringe Gefahr des Spartenegoismus                | Evtl. Schlichtung durch Geschäftsleiter notwendig |
