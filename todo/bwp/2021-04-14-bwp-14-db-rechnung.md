---
title: "14. DB-Rechnung"
date: 2021-02-01 09:50:17 +0200
---

Vollkostenrechnung vs. Teilkostenrechnung – (Deckungsbeitragsrechnung)

Ein Produzent von u.a. High-End-PC-Lautsprechern verkauft einen Lautsprecher T2000 zu 189,00 € netto. Zu diesem Preis
wurden im letzten Quartal 10.000 Stück produziert aber konnten nur zu einem kleinen Teil verkauft werden. Bei dieser
Stückzahl betragen die Kosten für den Rohstoffverbrauch 62,50 € und die Lohnkosten 50,00 € je Stück. An sonstigen Kosten
(Miete, Versicherungen, Abschreibungen,…) fallen noch 56,25 € je Stück an.

Die Preiskalkulation für einen Lautsprecher lautet nun:

$$
\begin{matrix}
  & Selbstkosten          & 168.75 &  100 % \\
+ & Gewinn (12%)          &  20.25 &   12 % \\
= & Verkaufspreis (netto) & 189.00 &  112 %
\end{matrix}
$$

Die Auftragslage lässt sehr zu wünschen übrig. In dieser Situation bietet uns eine große namenhafte dänische Firma an,
die komplette Kapazität von 10.000 Stück zu einem Preis von 159,00 Euro abzunehmen. Ermitteln Sie den daraus
entstehenden Gewinn/Verlust und entscheiden Sie, ob Sie der Lautsprecher in der gegebenen Situation der Firma verkaufen
wollen!

$$
\begin{matrix}
  & Selbstkosten          & 168.75 & 100 % \\
+ & Gewinn Verlust        &   9.75 &       \\
= & Verkaufspreis (netto) & 159.00 &
\end{matrix}
$$
