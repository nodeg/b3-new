---
title: "02. Portfolioanalyse"
date: 2019-10-17 10:05:00 +0200
---

Die von der amerikanischen Unternehmensberatung "Boston Consulting Group", kurz BCG, entwickelte Portfolio-Matrix
vergleicht das Marktwachstum von Produktgruppen mit dem relativen Marktanteil, den ein Unternehmen auf diesem Markt
innehat.

Auf der senkrechten Achse wird als unternehmensexterne Größe das Marktwachstum, d.h. der Anstieg der Nachfrage (bzw.
infolgedessen auch des Umsatzes) nach der jeweiligen Produktgruppe in Prozent angegeben. Die Grenze zwischen einem
tendenziell hohen und einem niedrigen Wachstum liegt bei 10%. Das bedeutet, dass ein Markt, der ein Wachstum von mehr
als 10% hat, ein Wachstumsmarkt ist. (Beispiel: Im Jahr 01 werden auf dem Markt für Standardbürostühle 100 Mio. €
umgesetzt. Im Jahr 02 liegt der Umsatz bei 120 Mio. € $$\rightarrow$$ Marktwachstum 20%).

Die waagrechte Achse gibt als unternehmensinterne Größe den relativen Marktanteil eines Unternehmens in Bezug auf den
stärksten Konkurrenten an. Hier wird die Grenze bei 100% gezogen, dort ist der Umsatz beider Unternehmen gleich groß.
Links davon ist der relative Marktanteil kleiner als 100%, d.h. das eigene Unternehmen verkauft weniger als der
Konkurrent.

Beispiel: Die DataSol GmbH macht mit Produkt X einen Umsatz von 30 Mio. € und der stärkste Konkurrent macht mit einem
vergleichbaren Produkt einen Umsatz von 60 Mio. €. Das heißt der relative Marktanteil der DataSol GmbH (hier: 50%) ist
kleiner als der der Konkurrenten.

Durch diese Einteilung lassen sich vier Felder bilden, die unterschiedliche Eigenschaften aufweisen.

Die Produkte werden in Form von Kreisen in den jeweiligen Feldern dargestellt, wobei die Kreisgröße die Höhe des
Umsatzes angibt, der mit diesem Produkt erzielt wird.

Nachdem ein Unternehmen seine Produkte in diese einzelnen Felder eingeordnet hat, lassen sich daraus strategische
(planerische) Entscheidungen ableiten. Im Folgenden werden die vier Produktfelder erklärt und mögliche strategische
Maßnahmen aufgezeigt. Allgemeine Strategieempfehlungen der BCG (=Normstrategien):

## Fragezeichen (question marks)

"Fragezeichen" sind durch ein hohes Marktwachstum und einen geringen Marktanteil gekennzeichnet. Aufgrund des hohen
Marktwachstums besteht jedoch die Aussicht, den Absatz und den Marktanteil zu erhöhen. Für ein Unternehmen stellt sich
jeweils die Frage, ob es sich lohnt, in ein Fragezeichen zu investieren. Aus diesem Grund sollten diese Produkte genau
beobachtet werden, um zu entscheiden, ob sie entweder stark gefördert werden (Offensivstrategie) oder aber aus dem
Absatzprogramm gestrichen werden.

## Sterne (stars)

"Starprodukte" leuchten am Markt. Sie haben ein hohes Wachstum und einen relativ hohen Markteinteil. Das Unternehmen ist
mit diesen Produkten Marktführer. Die Strategie für "stars" lautet, den Marktanteil mindestens auf seinem gegenwärtigen
standzuhalten oder nach Möglichkeit durch starke Investitionen auszubauen (Investitionsstrategie).

## Milchkühe (cash cows)

"Milchkühe" haben einen hohen relativen Marktanteil und sind Marktführer. Das Wachstum ist jedoch gering. Die Bezeichnung
"Milchkühe" kommt daher, dass sich die Produkte "melken" lassen. Das heißt, sie werfen hohe Gewinne ab. Wird eine
"Milchkuh" auch in Zukunft noch als erfolgsversprechend angesehen, so sollte der Marktanteil gehalten werden
(Abschöpfungsstrategie). Bestehen dagegen eher trüber Zukunftsaussichten, sollte man aus diesen Produkten jetzt so viel
wie möglich an Gewinn herausholen.

## Arme Hunde  (poor dogs)

Die "Armen Hunde" sind Problemprodukte eines Unternehmens. Sie besitzen einen niedrigen Marktanteil und ihr Markt wächst
wenig oder gar nicht. Diese Produkte erwirtschaften in der Regel niedrige Gewinne oder schreiben sogar rote Zahlen. Aus
diesem Grund sollte sich ein Unternehmen fragen, ob es solche Produkte nicht aus dem Markt nimmt und keine Investitionen
mehr in das Produkt steckt (Desinvestitionsstrategie).

## Zusammenfassung

Es lässt sich sagen, dass sich aus der Portfolioanalyse erste wichtige Hinweise und Ansatzpunkte für absatzpolitische
Maßnahmen ableiten lassen. Die Portfolio-Analyse ist somit auch Steuerungsinstrument im Rahmen des Absatzcontrollings.
Bevor jedoch endgültige absatzpolitische Entscheidungen getroffen werden, sollten unbedingt weitere Überlegungen
angestellt werden.
