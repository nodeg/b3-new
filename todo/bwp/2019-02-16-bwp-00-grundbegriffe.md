---
title:  "00. Grundbegriffe"
date:   2019-02-15 06:00:00 +0100
---

# Wirtschaftliche Grundbegriffe

Warum wirtschaften wir? - Um die Knappheit an Gütern zu überwinden.

- "Bedürfnisse" > "Güter"
- Wirtschaft versucht die Knappheit der Güter mit ökonomischen Mitteln zu überwinden.
- Wer wirtschaftlich handelt, hat Kontrolle, Einfluss und Macht.
- Es ist nicht möglich sich alle Bedürfnisse zu erfüllen. $$\rightarrow$$ Es besteht immer ein Wunsch nach mehr und
  hochwertigeren Produkten $$\rightarrow$$ Mangelempfinden
- **Bedürfnisse**: Was möchte ich haben? (=Wünsche)
- **Bedarf** (=Kaufkraft): Der Teil der Bedürfnisse, den sich ein Haushalt leisten kann.
- **Nachfrage**: Wenn finanzielle Mittel für einen tatsächlichen Kauf von Sachgütern oder Dienstleistungen ausgegeben
  werden.

{{< mermaid class="text-center">}}
graph LR;
    Bedürfnisse --> Bedarf
    Bedarf --> Nachfrage
{{< /mermaid >}}


