---
title:  "14. Unternehmensformen"
date:   2019-02-15 06:50:00 +0100
---

## Einzelunternehmen

- **Gründung**:
  - durch den Einzelunternehmer (alleiniger Gesellschafter des Unternehmens)
  - formlos - Gewerbeanmeldung (keine besonderen gesetzlichen Formvorschriften)
  - evtl. HR-Eintragung: Firma, Sitz, Gegenstand
- **Firma**:
  - Ohne HR-Eintrag: Vor- und Nachnahme des Geschäftsführers & Ergänzung um Branchen- oder Fantasienamens
  - Mit HR-Eintrag: Personen-, Sach- oder Fantasiename & Rechtsformzusatz (e.K., e.Kfr. oder e.Kfm.)
- **Kapitalaufbringung**: Kein gesetzliches Mindestkapital $\rightarrow$ Einzelunternehmer bringt Kapital alleine auf.
- **Geschäftsführung** & Vertretung: Durch den Einzelunternehmer
- **Haftung**: Unbeschränkt (mit Privat- und Gesellschaftsvermögen) & unmittelbar (direkt verklagbar)
- **Ergebnisverteilung**: alleiniger Gewinnanspruch & alleinige Verlustübernahme
- **Publizität**: keine Publizitätspflicht (keine Veröffentlichung des Geschäftsergebnisses)
- **Besteuerung**:
  - Einkommenssteuer
  - Gewerbesteuer

**Vorteile**:
- Geringer Aufwand zur Gründung und Führung: Einfacher Weg in die Selbstständigkeit
- Alleinige Gewinnbeteiligung

**Nachteile**:
- Haftung mit Privatvermögen

## Gesellschaftsunternehmen

### Personengesellschaft

#### Offene Handels Gesellschaft

- OHG = Offene Handels Gesellschaft
- Zweck: Betreiben eines Handelsgewerbes mit mehreren Personen in einer Firma.
- Stellt eine erweiterte Einzelunternehmung dar mit dem Motto "Gleiche Rechte - Gleiche Pflichten"
- i. d. R. für kleinere und mittlere Betriebe

- **Gründung**: Gesellschaftsvertrag zwischen zwei oder mehr Personen & Eintragung ins HRA mit Namen, Geb. Datum und
  Wohnort der Gesellschafter, sowie Firmensitz und Beginn der Gesellschaft
- **Geschäftsführung**: Jeder Gesellschafter vertritt das Unternehmen nach außen und ist somit berechtigt und
  verpflichtet die Geschäfte zu führen, jedoch können Ausnahmen mit dem Gesellschaftsvertrag geregelt werden. Für
  außergewöhnliche Geschäfte ist der Beschluss aller Gesellschafter erforderlich.
- **Haftung**: Für Verbindlichkeiten der Firma haftet jeder Gesellschafter unbeschränkt als Gesamtschuldner mit dem
  Privat- und Betriebsvermögen.
- **Ergebnisverteilung**: Jeder Gesellschafter erhält, ohne andere Regelung im Gesellschaftsvertrag, 4 % seiner
  Kapitaleinlage. Gewinn der übrig bleibt wird in gleichen Teilen an die Gesellschafter ausgeschüttet. Verluste werden
  gleichermaßen auf die Gesellschafter aufgeteilt. Ein Gesellschafter hat die Möglichkeit bis zu 4 % seines
  Kapitalanteils für private Zwecke zu entnehmen.
- **Finanzierung**: Die erhöhte Kreditwürdigkeit der OHG kommt zustande durch die Haftung von mindestens zwei
  Gesellschaftern mit ihrem Gesamtvermögen. Die Kapitalbasis wird erhöht durch nicht an die Gesellschafter
  ausgeschüttete Gewinne, die Aufnahme eben derer und Gesellschafter welche ihre Kapitaleinlagen erhöhen.
- **Besteuerung**: Die OHG ist kein selbstständiges Steuersubjekt, somit besteht eine Einkommenssteuerpflicht der
  Gesellschafter zum Zeitpunkt der Gewinnentstehung im Betrieb. Die Gehälter der geschäftsführenden Gesellschafter sind
  nicht als Betriebsausgaben abzugsfähig.
- **Auflösung**: Sollten keine anderen vertraglichen Regelungen getroffen werden gelten folgende Regelungen. Die
  Auflösung der OHG kann durch den Ablauf des Gesellschaftsvertrags, Beschluss der Gesellschafter, Eröffnung des
  Insolvenzverfahrens über des Gesellschaftsvermögens oder eine gerichtliche Entscheidung geschehen. Der Tod oder die
  Kündigung eines Gesellschafters hat keine Bewandtnis für die Existenz einer OHG.

#### Kommanditgesellschaft

- KG = Kommanditgesellschaft
- Zweck: Betreiben eines Handelsgewerbes mit mehreren Personen in einer Firma.
- Komplementär: Vollhaftende natürliche Person (Privat- und Firmenvermögen)
- Kommanditist: Teilhaftender Gesellschafter, kann natürlich als auch juristisch sein. Ist ein Komplementär eine GmbH,
  so firmiert die KG als _GmbH & Co KG_.

- **Gründung**: Eintragung ins HRA und Gesellschaftsvertrag, es ist im HR die Haftsumme (Einlage) der Kommanditisten
  eingetragen.
- **Geschäftsführung**: Die Komplementäre führen die Geschäfte der KG, Kommanditisten haben lediglich ein Kontrollrecht.
- **Haftung**: Ein Komplementär haftet unbeschränkt mit Privat- und Firmenvermögen. Ein Kommanditist lediglich mit der
  Einlage.
- **Ergebnisverteilung**: Wie bei der OHG erhält jeder Gesellschafter bis zu 4 % seiner Kapitaleinlage. Alles darüber
  hinausgehende muss im Gesellschaftsvertrag geregelt sein. I. d. R. ist es üblich, dass der Komplementär wegen seines
  höheren Risikos einen größeren Anteil erhält als ein Kommanditist. Auch Verluste werden "angemessen" aufgeteilt.
- **Finanzierung**: Die KG ähnelt einer Kapitalgesellschaft, da sie Kommanditisten aufnehmen kann, welche nicht mit der
  Geschäftsführung betraut werden. Kommanditisten haften nur beschränkt und sind somit keinem größeren Risiko
  ausgesetzt. Die erhöhte Transparenz und einfache Möglichkeit zur Kapitalerhöhung werden zumeist auch von Kreditgebern
  honoriert.
- **Besteuerung**: Der Gewinn wird einheitlich von der Gesellschaft ermittelt, dann nach einem bestimmten Schlüssel auf
  die Gesellschafter verteilt und von diesen individuell versteuert. Dies rührt daher, dass die KG eine
  Personengesellschaft ist.
- **Auflösung**: Kündigung oder Ablauf des Gesellschaftsvertrages, Beschluss der Gesellschafter, Konkurs über das
  Vermögen der Gesellschaft oder eines Komplementärs und durch den Tod eines Komplementärs, sofern der
  Gesellschaftsvertrag nichts anderes vorsieht.

### Kapitalgesellschaft

#### Aktien Gesellschaft

> AG = Aktien Gesellschaft

Die Aktiengesellschaft ist eine Kapitalgesellschaft. Die gegründete Gesellschaft ist von allen Gründern, dem ersten
Vorstand und dem ersten Aufsichtsratsvoritzenden zum Handelsregister (HRB) anzumelden. Die Eintragung ist konstiutiv und
die AG wird anschließend zu einer juristischen Person. Belegt einer der Gründer eine der Positionen Vorstands- oder
Aufsichtsratsmitglied doppelt, so muss die Gründung von einem fachkundigen Dritten geprüft werden. In jedem Fall
bestellt das Gericht Gründungsprüfer, welche nach der Anhörung der Industrie- und Handelskammer, einen Prüfungsbericht
aufstellen müssen.

Um eine AG zu Gründen werden mindestens 50.000€ benötigt. Das Grundkapital wird anschließend in Aktien zerlegt, dies
ermöglicht eine Beteiligung am unternehmen mit kleinen Beträgen. Die Personen die in dieser Weise an der Unternehmung
beteiligt sind werden als Aktionäre bezeichnet, sie sind somit am Grundkapital der Firma beteiligt.

```
§ 1 Wesen der Aktiengesellschaft
(1) Die Aktiengesellschaft ist eine Gesellschaft mit eigener Rechtspersönlichkeit. Für die Verbindlichkeiten der Gesellschaft haftet den Gläubigern nur das Gesellschaftsvermögen.
(2) Die Aktiengesellschaft hat ein in Aktien zerlegtes Grundkapital.
```

Die Beteiligung am Gewinn einer Aktiengesellschaft wird gemessen an der Anzahl der Aktien im Besitz einer Person. Die
Gewinnausschüttung wird Dividende genannt.

Die Vorteile einer AG sind die einfache Finanzierung und eine einfache Gründung. Im Fall, dass eine Expansion der AG
ansteht, kann mit sehr einfachem Wege neues Kapital erwirtschaftet werden durch die Aufnahme von neuen Aktionären. Es
können nicht nur private Personen Aktionäre sein, sondern auch juristische Personen. Ein weiterer Vorteil ist ebenfalls
die Haftung, eine AG haftet nur mit ihrem Gesellschaftsvermögen. Auch können Lieferanten, neue Kunden oder Mitarbeiter
durch Aktienanteile schnell und unkompliziert an das Unternehmen gebunden werden. Nachteile der AG sind ein erhöhter
Gründungsaufwand und das erhöhte Grundkapital. Ein weiterer großer Nachteil ist (gerade für kleine AGs) ein erhöhter
Verwaltungsaufwand. Auch sind die Vorschriften bezüglich Buchführung, Bilanzlegung und die Veröffentlichung hiervon ein
Einschnitt in den unternehmerischen Gestaltungsspielraum. Für die Mitglieder des Aufsichtsrates ergibt sich zudem der
Nachteil der persönlichen Haftung.

#### Gesellschaft mit beschränkter Haftung

- GmbH = Gesellschaft mit beschränkter Haftung
- **Gründung**: Konstitutiv (Rechtsfähig, wenn ins HRB eingetragen)
- TODO

### Genossenschaft

- Was ist das besondere an Genossenschaften? - Demokratische Struktur, alle Mitarbeiter sind auch gleichzeitig
  Miteigentümer
- Wer wird für die Gründung einer Genossenschaft benötigt? - Drei Personen & Satzung zur Klärung der rechtlichen
  Verhältnisse
- Welche Organe hat eine Genossenschaft? - Vorstand, Aufsichtsrat, Generalversammlung
- Wie sieht die Ergebnisverwendung einer Genossenschaft aus? - Gewinn wird einbehalten und an die Genossen ausgeschüttet
- Wie sieht der geschichtliche Hintergrund von Genossenschaften aus? - Landwirtschaftliche Einkaufsgemeinschaften
  $$\rightarrow$$ im Verbund günstigere Konditionen
- Wie lautet der Leitsatz von Genossenschaften? - "Gemeinsam sind wir stark" oder "Selbsthilfe, Selbstverantwortung,
  Selbstverwaltung"
- Was ist das besondere an der Rechtsform e.G im Vergleich zur AG und GmbH? - Im Vordergrund steht nicht die
  Gewinnerzielung, sondern die Förderung der Mitglieder.
- WIe wird man Mitglied einer Genossenschaftsbank? - Man leistet eine Einlage (= Geschäftsanteile!) die sie bei Austritt
  zurückbezahlt bekommen.
