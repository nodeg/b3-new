---
title: "10. Differenzkalkulation"
date: 2021-01-18 09:50:17 +0200
---

Die IT-Solutions GmbH kalkuliert die Selbstkosten für einen Blue-Ray-Rohling. An Einzelkosten wurden durch Belege ermittelt:
Fertigungsmaterial 0,45 €; Fertigungslöhne 0,17 € und Sondereinzelkosten der Fertigung 0,06 €.
Die Gemeinkostenzuschlagssätze für die vier Kostenstellen sind aus der Gesamtkalkulation bekannt:
MGK 8,35 %; FGK 174,00 %; VwGK 14,14 %; VtGK 5,83 %.

Aus Konkurrenzgründen kann der Rohling nur zu einem Listenverkaufs-preis von 1,39 € angeboten werden.

Wie hoch ist der Gewinn in € und Prozenten, wenn den Kunden 3% Skonto und 5% Rabatt eingeräumt werden?


|     | Schema                           | €    | %      | %      |
| --- | -------------------------------- | ---: | -----: | -----: |
|     | Fertigungsmaterial               | 0,45 |        | 100,00 |
| +   | Materialgemeinkosten             | 0,04 |        | 8,35   |
|     | Materialkosten                   | 0,49 |        |        |
|     | Fertigungslöhne                  | 0,17 |        | 100,00 |
| +   | Fertigungsgemeinkosten           | 0,30 |        | 174,00 |
| +   | Sondereinzelkosten der Fertigung | 0,06 |        |        |
|     | Fertigungskosten                 | 0,53 |        |        |
|     | Herstellkosten                   | 1,02 |        | 100,00 |
| +   | Verwaltungsgemeinkosten          | 0,14 |        | 14,14  |
| +   | Vertriebsgemeinkosten            | 0,06 |        | 5,83   |
|     | Selbstkosten                     | 1,22 | 100,00 |        |
| +   | Gewinn                           | 0,06 | 4,92   |        |
|     | Barverkaufspreis                 | 1,28 |        | 97,00  |
| +   | Kundenskonto                     | 0,04 |        | 3,00   |
|     | Zielverkaufspreis                | 1,32 | 95,00  | 100,00 |
| +   | Kundenrabatt                     | 0,07 | 5,00   |        |
|     | Listenverkaufspreis              | 1,39 | 100,00 |        |

Nebenrechnung:

Gewinn = (0,06 * 100) / 1,22 = 4,92 (%)
