---
title:  "18. Vollmacht und Prokura"
date:   2019-03-26 18:00:00 +0100
---

## Präambel

- Umfang der Vollmacht: Rechtsgeschäfte, die selbstständig durchgeführt werden können.
- Mit besonderer Ermächtigung: Geschäfte, für die eine besondere Ermächtigung benötigt wird.
- Erteilung der Vollmacht (=Beginn der Vollmacht): Wie kann die Vollmacht erteilt werden?
- Zeichnung: Unterschriftszusatz des Bevollmächtigten

## Prokura

- Ein Mitarbeiter der eine Prokura erhält wird auch Prokurist genannt!
- **Umfang der Vollmacht**: Alle gerichtlichen und außergerichtlichen Rechtsgeschäfte
- **Mit besonderer Ermächtigung**: Grundstücke verkaufen oder belasten
- **Erteilung der Vollmacht**:
    - Nur vom Kaufmann/Geschäftsführer persönlich
    - Muss ausdrücklich (schriftlich oder mündlich) erteilt werden
    - Muss im HR eingetragen werden (deklaratorische Wirkung)
- **Zeichnung**: _ppa._ oder _pp._ (per Prokura)

## Allgemeine Handlungsvollmacht/Generalhandlungsvollmacht

- Ein Mitarbeiter der mit einer Handlungsvollmacht ausgestattet wird, nennt sich Handlungsbevollmächtigter!
- **Umfang der Vollmacht**: Alle gewöhnlichen Rechtsgeschäfte (branchenabhängig)
- **Mit besonderer Ermächtigung**:
    - Grundstücke verkaufen oder belasten
    - Darlehen aufnehmen
    - Prozesse führen
- **Erteilung der Vollmacht**:
    - Schriftlich, mündlich oder stillschweigend durch Duldung
    - Keine Eintragung ins Handelsregister möglich
    - Jeder Bevollmächtigte kann im Rahmen seiner Vollmacht Untervollmachten vergeben
- **Zeichnung**: i.V. (in Vollmacht bzw. Vertretung) oder i.A. (im Auftrag)

## Arthandlungsvollmacht

- **Umfang der Vollmacht**: Eine bestimmte Art von Rechtsgeschäften (Bsp.: Einkäufer)
- **Mit besonderer Ermächtigung**: Alle Rechtsgeschäfte (außer der genehmigten Art)
- **Erteilung der Vollmacht**:
    - Schriftlich, mündlich oder stillschweigend durch Duldung
    - Keine Eintragung ins Handelsregister möglich
    - Jeder Bevollmächtigte kann im Rahmen seiner Vollmacht Untervollmachten vergeben
- **Zeichnung**: i.V. (in Vollmacht bzw. Vertretung) oder i.A. (im Auftrag)

## Einzelhandlungsvollmacht/Spezialhandlungsvollmacht

- **Umfang der Vollmacht**: Eim einzelnes Rechtsgeschäft (Bsp.: eine Bestellung vornehmen)
- **Mit besonderer Ermächtigung**: Alle Rechtsgeschäfte
- **Erteilung der Vollmacht**:
    - Schriftlich, mündlich oder stillschweigend durch Duldung
    - Keine Eintragung ins Handelsregister möglich
    - Jeder Bevollmächtigte kann im Rahmen seiner Vollmacht Untervollmachten vergeben
- **Zeichnung**: i.V. (in Vollmacht bzw. Vertretung) oder i.A. (im Auftrag)

## Übersicht

- **X**: Gesetzlich verboten
- **V**: besondere Vollmacht nötig
- **E**: Eigenverantwortlich möglich

| Unternehmer                    | Prokura | allgemeine Handlungsvollmacht | Artvollmacht (Bsp. Einkäufer) | Einzelvollmacht |
| ------------------------------ | :-----: | :---------------------------: | :---------------------------: | :-------------: |
| Eid leisten                    |    X    |               X               |               X               |        X        |
| Steuererklärung unterschreiben |    X    |               X               |               X               |        X        |
| Bilanz unterschreiben          |    X    |               X               |               X               |        X        |
| HR-Eintragungen anmelden       |    X    |               X               |               X               |        X        |
| Insolvenz anmelden             |    X    |               X               |               X               |        X        |
| Geschäft verkaufen             |    X    |               X               |               X               |        X        |
| Prokura erteilen               |    X    |               X               |               X               |        X        |
| Gesellschafter aufnehmen       |    X    |               X               |               X               |        X        |
| Grundstücke belasten           |    V    |               V               |               V               |        V        |
| Grundstücke verkaufen          |    V    |               V               |               V               |        V        |
| Grundstücke kaufen             |    E    |               V               |               V               |        V        |
| Wechsel unterschreiben         |    E    |               V               |               V               |        V        |
| Prozesse führen                |    E    |               V               |               V               |        V        |
| Darlehen aufnehmen             |    E    |               V               |               V               |        V        |
| Zahlungsgeschäfte erledigen    |    E    |               E               |               V               |        V        |
| Ersatzanschaffungen tätigen    |    E    |               E               |               V               |        V        |
| Mitarbeiter entlassen          |    E    |               E               |               V               |        V        |
| Mitarbeiter einstellen         |    E    |               E               |               V               |        V        |
| Einkaufen                      |    E    |               E               |               E               |        V        |
