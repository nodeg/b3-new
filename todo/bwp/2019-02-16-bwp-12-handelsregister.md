---
title:  "12. Handelsregister"
date:   2019-02-15 06:40:00 +0100
---

> Handelsregister: Ein vom Amtsgericht geführtes Verzeichnis aller Kaufleute eines Amtsgerichtsbezirks.

**Aufgabe**: Information der Öffentlichkeit über die rechtlichen Tatbestände der eingetragenen Firmen. Veröffentlichung
seit 2007 elektronisch.

Eintragung in:

- Abteilung A (HRA)
  - Einzelunternehmen
  - Personengesellschaft (OHG, KG)

- Abteilung B (HRB)
  - Kapitalgesellschaften (AG, GmbH)

Wirksamkeit der Eintragungen

- Konstitutiv (rechtserzeugend) $$\rightarrow$$ Tatbestand durch die Eintragung wirksam
- Deklaratorisch (rechtsbekundend) $$\rightarrow$$ wirksamer Tatbestand wird durch Eintragung dokumentiert
