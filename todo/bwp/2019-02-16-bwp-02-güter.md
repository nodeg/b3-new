---
title:  "02. Güter"
date:   2019-02-15 06:10:00 +0100
---

{{< mermaid class="text-center">}}
graph TD
    A[Güter]
    B[Freie Güter]
    C[wirtschaftliche/knappe Güter]
    D[materielle/Sachgüter]
    E[Immaterielle Güter]
    F[Dienstleistungen]
    G[Rechte]
    H[Konsumgüter Konsumtion]
    I[Produktionsgüter Produktion]
    J[Gebrauchsgüter]
    K[Verbrauchsgüter]
    L[Betriebsmittel]
    M[Werkstoffe]

    A --> B
    A --> C
    C --> D
    C --> E
    D --> H
    D --> I
    I --> L
    I --> M
    H --> J
    H --> K
    E --> F
    E --> G
{{< /mermaid >}}

- **Güter**: Alle Mittel zur Bedürfnisbefriedung.
  - **Freie Güter**: sind unendlich vorhanden, können durch besondere Umstände zu knappen Gütern werden. (Luft auf der
    ISS).
  - **Wirtschaftliche Güter**/knappe Güter: sind begrenzt verfügbar und haben einen Wert mit welchem sie Kosten
    verursachen.
    - **Materielle-/Sachgüter**:
      - **Konsumgüter**: Güter für Privatpersonen
        - **Gebrauchsgüter**:
        - **Verbrauchsgüter**:
      - **Produktionsgüter**: Für Unternehmen
        - **Betriebsmittel**: Gut, welches gebraucht wird.
        - **Werkstoffe**: Gut, welches verbraucht wird.
    - **Immaterielle Güter**:
      - **Dienstleistungen**: Der größte Teil des immateriellen Gutes ist eine Arbeitsleistung in Verbindung mit einer
        Produktion (bspw. Haarschnitt beim Friseur). [^1]
      - **Rechte**: Die Berechtigung für den Anspruch auf etwas. Dies gilt auch für Dinge wie bspw. Macht, aber genauso
        für Dinge wie Leben. [^2]
- **Komplementärgüter**: sich ergänzende Güter
- **Substitutionsgüter**: sich ersetzende Güter

[^1]: Quelle: <https://wirtschaftslexikon.gabler.de/definition/dienstleistungen-28662/version-252288>
[^2]: Quelle: <https://wirtschaftslexikon.gabler.de/definition/recht-45117/version-335379>
