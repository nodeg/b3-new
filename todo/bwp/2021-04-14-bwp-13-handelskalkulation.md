---
title: "13. Handelskalkulation"
date: 2021-01-30 09:50:17 +0200
---

Das Gewinn- und Verlustkonto der Mediaelectrics Handelsgesellschaft mbH:

| Soll                          | Gewinn- und  | Verlustkonto (Mediaelectronics) | Haben        |
| ----------------------------- | ------------ | ------------------------------- | ------------ |
| Aufwendungen für Waren        | 900.000,00   | Umsatzerlöse für Waren          | 1.400.000,00 |
| Aufwendungen für Gehälter     | 180.000,00   |                                 |              |
| Aufwendungen für Miete/Pacht  | 60.000,00    |                                 |              |
| Aufwendungen für Werbung      | 10.000,00    |                                 |              |
| Aufwendungen für Büromaterial | 50.000,00    |                                 |              |
| Eigenkapital                  | 200.000,00   |                                 |              |
|                               | 1.400.000,00 |                                 | 1.400.000,00 |


Situation 1:

Herr Weber, neuer Geschäftsführer der Mediaelectrics Handelsgesellschaft mbH, möchte anhand der GuV des letzten Monats
Zuschläge für die Stückkalkulation zur Berechnung der Selbstkosten von Handelswaren ermitteln. Lt. seinem Handbuch für
Kosten- und Leistungsrechnung muss er dazu zunächst die Einzel- und dann die Gemeinkosten für seine Kostenstellen
errechnen. Herr Weber findet diese so genannte "Industriekalkulation" für die Mediaelectronics Handelsgesellschaft
unpassend und beauftragt Sie sich „darum zu kümmern“.

1) Definieren Sie den Begriff "Kostenstelle" für die Handelsgesellschaft:
2) Welche Einzelkosten sind bei der Mediaelectronics lt. GuV angefallen?
3) Welche Gemeinkosten entnehmen Sie der GuV?
4) Welcher Zuschlagssatz ergibt sich für die Kalkulation der Gemeinkosten der Mediaelectronics Handelsgesellschaft?

Der _______________________________________ gibt das prozentuale Verhältnis der Handlungskosten (= Gemeinkosten) zum Wareneinsatz bzw. Bezugspreis der Ware                 (= Einzelkosten) an.
Berechnung:

Definition „Handlungskosten“:

5) Wie hoch waren die Selbstkosten für die Mediaelektronics lt. GuV des vergangenen Monats?
6) Zu welchem Stückpreis kann die Mediaelectronics die Notebooks anbieten, wenn die Notebooks zum Listeneinkaufspreis
   450,00 Euro, 3% Liefererskonto, 20% Liefererrabatt, Bezugskosten 5,40 Euro (netto) bezogen werden und mit 15%
   Gewinnaufschlag, 2% Geschäftskundenskonto und 10% Aktionsrabatt kalkuliert wird?
