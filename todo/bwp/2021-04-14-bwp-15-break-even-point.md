---
title: "15. Break-Even-Point"
date: 2021-02-02 09:50:17 +0200
---

> Der Break-Even-Point wird auch kritische Menge oder Gewinnschwelle genannt.

Ein Unternehmen kalkuliert mit 5000,00€ Fixkosten, die variablen Stückkosten betragen 100,00€.

$$k_f = \text{fixe Gesamtkosten}$$<br>
$$k_v / K_v = \text{variable Gesamtkosten}$$<br>
$$k_g Gesamtkosten (SK)$$

Stellen Sie den Sachverhalt der Gesamtkosten bei einer Kapazitätsgrenze von 150 Stück grafisch dar! Dazu zeichnen Sie
zuerst die Fixkosten, dann die variablen Kosten und am Schluss die Gesamtkosten (=fixe + variable Kosten) ein.

(Hinweis: x-Achse: 10 Stück = 1 Kästchen; y-Achse: 1000€ = 1 Kästchen)

Angenommen Sie können einen Marktpreis von 150,00€ erzielen:

a) Stellen Sie die Umsatzerlöse grafisch in der Zeichnung aus Aufgabe 1 dar.<br>
b) Ab welcher Produktionsmenge schreiben Sie schwarze Zahlen? $$\rightarrow$$ Break-Even-Point bei 100 Stück<br>
c) Die kritische Menge lässt sich auch rechnerisch bestimmen:

Umsatzerlöse = Kosten<br>
$$\text{Verkaufspreis} (p) * \text{Stückzahl} (x) = \text{Fixkosten} (K_r) + \text{variable Stückkosten} (k_v) * \text{Stückzahl} (x)$$

$$\rightarrow x = k_f / (p - k_v) = k_f / db = 5000 / (150-100) = 500 / 50 = 100$$

150 * x = 5000 + 100 * x
50x = 5000
x = 100
