---
title:  "10. Standortfaktoren"
date:   2019-02-15 06:25:00 +0100
---

> Bestimmungsgründe der Standortwahl werden auch Standortfaktoren genannt.

1. Standort (BWL): der Platz an dem sich der Betrieb ansiedelt.
2. Wirtschaftliche Standortfaktoren (mit Beispiel)
   - Kundennähe (vor allem Einzelhandel) - Absatzorientierung
   - Infrastruktur: Verkehr, Internet, ...
   - Konkurrenz
3. Politische Standortfaktoren
   - Steuern: Gewerbesteuer
   - Subventionen
   - Behördliche Auflagen
4. Irrationale Standortüberlegungen (gerade bei Neugründungen)
   - Psychologische Gründe
   - Gesundheitliche Gründe
   - Heimatverbundenheit
   - Familientradition
5. Standort des Ausbildungsbetriebes
   - Infrastruktur: Glasfaserleitungen
6. Verwendung von Vorteilen in Nachteilen (mit Beispielen)
    - Subventionen $$\rightarrow$$ Konkurrenz wird höher
    - Standort: Gewerbesteuer und Ghettoisierung
    - Kunden: Umgebungsstraßen $$\rightarrow$$ Kundenwegfall
