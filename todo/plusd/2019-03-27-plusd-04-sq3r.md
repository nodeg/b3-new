---
title:  "04. SQ3R"
date:   2019-02-13 12:00:00 +0100
---

Die SQ3R Technik ist eine fünfschrittige, professionelle Lesestrategie um Texte strukturiert durchzuarbeiten.

1. Schritt: Survey $$\rightarrow$$ Überblick<br>Sichtung von Inhaltsverzeichnissen, Kapitelüberschriften,
   Verzeichnissen (Sach-, Namens-, Literaturverzeichnis) und Kapitelüberschriften. Dies ermöglicht es einen Überblick
   über den Text zu bekommen, Einleitungen und Zusammenfassungen können eventuell weiterführende Quellen sein.
2. Schritt: Question $$\rightarrow$$ Fragen<br>Nachdem man einen Überblick über den Text hat, formuliert man Fragen an
   den Text. Je nach Leseinteresse können diese variieren. Was ist der Fokus, mit dem der Text bearbeitet werden soll?
   Hat man keinen speziellen Fokus, so kann man nach dem Thema des Textes, Aussagen zu den Themen, Einstellungen und
   Sichtweisen des Autors, aber auch nach den Absichten des Autors fragen. Zuallerletzt sollte man für sich selbst
   reflektieren, ob der Text neue Informationen/Sichtweisen/Aspekte enthält.
3. Schritt: Read $$\rightarrow$$ Lesen<br>Im Bezug auf den letzten Schritt liest man den Text nun durch. Wichtig ist
   immer aufmerksam nach Antworten für die herausgeschriebenen Fragen zu suchen. Sind größere Werke mit dieser Technik
   in Bearbeitung, so kann man dies auch abschnittsweise abarbeiten. Sind Fragestellungen in einem Abschnitt des Textes
   nicht enthalten, so wird dieser ignoriert.
4. Schritt: Recite $$\rightarrow$$ Rekapitulieren<br>Nach jedem längeren Abschnitt rekapituliert man das Aufgenommene
   im Gedächtnis und bringt zu Papier, welche Antworten der Text auf die eigenen Fragen gegeben hat. Hervorzuheben ist
   die Wichtigkeit der eigenen Formulierungen. Sie verbessern den Merkprozess und das Verständnis des Textes. Es dient
   somit der Selbstkontrolle. Falls das Thema in einem eigenen Text behandelt wird, bereitet man somit gleichzeitig
   eigene Formulierungen vor.
5. Schritt: Review $$\rightarrow$$ Rückblick<br>Mit einer finalen Überprüfung der Notizen aus Schritt 4 verifiziert man,
   dass Informationen, Aussagen und Meinungen richtig wiedergegeben und spezifisch genug beantwortet worden sind.
