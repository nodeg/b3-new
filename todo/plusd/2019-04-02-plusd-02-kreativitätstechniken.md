---
title:  "02. Kreativitätstechniken"
date:   2019-02-12 06:05:00 +0100
---

## Brainstorming

### Methode

- wörtliche Übersetzung: "Gehirn-Stürmen"
- sinnliche Übersetzung: Einen Sturm im Gehirn entfachen, um ein Problem zu lösen.
- Erfinder: Alex Osborn, USA, 1939
- bekannteste und (mit seinen Varianten) am meisten verbreitete Methode zur Ideenfindung
- gilt als Vorläufer aller existierenden Kreativitätstechniken

### Teilnehmer

**Moderator**<br>>
Der Moderator hat die Aufgabe, die Gruppe durch die Sitzung zu führen. Zu Beginn des Brainstormings konfrontiert er die
Sitzungsteilnehmer mit der Problemstellung, damit jedes Mitglied weiß, um was es konkret geht. Er achtet auf die
Einhaltung der Regeln.

**Gruppe aus ca. 5-12 Teilnehmern**<br>
Kleine Gruppen häufig zu wenige Ideen. Bei weniger als 5 Teilnehmern besteht die Gefahr, dass zu gleichförmig gedacht
wird und man so nur auf eine bestimmte Schiene von Lösungen kommt. Bei mehr als 12 Teilnehmern bilden sich
möglicherweise kleine Gruppen oder einzelne Personen gehen in der Gruppe unter. Nach Möglichkeit sollten die Teilnehmer
aus verschiedenen Sparten kommen. Nicht nur Fachleute sollten an der Brainstorming-Sitzung teilnehmen.

**Schriftführer**<br>
Er hat die Aufgabe, alle geäußerten Ideen (auf Papier, Folie, Flipchart, Tafel, o.ä.) - nach Möglichkeit für jeden
sichtbar (!) - festzuhalten.

_Dauer_: Eine Brainstorming-Sitzung sollte - je nach Problem - ca. 20min bis maximal 1 Stunde dauern.

### Ablauf einer Brainstorming-Sitzung

- Der Moderator erläutert vor Beginn der Brainstorming-Sitzung das Problem. Er beantwortet Verständnisfragen und gibt
  das Ziel der Problemlösung vor. Er soll signalisieren, dass wirklich ALLE Ideen willkommen sind.
- Während der Brainstorming-Sitzung äußern die Teilnehmer ganz spontan ihre Einfälle in beliebiger Reihenfolge. Während
  der Ideensammlung erfolgt keine Kritik, es wird nicht diskutiert.
- Der Schriftführer hält alle Ideen vollständig und selbstständig fest, am besten für alle Teilnehmer sichtbar auf
  einem Flipchart.
- Werden zu viele Ideen auf einmal genannt, sodass der Schriftführer nicht schnell genug mitschreiben kann, achtet der
  Moderator darauf, dass keine Einfälle verloren gehen.
- Bereits genannte Ideen können jederzeit aufgegriffen und weiter entwickelt werden.
- Der Moderator greift unterstützend ein, wenn etwas "aus dem Ruder läuft", z.B. wenn die Regeln verletzt werden oder
  das Ziel aus den Augen verloren wird. Er legt das Ende der Ideenfindung fest, wenn sich keine weiteren Einfälle mehr
  einstellen wollen.

### Regeln

- Quantität vor Qualität<br>
  Am wichtigsten ist die Menge der Vorschläge, Qualität ist zweitrangig.
- Keine "Selbstbeschränkung"! Jede Idee ist erlaubt und erwünscht! Jede spontane Idee, egal wie verrückt oder
  unrealistisch, soll in die Lösungsliste mit aufgenommen werden. Jeder Teilnehmer sollte Hemmungen ablegen und seiner
  Fantasie freien Lauf lassen.
- Erst sammeln, dann diskutieren!<br>
  Alle Einfälle sollten gleicher schriftlich festgehalten werden. Eine Diskussion über die Qualität oder
  Realisierbarkeit der einzelnen Ideen erfolgt erst in der Nachbearbeitungsphase.
- Keine "Killerphrase", keine Kritik!<br>
  Killerphrasen sind Ideenkiller. Bemerkungen wie "Das kann doch gar nicht funktionieren?!" oder "Wie soll man denn das
  realisieren?!" sind während des Brainstormings zu unterlassen! Es darf keine Kritik oder Bewertung an den
  vorgebrachten Lösungsvorschlägen geübt werden. Auch keine körpersprachliche Kritik (Nase rümpfen, Kopf schütteln,
  usw.) äußern! Es gibt kein "Urheberrecht" Einzelner auf eigenen Ideen - alle Ideen können, ja sollen aufgegriffen und
  "weitergesponnen" werden.

### Appendix

Neben dem "klassischen" Brainstorming gibt es zahlreiche Varianten (Kategorien-Brainstorming, Reizwort-Methode,
SIL-Methode, etc.), die im Internet zu finden sind.

## Brainwriting

- Ziel: Sammlung von Themen, Fragen, Ideen, Lösungsansätzen etc.
- Besonderheit: Die verbale Kommunikation wird durch die Schrift ersetzt, hierdurch sollen gruppendynamische Effekte
  umgangen werden.
- An die Ideen-Sammlung schließt sich eine Phase der Ideenbewertung an.
- Die Teilnehmerzahl, Dauer und benötigten Materialien variieren je nach Methode.

### 635-Methode

- Teilnehmerzahl: 6 Personen
- Dauer: 25min
- Material: 3 Blatt-Papier
- Vorbereitung: Auf jeweils ein Blatt Papier drei Spalten mit sechs Zeilen zeichnen. Es sollten sich 18 Felder ergeben.
- Ablauf:
    - Jeder Teilnehmer trägt seine Ideen in die nächste vollständig freie Zeile ein und füllt drei Kästchen aus.
    - Nach 5 Minuten wird das Blatt im Uhrzeigersinn weitergegeben.
    - Im Idealfall führt der Nächste die Idee des vorigen Teilnehmers weiter. Falls keine gefunden werden können neue
      Ideen gestartet werden.
- Ergebnis: 108 Ideen im Idealfall.
- Variationen: Mehr oder weniger Leute $$\rightarrow$$ Entsprechend müssen die Zeilen angepasst werden.

### Kärtchen-Technik

- Teilnehmerzahl: Variabel
- Dauer: 15-30min + Clustering der Karten
- Material: Kärtchen für alle Teilnehmer und eine Wand zum Clustering (Pinnwand/Tesa/Magneten)
- Vorbereitung: Die Aufgabenstellung an der Wand präsentieren und der Moderator sollte den Leuten auf die
  Problemstellung bewusst machen.
- Ablauf:
    - Jeder Teilnehmer schreibt pro Karte eine Idee auf. $\rightarrow$ Möglichst wenig Text!
    - Jeder Teilnehmer versucht in der vorgegebenen Zeit möglichst viele Ideen zu finden.
    - Nach Ende der Zeit sammelt der Moderator die Ideen ein.
    - Gemeinsames Clustering der Ideen.
    - Aufkommen von neuen Ideen beim Clustering mit aufnehmen und zuordnen.
    - Zuordnung überprüfen, nachdem alle Karten an der Wand hängen.
- Ergebnis: Vorsortiert nach Themenfeldern verschiedenste Ideen.

### Ideenhierarchie

- Teilnehmerzahl: 1 - n
- Dauer: Unspezifiziert
- Material: Pro Teilnehmer ein Blatt
- Vorbereitung: Die Teilnehmer einigen sich auf einen zentralen Begriff, welcher an einer von der Gruppe festgelegten
  Stelle platziert wird.
- Ablauf:
    - Nach unten wird die Idee genauer spezifiziert und nach oben hin werden allgemeinere Ideen platziert.
    - Jeder Gruppenteilnehmer platziert seine Ideen in der Matrix.
    - Bei der Auswertung werden alle Matrizen versucht in eine Matrix zu überführen und zu bewerten.
- Ergebnis: Eine große Matrix mit nach Gewichtung geordneten Ideen.
- Variationen: Die Idee wird in den Raum geworfen und gemeinsam auf einer großen Matrix platziert.

## Mindmapping

### Grundlagen

- Die Topografie des Gehirns ähnelt der einer Mindmap.
- Gedankengänge müssen nicht immer logisch sein bzw. sind es nicht.
- Erfinder: Tony Buzan (vor circa 30 Jahren)
- Sinn: Rechte und Linke Gehirn sollen gleichermaßen beansprucht werden, indem man sprachlich-logisches und
  intuitiv-bildhaftes Denken verbindet.
- Form: Zentrale Fragestellung in der Mitte und dann Baumartig davon nach außen gehen.
- Vorteil: Sehr freie Methode welche in verschiedenen Situationen angewendet werden kann.

### Anwendung

- Planung (persönliche Planung, Projekte, Verkauf, Zeitplanung, Urlaubsplanung, etc.)
- Ideenfindung und später Problemlösung
- Zusammenfassung (Buch, Artikel, Lernstoff, Film, Unterrichtsstunde, Seminar, Radio- oder TV-Sendung, etc.)
- Gliederungen
- Notieren (Mitschreiben von Besprechungen, Lehrveranstaltungen, Diskussionen, Interviews, etc.)

### Regeln

- Wenn Sie normale DIN-A4-Blätter benutzen, verwenden Sie das Blatt im Querformat! Möglich sind natürlich auch
  Overhead-Folien, ein Plakat an der Tafel oder Pinnwand.
- Beginnen Sie in der Mitte des Blattes. Das zu bearbeitende Thema wird in die Papiermitte geschrieben (oder gemalt). Um
  das Thema herum kann ein Rahmen, ein Kreis, eine Wolke etc. gemalt werden, um es herauszustellen.
- Die einzelnen Gedanken, Ideen etc. werden in Schlüsselworten auf vom Zentrum ausgehenden geschwungen Linien um das
  Thema herum geschrieben.
- Drehen Sie das Blatt beim Arbeiten nicht! Zeichen Sie weitergehend waagrechte Äste und Zweige und schreiben Sie die
  Wörter horizontal. (wenn Sie die Wörter verschieden ausrichten, benötigen Sie später, wenn Sie das Mind-Map benutzen
  wollen, zu viel Zeit zum Lesen).
- Schreiben Sie in Druckbuchstaben - das macht das Lesen einfacher. Auf den Hauptkästen können Sie GROSSDRUCKBUCHSTABEN
  verwenden, um wichtige Begriffe oder Schlüsselwörter hervorzuheben, ansonsten aber wegen der besseren Lesbarkeit
  Kleindruckbuchstaben.
- Jeder neue Gedanke bedeutet eine neue Linie. Falls es sich um einen Hauptast handelt, geht diese wieder vom Zentrum
  aus. Wird aber ein bereits vorhandener Gedanke weiter ausdifferenziert, werden Details erschlossen oder Beispiele
  genannt, muss eine neue Linie (Zweig) an ein bereits vorhandene anschließen. Das Mind-Map wächst so von innen nach
  außen, wobei durch die Struktur wichtige Begriffe in Zentrumsnähe, unwichtigere im Randbereich wiederzufinden sind.
- Benutzen Sie Farben, um verschiedene Wortgruppen hervorzuheben und voneinander zu unterscheiden, oder auch für Bilder.
- Zeichnen Sie Bilder und Symbole. Bilder enthalten mehr Informationen als Wörter. Versuchen Sie, Bilder zu finden, die
  die Informationen einer ganzen Gruppe von Wörtern zusammenfassen.
- Gebrauchen Sie Symbole, Zeichen und Pfeile, um aufzuzeigen, wo Verbindungen zwischen den einzelnen Teilen Ihres
  Mind-Maps bestehen, oder wenn Sie auf andere Materialien, zum Beispiel Zitate, Grafiken, Charts etc. hinweisen wollen,
  die nicht im Mind-Map enthalten sind.

## Ideenbewertung

### Nachbearbeitung der Kreativitätsmethoden

Nach der Anwendung von Kreativitätstechniken zur Ideenfindung muss eine Auswertung erfolgen, um festzustellen, welche
Einfälle realisiert werden können. Im Nachgang einer Brainstorming-Sitzung sollte zunächst eine Auswahl und Sortierung
der Ideen erfolgen.

Die Gruppe, die mit der Auswertung befasst ist, sollte zum Teil aus Teilnehmern der Ideenfindungs-Sitzung, z.T. aber
auch aus neuen Teammitgliedern bestehen. Nach Möglichkeit sollten hier auch Experten mitarbeiten, um die
Umsetzungschancen einer Idee festzustellen, müssen eventuell Fachleute zur Analyse herangezogen werden.

### Ablauf der Ideenbewertung

- Herausstreichen von Doppelnennungen und gleichen Einfällen/Auswahl einer überschaubaren Anzahl zur weiteren
  Bearbeitung
- Zusammenfassen von Ideenfeldern
- Einsatz von Ideenbewertungsmethoden
- evtl. Weitergabe zur Analyse an Fachleute zur Abschätzung der Realisierungschancen

### Methoden zur Ideenbewertung

Welche Methoden man zur Bewertung der Ideen einsetzt, hängt von der Art des Problems ab, aber auch davon, welche
Kriterien bei der Umsetzung der Idee zu beachten sind. Geeignete Methoden sind z.B.

#### PMI-Methode

Die Ideen werden in "Plus-Ideen" (unmittelbar verwertbar), "Minus-Ideen" (eher nicht verwertbar) und "interessante
Ideen" (prinzipiell verwertbar, aber noch näher zu untersuchen) eingeteilt, um einem ersten Überblick zu gelangen.

#### Punktebewertung

Brauchbare Lösungsvorschläge werden aufgelistet und von jedem Gruppenmitglied gepunktet. Dabei kann man sich
verschiedene Varianten ausdenken - auch abhängig von der Zahl der Ideen und der Gruppenmitglieder.

Damit man zu brauchbaren Ergebnissen kommt, kann man z.B. jedem Teammitglied mehrere Punkte zur Verfügung stellen, die
es nach Belieben verteilen darf.

#### Ideenbewertungsmatrix

Schema zur Bewertung der Ideen hinsichtlich ihrer konkreten Realisierungschancen im Unternehmen: Nicht nur die
Attraktivität der Idee wird beurteilt, sondern auch, ob sie im Einklang steht mit der s genannten "Corporate Identity",
dem Image, den Möglichkeiten und den Zielen des Unternehmens.

Die Bewertung geschieht in Form einer Matrix. Die horizontale Achse steht für die Attraktivität der Idee, die vertikale
für die Verträglichkeit der Idee in Bezug auf Ziele und Ressourcen/Möglichkeit des Unternehmens.

Attraktivität der Idee für Zielgruppe/Vereinbarkeit der Idee mit der Unternehmensphilosophie

|             |      hoch       |     mittel      |    niedrig    |
| :---------: | :-------------: | :-------------: | :-----------: |
|  **hoch**   |   beste Idee    | zweitbeste Idee |  zweifelhaft  |
| **mittel**  | zweitbeste Idee |   zweifelhaft   |   schlecht    |
| **niedrig** |   zweifelhaft   |    schlecht     | ganz schlecht |

Es ist ratsam, sich vorher Kriterien zu überlegen, nach denen beurteilt werden soll, wie attraktiv oder wie verträglich
eine Idee ist. Solche Kriterien können beispielsweise sein:

Kriterien der Attraktivität:
- originell
- einfach
- anwenderfreundlich
- einfach umsetzbar
- elegant
- schwer zu kopieren

Kriterien der Verträglichkeit:
- vereinbar mit Unternehmenszielen
- ausreichende Finanzmittel verfügbar (Kosten)
- genügend Personalressourcen verfügbar
- gut für das Unternehmens-Image

Natürlich können Sie oder Ihr Auftraggeber auch eigene Maßstäbe zugrunde legen, nach denen Sie beurteilen, ob eine Idee
tragfähig genug ist, umgesetzt zu werden.
