---
title:  "01. Infos zum SoLe Unterricht"
date:   2019-02-12 06:00:00 +0100
---

## Die Bedeutung von SoLe

- SoLe = Selbst organisiertes Lernen
- Arbeiten in kleinen Gruppen
- Eigenverantwortliches Erlernen von Inhalten in eigenem Tempo und eigenen Einheiten
- Gegensatz zur üblichen Vorgehensweise in der Schule: Unterrichtsstoff für Prüfungen wiedergeben $$\leftrightarrow$$
  eigenverantwortlich lernen und verantwortlich für andere handeln

## Das Ziel von SoLe

- Die Erkenntnis erlangen, dass reines Fachwissen nutzlos ist.
- "Über den Tellerrand hinaus schauen!"
- Soziale Fähigkeiten ausbauen $$\rightarrow$$ im Team arbeiten!
- Erlernen der Fähigkeit sich auf Unbekanntes einzustellen.
- Erlangen von methodischen Kompetenzen zum Arbeiten (Kreativitätstechniken)
- Bewusstwerden von kommunikativen Grundlagen (Kritikfähigkeit, Konfliktbewusstsein, fachliche Dispute, Reflektion
  seiner selbst und seines Umfeldes)

## Wie funktioniert SoLe?

- Auseinandersetzen mit einer praktischen Situation
- Praktische Probleme erfordern eine tiefere Auseinandersetzung mit dem Lösungsweg, als üblich in der Schule
- Teamwork löst die meisten Aufgabenstellungen, wenn korrekt angewendet.
- Der Weg zum Ziel muss selbst gefunden werden, nur so werden die Unterrichtsinhalte erlernt.
- Kreativitätstechniken bieten mindestens einen Anhaltspunkt.
- Dokumentation bietet Transparenz und ermöglicht eine effektivere Arbeit.
- Kaizen bietet eine Möglichkeit zur kontinuierlichen Verbesserung (japanische Philosophie):
    1. Informieren
    2. Planen
    3. Entscheiden
    4. Ausführen
    5. Kontrollieren
    6. Bewerten
- Der Kaizen stellt unter anderem folgende Fragen:
    - Was ist zu tun?
    - Wer macht es?
    - Warum wird es gemacht?
    - Wie wird es gemacht?
    - Wann wird es gemacht?
    - Wo soll es getan werden?
    - Wieso wird es nicht anders gemacht?
- Das Kernelement zum Erfolg ist der eigenen Verantwortung gerecht zu werden.
