---
title:  B3 Azubis
---

# Ankündigung für die Zukunft

{{< hint info >}}
Nachdem ich für die Anwendungsentwickler den Erwartungshorizont erhalten habe, ist mir aufgefallen das viel Stoff für die Abschlussprüfung fehlt. Sofern ihr Zugriff auf die aktuelle Liste habt, könnt ihr nachschauen was auf dieser
Webseite fehlt. Nach meinen Abschlussprüfungen werde ich versuchen euch den fehlenden Inhalt nachzutragen bzw. zu
organisieren, dass der Inhalt nachgetragen wird von anderen Mitazubis. Auch die Neuordnung der Ausbildungsberufe möchte
ich hier mit einpflegen. All das wird jedoch Zeit brauchen.
{{< /hint >}}


- 0101 - 01 - 02: Top-Down/Bottom-Up-Entwurf
- 0101 - 01 - 04: Datenflussplan
- 0101 - 01 - 07: Hierarchisierung (vllt)
- 0101 - 01 - 12: Paketdiagramm
- 0101 - 02 - 01: Platformunabhängige Sprachen
- 0101 - 02 - 02: OS-API zu Peripheriegeräten
- 0101 - 02 - 04: Events
- 0101 - 02 - 05: Datenbanktreiber
- 0101 - 02 - 07: vollständig
- 0101 - 02 - 09: Programmgenerator, CASE-Tool, Testsoftware
- 0101 - 02 - 10: vollständig
- 0101 - 04: vollständig
- 0102 - 01 - 02: vollständig
- 0102 - 01 - 03: vollständig
- 0102 - 01 - 04: vollständig
- 0102 - 01 - 05: vollständig
- 0103 - 01 - 02: Webservices & SOA
- 0105 - 01 - 02: Nutzwertanalyse
- 0105 - 02 - 01: Machbarkeitsprüfung
- 0105 - 02 - 02: Netzplan, Gantt-Diagramm, Terminliste, Vorgangsliste, Organigramm, Projektstrukturplan
- 0105 - 03 - 01: vollständig
- 0201 - 01 - 02: Unfallbericht
- 0201 - 01 - 03: Löschen
- 0202 - 01 - 01: Rechtsschutz
- 0202 - 01 - 03: Kennzahlen (TCO, ROI)
- 0202 - 02 - 01: Organigramm
- 0202 - 02 - 02: Darstellung - vollständig; Unterstützende IT-Systeme
- 0202 - 02 - 03: Management-Buy-Out
- 0202 - 02 - 04: vollständig
- 0202 - 03 - 01: ABC-Analyse
- 0202 - 04 - 01: Ausschreibungen (insb. elektronische)
- 0202 - 05 - 03: vollständig
- 0203 - 02 - 04: ABC-Analyse
- 0204 - 03 - 01: vollständig
- 0204 - 04 - 01: Linux
- 0205 - 01 - 01: Datenfluss
- 0205 - 01 - 05: Datenflussplan, EPK, Ereginisgesteuertes prozessketten- und Vorgangsdiagramm
- 0205 - 02 - 03: Bibliotheken, Hilfsprogrammen wie Debugger
- 0205 - 04 - 02: vollständig
- 0205 - 05 - 03: vollständig
- 0301 - 05: vollständig
- 0303 - 03: vollständig

Viel Vergnügen beim Lernen und Nutzen der Webseite,
Enno aka SchoolGuy