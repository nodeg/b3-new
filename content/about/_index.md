---
title: Über uns
---

Diese Seite soll die Anlaufstelle zur Prüfungsvorbereitung für Anwendungsentwickler der Berufsschule B3 aus dem sein. Von Azubis für Azubis.

Macht mit! Ihr könnt Änderungen und weitere Materialien durch Merge Requests im [Repository](https://gitlab.com/B3-Azubis-18/b3-azubis-18.gitlab.io) vorschlagen!
