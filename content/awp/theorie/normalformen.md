---
title: Normalformen
date: 2020-03-24 11:00:00 +0200
---

## Hintergrund zu Normalformen

In der Regel ist es nicht empfehlenswert, einzelne Tabellen mit festgelegten Datenfeldern (=Tabellenspalten) direkt in
eine Datenbanktabelle zu übernehmen. Oftmals enthalten sie duplizierte Informationen, sodass bei einer einfachen
Übernahme eine Mehrfachspeicherung von Daten die Folge ist (Redundanz).

Der Vorgang, der Daten aus einer Tabelle stufenweise in verschiedene Tabellen aufgliedert, nennt sich Normalisierung. Er
dient dem Zweck der Reduktion von Redundanzen und der Sicherstellung der logischen Korrektheit der Datenbank. Eine
Normalisierung erfolgt in der praktischen Anwendung in 3 Stufen. Entsprechend werden 3 Normalformen (kurz: NF)
unterschieden.

Nehmen wir als Grundlage zur Erläuterung der Normalisierung die Tabelle aus der Datei "Projektmanagement" aus dem
Fallbeispiel "Erstellung einer Firmendatenbank" her.

Mit den ersten Erfahrungen, die wir bei der Erstellung von ER-Diagrammen und logischen Modellen gemacht haben, können
uns verschiedene Sachverhalte auffallen. In einem ersten Schritt richten wir unsere Aufmerksamkeit auf die Darstellung
der Namen, die in unterschiedlicher Weise dargestellt werden ("Vorname Nachname" und "Nachname, Vorname").

## 1. Normalform

Regel: Eine Tabelle liegt in 1. Normalform vor, wenn alle Datenfelder (= Tabellenspalten) nicht mehr aufteilbare Werte
enthalten (d.h. die Werte müssen atomar vorliegen). Sie enthält nur elementare Attribute, d.h. Attribute weisen
innerhalb eines Tupels keine Wiederholgruppen auf.

Die in der Ausgangssituation gezeigte Tabelle widerspricht aufgrund der unterschiedlichen Art der Speicherung der Namen
dieser Regel. Teilen wir die Spalte "Name" in 2 separate Datenfelder "Name" und "Vorname" auf, so können wir die
Gültigkeit der Regel herstellen.

Aktuell können wir als Primärschlüssel die Kombination der Datenfelder "Pers.Nr." und "ProjektNr." identifizieren. Diese
Datenfelder werden auch als Schlüsselattribute bezeichnet. Die übrigen Attribute sind Nichtschlüsselattribute.

## 2. Normalform

Regel: Eine Tabelle liegt in 2. Normalform vor, wenn...

- sie die Regel der 1. Normalform erfüllt UND
- jedes Datenfeld nicht nur von einem Teil des Primärschlüssels, sondern vom gesamten

Primärschlüssel identifiziert wird. Dieser Sachverhalt wird auch als funktionale Abhängigkeit der Datenfelder vom
Primärschlüssel bezeichnet.

Wenn wir die in die 1. Normalform überführte Tabelle betrachten, dann fallen uns folgende Aspekte im Hinblick auf die
Regel der 2. Normalform auf:

- Die Datenfelder "Name", "Vorname", "Abt.Nr." und "Abt." sind für sich genommen jeweils allein abhängig von der
  Personalnummer (Pers.Nr.). Dies kann daran erkannt werden, dass beispielsweise der Wert "3" im Datenfeld "Pers.Nr."
  ausreicht, um die Daten der übrigen 4 Datenfelder eindeutig zu bestimmen. In diesem Fall gehören sie zu der Person
  "Gerd Richter". Die Projektnummer hat, obwohl sie zum Primärschlüssel gehört, keinen Einfluss auf die eindeutige
  Zuordnung der genannten 4 Datenfelder.
- Das Datenfeld "Beschreibung" ist nur abhängig von der Projektnummer und nicht von der Personalnummer. Beispielsweise
  hat das Projekt mit der Beschreibung "Kundenumfrage" stets die Projektnummer "1". Die Projektnummer reicht auch hier
  folglich aus, um die Beschreibung eindeutig identifizieren zu können.
- Das Datenfeld "Zeit" gibt an, welche Person wie lange an einem Projekt gearbeitet hat. Damit ist dieses Datenfeld von
  den beiden Datenfeldern "Pers.Nr." und "Projekt-Nr." des Primärschlüssels abhängig.

Mit diesen Erkenntnissen können wir die Tabelle aus der 1. Normalform in die 2. Normalform überführen, indem wir die
Tabelle in mehrere Tabellen aufteilen. Welche Datenfelder in welche Tabelle übernommen werden, ist abhängig von der
eindeutigen Identifizierbarkeit ihrer enthaltenen Werte durch einen Teil des Primärschlüssels oder dem gesamten
Primärschlüssel. Mehrfach gespeicherte Datensätze werden dabei reduziert auf einen Datensatz.

## 3. Normalform

Die 3. Normalform (3. NF)
Regel: Eine Tabelle liegt in 3. Normalform vor, wenn...

- sie die Regel der 2. Normalform erfüllt UND
- zwischen den Datenfeldern, die nicht den Primärschlüssel bilden, keine Abhängigkeiten bestehen.

Betrachten wir die in der 2. Normalform dargestellten Tabellen, so fällt uns auf, dass zwischen der Spalte "Abteilung"
("Abt.") und "Abteilungsnummer" ("Abt.Nr.") eine Abhängigkeit besteht. Dies ist der Fall, da beispielsweise der
Abteilung "Verkauf" stets die Abteilungsnummer "1" zugeordnet ist. Wir können die Werte der Spalte "Abteilung" eindeutig
durch die Werte der Spalte "Abteilungsnummer" identifizieren:

## Normalisierung vs. ER-Diagramme

| ER-Diagramme                                           | Normalisierung                                |
|--------------------------------------------------------|-----------------------------------------------|
| Top-Down-Ansatz (von den Anforderungen zum Modell)     | Bottom-Up-Ansatz (von der Tabelle zum Modell) |
| Vorgang geht vergleichsweise schnell                   | Vorgang geht vergleichsweise langsam          |
| Im Fokus der Prüfung stehen die Anforderungen          | Im Fokus der Prüfung stehen die Daten         |
| Fachliche Kenntnisse sind zentral (Unternehmenswissen) | Mathematische Grundlagen sind zentral         |

Fazit: Ein Modell wird vorzugsweise top-down erstellt und bottom-up geprüft.

## Weiteres Wissenswertes/Ergänzungen

Tabellen (Relationen) sollten so geplant werden, dass
- logische Widersprüche (Inkonsistenzen, Anomalien) in der Datenbasis und
- Datenredundanz (Mehrfachspeicherung gleicher Daten) vermieden sowie
- eine höchstmögliche Flexibilität und
- schneller Zugriff gewährleistet werden.

Ziel der Normalisierung ist Reduktion bzw. Vermeidung von Redundanzen, da diese im Betrieb der Datenbank zu
Integritätsverletzungen führen können.

Normalformen bei relationalen Datenmodellen (logisches Modell):

1. Eine Relation befindet sich in der ersten Normalform (1.NF), wenn alle Attribute nicht mehr aufteilbare Werte
   umfassen (d.h. die Werte müssen atomar vorliegen). Darüber hinaus enthält sie nur elementare Attribute, d.h. die
   Attribute innerhalb eines Tupels weisen keine Wiederholgruppen auf.
2. Eine Relation ist in der zweiten Normalform (2.NF), wenn sie in der 1.NF vorliegt und
   jedes Attribut, das nicht zum Primärschlüssel gehört, alle Attribute des Primärschlüs-
   sels (mehrere Attribute zusammen können einen Primärschlüssel bilden) zur eindeu-
   tigen Identifikation benötigt. D.h. jedes Nichtschlüssel-Attribut muss voll funktional
   vom Schlüssel abhängig sein.
   Wenn der Primärschlüssel nur aus einem Attribut besteht, ist jede Relation (Tabelle),
   die sich in der 1. Normalform befindet, zwangsläufig auch in der 2. Normalform.
3. Eine Relation befindet sich in der dritten Normalform (3.NF), wenn sie die 2.NF erfüllt und alle Attribute, die nicht
   zum Primärschlüssel gehören, direkt von diesem abhängen. Alle Nicht-Primärschlüssel-Attribute müssen also voneinander
   unabhängig sein. Es ist nicht erlaubt, dass ein Attribut, das nicht zum Primärschlüssel gehört, nur indirekt
   (transitiv) von diesem abhängt.

Ermittlung eines Primärschlüssels:
Ist kein Primärschlüssel zu einer Tabelle vorgegeben, dann muss ein Attribut (Feld) oder eine Kombination von Attributen
gefunden werden, die jeden Datensatz eindeutig identifiziert. Dieses Attribut bzw. diese Attributkombination wird
Primärschlüssel genannt. Die Integrität einer Relation ist gewahrt, wenn ihr Primärschlüssel eindeutig und nicht leer
ist. Grundsätzlich gibt es zwei Möglichkeiten für einen Primärschlüssel:

- Eine Kombination aus einem oder mehreren schon vorhandenen Feldern kann zum natürlichen Primärschlüssel erklärt
  werden.
- Es kann ein künstlicher Primärschlüssel erzeugt werden, in dem man ein Feld einfügt, das die Datensätze der Reihe nach
  durchnummeriert.

Ein Mehrfelder-Primärschlüssel muss eine minimale Kombination von Merkmalen sein, d.h. kein Merkmal der Kombination kann
gestrichen werden, ohne dass die Eindeutigkeit der Identifikation verloren geht (Minimalitätsbedingung).

Mit einem Mehrfelder-Primärschlüssel sind meist folgende Nachteile verbunden:
- Man muss genau überlegen, ob die gleiche Kombination von Attributwerten (Mehrfelder-Primärschlüssel) in der Tabelle
  nicht doppelt vorkommen kann.
- Je länger der Primärschlüssel, desto länger die Zugriffszeit. Da der Primärschlüssel für Verknüpfungen benötigt wird,
  sollte er nicht zu lang sein (z.B. Primärschlüssel besteht aus 3 Attributen $$\rightarrow$$ der dazu gehörende
  Fremdschlüssel besteht auch aus 3 Attributen).
- Bei der Planung der Tabellen ist besonders darauf zu achten, dass jedes Attribut, das nicht zum Primärschlüssel
  gehört, alle Attribute des Primärschlüssels zur eindeutigen Identifikation benötigt (siehe: volle funktionale
  Abhängigkeit, 2. Normalform).
- Personenbezogene Bestandteile des Schlüssels (z.B. Geburtsdatum) können zu Datenschutzproblemen führen.

Außerdem lassen nicht alle Datenbankmanagementsysteme die Definition von Mehrfelder-Primärschlüsseln zu.

In der Regel ist also ein Einfeld-Primärschlüssel einem Mehrfelder-Primärschlüssel vorzuziehen. Bei der Realisierung von
n:m-Beziehungen sind jedoch Mehrfelder-Primärschlüssel oft angebracht.

Normalformen und die zu erfüllenden Regeln:

Um die Normalform einer Tabelle zu bestimmen, sollten die 7 nachfolgenden Regeln geprüft und umgesetzt werden. Die
Regeln stellen Bedingungen zur Erreichung der neben der Regel genannten Normalform dar.

| NF | Regel | Beschreibung                                                                                                                             |
|----|-------|------------------------------------------------------------------------------------------------------------------------------------------|
| 1  | 1     | Jede Teilinformation sollte in einem eigenen Feld gespeichert werden.                                                                    |
| 1  | 2     | Jeder Datensatz (Tupel, Entität) einer Tabelle muss durch einen Primärschlüssel eindeutig identifizierbar sein.                          |
| 1  | 3     | Für aufzählende Felder sollte eine eigene Tabelle angelegt werden.                                                                       |
| 1  | 4     | Felder, die nicht für jeden Datensatz ausgefüllt werden können, sollten in einer eigenen Tabelle gespeichert werden.                     |
| 2  | 5     | Jedes Attribut, das nicht zum Primärschlüssel gehört, muss alle Attribute des Primärschlüssels zur eindeutigen Identifikation benötigen. |
| 3  | 6     | Felder, die in keinem direkten Bezug zum Primärschlüssel der Tabelle stehen, sollten in einer eigenen Tabelle gespeichert werden.        |
| 3  | 7     | Felder sollten keine berechenbaren Daten enthalten.                                                                                      |
