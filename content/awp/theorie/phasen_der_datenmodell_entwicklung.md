---
title: Phasen der Datenmodell Entwicklung
date: 2019-10-18 10:00:00 +0200
---

## Allgemeines

| Phase | Tätigkeit                                                                                                                                                                                                                                                                                            | Dokument                                        |
|-------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------|
| 1.    | Externe Phase<br><br>- Ermittlung des Informationsbedarfs der Benutzer<br>- Strukturierung dieser Informationen                                                                                                                                                                                      | Anforderungsdefinition                          |
| 2.    | Konzeptionelle Phase<br><br>- Formale und strukturierte Beschreibung aller relevanten Objekte und deren Beziehungen untereinander                                                                                                                                                                    | Semantisches Modell (Entity-Relationship-Model) |
| 3.    | Logische Phase<br><br>- Überführung des ERM in Tabellen anhand der Abbildungsregeln in ein logisches Modell<br>- Überprüfung des logischen modells mit Hilfe der Normalisierung<br>- Festlegung der Integritätsbedingungen                                                                           | Logisches/relationales Datenbankmodell          |
| 5.    | Physische Phase<br><br>- Erzeugung der Datenbank und der entsprechenden Tabllen<br>- Festlegung der Sichten und der jeweiligen Zugangsberechtigungen<br>- Erstellung der Sichten und Zugangsberechtigungen<br>- Optimierung der Datenbank: z.B. Erzeugen von Indices zur Beschleunigung des Ablaufes | Kommentiertes SQL-Skript                        |

Die 1. und 2. Phase laufen unabhängig vom Datenbank Management System (DBMS) ab. Die 3. Phase hängt teilweise davon ab
und die 4. Phase hängt vollständig von verwendeten DBMS ab.

- Modell: zweckorienterte, vereinfachte und strukturierte Abbildung der Wirklichkeit
- Daten: zum Zweck der Verarbeitung gebildete Informationen
- DV: Kurz für
  [Datenverarbeitungssystem](https://wirtschaftslexikon.gabler.de/definition/datenverarbeitungssystem-27537), beschreibt
  ein System welches Daten verarbeitet.

## Informationsstruktur ermitteln (Externe Phase)

In der externen Phl!lse wird die Informetlonsstruktur des Detonmodells ermittelt.

- Top-down-Ansetz (globales Dntenmodell) bzw.
- Bottom-up-Ansetz (anwendungsorientiertes Detenmodell)

Nennen Sie Möglichkeiten die Informationsstruktur des Autohauses Nettmanns zu ermitteln:

Ermittlung aufgrund von Realitätsbeobachtungen

- die Objekttypen (Entitäten) "Schüler", "Lehrer" und "Klassen".
- der Beziehungstyp "unterrichtet".

Ermittlung aufgrund von Benutzersichtanalysen (Bottom UP)

- Benutzersichten sind Formulare, Berichte ... ("Klassenliste", "Zeugnisausdruck", ...)

Ermittlung aufgrund von Datenbestandsanalysen

- Integration bestehender Datenbestände (z.B. beim Wechsel des Schulverwaltungssytems)
