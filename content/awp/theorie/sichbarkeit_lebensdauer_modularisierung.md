---
title: Sichtbarkeit, Lebensdauer & Modularisierung
date: 2019-04-04 06:00:00 +0100
---

## Sichtbarkeit & Gültigkeit von Variablen

Jede Variable besitzt zwei Eigenschaften: Gültigkeit (Sichtbarkeit) und Lebensdauer. Globale Variablen werden
automatisch vom Compiler mit null initialisiert und existieren so lange, wie das Programm ausgeführt wird. Auf eine
globale Variable kann von überall im Programm zugegriffen werden, z. B. von Funktionen aus. Als lokal werden alle
Variablen bezeichnet, die nicht global sind. Lokale Variablen werden innerhalb eines Blocks definiert und sind nur
innerhalb dieses Blockes gültig, also sichtbar. So beschreibt die Gültigkeit, von wo aus auf die Variablen zugegriffen
werden kann.

Nun gibt es in C++ die Möglichkeit die Gültigkeit und die Lebensdauer einer lokalen Variable zu beeinflussen. Der
Speicherklassenmodifikator static bewirkt innerhalb einer Funktion (ähnlich einer globalen Variablen), dass diese
Variable auf einer festen Speicheradresse gespeichert wird. Diese wird nach dem Schließen des Blocks, in dem sie
definiert wurde, nicht zerstört. Wird der gleiche Block noch einmal betreten, so besitzt die Variable immer noch
denselben Wert, der ihr zuletzt zugewiesen wurde. Die Lebensdauer beschreibt also, wie lange sich die Variable im
Speicher befindet.

Folgendes Beispiel zeigt das Verhalten einer statischen Variablen. Diese ist nur innerhalb der Funktion, also des Blocks
gültig und somit sichtbar.

{% highlight c++ %}
#include <stdio.h>

void funktion()
{
    // x wird nur ein einziges Mal initialisiert, trotz der Mehrfachaufrufe in main(), durch static wird die Lebensdauer
    // erhöht.
    static int x = 0;
    // Wert von x ausgeben (1. Aufruf: 0...
    std::cout << x << std::endl;
    // ... dann bei jedem weiteren Aufruf um 1 inkrementiert)
    x++
}

int main()
{
    funktion(); // Ausgabe: 0
    funktion(); // Ausgabe: 1
    funktion(); // Ausgabe: 2
    return 0;
}
{% endhighlight %}

**Anmerkung**: Für die Lösung der Aufgabe benötigen Sie eine Zeichenkette für das Passwort. Diese nennen sich Strings
und sind ein Array von chars. Um Strings verwenden zu können, müssen Sie die Bibliothek `<string>` im Kopf ihres
Programms einbinden. Statt einem einfachen Hochkomma (wie bei char) werden bei der Initialisierung von Strings doppelte
Hochkommas verwendet. Definitionsbeispiel: `string str _geheimwort = "geheim";`

## Modularisierung

> Eine sinnvolle Modularisierung im Programmtext ist für jegliche Art von Projekten zwingend erforderlich!

Besteht ein Programm aus mehreren Modulen (.cpp-Dateien), werden bei der Kompilierung beide Module getrennt voneinander
übersetzt. Die im Modul 1 definierten Variablen, sind im Modul 2 nicht bekannt und können daher auch dort nicht
verwendet werden. Damit diese Variablen im Modul 2 genutzt werden können, müssen sie dort deklariert werden.

Für eine solche Deklaration, die dem Compiler anzeigt, dass die betreffende Variable anderswo im Programm definiert ist,
benötigt man das Schlüsselwort extern. Ansonsten gleicht die Deklaration einer Variable syntaktisch ihrer Definition:

{% highlight c++ %}
extern Datentyp_der_Variablen Name_der_Variablen
{% endhighlight %}

Die Deklaration einer Variable ist - wie die Deklaration einer Funktion - nur eine Bekanntmachung für den Compiler, die
über ein an anderer Stelle definiertes Datenobjekt informiert und dabei kein neues erzeugt. Das bedeutet, dass dabei
auch kein Speicherplatz für das Datenobjekt reserviert wird.

![AWP - Modularisierung 01](/AWP/05-Modularisierung-01.jpg)

Das Einbinden der header-Datei `code.h` mit der Deklaration der Funktion `function(int izahl)` aus `code.cpp` ermöglicht
den Aufruf dieser Funktion in `main.cpp`. Gleichzeitig kann durch `extern int a` die globale Variable `a` im Modul
`code.cpp` verwendet werden. Die beispielhafte Ausgabe des Programms lautet: 4

![AWP - Modularisierung 02](/AWP/05-Modularisierung-02.jpg)
