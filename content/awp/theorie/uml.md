---
title: UML
date: 2020-11-20 08:30:00 +0200
---

{{< tabs "uml" >}}

{{< tab "Anwendungsdiagram" >}}

Anwendungsfalldiagramme stellen eine zentrale Kommunikationsbasis zwischen Anforderungsermittler (Analytiker) und dem
Benutzer (Auftraggeber) dar. Besondere Bedeutung hat sie deshalb in der Analysephase eines objektorientierten
Entwicklungsprozesses, wo es darum geht festzustellen, was ein System leisten soll nicht wie das genau erfolgen soll.
Detaillierte Abläufe sind hier nicht erkennbar, sondern in anderen Verhaltensdiagrammen (z.B. Aktivitätsdiagramm,
Zustandsdiagramm).

Ein Anwendungsfall (Use Case) wird stets von einem Akteur angestoßen und beschreibt einen Geschäftsvorfall (bei der
Geschäftsprozess-Modellierung) oder eine typische Interaktion eines Anwenders (Akteur oder actor) mit einem
Computersystem (bei der Software-Modellierung).

## Graphische Darstellung

| Akteur | Anwendungsfall |
| ------ | -------------- |
| ![AWP - UML Use Case - Rolle des Akteurs](/AWP/02-usecase-1.png) | ![AWP - UML Use Case - Anwednungsfall](/AWP/02-usecase-1.png) |
| Die Rolle/Funktion die ein Mensch (bzw. ein externer Prozess, externes System) ausfüllt wird unter/in das graphische Symbol geschrieben. Das Strichmännchen steht eher für eine Person, im Rechteck steht eher ein externes System. | Der Name des Anwendungsfalls beschreibt die Funktionalität aus Sicht des Akteurs und sollte immer ein Verb beeinhalten |

Besteht zwischen einem Anwendungsfall und einem Akteur eine Beziehung, wird dies durch eine gerade Verbindungslinie
dargestellt. Die Anwendungsfälle eines Systems werden umrahmt und der Systemname angegeben.

![AWP - UML Use Case - Beziehung Akteur-System](/AWP/02-usecase-3.png)

Beispiel: Anwendungsfalldiagramm Vermögensanlage
Ein Sachbearbeiter einer Unternehmung kauft Anlagen und überprüft die Liquidität. Nur der Finanzchef darf Risikoanlagen
kaufen.

![AWP - UML Use Case - Beziehung Beispiel](/AWP/02-usecase-4.png)

## Beziehungen zwischen Anwendungsfällen

Neben den Verbindungen zwischen Akteuren und Anwendungsfällen lassen sich auch Beziehungen zwischen den
Anwendungsfällen darstellen.

### Die Enthält-Beziehung (include relationship)

Beispiel: Vermögensanlage
In dem Anwendungsfall „Anlage kaufen“ ist immer auch eine Liquiditätsprüfung enthalten.

![AWP - UML Use Case - Include Relationship Beispiel](/AWP/02-usecase-5.png)

allgemeine graphische Darstellung der Enhält-Beziehung:

![AWP - UML Use Case - Include Relationship](/AWP/02-usecase-6.png)

**Merke**:
Der Anwendungsfall A enthält den Anwendungsfall B. Anwendungsfall B wird immer verwendet, wenn Anwendungsfall A
auftritt. Der Pfeil ist vom benutzenden zum benutzten Anwendungsfall gerichtet.
Die include-Beziehung eignet sich, etwas darzustellen, das in mehreren Anwendungsfällen vorkommt, um so Redundanzen zu
vermeiden.

### Die Erweitert-Beziehung (extend relationship)

Beispiel: Vermögensanlage
Wenn eine Anlage gekauft wird, muss ein Depot vorhanden sein. Wenn noch kein Depot vorhanden ist, muss eines angelegt
werden. Nimmt man den Schritt „Depot eröffnen“ hinzu, ergibt sich ein neuer Anwendungsfall. Dies ist eine Erweiterung
des ursprünglichen Verfahrens.

Abhängig davon, ob es ein Depot gibt, wird das „Depot eröffnen“ durchgeführt.
Man sagt: Der neue Anwendungsfall erweitert unter bestimmten Umständen den ursprünglichen Anwendungsfall
(Basis-Anwendungsfall).

![AWP - UML Use Case - Extend Relationship Beispiel](/AWP/02-usecase-7.png)

allgemeine graphische Darstellung der Erweitert-Beziehung

![AWP - UML Use Case - Extend Relationship](/AWP/02-usecase-8.png)

**Merke**:
Die extend-Beziehung von einem Anwendungsfall B zu einem Anwendungsfall A bedeutet, dass eine Instanz von Anwendungsfall
A unter bestimmten Bedingungen durch das im Anwendungsfall B beschreibende Verhalten erweitert werden kann.
Anwendungsfall A kann alleine ausgeführt werden, die Funktionalität des Anwendungsfalls B ist eine optionale
Erweiterung.
Im Anwendungsfall A sind extension points (ein oder mehrere Erweiterungspunkte) aufgelistet.
Wenn nötig kann die Bedingung, die an einen extension point geknüpft ist, als Notiz mit dem Pfeil der
Erweiterungsbeziehung verbunden werden.

### Die Generalisierungs-Beziehung

Beispiel: Vermögensanlage
Der Kauf einer mit Risiko behafteten Anlage bedarf einer gesonderten Behandlung, die von dem Finanzchef persönlich
durchgeführt wird. Der Anwendungsfall "Risikoanlage kaufen" ist also somit ein Spezialfall des Anwendungsfalles "Anlage
kaufen".
Diese Spezialisierung stellt wie beim Klassendiagramm die Vererbungsbeziehung dar.

![AWP - UML Use Case - Generalisierungs-Beziehung](/AWP/02-usecase-9.png)

## Vorgehensweise beim Erstellen von Use-Case-Diagrammen

1. Das System benennen und alle Akteure herausfinden.
2. Die wesentlichen Anwendungsfälle festhalten, ohne sich in Details zu verlieren (Beschreibung des Anwendungsfalls
   sollte ein Verb beinhalten. Er sollte einzeln ausgeführt werden können und muss mit einem Akteur verbunden sein).
3. Gemeinsame Anwendungsfälle ermitteln und mit ``<<include>>`` verbinden.
4. Anwendungsfälle notieren, die nur unter bestimmten Bedingungen andere Anwendungsfälle erweitern (``<<extend>>``).
5. Untersuchen ob Generalisierungs-Beziehungen zwischen Anwendungsfällen enthalten sein könnten.


 {{< /tab >}}

 {{< tab "Aktivitätsdiagramm" >}}


Aktivitätsdiagramme bieten die Möglichkeit, Prozesse abzubilden, die sich aus mehreren Aktionen zusammensetzen.

Eine Aktion ist ein elementarer Vorgang, der nicht weiter zerlegt werden kann oder soll. Eine Aktivität ist eine Folge
von Aktionen, die ein bestimmtes Ergebnis liefern und unter einem gemeinsamen Namen zusammengefasst werden. In dieser
Hinsicht ähneln Aktivitätsdiagramme den Programmablaufplänen, die in der EDV zur Darstellung von Programmabläufen
dienen.

Die wesentlichen Elemente eines Aktivitätsdiagramms sind

1. Aktionen
2. Steuerungselemente
3. Objektknoten
4. Verbindungskanten

Als Symbol für eine Aktion verwendet die UML ein Rechteck mit abgerundeten Ecken:

{{< mermaid class="text-center">}}
graph TD
  id1(Aktionsname)
{{< /mermaid >}}

Abläufe ergeben sich durch die Verknüpfung mehrerer Aktionen durch so genannte Kanten. Dabei handelt es sich um
Verbindungspfeile (gerichtete Kanten), die eine Reihenfolge der durchzuführenden Aktionen festlegen.

Die Ablaufsteuerung erfolgt über Bedingungen, die beispielsweise zur Verzweigung oder Zusammenführung von Teilabläufen
führen. Damit sind auch parallele Prozesse in Aktivitätsdiagrammen darstellbar.

Beispiel für Verzweigung:

{{< mermaid class="text-center">}}
graph TD
    id1(Aktion 1)
    id2{ }
    id3(Aktion 1.1)
    id4(Aktion 1.2)
    id1 --> id2
    id2 -- Bedingung1.1 --> id3
    id2 -- Bedingung1.2 --> id4
{{< /mermaid >}}

Objektknoten repräsentieren Daten oder Akteure, die im Rahmen einer Aktivität von Bedeutung sind. Sie werden durch ein
Rechteck repräsentiert, das mit dem Namen des Objektes beschriftet ist.

Beispiel:

{{< mermaid class="text-center">}}
graph LR
  id1[Auftrag]
  id2(Auftrag abwickeln)
  id3[Rechnung]
  id1 -.-> id2
  id2 -.-> id3
{{< /mermaid >}}

## Aktionen

Durch die Verknüpfung von Aktionen über gerichtete Kanten (Pfeile) entsteht eine zeitlich geordnete Folge von Aktionen:

{{< mermaid class="text-center">}}
graph LR
  id1(Aktion 1) --> id2(Aktion 2)
{{< /mermaid >}}

Wenn man eine Folge von Aktionen mit einem Rahmen versieht (einschließlich Beschriftung), werden Aktionen zu einer
Aktivität zusammengefasst:

{{< mermaid class="text-center">}}
graph TD
    id1(Wasser in Kaffeemaschine füllen)
    id2(Filterpapier einlegen)
    id3(Kaffee in Filter einfüllen)
    id4(Kaffeemaschine einschalten)
    id1 --> id2 --> id3 --> id4
{{< /mermaid >}}

Der Text innerhalb des Aktionssymbols ist beliebig; es ist somit auch möglich, hier eine Ablaufbeschreibung z.B. mit
Pseudocode einzufügen. Wenn diese Ablaufbeschreibung allerdings zu umfangreich wird, sollte die Aktion besser als eigene
Aktivität modelliert werden.

## Steuerungselemente

Steuerungselemente regeln die zeitliche und logische Abfolge von Aktionen im Rahmen einer Aktivität. Neben Start und
Ende bilden sie Verzweigungen und Verbindungen von Abläufen und lenken parallele Vorgänge.

### Startknoten

Der Startknoten markiert den Beginn eines Ablaufs in einem Aktivitätsdiagramm. Von einem Startknoten dürfen beliebig
viele Kanten ausgehen, jedoch keine zu ihm hinführen!

Wenn in einem Aktivitätsdiagramm mehrere Startknoten vorkommen, so kann der Beginn alternativ oder auch parallel
erfolgen. Der Eintrittspunkt in ein Aktivitätsdiagramm kann auch ein Objektknoten sein.

### Endknoten

Ein Endknoten markiert das Ende eines Ablaufs. Dabei wird zwischen dem Ende eines Teilablaufs und dem Ende der gesamten
Aktivität unterschieden. Der Endknoten für Aktivitäten: `TODO: GRAFIK`, der für Teilabläufe: `TODO: GRAFIK`
Wenn nur ein Teilablauf beendet ist, können andere (parallele) Abläufe der Aktivität weiter laufen.

### Splitting und Synchronisation

Die Abläufe in einem Aktivitätsdiagramm können geteilt und wieder zusammen geführt werden. Durch die Teilung (Splitting)
entstehen parallele Abläufe, die zu zeitgleichen Aktivitäten führen. Umgekehrt werden parallele Abläufe durch
Synchronisation wieder zu einer gemeinsamen Folge von Aktionen zusammen geführt.

| Splitting | Synchronisation |
| --- | --- |
| ![AWP - Aktivitätsdiagramm - Splitting](/AWP/04-aktivitaetsdiagramm-01.png) | ![AWP - Aktivitätsdiagramm - Synchronisation](/AWP/04-aktivitaetsdiagramm-02.png) |
| Nach Ausführung von Aktion 1 leiten Aktion 2 und Aktion 3 zeitgleiche, voneinander unabhängige Abläufe ein. Der Balken bewirkt die Auftrennung. | Die zunächst parallel verlaufenden Aktionen A und B werden durch die Synchronisation am Balken zusammen geführt. Anschließend wird Aktion C ausgeführt. Bevor Aktion C ausgeführt werden kann, müssen beide Vorgänger (Aktion A und Aktion B) beendet sein! |

Das Beispiel "Frühstück vorbereiten" soll Splitting und Synchronisation veranschaulichen:

![AWP - Aktivitätsdiagramm - Frühstück Beispiel](/AWP/04-aktivitaetsdiagramm-03.png)

### Verzweigung

Verzweigungen dienen in Aktivitätsdiagrammen dazu, alternative Abläufe darzustellen.

{{< mermaid class="text-center">}}
graph TD
    id1(Deckung prüfen)
    id2{ }
    id3(auszahlen)
    id4(Zahlung verweigern)
    id1 --> id2
    id2 -- deckung=true --> id3
    id2 -- deckung=false --> id4
{{< /mermaid >}}

### Verbindung

Ein Verbindungsknoten führt mehrere Kanten zu einem einzigen Knoten zusammen:

{{< mermaid class="text-center">}}
graph TD
    id1(Aktion C)
    id2{ }
    id3(Aktion A)
    id4(Aktion B)
    id3 --> id2
    id4 --> id2
    id2 --> id1
{{< /mermaid >}}

Im Gegensatz zur Synchronisation kann Aktion C starten, wenn zumindest eine der beiden Aktionen A oder B beendet sind.
Bei der Synchronisation müssen Aktion A und Aktion B zwingend beide beendet sein, bevor Aktion C startet.

## Objektknoten

Mithilfe von Objektknoten werden Daten und Werte in eine Aktivität eingebracht. Die Darstellung erfolgt über ein
Rechteck, das mit einer Beschriftung zur Kennzeichnung des Knotentyps versehen wird:

![AWP - Aktivitätsdiagramm - Objektknoten](/AWP/04-aktivitaetsdiagramm-04.png)

Objektknoten können als Eingangs einer Aktion auftreten. Sie liefern dann Daten oder Werte, die erforderlich sind, um
die betreffende Aktion auszuführen:

![AWP - Aktivitätsdiagramm - Objektknoten](/AWP/04-aktivitaetsdiagramm-05.png)

Für die Durchführung der Aktion "Kaffee zubereiten" wird ein Kaffeefilter-Objekt benötigt.

Ebenso können Aktionen Ergebnisse in Form von Daten oder Werten haben. Dann ergibt sich aus einer Aktion ein
Objektknoten:

![AWP - Aktivitätsdiagramm - Objektknoten](/AWP/04-aktivitaetsdiagramm-06.png)

Für einen Objektknoten kann auch ein bestimmter Zustand angegeben werden, der in eckigen Klammern hinter dem Knotentyp
angegeben wird:

![AWP - Aktivitätsdiagramm - Objektknoten](/AWP/04-aktivitaetsdiagramm-07.png)

## Kontrollfluss/Objektfluss

### Kontrollfluss

Als Kontrollfluss bezeichnet man eine Kante (Pfeillinie), die entweder 2 Aktionen oder eine Aktion mit einem
Steuerungselement verbindet:

![AWP - Aktivitätsdiagramm - Kontrollfluss](/AWP/04-aktivitaetsdiagramm-08.png)

### Objektfluss

Ein Objektfluss ist eine Kante, die mindestens an einem Ende mit einem Objektknoten verbunden ist:

![AWP - Aktivitätsdiagramm - Objektfluss](/AWP/04-aktivitaetsdiagramm-09.png)

Hier sind alle Kanten Objektflüsse (trotz des Steuerelementes zwischen Aktion 1 und Aktion 2 bzw. Aktion 3 erfolgt die
Verbindung der Aktionen über einen Objektknoten).

### Bedingungen

Kanten können an Bedingungen geknüpft werden. Ein Übergang von einer Aktion zur nächsten ist nur dann möglich, wenn die
Bedingung erfüllt ist. Die Bedingung wird in eckigen Klammern notiert:

![AWP - Aktivitätsdiagramm - Bedingungen](/AWP/04-aktivitaetsdiagramm-10.png)

Übersichtlicher ist es, Bedingungen über Verzweigungen zu realisieren. Dadurch kann es auch nicht zu dem Fall kommen,
dass eine Aktivität aufgrund einer niemals wahr werden - den Bedingung irgendwo "hängen bleibt" und nicht ordnungsgemäß
beendet wird.

## Verantwortlichkeitsbereiche

Aktivitätsdiagramme können in Verantwortlichkeitsbereiche (swimlanes) aufgeteilt werden. Die einzelnen
Verantwortlichkeitsbereiche werden durch senkrechte Linien optisch voneinander getrennt. Jeder
Verantwortlichkeitsbereich stellt dabei eine Aktivität dar, die wiederum aus einzelnen Aktionen bestehen können. Bei der
Aufteilung des Aktivitätsdiagramms in Verantwortlichkeitsbereiche lassen sich auch die Aktivitäten unterschiedlicher
Objekte in einem einzigen Diagramm darstellen:

![AWP - Aktivitätsdiagramm - Verantwortlichkeitsbegreiche](/AWP/04-aktivitaetsdiagramm-11.png)

Das Aktivitätsdiagramm zeigt das Verhalten der beiden beteiligten Objekte `konto1` und `konto2`.

{{< /tab >}}

{{< tab "Sequenzdiagramm" >}}

## Was ist ein Sequenzdiagramm?

- Ein Sequenzdiagramm zeigt Objekte, die eine Reihe von Nachrichten in einer zeitlich begrenzten Situation austauschen,
  wobei der zeitliche Ablauf betont wird
- Nachricht $$\rightarrow$$ Aktivierung der Methode gleichen Namens
- 2 Dimensionen:
  - Vertikale: Zeit
  - Horizontale: Objekte
- Jedes Objekt hat eine senkrechte Lebenslinie
- Ein Balken auf der Lebenslinie zeigt Aktivität an
- Nachrichten sind Pfeile, Antworten gestrichelte Pfeile

> Sequenzdiagramme verwendet man zur Darstellung des zeitlich geordneten Nachrichtenflusses zwischen Objekten

## Notationselemente

### Objektsymbole

Am oberen Ende des Diagramms werden Objektsymbole angetragen.

Bei diesen Objekten handelt es sich nicht um spezielle Objekte, sondern um Stellvertreter für beliebige Objekte der
angegebenen Klasse.

Daher werden sie als anonyme Objekte (d.h. `:Klasse`) bezeichnet.

Die Reihenfolge der Objekte ist beliebig, sie soll so gewählt werden, dass ein übersichtliches Diagramm entsteht.

![AWP - Sequenzdiagramm - 01](/AWP/05-sequenzdiagramm-01.png)

### Lebenslinie / Objektlinie

Jedes Objekt wird durch eine gestrichelte Linie im Diagramm dargestellt.

Die Linie beginnt nach dem Erzeugen des Objektes und endet mit dem Löschen. Existiert das Objekt während der gesamten
Ausführungszeit des Szenarios, so ist die Linie von oben nach unten durchgezogen.

Das Rechteck, d.h. die "Verdickung der Linie" zeigt an, wann das Objekt aktiv ist.

X Markiert das Löschen eines Objektes

![AWP - Sequenzdiagramm - 02](/AWP/05-sequenzdiagramm-02.png)

### Botschaften / Nachrichten

Dienen zum Aktivieren der Operationen (Methodenaufrufe). Jede Botschaft wird als Pfeil vom Sender zum Empfänger
gezeichnet.

Der Pfeil wird mit dem Namen der aktivierten Operation (Methode) beschriftet. Die Nachricht kann synchron
($$\rightarrow$$ - Pfeilspitze sollte gefüllt sein; Sender gesperrt, wartet auf Antwort) oder asynchron
($$\rightarrow$$; Sender nicht gesperrt) sein.

Nach Beenden einer synchronen Operation kann ein gestrichelter Pfeil zeigen, dass Kontrollfluss zur aufrufenden
Operation zurückgeht ($$\dashleftarrow$$; optional).

![AWP - Sequenzdiagramm - 03](/AWP/05-sequenzdiagramm-03.png)

### Interaktion

### Beispiel: Schleife = Loop

Die Schleife gibt an, dass eine Nachricht mehrmals an viele Empfängerobjekte gesendet wird.

![AWP - Sequenzdiagramm - 04](/AWP/05-sequenzdiagramm-04.png)

### Beispiel: Bedingung = Alternative

Es kann angegeben werden, dass eine Nachricht nur dann gesendet wird, wenn eine bestimmte Bedingung zutrifft.

Leere "else"-Zweige können weggelassen werden.

![AWP - Sequenzdiagramm - 05](/AWP/05-sequenzdiagramm-05.png)

### Selbstdelegation

Ein Objekt kann u.U. Nachrichten an sich selbst senden (eigene Methoden aufrufen).

Hier ruft ein Objekt der Klasse CArtikel (z.B. nach jeder Entnahme) eine eigene Methode auf, die prüft ob ein Bestand
erreicht oder unterschritten wurde, der eine Nachbestellung erforderlich macht.

![AWP - Sequenzdiagramm - 06](/AWP/05-sequenzdiagramm-06.png)

## Beispiel

### Praktisches Beispiel

Klassendiagramm:

![AWP - Sequenzdiagramm - 07](/AWP/05-sequenzdiagramm-07.png)

Sequenzdiagramm für obiges Klassendiagramm:

![AWP - Sequenzdiagramm - 08](/AWP/05-sequenzdiagramm-08.png)

### Theoretisches Beispiel mit Erläuterungen

![AWP - Sequenzdiagramm - 09](/AWP/05-sequenzdiagramm-09.png)

{{< /tab >}}

{{< /tabs >}}