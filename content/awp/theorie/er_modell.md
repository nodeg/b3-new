---
title: Entity-Relationship-Modell
date: 2019-10-17 10:00:00 +0200
---
{{< tabs "er-modell" >}}

{{< tab "ER-Modell" >}}
## Hintergrund

Vor der Erstellung einer Datenbank muss eine Anforderungsanalyse durchgeführt und ein Datenbankentwurf erstellt werden.

Im Rahmen der Anforderungsanalyse wird ermittelt, welche Informationen und Vorgänge in der Datenbank gespeichert werden
müssen. Dieser Prozess wird in enger Zusammenarbeit mit dem Fachbereich und ggf. den (zukünftigen) Kunden durchgeführt.
Aus der Anforderungsanalyse resultiert die Anforderungsspezifikation (auch: Pflichtenheft).

Für die Erstellung eines Datenbankentwurfs wird die Anforderungsspezifikation genutzt. Dabei wird auf Grundlage der
ermittelten Anforderungen zunächst ein konzeptioneller Entwurf und anschließend ein physischer Entwurf der Datenbank
erstellt. Der konzeptionelle Entwurf legt den Fokus auf die Gestaltung der logischen Datenstrukturen. Der physische
Entwurf verfolgt das Ziel der Effizienzsteigerung, ohne die logische Struktur der Daten zu verändern.

Im Rahmen des konzeptionellen Entwurfes wird in der Praxis das Entity-Relationship-Modell (übersetzt:
Gegenstands-Beziehungs-Modell, kurz: ERM, ER-Modell, ER-Diagramm) erzeugt. Das Modell dient der Veranschaulichung und
der Beschreibung der logischen Struktur der zu erstellenden Datenbank. Es wird darüber hinaus auch zu Optimierungs-
und Dokumentationszwecken verwendet.

## Das Entity-Relationship-Modell

Entity-Relationship-Modelle setzen sich zusammen aus Symbolen:

| ERM-Symbol                                                      | Beschreibung                                                                                                                                                                                                                        |
|-----------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| ![AWP - 03 - Enitity](/AWP/03-entity.png)            | Eine Entität (engl. entity) ist eine Zusammenfassung von Objekten mit gleichen Eigenschaften unter einem Oberbegriff.<br><br>**Beispiel**:<br>_Anforderung_: Max und Otto sind Kunden.<br>_Objekte_: Max, Otto.<br>_Entity_: Kunde. |
| ![AWP - 03 - Relationship](/AWP/03-relationship.png) | Beziehungen (engl. relationship) beschreiben Wechselwirkungen und Abhängigkeiten zwischen Entitäten.<br><br>**Beispiel**:<br>_Anforderung_: Der Kunde mietet ein Fahrzeug.<br>_Entities_: Kunde, Fahrzeug<br>_Beziehung_: Mieten    |
| ![AWP - 03 - Attribut](/AWP/03-attribut.png)         | Ein Attribut bezeichnet eine Eigenschaft einer Entität oder einer Beziehung.<br><br>**Beispiele**:<br>_Anforderung_: Alle Kunden haben eine Kundennummer.<br>_Entity_: Kunde.<br>_Attribut_: Kundennummer.                          |
| ![AWP - 03 - Verbinder](/AWP/03-verbinder.png)       | _Anforderung_: Die Miete ist ein Preis, den ein Kunde für ein geliehenes Fahrzeug zahlen muss.<br>_Beziehung_: Mieten<br>_Attribut_: Preis<br><br>**Verbindungslinien** verknüpfen die ERM-Symbole untereinander.                   |

Beziehungen zwischen zwei Entitäten EI und E2 werden wie folgt unterschieden:

- 1:1-Beziehung: Die Beziehung gilt für jedes Objekt der ersten Entität El mit genau einem Objekt der zweiten Entität
  E2.
- 1:n-Beziehung: Die Beziehung gilt für jedes Objekt der ersten Entität El mit n Objekten der zweiten Entität E2.
- n:1-Beziehung: Die Beziehung gilt für n Objekte der ersten Entität E1 mit genau einem Objekt der zweiten Entität E2.
- n:m-Beziehung: Die Beziehung gilt für n Objekte der ersten Entität EI mit m Objekten der zweiten Entität E2.

Beispiel:

- **Anforderung**: Ein Kunde kann nur ein Fahrzeug zur selben Zeit mieten.
- **Bedeutung**: Wenn Max und Otto (=Objekte) als Kunden Fahrzeuge mieten wollen, dann kann jeder von ihnen nur ein
  Fahrzeug zur selben Zeit mieten.
- **Beziehung**: 1:1-Beziehung.
- **Folge**: In dem Entity-Relationship-Modell wird die Beziehung zwischen den Entitäten Kunden und Fahrzeug als
  1:1-Beziehung kenntlich gemacht!

{{< /tab >}}


{{< tab "Konzeptionelle Phase" >}}


Hier soll in der konzeptionellen Phase das Entlty-Relatlonship-Modell (ER-Modell) verwendet werden, weil es in der
Praxis weit verbreitet Ist und sich für die Entwicklung relationaler Datenbankmodelle bewährt hat. Nachfolgend sind die
Elemente von Datenmodellen beschrieben.

Eine Entität (engl. entity): Ist eine eindeutige identifizierbare Einheit (z.B. Individuum (Schüler), Objekt (Raum),
abstraktes Objekt (Kurs), Ereignis (mdl. Prüfung))

Eine Entitätsmenge (engl. entity set): Fasst alle Entitäten zusammen, die durch gleiche Merkmale nicht notwendigerweise
aber durch gleiche Merkmalsausprägungen, charakterisiert sind. (z.B. alle Schüler)

Zur grafischen Darstellung von Entitätsmengen werden Rechtecke verwendet.

Eine Beziehung: Ist die Wechselwirkung zwischen zwei Entitäten (Entitätsmengen) und wird ausgedrückt durch den
Beziehungstyp (Kardinalität der Assoziation)

Zur grafischen Darstellung von Beziehungen werden Rauten verwendet.

| Anzahl der Entität die einer anderen Entität zugeordnet werden können | Symbol     | Bezeichnung des Kardinalitätstyps der Assoziation |
|-----------------------------------------------------------------------|------------|---------------------------------------------------|
| Keine oder maximal eine                                               | 1          | einfach                                           |
| Keine, eine oder mehrere                                              | n (oder m) | mehrfach (viele, komplex)                         |

![AWP - ER-Beziehungen](/AWP/05-ER-Beziehungen.png)

Ein Attribut: Beschreibt eine bestimmte Eigenschaft, die sämtliche Entitäten einer Entitätsmenge aufweisen (z.B. Name).

Das Entity-Relationship-Modell (semantisches Modell)

Zwischen 2 Entitäten kann es mehrere Beziehungen geben. Diese müssen aber etwas unterschiedliches beschreiben.

Bei der Bestimmung der Kardinalitäten bietet sich folgender Fragesatz an. Dabei erfolgt die Überprüfung stets in beiden
Richtungen. Die Kardinalität wird dabei stets an der gegenüberliegenden Entität festgehalten.

Fragesatz: Eine konkrete <Entität> hat <Beziehung> zu wie vielen <Entitäten>?

{{< /tab >}}

{{< tab "Er-Modell to logisches Modell" >}}

## Vorgehensweise zur Überführung von ER-Modellen in logische Modelle

{{< mermaid class="text-center">}}
graph TD;
    A[Eintitätsmengen in Relationen überführen]
    B[Primärschlüssel für jede Relation identifizieren]
    C[Beziehungen auflösen]
    D[1:n-Beziehung auflösen]
    E[1:1 Beziehung auflösen]
    F[n:m Beziehung auflösen]
    G{Kardinalität}
    A --> B
    B --> C
    C --> G
    G --> D
    G --> E
    G --> F
{{< /mermaid >}}

## 1:n Beziehung auflösen

{{< mermaid class="text-center">}}
graph TD;
    A[1:n-Beziehung auflösen]
    B[Primärschlüssel der 1-Relation in n-Relation als Fremdschlüssel übertragen]
    C["1:Relation und n-Relation verbinden (Verbindung startet bei Primärschlüssel und endet bei Fremdschlüssel)"]
    D[1:n Kardinalität erfassen]

    A --> B
    B --> C
    C --> D
{{< /mermaid >}}

## 1:1 Beziehung auflösen

{{< mermaid class="text-center">}}
graph TD;
    A[1:1-Beziehung auflösen]
    B{Variante?}
    C[Primärschlüssel der 1-Relation in andere 1-Relation als Fremdschlüssel übertragen]
    D[Einheitlichen Primärschlüssel für beide 1-Relationen identifizieren]
    E[Beide Relationen zu einer Relation zusammenfassen]
    F["Beide 1-Relationen verbinden (Verbindung startet bei Primärschlüssel und endet bei Fremdschlüssel)"]
    G["Beide 1-Relationen verbinden (Verbindung startet bei Primärschlüssel und endet bei anderen Primärschlüssel)"]
    H[1:1-Kardinalität erfassen]

    A --> B
    B --> C
    B --> D
    B --> E
    C --> F
    D --> G
    F --> H
    G --> H
{{< /mermaid >}}

## n:m Beziehung auflösen

{{< mermaid class="text-center">}}
graph TD;
    A[n:m-Beziehung auflösen]
    B[Neue Relation anlegen]
    C[Primärschlüssel der ursprünglichen Relation als Fremdschlüssel in die neue Relation übernehmen]
    D[Beide Fremdschlüssel der neuen Relation als Primärschlüssel kennzeichnen]
    E["Urpsrüngliche Relationen mit der neuen Relation verbinden (Verbindung stratet bei Primärschlüssel und ende bei entsprechendem Fremdschlüssel)"]
    F[1:n-Kardinalitäten erfassen]

    A --> B
    B --> C
    C --> D
    D --> E
    E --> F
{{< /mermaid >}}

{{< /tab >}}
{{< /tabs >}}