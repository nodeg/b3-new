---
title: Struktur einer Schuldatenbank
date: 2019-10-18 10:49:17 +0200
---

# Struktur der Datenbank
* Lehrkraft
   * Name
   * Anschrift
   * Telefonnummer
   * Klassen
* Klasse
   * Klassenlehrer
   * Klassensprecher
   * Schüler
* Schüler
   * Name
   * Anschrift
   * Telefonnummer

## Objekte
* Lehrkraft
* Klassen
* Schüler

## Beziehungen
* unterrichten/leiten
* verantwortlich/zugeordnet
* ist
