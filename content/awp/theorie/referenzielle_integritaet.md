---
title: Referenzielle Integrität
date: 2020-05-20 10:49:17 +0200
---

## Anomalien

### Einfügeanomalie

Gehen wir von einer Datenbanktabelle in der 1. NF aus. Diese Tabelle enthält atomare Felder. Soll jedoch nun in ein oder
mehrere Spalten eine Information eingetragen werden, die nicht direkt abhängig ist vom Primärschlüssel sind, so fehlt
der Primärschlüssel für dieses Objekt und die neue Information kann nicht oder nur schwer in der Datenbank persistiert
werden.

Mögliche Lösung: Die Tabelle weiter normalisieren.

### Löschanomalie

Gehen wir von einer Datenbanktabelle in der 1. NF aus. Diese Tabelle enthält atomare Felder. Soll jedoch nun eine
Information gelöscht werden, welche von mehreren Datensätzen referenziert wird, so ist dies zunächst lediglich non
trivial. Möchte mann jedoch eine Information löschen die Referenzen enthält (sozusagen den Hauptdatensatz), so würden
auch Informationen gelöscht werden, welche vielleicht erhalten bleiben sollen. Die Sitation in der man durch Löschen
eines Datensatzes, andere benötigte Informationen mitlöscht, bezeichnet man als Löschanomalie.

Mögliche Lösung: Tabelle weiter normalisieren, um lediglich Referenzen zu löschen.

### Änderungsanomalie

Ist eine Information Redundant über eine Datenbank hinweg gespeichert, müssen bei einer Änderung alle vorkommen geändert
werden. Trotz Automatisierung solcher Vorgänge, können logische Verknüpfungen zu anderen Vorkommen der Information
übersehen werden. Dies führt zu Dateninkonsistenzen im besten Fall.

Mögliche Lösung: Eine weitere Normalisierung um Redundanzen zu vermeiden kann in vielen Fällen helfen.

### Fazit

Anomalien in Datenbanken treten mitunter bei einer nicht existierenden oder fehlerhaften Normalisierung auf und führen
zu Inkonsistenzen. In der Datenbankentwicklung ist die 3. NF oft ausreichend, um die perfekte Balance aus Redundanz,
Performance und Flexibilität für eine Datenbank zu gewährleisten. Sie eliminiert auch die meisten Anomalien in einer
Datenbank. Aber nicht alle...


## Referenzielle Integrität

Die referenzielle Integrität (nach alter Rechtschreibung: Referentielle Integrität) ist die Art der Integrität, die sich
auf die Beziehungen zwischen den Tabellen einer Datenbank bezieht. Zur Gewährleistung der referenziellen Integrität
dürfen Fremdschlüssel von Datensätzen nur auf existierende Primärschlüssel der referenzierten Tabelle verweisen. In dem
Fall gilt:

1. Ein neuer Datensatz mit einem Fremdschlüssel kann nur dann in einer Tabelle eingefügt werden, wenn in der
   referenzierten Tabelle ein Datensatz mit entsprechendem Wert im Primärschlüssel existiert.
2. Eine Löschung oder Änderung eines Wertes eines Primärschlüssels ist nur möglich, wenn dieser Wert des
   Primärschlüssels nicht durch einen Fremdschlüssel referenziert wird.

Tabellen mit Fremdschlüsseln als Attributen werden in der Literatur als Kindtabellen oder Detailtabellen bezeichnet.
Tabellen, die einen referenzierten Primärschlüssel besitzen, werden Elterntabellen oder Haupttabellen genannt.


## Referenzkontrolle

Es gibt in der Regel drei Varianten der Referenzkontrolle die im folgenden ausgeführt werden. Alle drei Varianten
existieren in der Realtität, jedoch sind Variante zwei und drei öfter zu finden.

### Variante 1: Keine Referenzkontrolle...

- ...bei einem Einfügen neuer Datensätze: Das Einfügen neuer Datensätze in der Kindtabelle ist uneingeschränkt zulässig.
  Die Werte des Fremdschlüssels müssen die Werte eines Primärschlüssels der Elterntabelle nicht referenzieren.
- ...bei Veränderung von Datensätzen: Das Verändern von Datensätzen der Elterntabelle ist für alle Attribute
  uneingeschränkt zugelassen. Eine Änderung von Werten des Primärschlüssels in der Elterntabelle bewirkt keine
  Veränderung der Werte des Fremdschlüssels in den Kindtabellen.
- ...bei Löschung von Datensätzen: Das Löschen von Datensätzen der Elterntabelle ist uneingeschränkt zugelassen. Eine
  Löschung von Datensätzen in der Elterntabelle bewirkt keine Veränderung der Werte des Fremdschlüssels in den
  Kindtabellen.

Eine Einhaltung der referenziellen Integrität wird in dieser Variante weder geprüft noch sichergestellt. Fun Fact: Ein
solches Vorgehen wird in der Informatik generell auch als Vogel-Strauß-Politik bezeichnet. Denn wer den Kopf in den Sand
steckt, sieht das Problem nicht mehr...

### Variante 2: Kontrolle der referenziellen Integrität...

- ...bei einem Einfügen neuer Datensätze: Das Einfügen neuer Datensätze in der Kindtabelle ist nur dann möglich, wenn
  die für den Fremdschlüssel angegebenen Werte (bzw. Wertkombinationen) einen Primärschlüssel der Elterntabelle
  referenzieren.
- ...bei Veränderung von Datensätzen: Das Verändern von Werten des Primärschlüssels der Elterntabelle ist nur dann
  möglich, wenn es keinen Datensatz mehr in der Kindtabelle gibt, der den zu verändernden Primärschlüssel referenziert.
- ...bei Löschung von Datensätzen: Das Löschen von Datensätzen der Elterntabelle ist nur dann möglich, wenn es keinen
  Datensatz mehr in der Kindtabelle gibt, der den zu löschen- den Primärschlüssel referenziert.

### Variante 3: Kontrolle der referenziellen Integrität mit...

- ...Änderungsweitergabe (auch als "kaskadierendes Ändern" bezeichnet): Das Verändern von Werten des Primärschlüssels
  der Elterntabelle bewirkt eine Veränderung der Werte des Fremdschlüssels bei allen Datensätzen in der Kindtabelle, die
  den Primärschlüssel referenzieren.
- ...Löschweitergabe (auch als "kaskadierendes Löschen" bezeichnet: Das Löschen von Datensätzen der Elterntabelle
  bewirkt eine Löschung aller Datensätze in der Kindtabelle, die den Primärschlüssel referenzieren.

