---
title: Datenbank Grundlagen
date: 2019-10-12 10:49:17 +0200
---

## Einführung

Um die immer weiter anwachsende Informationsflut in den Griff zu bekommen, werden in zunehmendem Maße Datenbanken zur
Verwaltung der Informationen eingesetzt.<br>
Durch unkontrolliert wachsende Datenbestände, ist in bestimmten Bereichen ein Datenchaos entstanden. Verursacht wird das
Datenchaos u.a. durch die eigenständige isolierte Datenhaltung für einzelne Anwendungen. Beispielsweise kann es in einem
Betrieb vorkommen, dass Kundendaten für unterschiedliche Anwendungen jeweils neu gespeichert werden, z.B. für
Abrechnungen, für die Zusendung von Informationsmaterial, für Service und Beratung. Mit der Mehrfachspeicherung gleicher
Informationen (Redundanz) wird nicht nur Speicherplatz verschwendet, sondern sie führt auch dazu, dass die
Informationsverarbeitung nicht effizient genug ist (mehrfache Datenpflege ist notwendig).

Die schwerwiegendsten Fehler können aber dadurch entstehen, dass bei Mehrfachspeicherung der Daten unterschiedliche
Änderungen in den einzelnen Anwendungen vorgenommen werden; dadurch sind die Daten nicht mehr widerspruchsfrei
(Verletzung der Datenintegrität). Dieser Zustand führt dann zum Chaos in der Datenorganisation.

Zur Lösung dieser Probleme soll die integrierte Informationsverarbeitung beitragen. Sie bedeutet Datenintegration und
Vorgangsintegration.

Datenintegration wird durch eine Datenbasis (Datenbank) erreicht, die von mehreren Anwenungen in unterschiedlichen
betrieblichen Funktionsbereichen gemeinsam genutzt wird. Ziel ist die zusammenfassende Abbildung der gesamten
Organisation und ihrer Beziehungen zur Umwelt in einer einzigen Datenbasis (globales Modell).

Vorgangs-, Funktions- oder Prozessintegration erfolgen durch die IT-technische Verknüpfung von arbeitsteilig in
verschiedenen Abteilungen abzuwickelnden Vorgängen zu Ablaufketten (z.B. Auftragsabwicklung vom Kundenauftrag bis zu
Auslieferung und Entsorgung).

Eine wichtige Voraussetzung für die Realisation der integrierten Informationsverarbeitung sind geeignete
Datenbankverwaltungssysteme, denen bestimmte logische Datenmodelle zugrunde liegen.

Das hierarchische Datenbankmodell ist das älteste logische Datenbankmodell, es bildet die reale Welt durch eine
hierarchische Baumstruktur ab.

Das nächste Datenmodell ist das Netzwerk-Modell. Es fordert keine strenge Hierarchie, d.h. ein Datensatz kann mehrere
Vorgänger haben. Auch können mehrere Datensätze an oberster Stelle stehen.

Aber erst das relationale Datenmodell schaffte in den 1990ern einen technischen Durchbruch. Eine relationale Datenbank
kann man sich als eine Sammlung von Tabellen (den Relationen) vorstellen, in welchen Datensätze abgespeichert sind.

Mit dem Aufkommen objektorientierter Programmiersprachen werden vermehrt Objektrelationale und Objektorientierte
Datenbanken eingesetzt.

Wenn z.B. im Internet sehr große Datenmengen verarbeitet werden müssen werden auch sogenannte NoSQL-Datenbanken
eingesetzt. Hier unterscheidet man z.B. Key Value Stores-, Spaltenorientierte-, Dokumentenorientierte- und
Grafendatenbanken.

Da ca. 90% der eingesetzten Datenbanken auf dem relationalen Datenbankmodell basieren, wird im folgenden hauptsächlich
auf dieses Modell eingegangen.

## Neun Ziele der Datenorganisation

Unter dem Begriff Datenorganisation werden alle Verfahren zusammengefasst, die dazu dienen, Daten:

- zu strukturieren und
- auf Datenträgern zu speichern (schreibender Zugriff) und für den lesenden Zugriff verfügbar zu halten.

Die neun Ziele der Datenorganisation sind:

### 1) Datenunabhängigkeit

- Unabhängigkeit vom Anwendungsprogramm: Die Daten sind anwendungsneutral gespeichert, d.h. unabhängig vom erzeugenden
  oder benutzenden Anwendungsprogramm (im Gegensatz zur integrierten Verarbeitung mit Dateiorganisation).
- Unabhängigkeit der logischen von der physischen Datenorganisation: Der Benutzer muss nur die Datenstrukturen kennen.
  Methoden zum Suchen, Ändern, Einfügen und Löschen von Datensätzen werden vom Datenbankverwaltungssystem zur Verfügung
  gestellt.
- Physische Datenunabhängigkeit: Das Datenbankverwaltungssystem steuert und überwacht (um Zusammenspiel mit dem
  Betriebssystem) die peripheren Geräte, blockt bzw. entblockt Sätze, kontrolliert Überlaufbereiche, belegt
  Speicherräume oder gibt sie frei usw.

### 2) Benutzerfreundlichkeit

Leicht zu erlernbare Benutzersprachen ($$\rightarrow$$ SQL) ermöglichen sowohl dem professionellen Benutzer
(Systementwickler, Programmierer) als auch dem Endbenutzer eine einfache Handhabung der Daten. Die Benutzersprachen
sollten durch grafische Bedienoberflächen unterstützt werden (GUI).

### 3) Mehrfachzugriff

Jeder, der autorisiert ist, darf im Mehrbenutzerbetrieb zu jeder Zeit auf die gespeicherten Daten zugreifen.

### 4) Flexibilität

Die Daten müssen in beliebiger Form verknüpfbar sein (mehrdimensionaler Zugriff, Vielfachzugriff). Sie müssen sowohl den
fortlaufenden als auch den wahlfreien Zugriff ermöglichen.

### 5) Effizienz

Die Zeiten für die Abfrage und für die Verarbeitung müssen kurz sein, ebenso für Änderungen und Ergänzungen des
Datenbestandes.

### 6) Datenschutz

Die Daten sind vor unbefugtem Zugriff (Missbrauch) zu schützen:

- Ist der Teilnehmer überhaupt zugriffsberechtigt?
- Ist der Teilnehmer nur zu bestimmten Daten zugriffsberechtigt?
- Ist der Teilnehmer nur zu Abfragen oder auch zu Änderungen berechtigt?

### 7) Datensicherheit

Die Daten müssen gegen Programmfehler und Hardware-Ausfälle und Hardware-Ausfälle gesichert sein. Das Datenbanksystem
soll nach Störungsfällen den korrekten Zustand wiederherstellen (recovery).

### 8) Datenintegrität
### 9) Redundanzfreiheit

## Das Konzept des Datenbanksystems

## Aufbau und Arbeitsweise von Datenbanksystemen

