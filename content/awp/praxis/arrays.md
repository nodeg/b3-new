---
title:  Arrays
date:   2019-04-04 06:00:00 +0100
---
## Allgemeines

Ein Feld wird auch Array genannt. Streng genommen sind diese keine Funktion die C++ ist, sondern C. Intern ist ein Array
lediglich eine Sequenz von Objekten desselben Typs direkt hintereinander im Speicher allokiert.

## Syntax

Einen Array initialisieren:

````c
// Initialisierung eines Arrays
char chArray[10];

// Zugriff auf Elemente (lesend)
char *pch = chArray;        // Evaluates to a pointer to the first element.
char   ch = chArray[0];     // Evaluates to the value of the first element.
ch = chArray[3];            // Evaluates to the value of the fourth element.

// Zugriff auf Elemente (schreibend)
chArray[5] = 'a';

## Eindimensionale Felder

Eindimensionale Felder werden auch Arrays genannt. Sie befähigen uns mehrere Werte vom gleichen Datentyp gruppiert
anzulegen und zu verwalten. Dies erspart uns Konstrukte wie bspw. `zahl_1, zahl_2, zahl_3, ..., zahl_n`.

Formal wird ein Array folgendermaßen definiert: `Datentyp Name[Größe];`. Arrays werden im Arbeitsspeicher immer
zusammenhängend reserviert.

Wenn wir einen Array vom Datentyp `int` mit der Länge 10 definieren wollen, ergibt das: `int zahlen[10];`. Das Problem
ist, dass an der Stelle im Arbeitsspeicher bereits Werte standen. Das Array ist also nicht leer.

Für das Problem, dass Arrays nach der Definition nicht leer sind gibt es zwei Lösungsmöglichkeiten:
1. Manuelles Setzen der Werte mit einer `for`-Schleife
2. Bei der Initialisierung sofort alle Werte auf 0 setzen: `int zahlen[10] = { 0 };`

Weitere Beispiele um Arrays zu definieren:
````c++
int zahlen_1[10] = { 2 };           // 2 0 0 0 0 0 0 0 0 0
int zahlen_2[10] = { 1, 2, 3 }      // 1 2 3 0 0 0 0 0 0 0
````

Um auf einen Array lesend zuzugreifen ist folgende Syntax zu verwenden: `variable = zahlen[Platz];`. Hiermit würde in
der Variable `variable` der Wert aus `zahlen` an Platzierung `Platz` gespeichert werden.<br>
Um auf einen Array schreibend zuzugreifen ist folgende Syntax zu verwenden: `zahlen[Platz] = 5;`. Hiermit wird dem Array
`zahlen` an Platz `Platz` der Wert `5` zugewiesen.

**Bei lesendem um schreibendem Zugriff ist zu beachten, dass C++ Nullbasiert arbeitet!** Nullbasiert bedeutet, dass der
erste Platz im Array nicht mit `1` nummeriert ist, sondern mit `0`. Die zweite Position in einem Array wird also mit
`array[1]` gelesen oder gesetzt!

## Mehrdimensionale Felder

Da ein Feld lediglich eine Sequenz gleicher Objekte ist, kann ein Feld somit auch selbst als Elemente Felder haben.
Diese Verschachtelung ist theoretisch endlos möglich. Im folgenden ein paar Beispiele zur Erläuterung:

````c
// Initialisierung einiger Arrays
int i1[5][7];
int i2[10][2][5];

// Zugriff auf Elemente (lesend)
// Bsp. von: https://docs.microsoft.com/de-de/cpp/cpp/arrays-cpp?view=vs-2019#multidimensional-arrays
// using_arrays_2.cpp
// compile with: /EHsc /W1
#include <iostream>
using namespace std;
int main() {
   double multi[4][4][3];   // Declare the array.
   double (*p2multi)[3];
   double (*p1multi);

   cout << multi[3][2][2] << "\n";   // C4700 Use three subscripts.
   p2multi = multi[3];               // Make p2multi point to
                                     // fourth "plane" of multi.
   p1multi = multi[3][2];            // Make p1multi point to
                                     // fourth plane, third row
                                     // of multi.
}

// Zugriff auf Elemente (schreibend)
multi[2][3] = 5.3;
````

## Weiterführendes

In C++ wird nicht empfohlen diese C-Style Arrays zu benutzen. Stattdessen verwendet lieber `std::vector` oder
`std::array`.

Weiterführende Links und Quellen für diese Seite:

- [MS-Docs](https://docs.microsoft.com/de-de/cpp/cpp/arrays-cpp?view=vs-2019)
- [Offizielle C++ Referenz](http://www.cplusplus.com/doc/tutorial/arrays/)
- [W3 Schools](https://www.w3schools.com/cpp/cpp_arrays.asp)
