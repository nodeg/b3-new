---
title: Verkettete Liste
date: 2020-11-20 08:30:00 +0200
---

## Problem

![AWP - Verkettete Liste - Problem](/AWP/01-liste-01.png)

Wir benötigen eine Datenstruktur mit Daten- und Adressbereich!

## Datenstruktur für die verkettete Liste

```cs
// Dies ist "class" anstatt "struct", da dies C# und nicht wie letztes Schuljar C++ ist.
class CPupil
{
    // Datenbereich
    public int iNr;
    public string strName;
    public string strLocation;
    public string strPhone;

    // Addressbereich
    public CPupil Next = null; // Zeigt auf den nächsten Datensatz
}
```

## Voraussetzungen für eine verkette Liste

- Jede verkettete Liste benötigt einen definierten Anfang! $$\rightarrow$$ Bereitstellung eines Listen-Ankers:
  `CPupil Anchor = null;`
- Speicher für ein neues Element allokieren
```
CPupil Alloc;
Alloc = new CPupil();
```
- Freigeben des Speichers eines Elementes
```
// einer Referenzvariablen null zuweisen, GC!
Alloc = null;
```

## Grundoperationen mit verketteten Listen

- Neues Element am Ende anfügen
- Element löschen
- Gesamte Liste löschen
- Element in der Liste suchen
- Liste nach einem bestimmten Kriterium sortieren
- usw.

## Operation: Element am Ende anfügen

- Unterscheidung zweier Fälle notwendig:
  - Die Liste ist leer $$\rightarrow$$ der Anker zeigt auf null
  - Die Liste enthält bereits Elemente $$\rightarrow$$ der Anker zeigt auf das erste Element

![AWP - Verkettete Liste - Problem](/AWP/01-liste-02.png)

![AWP - Verkettete Liste - Problem](/AWP/01-liste-03.png)

![AWP - Verkettete Liste - Problem](/AWP/01-liste-04.png)

Struktogramm:

![AWP - Verkettete Liste - Add End](/AWP/01-liste-07.png)

## Operation: Element der Liste löschen

- Unterscheidung zweier Fälle notwendig:
  - Das erste Element der Liste soll gelöscht werden $$\rightarrow$$ der Anker ändert sich
  - Irgendein Element der Liste soll gelöscht werden $$\rightarrow$$ der Anker behält seinen Wert

![AWP - Verkettete Liste - Problem](/AWP/01-liste-05.png)

![AWP - Verkettete Liste - Problem](/AWP/01-liste-06.png)

Struktogramm:

![AWP - Verkettete Liste - Remove End](/AWP/01-liste-08.png)
