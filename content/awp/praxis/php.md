---
title: PHP
date: 2020-03-20 10:10:00 +0200
---

{{< tabs "php" >}}
{{< tab "Grundlagen" >}}

## Was ist PHP?

PHP ist eine Skriptsprache, die in HTML eingebettet wird. Bevor die Seite vom Server ausgeliefert wird, muss sie von
einem PHP-Interpreter zu reinem HTML übersetzt werden.

```php
<html>
    <head>
        <title>PHP-Test</title>
    </head>
    <body>
        <h1>Hier kommt eine dynamische Ausgabe!</h1>
        <?php
            $ergebnis = 4 + 4;
            echo "<p>4 + 4 = $ergebnis</p>";
        ?>
    </body>
</html>
```

Die obige Datei sieht auf den ersten Blick wie eine normale HTML-Seite aus. Interessant wird es in den Zeilen 9 bis 12.
Dort ist in den Tag `<?php ?>` der eigentliche PHP-Quelltext eingebettet. Wenn die Seite vom Webserver angefordert wird,
wertet zunächst ein PHP-Interpreter diesen Quelltext aus und ersetzt ihn durch die Ausgaben des Skripts. Das Ergebnis
sieht folgendermaßen aus:

```html
<html>
    <head>
        <title>PHP-Test</title>
    </head>
    <body>
        <h1>Hier kommt eine dynamische Ausgabe!</h1>
        <p>4 + 4 = 8</p>
    </body>
</html>
```

## Wie funktioniert PHP?

1. Der Webbrowser fragt beim Webserver nach der Datei `/vierplusvier.php`.
1. Der Webserver liest die angefragte Datei von der Festplatte.
1. Der Webserver sendet die Datei an den PHP-Interpreter.
1. Der PHP-Interpreter führt den Code aus und liefert den erzeugten HTML-Quelltext an den Webserver zurück.
1. Der Webserver sendet den erzeugten HTML-Quelltext an den Webbrowser. Dieser wertet den HTML-Quelltext aus und zeigt
   das Ergebnis an.

## PHP Basics

### Installation

Die Installation von PHP ist abhängig von Betriebssystem und Webserver, sowie dem gewünschten Operationsmodus.
In der Berufsschule bauen die Beispiele auf `XAMPP` auf, was kurz ist für Apache, MariaDB, PHP & Perl.

Für die genaue Anleitung möchte ich euch bitte auf die
[Dokumentation/Website](https://www.apachefriends.org/de/index.html) verweisen.

### Erste Schritte

Nachdem wir von den obigen Abschnitten bereits wissen das wir für PHP einen Webserver und den PHP Interpreter als
Minimum benötigen und auch schon wissen wie wir PHP in ein HTML Dokument einbauen können wir eigentlich schon loslegen.

Wichtig zu wissen ist, dass ein Webserver in der Regel ein im Hintergrund laufender Prozess. Dieser Prozess muss
entweder beim starten des Rechners automatisch gestartet werden oder vom Benutzer gestartet werden. Ist dies nicht der
Fall, so ist die PHP Datei nicht mehr als eine Textdatei. Der Webserver muss zuvor auch für die Benutzung von PHP
konfiguriert werden.

### Inhalte ausgeben

Möchte man aus seinem PHP-Skript etwas ausgeben, so tut man dies sauber mit `echo "Text"`. Hierbei kann wie oben im
Beispiel zu sehen natürlich auch HTML Code mit ausgegeben werden. Die unsaubere Variante ist einfach eine Variable
ohne jeglichen Kontext in den Quellcode zu packen. Zumeist wird der Inhalt dann einfach ausgegeben.

Beim Ausgeben der Inhalte ist zu beachten das `echo` immer Texte ausgeben möchte. Heißt das auszugebende Objekt muss ein
Text sein. Für Zahlen ist PHP so gutmütig und konvertiert diese automatisch in Text. Für komplexere Objekte und Arrays
muss eine explizite Konvertierung in Text vorgenommen werden, damit diese korrekt dargestellt werden können.

### Besondere Variablen

In PHP gibt es das Konzept von Superglobalen Variablen. Diese sind die folgenden nach aktuellem Stand (2020-09-28):

- `$GLOBALS`
- `$_SERVER`
- `$_GET`
- `$_POST`
- ...

Eine aktuelle Auflistung gibt es immer hier: <https://www.php.net/manual/de/language.variables.superglobals.php>

Diese erfüllen alle einen Zweck und sind vorhanden im korrekten Kontext. Sie müssen nicht vom Programmierer befüllt
werden. Wichtig ist zu wissen wie sie heißen, damit Namenskonflikte verhindert werden.

### Verzweigungen, Schleifen und Logische Operationen

Im folgenden eine Auflistung der Standardkonzepte von PHP. Erklärungen zu den jeweiligen Konstrukten sind genügend im
Netz zu finden bzw. in früheren Posts von AWP mit der Sprache C++.

If-Else:

```php
if ($a > $b) {
    echo "a ist größer als b";
} elseif ($a == $b) {
    echo "a ist gleich groß wie b";
} else {
    echo "a ist kleiner als b";
}
```

Und für unsere Schlangenverrückten:

```php
if ($a > $b):
    echo $a." ist größer als ".$b;
elseif ($a == $b): // elseif in einem Wort!
    echo $a." ist gleich groß wie ".$b;
else:
    echo $a." ist weder größer als noch gleich wie ".$b;
endif;
```

Switch-Case:

```php
switch ($i) {
    case 0:
        echo "i ist gleich 0";
        break;
    case 1:
        echo "i ist gleich 1";
        break;
    case 2:
        echo "i ist gleich 2";
        break;
    default:
           echo "i ist nicht gleich 0, 1 oder 2";
}
```

Für unsere Python verrückten:

```php
switch ($i):
    case 0:
        echo "i ist gleich 0";
        break;
    case 1:
        echo "i ist gleich 1";
        break;
    case 2:
        echo "i ist gleich 2";
        break;
    default:
        echo "i ist nicht gleich 0, 1 oder 2";
endswitch;
```

For:

```php
for ($i = 1; $i <= 10; $i++) {
    echo $i;
}
```

```php
$arr = array(1, 2, 3, 4);
foreach ($arr as &$value) {
    $value = $value * 2;
}
```

While-Do:

```php
$i = 1;
while ($i <= 10) {
    echo $i++;
}
```

Do-While:

```php
$i = 0;
do {
    echo $i;
} while ($i > 0);
```

Eine aktuelle Liste gibt es immer zu finden auf: <https://www.php.net/manual/de/language.control-structures.php>

{{< /tab >}}
{{< tab "Sessions" >}}

## Was ist eine Session?

Moderne Applikationen können nicht abgebildet werden indem ein einziger Aufruf an einen Webserver getätigt wird. Sendet
ein Client jedoch eine Anfrage an einen Webserver, so sieht diese Anfrage (ohne zusätzliche Hilfsmittel) genauso aus
wie jede andere auch. Lediglich die IP-Adresse könnte zur Identifikation genutzt werden. Da jedoch (wie an einer
Berufsschule bspw.) mehrere Clients über eine IP-Adresse häufig Anfragen an einen Webserver schicken, kann dies nicht
als zuverlässiges Merkmal genutzt werden.

Um dieses Problem zu lösen, muss somit eine eindeutige Zuordnung von Client zu mehreren Anfragen, die zusammen gehören,
ermöglicht werden. Hierzu bedient man sich der Technik der Sessions. Diese ist nur möglich, wenn ein sogenanntes Cookie
gesetzt wird. Dieses Cookie hat als Inhalt ein Identifikationsmerkmal, welches auf dem Server für eine gewisse (kurze)
Zeit ein Gegenstück hat. Dieses Merkmal ist im allgemeinen geheim, da dies als Zwischenspeicher für wichtige
Informationen genutzt wird.

In PHP ist dies per default auf dem Client ein Cookie mit dem Namen `PHPSESSID`, welcher einen zufälligen Text aus
Buchstaben und Zahlen enthält, welcher Serverseitig zufällig generiert wurde, enthält. Auf dem Server liegt nun in einem
temporären Verzeichnis eine Datei, welche denselben Namen trägt, wie der Cookie an Inhalt hat. Somit ist eine Zuordnung
hergestellt, wenn der Browser mit jeder Anfrage den Cookie mit in seiner Anfrage überträgt.

Ein Client hat niemals das Wissen darüber welche Inhalte in der Session Serverseitig zwischen gespeichert werden. Er
sendet das Cookie lediglich als Identifikation mit.

> ACHTUNG: Eine Session ist in der modernen Welt nicht mehr als "sicheres" Identifikationsmerkmal anzusehen. Eine
> Session kann geklaut werden von einem Angreifer. Die Identifikation das die Anfrage von einem Client kommt, muss
> mindestens über einen oder mehrere weitere Wege gewährleistet werden. Welche dies sind, ist von Programm zu Programm
> unterschiedlich.

## `$_SESSION` in PHP

In PHP gibt es bestimmte superglobale (bzw. immer magisch verfügbare) Variablen, welche einen bestimmten Zweck erfüllen
und von PHP automatisch mit bestimmten Inhalten gefüllt werden. Die Variable `$_SESSION` enthält immer die Werte zu den
jeweiligen Schlüsseln der Session, welche von PHP gefunden wurde. Da eine PHP Anwendung nicht zwangsweise eine Session
besitzen muss, kann dieses Array auch leer sein bzw. nicht existieren.

Eine Session in PHP startet man mit der Funktion `session_start()` und die Inhalte müssen von der Anwendung befüllt
werden. Eine Session kann beendet werden mit `session_destroy()`.

Der lesende Zugriff auf diese superglobale Variable erfolgt wie bei einem normalen Array:
`$Groesse = $_SESSION[“txtSize“];`

Der schreibende Zugriff auf diese superglobale Variable erfolgt ebenfalls wie bei einem normalen Array:
`$_SESSION[‘txtSize‘] = $Groesse;`

Aufgrund der Besonderheiten von PHP kann in diesem Array natürlich mit einem belibigen Schlüssel oder Index natürlich
auch ein weiteres Feld gespeichert werden. Der folgende Code ist also natürlich vollkommen valide:
`echo $_SESSION['Noten'][1];` Lesbarer wäre allerdings untenstehender Codeblock:

```php
$meineNoten = $_SESSION [‘Noten‘];
echo $meineNoten[1];
```
{{< /tab >}}
{{< tab "Funktionen" >}}



## PHP Funktionen

Das folgende Stück aus Pseudocode zeigt eine allgemeine PHP Funktion. Die Parameterliste ist mit einem Komma getrennt.

```php
function Funktionsname(Parameterliste) {
    //Anweisungen
}
```

Hier nun ein explizites Beispiel einer Funktion welche zwei Parameter entgegen nimmt und diese ausgibt miteinander
verkettet.

```php
function test($Wort1, $Wort2) {
    echo $Wort1 . $Wort2;
}
```

Die über diesem Absatz definierte Funktion wird nun im Folgenden aufgerufen:

```php
test("Hallo", " Welt");
```

Das folgende Beispiel definiert nun eine Funktion mit Rückgabewert. In diesem Fall wird lediglich der Paramter welcher
der Funktion übergeben wird zurückgegeben.

```php
function test($Wert) {
    return $Wert;
}
echo test("Hallo");
```

Dieses Beispiel ist nun ein klein wenig komplizierter und verbindet Funktionsargument, Verarbeitung in der Funktion und
Rückgabe mit der Ausgabe des zurück gegebenen Wertes. Es verdeutlicht auch das Prinzip "Call-By-Value".

```php
function test($Wert) {
    $Wert++;
    return $Wert;
}
$Zahl=1;
echo $Zahl . "/" . test($Zahl) . "/" . $Zahl;

```

Dieses Beispiel ist nun ein klein wenig komplizierter und verbindet Funktionsargument, Verarbeitung in der Funktion und
Rückgabe mit der Ausgabe des zurück gegebenen Wertes. Es verdeutlicht auch das Prinzip "Call-By-Reference".

```php
function test(&$Wert) {
    $Wert++;
    return $Wert;
}
$Zahl=1;
echo $Zahl . "/" . test($Zahl) . "/" . $Zahl;

```

Dieses Beispiel soll nun verdeutlichen das es verschiedene Gültigkeitsbereiche in PHP gibt. Die Namensgebung einer
Variable muss lediglich im selben Bereich eindeutig sein.

```php
$Zahl=1;
function test() {
    $Zahl=2;
    return $Zahl;
}
echo $Zahl . "/" . test() . "/" . $Zahl;

```

Das Schlüsselwort `global` sucht nach einer Variable mit der Bezeichnung, die darauf folgt und macht sie für den lokalen
Bereich verfügbar. Dies sollte sparsam eingesetzt werden, da es einfach übersehen werden kann und auch schnell Fehler
produziert.

```php
$Zahl=1;
function test() {
    global $Zahl;
    $Zahl=2;
    return $Zahl;
}
echo $Zahl . "/" . test() . "/" . $Zahl;

```

## Arrays als Funktionsparameter und Rückgabewerte

```php
$arDaten = array(123, 456, 789);
function AusgabeInhalte($arWerte){
    foreach($arWerte as $Element)
        echo $Element . "<br>";
}
AusgabeInhalte($arDaten);
```

```php
function test(){
    $arDaten[] = 456;
    $arDaten[] = 123;
    $arDaten[] = 789;
    return $arDaten;
}
$arWerte = test();
// Variante 1
for($i=0; $i<count($arWerte); $i++)
    echo $arWerte[$i] . "<br>\n ";
// Variante 2
foreach ($arWerte as $Eintrag)
    echo $Eintrag . "<br>\n ";
```

## Auslagern von Quellcode - require vs include

Wenn ein Projekt eine gewisse Größe erreicht ist es nicht mehr sinnvoll irgendwann alles in einer Datei zu pflegen.
Der PHP Interpreter liest aber normalerweise trotzdem nur die gerade angefragte Datei des Webservers. Um dieses Problem
zu lösen, gibt es die Schlüsselwörter `require` und `include`. Beide Schlüsselwörter laden an der Stelle wo sie stehen
eine weitere angegebene PHP Datei herein und interpretieren diese, nachdem die Interpretation abgeschlossen ist, wird
die Interpretation der "übergeordneten" Datei fortgesetzt.

Wichtig zu wissen ist folgender Effekt: Wir haben die folgenden beiden Dateien

`main.php`

```php
<?php
    require ("Funktionen.inc.php");
    Ausgabe();
?>
```

`Funktionen.inc.php`

```php
<?php
    function Ausgabe(){
        echo "externes Hallo";
    }
    echo "Jetzt kommt ein <br>";
?>
```

Das Ergebnis ist nun folgendes nach dem Interpretieren der Datei:

```php
<?php
    function Ausgabe(){
        echo "externes Hallo";
    }
    echo "Jetzt kommt ein <br>";
    Ausgabe();
?>
```

Was bedeutet das nach dem Ausführen der vollständigen PHP-Datei das ganze so aussieht:

```html
Jetzt kommt ein <br>
externes Hallo
```

Wir hätten obigen Code natürlich auch statt `require("Funktionen.inc.php)` mit `include("Funktionen.inc.php)` schreiben
können. Warum haben wir das nicht?

Der Unterschied von `require` vs `include` ist das Verhalten wenn die Datei `Funktionen.inc.php` nicht gefunden werden
kann. `require` gibt an der Stelle einen fatalen Fehler aus und beendet die Ausführung des Skriptes, wohingegen
`include` lediglich eine Warnung ausgibt und anschließend mit der Ausführung des Skriptes fortführt. Je nach
Anwendungsfall kann es sein das das eine oder andere Schlüsselwort sinnvoller ist oder eben nicht.

{{< /tab >}}
{{< tab "Einbindung von SQL" >}}

# PHP und SQL

In PHP existieren drei Varianten um auf eine MySQL Datenbank zuzugreifen.

- MySQL: Seit PHP7 nicht mehr möglich
- MySQLi (MySQL Improved Extension): Seit PHP 5
- PDO (PHP Data Objects): Aktuellste Schnittstelle und unabhängig von dem verwendeten Datenbanksystem. Der Zugriff erfolgt allerdings ausschließlich objektorientiert und ist daher für AWP11 Unterricht nicht geeignet.

## MySQLi Grundlagen

### Verbindung herstellen

```php
$hostname = '127.0.0.1';
$username = 'Paul';
$password = '';
$databasename = 'meineDatenbank';

$Con = mysqli_connect($hostname, $username, $password, $databasename);
```

Prüfung ob Verbindung erfolgreich hergestellt wurde `die()` und `exit()` sind dabei synonym verwendbar.

```php
if(!$Con){
    exit("Datenbank Verbindungsfehler: " . mysqli_connect_error());
}
```

Charset festlegen damit Umlaute z. B. bei "Zutritt gewährt" korrekt in die DB geschrieben werden.

```php
mysqli_set_charset($Con, "utf8");
```

### SQL Query-String erstellen

Insbesondere komplexe SQL Statements sollten zuvor in einer Variable gespeichert werden. Dies sorgt für einen übersichtlicheren Quellcode.

```php
$Query = "INSERT INTO Kunden(K_KName, K_KStrasse) VALUES('";
$Query .= $Name."','";
$Query .= $Strasse."')";
```

Oder:

```php
$Update = "UPDATE user SET email = $EMail WHERE id = $UserID";
```

Via `var_dump($Query)` lässt sich das SQL Statement auf Fehler überprüfen.
Sie sollten niemals direkt `$_POST` Inhalte an den Datenbankserver schicken. In dem oberen Beispiel ist davon auszugehen, dass `$Name` und `$Strasse` bereits auf Schadcode geprüft wurde. Ansonsten besteht die Gefahr einer SQL Injection. Die Verwendung von Prepare-Statements ist zu empfehlen. Nähere Informationen hierzu unter: php-einfach MySQLi Crashkurs
SQL Query-String absenden

### DQL Abfragen

```php
$Result = mysqli_query($Con, $Query);
```

`$Result` enthält eine Menge von Datensätzen (= Recordset) oder im Falle eines Fehlers den Wert `False`. Die Datensätze könnten beispielsweise folgendermaßen ausgegeben werden:

```php
if ($Result != false) {
	foreach ( $Result as $Data) {
		echo $Data["Name"]. " - ";
		echo $Data["Hund"]. " - <br>";
	}
}
```

### DDL/DML Statements

```php
mysqli_query($Con, $Update);
```

Auch bei DDL/DML Abfragen ist zu prüfen, ob das Statement erfolgreich ausgeführt wurde. Zum Beispiel so:

```php
if (mysqli_query($Con, $Update) == false) {
	echo "Update-Fehler: " . mysqli_error($Con);
};
```

### Trennen der Datenbankverbindung

Je hergestellte Verbindung soll immer sauber getrennt werden, nachdem sie nicht mehr benötigt wird.

```php
mysqli_close($Con);
```

{{< /tab >}}
{{< /tabs >}}