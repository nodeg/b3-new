---
title: Operatoren
date: 2019-10-18 10:00:00 +0200
---

## Adressoperator &

Merke: Der Adressoperator `&` ermittelt von einem Ausdruck die zugehörige Adresse im Hauptspeicher des Computers.

Beispiel:
````c
int i_v = 0xl 0 ; // die Va r i abl e i v e rh äl t den Hex-We rt 01⁄210

// Die Adresse der Va riabl e n :L_v wird dem Ze lge r pi v zugewiesen :
int * pi_v = &i v;
````

## Dereferenzerierungsoperator *

Merke: Der Dereferenzierungsoperator `*` interpretiert den Ausdruck hinter dem Dereferenzierungsoperator als Zeiger. Der
Inhalt des Zeigers wird als Adresse interpretiert, also der Speicherplatz mit dieser Adresse aktiviert. Anschließend
wird der Inhalt des Speicherplatzes mit dieser Adresse ausgelesen.

Beispiel: `i_v = *pi v; Wert`

Ist der Inhalt des Zeigers `pi_v` die Zahl `0`, so wird in der Codezeile versucht, den Inhalt der Adresse `O` zu
ermitteln. Dieser Speicher ist jedoch gegen softwareseitige Zugriffe geschützt. Es wird daher bei einem "Nullpointer"
eine Fehlermeldung ausgelöst.

Ein uninitialisierter Zeiger ist unbedingt zu vermeiden, sonst kann ein unregelmäßig auftretender Fehler entstehen! Wenn
den Zeiger zufälligerweise eine ungültige oder undefinierte Adresse enthält und auf diesen dereferenzierend zugegriffen
wird, wird das Programm ggf. mit einem Laufzeitfehler beendet!

Dringende Empfehlung:
Zeiger müssen bei Zeigerdeinition sofort initialisiert werden! Der Zeiger muss nach der Initialisierung entweder eine
gültige Adresse oder `NULL` enthalten. Wenn ein Zeiger ungültig wird, muss er sofort auf `NULL` gesetzt werden!

Beispiele:
````
// Der Zeiger pi_v wird mit NULL initialisiert
// -> offensichtlich kein sinnvoller/ungültiger Inhalt
int * pi_v = NULL;
````

````
int i_v = Ox1O;
// Adresse der Variablen i_v wird dem Zeiger pi_ v zugewiesen -> gültige Adresse: OK
int * pi_v = &i_v;
````

## Dynamisches Allokieren von Speicher

Es gibt in C++ die Möglichkeit, ohne das Vorhandensein einer "normalen" Variable einem Zeiger Speicher zuzuweisen.

````c
float *pfZeiger;
pfZeiger = new float;
````

Die erste Zeile des Beispiels erstellt einen Zeiger für eine Variable des Typs float und die zweite Zeile allokiert
Speicher und weißt die Anfangsadresse dieses Speichers an `pfZeiger` zu.

Der `new`-Operator fordert Speicher vom Betriebssystem an. Das Betriebssystem liefert daraufhin als Ergebnis eine
Speicheradress, die die Anfangsadresse des allokierten Speicherbereiches darstellt. Hierbei sind zwei Dinge zu beachten:

1. Der Speicher der durch `new` reserviert wurde, hat keinen Namen.
2. Der Speicher der reserviert wurde hat irgendeinen Wert, da die Variable nicht initialisiert wurde.

Ansonsten ist alles wie gehabt. Der Zeiger zeigt auf eine andere Variable und hat daher als Wert die Speicheradresse
jener (unbenannten) Variable.

Der Zugriff auf die rechts stehende Variable kann jetzt nur noch durch den Zeiger passieren, weil ja kein Variablenname
vorhanden ist! Ansonsten ist die Variable wie ein Zeiger zu behandeln.

Lesend: `cout << *pfZeiger;`
Schreibend: `*pfZeiger = 3.1315926`