---
title: SQL
date: 2020-02-04 10:00:00 +0200
---

## Allgemeines

SQL ist eine Datenbanksprache. Sie dient zur Definition von Datenstrukturen in relationalen Datenbanken sowie zum
Bearbeiten (Einfügen, Verändern, Löschen) und Abfragen von darauf basierenden Datenbeständen. Ihre Syntax ist relativ
einfach aufgebaut und semantisch an die englische Umgangssprache angelehnt.

SQL-Ausdrücke lassen sich in vier Kategorien unterteilen:

- Data Query Language (DQL): Befehle zur Abfrage und Aufbereitung der gesuchten Informationen
- Data Manipulation Language (DML): Befehle zur Datenmanipulation (Ändern, Einfügen, Löschen)
- Data Definition Language (DDL): Befehle zur Definition des Datenbankschemas
- Data Control Language (DCL): Befehle für die Rechteverwaltung und Transaktionskontrolle.

## Datenbankmodell

Um Ausdrücke in SQL zu formulieren, müssen das Datenbankmodell (Landkarte) und die sich daraus ergebenden
Datenbanktabellen bekannt sein.

## Tabellen

Relationale Datenbanken speichern die Daten in Tabellen. Um SQL zu verstehen und zu sprechen, muss man den Aufbau dieser
Tabellen verstehen. Datenbanktabellen haben genau wie Excel-Tabellen Zeilen und Spalten. Die Tabellen ergeben sich in
unserem Fall direkt aus dem ER-Modell oben. Gehen Sie folgendermaßen vor: 1. Erstellen sie jeweils eine Tabelle für jede
Entitätsmenge (Rechteck). 2. Erstellen Sie in jeder Tabelle eine Spalte für jedes Attribut (Ellipse) der entsprechenden
Entitätsmenge.

## SQL-Ausdrücke

Jetzt, wo Sie die Landkarte von SQL Island kennen, können wir anfangen, die Sprache zu lernen. SQL-Ausdrücke bestehen
fast immer aus folgenden Komponenten:

{% highlight plaintext %}
[BEFEHL] [Spalte(n)] [Tabelle(n)] [BEDINGUNG] [SORTIER-/GRUPPENFUNKTION];
{% endhighlight %}

Die Bedingung und die Sortier-/Gruppenfunktionen dabei sind optional. Außerdem können Letztere auch direkt nach dem
Befehl kommen. Dazu aber später mehr.

## SQL-Abfragen

Für den Anfang sind die wichtigsten SQL-Ausdrücke die SQL-Abfragen. Sie beginnen immer mit dem Befehl SELECT.
