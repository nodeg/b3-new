---
title: Zustandsdiagramme
date: 2021-01-15 08:30:00 +0200
---

Ein Zustandsdiagramm zeigt eine Folge von Zuständen, die ein Objekt im Rahmen eines Programms oder in einem
Geschäftsprozess einnehmen kann und aufgrund welcher Ereignisse Zustandsänderungen stattfinden. (vgl.
UML Erler/Ricken S.208ff) Man nennt diesen Zusammenhang zwischen Zuständen und Zustandsübergängen auch Zustandsautomat.

![AWP - Zustandsdiagramm - 1](/AWP/04-zustandsdiagramm-1.png)

- Jedes Zustandsdiagramm weist einen **Startzustand** auf und es kann auch einen **Endzustand** haben. Diese Zustände
  werden als **Pseudozustände** bezeichnet, da ein Objekt sie nicht wirklich einnehmen kann. Neu erzeugte Objekte
  befinden sich immer in dem auf den Startzustand folgenden Zustand. Der Endzustand kennzeichnet das Ende des
  Lebenszyklus eines Objektes.
- Der Zustand eines Objektes wird beschrieben durch die Werte einer oder mehrerer Attribute dieses Objektes, sogenannte
  **Zustandsvariable**.
- Jeder Zustand kann **mit Aktionen verbunden** sein, die automatisch beim Eintritt in diesen Zustand erfolgen
  (**entry**), die automatisch beim Verlassen des Zustandes ausgeführt werden (**exit**) oder die so lange ausgeführt
  werden, wie das Objekt in diesem Zustand verweilt (**do**).
- Ein **Zustandsübergang** wird immer durch ein **Ereignis** verursacht, z.B. durch einen Methodenaufruf, eine wahr
  gewordene Bedingung, durch das Erreichen eines bestimmten Zeitpunktes oder ein implizites Ereignis (wenn eine mit dem
  Zustand verbundene Aktivität endet).
- Alle im Zustandsdiagramm enthaltenen **Aktionen/Aktivitäten** und oft auch die dort auftretenden **Ereignisse** sind
  als **Methoden im Klassendiagramm** enthalten.

Beispiel:

- Zustandsdiagramm für ein einfaches Telefon:
  ![AWP - Zustandsdiagramm - 2](/AWP/04-zustandsdiagramm-2.png)
- Zustandsdiagramm für ein einfaches Telefon mit Unterzuständen des Zustandes aktiv:
  ![AWP - Zustandsdiagramm - 3](/AWP/04-zustandsdiagramm-3.png)
