---
title: AWP
---

# Anwendungsentwicklung und Programmierung

## 10. Jahrgangsstufe

* Grundbegriffe der Programmierung
  * Algorithmus
  * Compiler
  * Interpreter
  * Datenstruktur
* Programmiersprachen
* Einführung in die IDE Visual Studio (Code)
* Struktogramme
* Konstante und Variablen
* Entscheidungen
* Schleifen
* Funktionen (Deklaration, Definition, Aufruf, Call by Value/Reference)
* Ein- und mehrdimensionale Felder
* Modularisierung von Softwareprojekten
* Grundlagen der Objektorientierung
  * Klasse
  * Objekt
  * Methode
  * Eigenschaften/Attribute
  * Konstruktor/Destruktor
  * Datenkapselung
  * Assoziation
  * Aggregation
  * Komposition
  * Vererbung und Polymorphie
  * abstrakte Klassen
  * Entwurfsmuster
  * Vererbungsbeziehung

## 11. Jahrgangsstufe

* C++
  * Wiederholung der Grundlagen selbstdefinierte Datenstrukturen
  * Zeiger Grundlagen
  * Zeiger anwenden (z.B. Übergabe von Feldern)
  * dynamische Speicherverwaltung (z.B. Erweiterung von Feldern zur Laufzeit)
  * Lesen/Schreiben von/in Datei
* PHP
  * Einführung HTML (Grundstruktur und Tabellen)
  * Einführung PHP - Grundelemente der Sprache (Gegenüberstellung PHP <-> C++)
  * Formulare in HTML und deren Auswertung mit PHP
  * Funktionen und Modularisierung
* Datenbanktechnik
  * Ziele und Modelle
  * Schichtenarchitektur
  * Relationale Datenbanksysteme
  * Grundlagen der Datenmodellierung (ER-Modell)
  * Grundlagen der Datenmodellierung (Logisches Modell, Abbildungsregeln)
  * Normalform, Normalisierung
  * Referentielle Integrität
  * SQL
  * DDL/DML
  * Tabellen erstellen, löschen, ändern
  * Datenmanipulation: Datensätze einfügen, ändern, löschen

## 12. Jahrgangsstufe

* Entscheidungstabellen
* Überblick über Softwarequalitätsmanagement
* Qualitätsmerkmale von Software
* Softwaretest (Testverfahren, Blackbox, Whitebox, System- u. Abnahmetest)
* Objektorientierte Softwareentwicklung (Entwicklungsphasen, Vorgehensmodelle)
* UML
  * Anwendungsfalldiagramm
  * Aktivitätsdiagramm
  * Klassendiagramm
  * Sequenzdiagramm
* Einführung in C#
  * Grundlagen
  * Einführung in die GUI-Programmierung
* Grundlagen der Objektorientierung
  * Klasse
  * Objekt
  * Methode
  * Eigenschaften/Attribute
  * Konstruktor/Destruktor
  * Datenkapselung
  * Assoziation
  * Aggregation
  * Komposition
  * Vererbung und Polymorphie
  * abstrakte Klassen
  * Entwurfsmuster
  * Vererbungsbeziehung
* Grundlagen von Baumstrukturen
